<?php

namespace App\Controller;

use App\Entity\FileUpload;
use App\Form\FileUploadType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('upload/')]
class FileUploadController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly Security $security
    ) {
    }

    /** ==================== FILE UPLOADS =================================== */
    #[Route('view-pdfs', name: 'app_file_upload_list')]
    public function index()
    {
        $entities = $this->em->getRepository(FileUpload::class)->findAll();

        return $this->render('Uploads/pdf_submissions.html.twig', ['entities' => $entities]);
    }

    #[Route('publication', name: 'app_submit_pub')]
    public function new(Request $request)
    {
        $entity = new FileUpload();
        $form = $this->createForm(FileUploadType::class, $entity);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $entity->setMimeType($entity->getPdfFile()->getMimeType());
                $entity->setPath();

                $this->em->persist($entity);
                $this->em->flush();

                $entity = new FileUpload();
                $nextForm = $this->createForm(FileUploadType::class, $entity);

                return $this->render('Uploads/submit_file.html.twig', [
                    'form' => $nextForm->createView(),
                    'success' => true,
                    'error' => false,
                ]);
            } elseif ($form->getErrors()->count() > 0) {
                $errorMessage = $form->getErrors(true)->current()->getMessage();

                return $this->render('Uploads/submit_file.html.twig', [
                    'form' => $form->createView(),
                    'success' => false,
                    'error' => $errorMessage,
                ]);
            }
        }

        return $this->render('Uploads/submit_file.html.twig', [
            'form' => $form->createView(),
            'success' => null,
            'error' => null,
        ]);
    }

    /**
     * Deletes a File.
     */
    #[Route('pub/{id}/delete', name: 'app_delete_pub')]
    public function delete(FileUpload $entity)
    {
        $this->em->remove($entity);
        $this->em->flush();

        return new Response();
    }

    /**
     * Updates the file. This tracks who viewed the pdf last.
     */
    #[Route('pub/{id}/update', name: 'app_update_pub')]
    public function update(FileUpload $entity)
    {
        $user = $this->security->getUser();
        $entity->setUpdatedBy($user);
        $entity->setUpdated(new \DateTime('now', new \DateTimeZone('UTC')));
        $this->em->persist($entity);
        $this->em->flush();

        $userName = $user->getFirstName().' '.substr($user->getLastName(), 0, 1);
        $response = new JsonResponse();
        $response->setData($userName);

        return $response;
    }
}
