<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Attribute\Route;

/**
 * Admin controller.
 */
#[Route('super/')]
class SuperAdminController extends AbstractController
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    /**
     * Lists all users active online in the last 24 hours.
     */
    #[Route('online-users', name: 'super_user_online')]
    public function online()
    {
        $entities = $this->em->getRepository(User::class)->findOnlineNow();

        return $this->render('Admin/super/online.html.twig', ['entities' => $entities]);
    }
}
