<?php

namespace App\Controller;

use App\Dto\TaxonDto;
use App\Entity\Interaction;
use App\Entity\Rank;
use App\Entity\Taxon;
use App\Service\Aggregator\TaxonBranchInteractionAggregator;
use App\Service\Aggregator\TaxonomyMap;
use App\Service\Utility\SerializeData;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;
use JetBrains\PhpStorm\Deprecated;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfonycasts\MicroMapper\MicroMapperInterface;

/**
 * Handles individual entity (interaction and taxon) show pages.
 */
class ShowEntityController extends AbstractController
{
    public function __construct(
        private readonly TaxonBranchInteractionAggregator $aggregator,
        private readonly TaxonomyMap                      $taxonomyMap,
        private readonly SerializeData                    $serializer,
        private readonly EntityManagerInterface           $em,
        private readonly MicroMapperInterface             $microMapper,
//        private Stopwatch                                 $stopwatch,
    )
    {
    }

    /* ----------------------------- INTERACTION -------------------------------- */
    /** Opens the Interaction show page. */
    #[Route('interaction/{id}', name: 'app_show_interaction', methods: ['GET'])]
    public function showInteraction(Interaction $entity): Response
    {
        $returnData = [
            'object' => $entity->getObject()->getDisplayName(),
            'show' => 'interaction',
            'subject' => $entity->getSubject()->getDisplayName(),
            'tags' => $entity->getTagNames(),
            'type' => $entity->getInteractionType()->getActiveForm()
        ];

        return $this->render('Entity/interaction.html.twig', $returnData);
    }

    /**
     * @deprecated
     * @param $id
     * @return RedirectResponse
     */
    #[Deprecated("Use the 'showInteraction' action instead.")]
    #[Route('show/interaction/{id}', name: 'app_redirect_show_interaction', methods: ['GET'])]
    public function redirectInteraction($id): RedirectResponse
    {
        return $this->redirectToRoute('app_show_interaction', ['id' => $id]);
    }
    /**
     * Returns detailed interaction data with taxonomic hierarchies.
     * Provides a flattened interaction record enriched with:
     * - Complete taxonomic lineage for both subject and object taxa
     * - Full list of taxonomic ranks for hierarchy context
     *
     * @param Interaction $entity Auto-injected interaction entity from route param
     *  @return JsonResponse JSON containing interaction data, taxonomies, and rank info
     * @return JsonResponse {
     *      entity: flattened interaction record
     *      ranks: object with all rank names keyed by ordinal
     *  }
     * @throws Exception
     * @todo Add OpenAPI/Swagger annotations to document the response structure
     */
//    #[Route('get/interaction/{id}', methods: ['GET'])]
    #[Route('api/v1/interaction/{id}/overview', name: 'app_get_interaction', methods: ['GET'], condition: 'request.isXmlHttpRequest()')]
    public function getInteraction(Interaction $entity): JsonResponse
    {
        $serialized = $this->serializer->serializeRecord($entity, 'flat');
        $subjectTaxonomy = $this->taxonomyMap->generateMap($entity->getSubject());
        $objectTaxonomy = $this->taxonomyMap->generateMap($entity->getObject());

        $context = ['json_encode_options' => JSON_FORCE_OBJECT];
        $subject = $this->serializer->serialize($subjectTaxonomy, 'json', $context);
        $object = $this->serializer->serialize($objectTaxonomy, 'json', $context);

        $returnData = [
            'entity' => $serialized,
            'subjectTaxonomy' => $subject,
            'objectTaxonomy' => $object,
            'ranks' => $this->em->getRepository(Rank::class)->getAllRankNamesByOrdinal(),
        ];

        $response = new JsonResponse();
        $response->setData($returnData);

        return $response;
    }

    /* ----------------------------- TAXON -------------------------------- */
    /** Opens the Taxon show page. The taxon overview will be displayed to the user. */
    #[Route('taxon/{id}', name: 'app_show_taxon', methods: ['GET'])]
    public function showTaxon(Taxon $entity): Response
    {
        $returnData = [
            'displayName' => $entity->getDisplayName(),
            'show' => 'taxon'
        ];

        return $this->render('Entity/taxon.html.twig', $returnData);
    }

    /**
     * @deprecated
     * @param $id
     * @return RedirectResponse
     */
    #[Deprecated("Use the 'showTaxon' action instead.")]
    #[Route('show/taxon/{id}', name: 'app_redirect_show_taxon', methods: ['GET'])]
    public function redirectShowTaxon($id): RedirectResponse
    {
        return $this->redirectToRoute('app_show_taxon', ['id' => $id]);
    }

    /**
     * Provides a consolidated overview of a taxon entity, combining its hierarchical
     * taxonomy structure and interaction metrics into a single response.
     *
     * The overview includes:
     * - Quantitative data: Aggregated interaction counts by type and geographical region
     * - Structural data: Complete taxonomic hierarchy showing the taxon's position and relationships
     *
     * @param Taxon $entity Auto-injected taxon entity from route param
     * @return JsonResponse {
     *     counts: {
     *         total: int,
     *         types: array<string, int>,
     *         locations: array<string, array>
     *     }
     *     ranks: object with all rank names keyed by ordinal
     *     taxon: taxon DTO
     *     taxonomy: string JSON-encoded taxonomy tree
     * }
     * @throws Exception
     * @todo Add OpenAPI/Swagger annotations to document the response structure
     */
//    #[Route('get/taxon/{id}', methods: ['GET'])]
    #[Route('api/v1/taxon/{id}/overview', name: 'app_get_taxon', methods: ['GET'], condition: 'request.isXmlHttpRequest()')]
    public function getTaxonSummary(Taxon $entity): JsonResponse
    {
        $counts = $this->aggregator->aggregate($entity);
        $ranks = $this->em->getRepository(Rank::class)->getAllRankNamesByOrdinal();

        $taxonomy = $this->taxonomyMap->generateMap($entity);
        $context = ['json_encode_options' => JSON_FORCE_OBJECT];
        $serializedTaxonomy = $this->serializer->serialize($taxonomy, 'json', $context);

        $response = new JsonResponse();
        $response->setData([
            'counts' => $counts,
            'ranks' => $ranks,
            'entity' => $this->microMapper->map($entity, TaxonDto::class),
            'taxonomy' => $serializedTaxonomy
        ]);

        return $response;
    }
}
