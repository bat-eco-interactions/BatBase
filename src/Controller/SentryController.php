<?php

namespace App\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

/**
 * Sentry issue tracking controller.
 */
#[Route('sentry/')]
class SentryController extends AbstractController
{
    public function __construct(
        private HttpClientInterface $client,
        private LoggerInterface $logger
    ) {
    }

    /**
     * Proxies Sentry envelope requests through a tunnel for specific project IDs.
     *
     * This method acts as an intermediary for Sentry envelope requests, forwarding
     * them to the appropriate Sentry API endpoint based on the project ID extracted
     * from the request header's DSN. Only envelopes for known project IDs are forwarded.
     * Ensures that Sentry/browser(js) events are not obstructed by stringent ad-blockers.
     *
     * @param Request $request The incoming HTTP request containing the Sentry envelope.
     * @return Response The response from the Sentry API if the project ID is known.
     */
    #[Route('tunnel', name: 'app_sentry_tunnel')]
    public function sentryProjectProxy(Request $request) {
        // Change $host appropriately if you run your own Sentry instance.
        $host = "sentry.io";
    
        // An array with the project IDs you want to accept through this proxy.
        $known_project_ids = [$this->getParameter('app.sentry.proxy')];
    
        $envelope = $request->getContent();
        $pieces = explode("\n", $envelope, 2);
        $header = json_decode($pieces[0], true);
    
        if (isset($header['dsn'])) {
            $dsn = parse_url($header['dsn']);
            $project_id = intval(trim($dsn['path'], '/'));
    
            if (in_array($project_id, $known_project_ids)) {
                $response = $this->client->request(
                    'POST', 
                    "https://$host/api/$project_id/envelope/", [
                        'headers' => [
                            'Content-Type' => 'application/x-sentry-envelope',
                        ],
                        'body' => $envelope,
                    ]
                );
                $statusCode = $response->getStatusCode();
                $content = $response->getContent();
                return new Response($content, $statusCode);
            } else {
                return new Response("Unknown project ID.", 400);
            }
        } else {
            return new Response("Invalid Sentry envelope.", 400);
        }
    }

    /**
     * Tests the Sentry php integration.
     */
    // #[Route('test', name: 'app_sentry_test')]
    // public function testLog()
    // {
    //     // the following code will test if monolog integration logs to sentry
    //     $this->logger->error('My custom logged error.', ['some' => 'Context Data']);

    //     // the following code will test if an uncaught exception logs to sentry
    //     throw new \RuntimeException('Example exception.');
    // }
}
