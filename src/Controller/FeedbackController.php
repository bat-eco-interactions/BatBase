<?php

namespace App\Controller;

use App\Entity\Feedback;
use App\Entity\SystemDate;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Attribute\Route;

/**
 * Feedback controller.
 */
class FeedbackController extends AbstractController
{
    protected $requestStack;

    public function __construct(
        private readonly EntityManagerInterface $em,
        RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * Lists all Feedback entities.
     */
    #[Route('feedback', name: 'app_feedback', methods: ['GET'])]
    public function index()
    {
        $entities = $this->em->getRepository(Feedback::class)->findAll();

        return $this->render('Feedback/index.html.twig', ['entities' => $entities]);
    }

    /**
     * Ajax action to create a new Feedback entity.
     */
    #[Route('feedback/post', name: 'app_feedback_post', methods: ['POST'])]
    public function post()
    {
        $request = $this->requestStack->getCurrentRequest();
        $requestContent = $request->getContent();
        $feedbackData = json_decode((string) $requestContent);
        $route = $feedbackData->route;
        $topic = $feedbackData->topic;
        $content = $feedbackData->feedback;

        $entity = new Feedback();
        $entity->setTopic($topic);
        $entity->setContent($content);
        $entity->setRoute($route);
        $entity->setStatus(3);  // Index for:['Closed', 'Follow-Up', 'Read', 'Unread']

        $this->em->persist($entity);
        $this->em->flush();

        $this->em->getRepository(SystemDate::class)->trackUpdate('Feedback');

        $response = new JsonResponse();
        $response->setData(['feedback' => $feedbackData]);

        return $response;
    }

    /**
     * Ajax action to retrieve all data for a Feedback entity.
     */
    #[Route('feedback/update/{id}', methods: ['POST'], name: 'app_feedback_update', condition: 'request.isXmlHttpRequest()')]
    public function update(Feedback $entity)
    {
        $request = $this->requestStack->getCurrentRequest();
        $requestContent = $request->getContent();
        $feedbackData = json_decode((string) $requestContent);

        $assignedUserId = $feedbackData->assignedUserId;
        $adminNotes = $feedbackData->adminNotes;
        $status = $feedbackData->status;

        $asgnUser = $this->em->getRepository(User::class)->find($assignedUserId);

        $entity->setAssignedUser($asgnUser);
        $entity->setAdminNotes($adminNotes);
        $entity->setStatus($status);

        $this->em->persist($entity);
        $this->em->flush();

        $this->em->getRepository(SystemDate::class)->trackUpdate('Feedback');

        $response = new JsonResponse();
        $response->setData(['feedback' => $feedbackData]);

        return $response;
    }

    /**
     * Ajax action to retrieve all data for a Feedback entity.
     */
    #[Route('feedback/load/{id}', name: 'app_feedback_load', methods: ['POST'], condition: 'request.isXmlHttpRequest()')]
    public function load(Feedback $entity)
    {
        $response = new JsonResponse();
        $response->setData([
            'feedback' => [
                'id' => $entity->getId(),
                'from' => $this->getUserData($entity->getCreatedBy()),
                'topic' => $entity->getTopic(),
                'content' => $entity->getContent(),
                'submitted' => $entity->getCreated(),
                'status' => $entity->getStatus(),
                'notes' => $entity->getAdminNotes(),
                'assigned' => $this->getUserData($entity->getAssignedUser()),
                'users' => $this->getAdminData(),
            ],
        ]);

        return $response;
    }

    private function getUserData($user)
    {
        return is_null($user) ? ['email' => null, 'name' => null, 'id' => null]
            : ['email' => $user->getEmail(),
                'name' => $user->getFirstName().' '.$user->getLastName(),
                'id' => $user->getId()];
    }

    private function getAdminData()
    {
        $adminUsers = $this->em->getRepository(User::class)->findAdmins();
        $users = [];
        foreach ($adminUsers as $user) {
            array_push($users, $this->getUserData($user));
        }

        return $users;
    }

    /**
     * Deletes a Feedback entity.
     * Note: not used in this application. 'Closed' state hides feedback from the user.
     */
    // #[Route('{id}', name: 'feedback_delete', methods: ['DELETE'])]
    // public function delete(Feedback $entity)
    // {
    //     $request = $this->requestStack->getCurrentRequest();
    //     $form = $this->createDeleteForm($entity->getId());
    //     $form->handleRequest($request);

    //     if ($form->isSubmitted()) {
    //         $this->em->remove($entity);
    //         $this->em->flush();
    //     }

    //     return $this->redirect($this->generateUrl('feedback'));
    // }

    // /**
    //  * Creates a form to delete a Feedback entity by id.
    //  *
    //  * @return Form The form
    //  */
    // private function createDeleteForm(int $id): Form
    // {
    //     return $this->createFormBuilder()
    //         ->setAction($this->generateUrl('feedback_delete', ['id' => $id]))
    //         ->setMethod(Request::METHOD_DELETE)
    //         ->add('submit', 'submit', ['label' => 'Delete'])
    //         ->getForm()
    //     ;
    // }
}
