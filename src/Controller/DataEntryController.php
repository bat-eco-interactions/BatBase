<?php

namespace App\Controller;

use App\Service\DataEntry\DataEntryManager;
use App\Service\DataEntry\DataReviewManager;
use App\Service\DataEntry\EntryUtility;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

/**
 * Date-entry/edit form controller.
 */
#[Route('crud/', name: 'app_')]
class DataEntryController extends AbstractController
{
    public function __construct(private readonly DataEntryManager $dataManager, private readonly DataReviewManager $dataReview, private readonly EntryUtility $util)
    {
    }

    /* ========================== CREATE ENTITY ================================= */
    // * @Route("/entries", name="entry_create", methods={"POST"})
    /**
     * Creates a new Entity, and any new detail-entities, from the form data.
     */
    #[Route('create', name: 'entity_create', condition: 'request.isXmlHttpRequest()')]
    #[IsGranted('ROLE_EDITOR')]
    public function entityCreate(Request $request)
    {
        $formData = json_decode($request->getContent());                        // print("\nForm data =");print_r($formData);
        $coreName = $formData->coreEntity;                                      // print("coreName = ". $coreName);

        $returnData = $this->dataManager->createEntity($coreName, $formData);

        return $this->sendDataAndResponse($returnData);
    }

    /* ========================== DELETE ENTITY ================================= */
    // * @Route("/entries/{id}", name="entry_delete", methods={"DELETE"})
    /**
     * Flags entity for delete. Interactions are soft-deleted and all others have
     * "[DELETED]" added to their name and are automatically hidden from the user.
     */
    #[Route('delete', name: 'entity_delete', condition: 'request.isXmlHttpRequest()')]
    #[IsGranted('ROLE_EDITOR')]

    public function entityDelete(Request $request)
    {
        $formData = json_decode($request->getContent());                        // print("\nForm data =");print_r($formData);
        $coreName = $formData->coreEntity;                                      // print("coreName = ". $coreName);

        $returnData = $this->dataManager->deleteEntity($coreName, $formData);

        return $this->sendDataAndResponse($returnData);
    }

    /* ========================== EDIT ENTITY =================================== */
    // * @Route("/entries/{id}", name="entry_update", methods={"PUT"})
    /**
     * Edits entity, reporting any errors or returning the serialized results.
     * The database transaction is cancelled, leaving the DB unmodified, and the
     * form-data is quarantined, pending review and approval by a data manager.
     */
    #[Route('edit', name: 'entity_edit', condition: 'request.isXmlHttpRequest()')]
    #[IsGranted('ROLE_EDITOR')]
    public function entityEdit(Request $request)
    {
        $formData = json_decode($request->getContent());                        // print("\nForm data =");print_r($formData);
        $coreName = $formData->coreEntity;                                      // print("coreName = ". $coreName);

        $returnData = $this->dataManager->editEntity($coreName, $formData);

        return $this->sendDataAndResponse($returnData);
    }

    /* ------------------------- ENTITY FORENSICS ------------------------------- */
    /**
     * Returns entity forensic-data.
     */
    #[Route('forensics', name: 'entity_forensics', condition: 'request.isXmlHttpRequest()')]
    #[IsGranted('ROLE_MANAGER')]
    public function getEntityForensicData(Request $request)
    {
        $formRecord = json_decode($request->getContent());
        $classname = $formRecord->classname;
        $id = $formRecord->id;

        $entity = $this->util->getEntity($classname, $id);
        if (!$entity) {
            throw $this->createNotFoundException("Unable to find [$classname] [$id].");
        }

        $returnData = $this->util->getEntityForensics($entity);

        $response = new JsonResponse();
        $response->setData($returnData);

        return $response;
    }

    /* ========================== DATA-REVIEW =================================== */
    /**
     * Handles updates for the data-review process.
     *
     * Creates/edits data, reporting any errors or returning the serialized results.
     * The database transaction is cancelled, leaving the DB unmodified, and the
     * form-data is quarantined, pending review and approval by a data manager.
     */
    #[Route('review', name: 'entity_review', condition: 'request.isXmlHttpRequest()')]
    #[IsGranted('ROLE_CONTRIBUTOR')]
    public function entityReview(Request $request)
    {
        $data = json_decode($request->getContent());
        $returnData = $this->dataReview->handleDataReview($data, $this->getUser());

        return $this->sendDataAndResponse($returnData);
    }

    /* ------------------------- STORE LOCAL-RESULTS ---------------------------- */
    /**
     * The ReviewEntry is returned from the server with temporary data used for the
     * serialization. It is modified to match the locally quarantined-data before
     * being stored in the contributor's local-database. The prepared entry is then
     * set in the ReviewEntry paylod. It will be used to sync the contributor's
     * quarantined-data throughout the review process.
     */
    #[Route('review/entry', name: 'entity_review_entry', condition: 'request.isXmlHttpRequest()')]
    #[IsGranted('ROLE_CONTRIBUTOR')]
    public function reviewEntryPayload(Request $request)
    {
        $content = json_decode($request->getContent());
        $returnData = $this->dataReview->setReviewEntryPayload($content);

        $response = new JsonResponse();
        $response->setData($returnData);

        return $response;
    }

    /* ----------------------------- UPDATE LOG --------------------------------- */
    /**
     * Logs updates to ReviewEntry.
     */
    #[Route('review/log', name: 'entity_review_log', condition: 'request.isXmlHttpRequest()')]
    #[IsGranted('ROLE_CONTRIBUTOR')]
    public function addReviewLog(Request $request)
    {
        $content = json_decode($request->getContent());
        $returnData = $this->dataReview->addReviewEntryLogs($content);

        $response = new JsonResponse();
        $response->setData($returnData);

        return $response;
    }

    /* ====================== UPDATE CITATION TEXT ============================== */
    /**
     * Updates the Citation and Source entities with the updated citation text.
     */
    #[Route('edit/citation', name: 'edit_citation_text', condition: 'request.isXmlHttpRequest()')]
    #[IsGranted('ROLE_EDITOR')]
    public function citationTextUpdate(Request $request)
    {
        $data = json_decode($request->getContent());

        $returnData = $this->dataManager->updateCitationText($data);

        return $this->sendDataAndResponse($returnData);
    }

    /* ======================= SERIALIZE AND RETURN ============================= */
    /** Sends an object with the entities' serialized data back to the crud form. */
    private function sendDataAndResponse($entityData)
    {
        if (!property_exists($entityData, 'coreEntity')) {
            return $this->sendResponse($entityData);
        }
        if (gettype($entityData->coreEntity) !== 'string') {
            $this->util->serializeDataResults($entityData);
        }

        if (property_exists($entityData, 'error')) {
            return $this->sendErrorResponse($entityData->error);
        }

        return $this->sendResponse($entityData);
    }

    private function sendResponse($data)
    {
        $response = new JsonResponse();
        $response->setData($data);

        return $response;
    }

    /** Logs the error message and returns an error response message. */
    private function sendErrorResponse($e)
    {
        $data = gettype($e) === 'string' ? $e : $e->getMessage();
        $response = new JsonResponse();
        // $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        $response->setData($data);

        return $response;
    }
}
