<?php

namespace App\Controller;

use ApiPlatform\Api\IriConverterInterface;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LoginController extends AbstractController
{
    /**
     * Api JSON login route.
     *
     * @param IriConverterInterface $iriConverter
     * @param AuthenticationUtils $authenticationUtils
     * @param User|null $user
     * @return Response
     */
    #[Route('/api/login', name: 'api_login', methods: ['POST'])]
    public function jsonLogin(IriConverterInterface $iriConverter, AuthenticationUtils $authenticationUtils, #[CurrentUser] ?User $user, ): Response
    {
        if (null === $user) {
            $error = $authenticationUtils->getLastAuthenticationError();

            if (!$error) {
                $error = 'Invalid login request: check that the Content-Type header is "application.json"';
            }

            return $this->json([
                'error' => $error,
            ], Response::HTTP_UNAUTHORIZED);
        }

        return new Response(null, 204, [
            'Location' => $iriConverter->getIriFromResource($user)
        ]);
    }

    /**
     * User Interface login via /login page.
     *
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    #[Route('login', name: 'app_login', methods: ['GET', 'POST'])]
    public function appLogin(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('app_home');
        }
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render(
            'login/index.html.twig',
            ['last_username' => $lastUsername, 'error' => $error]
        );
    }

    #[Route('logout', name: 'app_logout')]
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
