<?php

namespace App\Controller;

use App\Service\Aggregator\ProjectStatSummary;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Attribute\Route;

class HeaderStatsController extends AbstractController
{
    public function __construct(private readonly ProjectStatSummary $stats)
    {
    }

    /** Returns an array of status strings representing current entity-metrics. */
    #[Route('stats/{tag}', name: 'app_page_stats', methods: ['GET'])]
    public function pageStatistics($tag)
    {
        $data = $this->stats->getProjectStatSummary($tag);

        $response = new JsonResponse();
        $response->setData($data);

        return $response;
    }
}
