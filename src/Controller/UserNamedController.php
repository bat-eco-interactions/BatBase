<?php

namespace App\Controller;

use App\Entity\UserNamed;
use App\Service\Utility\LogError;
use App\Service\Utility\SerializeData;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

/**
 * Saves and displays user specified data sets: Interactions and Filters.
 */
#[Route('lists/', name: 'app_')]
class UserNamedController extends AbstractController
{
    public function __construct(private readonly EntityManagerInterface $em, private readonly LogError $logger, private readonly SerializeData $serialize)
    {
    }

    /* ------------------------------ CREATE -------------------------------------- */
    /**
     * Saves a new set of user-named data.
     */
    #[Route('create', name: 'list_create', condition: 'request.isXmlHttpRequest()')]
    public function listCreate(Request $request)
    {
        $requestContent = $request->getContent();
        $data = json_decode($requestContent);
        $list = new UserNamed();

        $returnData = new \stdClass();
        $returnData->name = $data->displayName;
        $returnData->entity = $list;

        $list->setCreatedBy($this->getUser());
        $list->setLastLoaded(new \DateTime('now', new \DateTimeZone('UTC')));
        $this->setListData($data, $list);

        $this->em->persist($list);
        $returnData->edits = false;

        return $this->attemptListFlushAndSendResponse($returnData);
    }

    /* ------------------------------ DELETE -------------------------------------- */
    /**
     * Deletes the user-named filter set or interaction list.
     */
    #[Route('remove', name: 'list_delete', condition: 'request.isXmlHttpRequest()')]
    public function listDelete(Request $request)
    {
        $requestContent = $request->getContent();
        $data = json_decode($requestContent);
        $list = $this->em->getRepository(UserNamed::class)->findOneBy(['id' => $data->id]);

        $returnData = new \stdClass();
        $returnData->id = $data->id;
        $returnData->displayName = $list->getDisplayName();
        $returnData->type = $list->getType();

        $this->em->remove($list);

        return $this->attemptListFlushAndSendResponse($returnData, true);
    }

    /* ------------------------------ EDIT ---------------------------------------- */
    /**
     * Updates the user-named filter set or interaction list.
     */
    #[Route('edit', name: 'list_edit', condition: 'request.isXmlHttpRequest()')]
    public function listEdit(Request $request)
    {
        $requestContent = $request->getContent();
        $data = json_decode($requestContent);
        $list = $this->em->getRepository(UserNamed::class)->findOneBy(['id' => $data->id]);

        $returnData = new \stdClass();
        $returnData->name = $data->displayName;
        $returnData->entity = $list;
        $returnData->edits = new \stdClass();

        $list->setLastLoaded(new \DateTime('now', new \DateTimeZone('UTC')));
        $this->setListData($data, $list, $returnData->edits);
        $this->em->persist($list);

        return $this->attemptListFlushAndSendResponse($returnData);
    }

    /* ---------- Set List Data --------------------------------------------- */
    private function setListData($data, &$entity, &$editing = null): void
    {
        foreach ($data as $field => $val) {
            $this->setDataAndTrackEdits($entity, $field, $val, $editing);
        }
    }

    /**
     * Checks whether current value is equal to the passed value. If not, the
     * entity is updated with the new value and the field is added to the edits obj.
     */
    private function setDataAndTrackEdits(&$entity, $field, $newVal, &$editing = null): void
    {
        if ($field == 'id') {
            return;
        }
        $setField = 'set'.ucfirst((string) $field);

        if ($editing !== null) {
            $getField = 'get'.ucfirst((string) $field);

            $curVal = $entity->$getField();
            if ($curVal === $newVal) {
                return;
            }
            $editing->$field['old'] = $curVal;
        }

        $entity->$setField($newVal);
    }

    /* ---------- Flush and Return Data --------------------------------------- */
    /**
     * Attempts to flush all persisted data. If there are no errors, the created/updated
     * data is sent back to the crud form; otherise, an error message is sent back.
     */
    private function attemptListFlushAndSendResponse($data, $delete = false)
    {
        try {
            $this->em->flush();
        } catch (\Exception $e) { //DBALException|
            return $this->sendErrorResponse($e);
        }

        return $this->sendListDataAndResponse($data, $delete);
    }

    /** Sends an object with the entities' serialized data back to the crud form. */
    private function sendListDataAndResponse($data, $delete)
    {
        if (!$delete) {
            try {
                $data->entity = $this->serialize->serializeRecord($data->entity);
            } catch (\Throwable|\Exception $e) {
                return $this->sendErrorResponse($e);
            }
        }
        $response = new JsonResponse();
        $response->setData(['list' => $data]);

        return $response;
    }

    /** Logs the error message and returns an error response message. */
    private function sendErrorResponse($e)
    {
        $this->logger->logError($e);
        $response = new JsonResponse();
        $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        $response->setData(['error' => $e->getMessage()]);

        return $response;
    }
}
