<?php

namespace App\Controller;

use App\Entity\ReviewEntry;
use App\Entity\SystemDate;
use App\Entity\UserNamed;
use App\Service\Utility\SerializeData;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;

/**
 * public:
 *     getDataLastUpdatedState
 *     getUpdatedEntityData
 *     serializeGeoJsonData
 *     serializeInteractionData
 *     serializeLocationData
 *     serializeSourceData
 *     serializeTaxonData
 *     serializeUserListData
 *
 * TOC:
 *     GET UPDATED DATA
 *     GET ALL ENTITY DATA
 *         FETCH INTERACTION DATA
 *         FETCH LOCATION DATA
 *         FETCH SOURCE DATA
 *         FETCH TAXON DATA
 *         FETCH USER DATA
 *         FETCH PENDING DATA
 */
#[Route('fetch/')]
class DataFetchController extends AbstractController
{
    /* ____________________ CONSTRUCT/CONFIGURE COMMAND _________________________ */
    public function __construct(private EntityManagerInterface $em, private readonly SerializeData $serialize)
    {
    }

    /* ++++++++++++++++++++++ GET UPDATED DATA ++++++++++++++++++++++++++++++++++ */
    /**
     * Returns an object with the lastUpdated datetime for the system and for
     * each entity.
     */
    #[Route('data-state', name: 'app_data_updated_state')]
    public function getDataLastUpdatedState()
    {
        $entities = $this->em->getRepository(SystemDate::class)->getLastUpdatedState();

        $response = new JsonResponse();
        $response->setData(['state' => $entities]);

        return $response;
    }

    /**
     * Serializes and returns all entities of the passed class that have been
     * updated since the passed 'lastUpdatedAt' time.
     */
    #[Route('sync-data', name: 'app_ajax_sync_data')]
    public function getUpdatedEntityData(Request $request)
    {
        $pushedData = json_decode($request->getContent());
        $entity = $pushedData->entity;
        $lastUpdatedAt = $pushedData->updatedAt;

        $data = $this->getAllUpdatedData($entity, $lastUpdatedAt);

        $response = new JsonResponse();
        $response->setData([$entity => $data]);

        return $response;
    }

    /**
     * All entities updated since the lastUpdatedAt time are serialized and
     * returned in a data object keyed by id.
     */
    private function getAllUpdatedData($entity, $lastUpdatedAt)
    {
        $updated = $this->em->getRepository(SystemDate::class)
            ->findUpdatedEntityData($entity, $lastUpdatedAt);

        return $this->serialize->serializeRecords($updated, $this->em);
    }

    /* +++++++++++++++++++++ GET ALL ENTITY DATA ++++++++++++++++++++++++++++++++ */
    private function getSerializedEntities($entity, $group = 'normal'): \stdClass
    {
        $entities = $this->em->getRepository("\App\Entity\\".$entity)->findAll();

        return $this->serialize->serializeRecords($entities, $this->em, $group);
    }

    /* =================== FETCH INTERACTION DATA =============================== */
    /**
     * Returns serialized data objects for Interaction and Interaction Type.
     */
    #[Route('interaction', name: 'app_serialize_interactions')]
    public function serializeInteractionData()
    {
        $interaction = $this->getSerializedEntities('Interaction');
        $intType = $this->getSerializedEntities('InteractionType');
        $tag = $this->getSerializedEntities('Tag');
        $validInts = $this->getSerializedEntities('ValidInteraction');
        $response = new JsonResponse();
        $response->setData(['interaction' => $interaction, 'interactionType' => $intType, 'tag' => $tag, 'validInteraction' => $validInts]);

        return $response;
    }

    /* =================== FETCH LOCATION DATA ================================== */
    /**
     * Returns serialized data objects for Habitat Type, Location Type, and Location.
     */
    #[Route('location', name: 'app_serialize_location')]
    public function serializeLocationData()
    {
        $habitatType = $this->getSerializedEntities('HabitatType');
        $location = $this->getSerializedEntities('Location');
        $locType = $this->getSerializedEntities('LocationType');
        $response = new JsonResponse();
        $response->setData(['habitatType' => $habitatType, 'location' => $location, 'locationType' => $locType]);

        return $response;
    }

    /**
     * Returns serialized data objects for Habitat Type, Location Type, and Location.
     */
    #[Route('geoJson', name: 'app_serialize_geojson')]
    public function serializeGeoJsonData()
    {
        $geoJson = $this->getSerializedEntities('GeoJson');
        $response = new JsonResponse();
        $response->setData(['geoJson' => $geoJson]);

        return $response;
    }

    /* =================== FETCH SOURCE DATA ==================================== */
    /**
     * /**
     * Returns serialized data objects for all entities related to Source.
     */
    #[Route('source', name: 'app_serialize_source')]
    public function serializeSourceData()
    {
        $author = $this->getSerializedEntities('Author');
        $citation = $this->getSerializedEntities('Citation');
        $citType = $this->getSerializedEntities('CitationType');
        $publication = $this->getSerializedEntities('Publication');
        $pubType = $this->getSerializedEntities('PublicationType');
        $publisher = $this->getSerializedEntities('Publisher');
        $source = $this->getSerializedEntities('Source');
        $srcType = $this->getSerializedEntities('SourceType');
        $response = new JsonResponse();
        $response->setData(['author' => $author, 'citation' => $citation, 'citationType' => $citType, 'publication' => $publication, 'publicationType' => $pubType, 'publisher' => $publisher, 'source' => $source, 'sourceType' => $srcType]);

        return $response;
    }

    /* ===================== FETCH TAXON DATA =================================== */
    /**
     * Returns data necessary to load the  taxon tree. This is the first batch
     * downloaded when local data is initialized.
     */
    #[Route('taxon', name: 'app_serialize_taxon')]
    public function serializeTaxonData()
    {
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        $group = $this->getSerializedEntities('Group');
        $groupRoot = $this->getSerializedEntities('GroupRoot');
        $rank = $this->getSerializedEntities('Rank');
        $taxa = $this->getSerializedEntities('Taxon');
        $response = new JsonResponse();
        $response->setData(['group' => $group, 'groupRoot' => $groupRoot, 'rank' => $rank, 'taxon' => $taxa]);

        return $response;
    }

    /* ==================== FETCH USER DATA ===================================== */
    /**
     * Gets all UserNamed entities created by the current user.
     */
    #[Route('user', name: 'app_serialize_users')]
    public function serializeUserData()
    {
        $lists = $this->getUserNamedData();
        $users = $this->getSerializedEntities('User');
        // $userId = $this->getUser() ? $this->getUser()->getId() : null;
        $response = new JsonResponse();
        $response->setData(['lists' => $lists, 'users' => $users]);

        return $response;
    }

    private function getUserNamedData()
    {
        $returnData = [];
        $lists = $this->em->getRepository(UserNamed::class)
            ->findBy(['createdBy' => $this->getUser()]);

        foreach ($lists as $list) {
            $json = $this->serialize->serializeRecord($list);
            if (!$json) {
                continue;
            }
            array_push($returnData, $json);
        }

        return $returnData;
    }

    /* =================== FETCH PENDING DATA =================================== */
    /**
     * Gets all data relevant to the data-review process.
     */
    #[Route('review', name: 'app_serialize_review')]
    public function serializeReviewEntries()
    {
        $contributions = $this->getContributorReviewEntries();
        $reviewEntry = $this->ifDataManagerGetAllReviewEntries();
        $stages = $this->getSerializedEntities('ReviewStage');
        $response = new JsonResponse();
        $response->setData(['contributions' => $contributions, 'reviewEntry' => $reviewEntry, 'reviewStages' => $stages]);

        return $response;
    }

    private function getContributorReviewEntries()
    {
        $entities = $this->em->getRepository(ReviewEntry::class)
            ->findBy(['createdBy' => $this->getUser()]);

        return $this->serialize->serializeRecords($entities, $this->em);
    }

    private function ifDataManagerGetAllReviewEntries()
    {
        if ($this->isGranted('ROLE_MANAGER')) {
            $entities = $this->em->getRepository(ReviewEntry::class)->findAll();
            return $this->serialize->serializeRecords($entities, $this->em);
        } else {
            return new \stdClass;
        }
    }
}
