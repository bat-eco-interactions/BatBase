<?php

namespace App\Controller;

use App\Entity\Citation;
use App\Entity\ContentBlock;
use App\Entity\User;
use App\Form\ContentBlockType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

/**
 * Content Block controller.
 */
class ContentBlockController extends AbstractController
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    /** Renders the page template with its content blocks. */
    #[Route('/', name: 'app_home')]
    public function home()
    {
        $blocks = $this->em->getRepository(ContentBlock::class)->findPageContent('home');

        return $this->render('ContentBlock/home.html.twig', ['entities' => $blocks]);
    }

    /** Renders the page template with its content blocks. */
    #[Route('about', name: 'app_about')]
    public function about()
    {
        $blocks = $this->em->getRepository(ContentBlock::class)->findPageContent('about');
        $editors = $this->em->getRepository(User::class)->findEditors();

        $returnData = [
            'entities' => $blocks,
            'editors' => $editors
        ];

        return $this->render('ContentBlock/about.html.twig', $returnData);
    }

    /** Renders the page template with its content blocks. */
    #[Route('bibliography', name: 'app_biblio')]
    public function bibilography()
    {
        $citations = $this->em->getRepository(Citation::class)->findValidCitations();

        return $this->render('Bibliography/biblio.html.twig', ['citations' => $citations]);
    }

    /** Renders the page template with its content blocks. */
    #[Route('definitions', name: 'app_definitions')]
    public function definitions()
    {
        $blocks = $this->em->getRepository(ContentBlock::class)->findPageContent('definitions');

        return $this->render('ContentBlock/definitions.html.twig', ['entities' => $blocks]);
    }

    /**
     * Renders the page template with its content blocks.
     * Deprecated: This route has been replaced with '/explore'.
     */
    #[Route('search', name: 'app_search_show')]
    public function search()
    {
        return $this->redirectToRoute('app_explore_page');
    }

    /** Renders the page template with its content blocks. */
    #[Route('explore', name: 'app_explore_page')]
    public function explore()
    {
        return $this->render('ContentBlock/explore/explore.html.twig', []);
    }

    /** Renders the page template with its content blocks. */
    #[Route('db', name: 'app_db_top')]
    public function db()
    {
        $blocks = $this->em->getRepository(ContentBlock::class)->findPageContent('about-db');

        return $this->render('ContentBlock/db_top.html.twig', ['entities' => $blocks]);
    }

    /** Renders the page template with its content blocks. */
    #[Route('future-developments', name: 'app_future_dev')]
    public function futureDevelopments()
    {
        $blocks = $this->em->getRepository(ContentBlock::class)->findPageContent('future-developments');

        return $this->render('ContentBlock/future_dev.html.twig', ['entities' => $blocks]);
    }

    /** ------------ CONTENT BLOCK ENTITY ACTIONS --------------------------- */
    /**
     * Lists all Content Block entities.
     */
    #[IsGranted('ROLE_ADMIN')]
    #[Route('admin/contentblock', name: 'admin_content_block')]
    public function index()
    {
        $entities = $this->em->getRepository(ContentBlock::class)->findBy(
            [],
            ['name' => 'ASC']
        );

        return $this->render('ContentBlock/entity/index.html.twig', ['entities' => $entities]);
    }

    /**
     * Creates a new Content Block entity.
     */
    #[IsGranted('ROLE_ADMIN')]
    #[Route('admin/contentblock/create', name: 'admin_content_block_create', methods: ['POST'])]
    public function create(Request $request)
    {
        $entity = new ContentBlock();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $this->em->persist($entity);
            $this->em->flush();

            return $this->redirect(
                $this->generateUrl(
                    'admin_content_block_show',
                    ['slug' => $entity->getSlug()]
                )
            );
        }

        return $this->render('ContentBlock/entity/new.html.twig', ['entity' => $entity, 'form' => $form->createView()]);
    }

    /**
     * Creates a form to create a Content Block entity.
     *
     * @param ContentBlock $entity The entity
     *
     * @return Form The form
     */
    private function createCreateForm(ContentBlock $entity): Form
    {
        $form = $this->createForm(ContentBlockType::class, $entity, ['action' => $this->generateUrl('admin_content_block_create'), 'method' => 'POST']);
        $form->add('submit', SubmitType::class, ['label' => 'Create']);

        return $form;
    }

    /**
     * Displays a form to create a new ContentBlock entity.
     */
    #[IsGranted('ROLE_ADMIN')]
    #[Route('admin/contentblock/new', name: 'admin_content_block_new')]
    public function new()
    {
        $entity = new ContentBlock();
        $form = $this->createCreateForm($entity);

        return $this->render('ContentBlock/entity/new.html.twig', ['entity' => $entity, 'form' => $form->createView()]);
    }

    /**
     * Finds and displays a Content Block entity.
     */
    #[IsGranted('ROLE_ADMIN')]
    #[Route('admin/contentblock/{slug}', methods: ['GET'], name: 'admin_content_block_show')]
    public function show(ContentBlock  $entity)
    {
        return $this->render('ContentBlock/entity/show.html.twig', ['entity' => $entity]);
    }

    /**
     * Displays a form to edit an existing Content Block entity.
     */
    #[IsGranted('ROLE_ADMIN')]
    #[Route('admin/contentblock/{slug}/edit', name: 'admin_content_block_edit')]
    public function edit(ContentBlock $block, Request $request)
    {
        $form = $this->createForm(ContentBlockType::class, $block);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var ContentBlock $block */
            $block = $form->getData();
            $this->em->persist($block);
            $this->em->flush();

            return $this->redirectToRoute('admin_content_block_show', [
                'slug' => $block->getSlug(),
            ]);
        }

        return $this->render('ContentBlock/entity/edit.html.twig', [
            'form' => $form->createView(),
            'contentblock' => $block,
        ]);
    }

    /**
     * Edits an existing Content Block entity from the WYSIWYG editor.
     */
    #[IsGranted('ROLE_ADMIN')]
    #[Route('admin/contentblock/{slug}/update', name: 'admin_content_block_update', methods: ['PUT', 'POST'], condition: 'request.isXmlHttpRequest()')]
    public function update(Request $request, ContentBlock $entity)
    {
        $requestContent = $request->getContent();
        $pushedData = json_decode($requestContent);
        $content = $pushedData->content;

        $entity->setContent($content);
        $this->em->flush();

        $response = new JsonResponse();
        $response->setData(['contentblock' => 'success']);

        return $response;
    }

    /**
     * Deletes a Content Block entity.
     * Not available in the UI currently.
     */
    #[IsGranted('ROLE_ADMIN')]
    #[Route('admin/contentblock/{slug}/delete', name: 'admin_content_block_delete')]
    public function delete(Request $request, ContentBlock $entity)
    {
        $form = $this->createDeleteForm($entity->getSlug());
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $this->em->remove($entity);
            $this->em->flush();
        }

        return $this->redirect($this->generateUrl('admin_content_block'));
    }

    /**
     * Creates a form to delete a Content Block entity by id.
     *
     * @return Form The form
     */
    private function createDeleteForm(string $slug): Form
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_content_block_delete', ['slug' => $slug]))
            ->setMethod(Request::METHOD_DELETE)
            ->add('submit', SubmitType::class, ['label' => 'Delete'])
            ->getForm()
        ;
    }
}
