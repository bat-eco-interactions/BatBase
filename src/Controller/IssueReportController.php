<?php

namespace App\Controller;

use App\Entity\ImageUpload;
use App\Entity\IssueReport;
use App\Service\Utility\LogError;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class IssueReportController extends AbstractController
{
    public function __construct(private readonly EntityManagerInterface $em, private readonly LogError $logger)
    {
    }

    /**
     * Creates a new Entity, and any new detail-entities, from the form data.
     */
    #[Route('issue/report', name: 'app_issue_report', condition: 'request.isXmlHttpRequest()')]
    public function issueReport(Request $request)
    {
        $report = new IssueReport();

        $report->setDescription($request->get('description'));                  // dump($request->files->all());
        $report->setStepsToReproduce($request->get('stepsToReproduce'));
        $report->setMiscInfo($request->get('miscInfo'));

        $files = [];
        foreach ($request->files->all() as $file) {
            $upload = new ImageUpload();
            $upload->setImage($file);
            $upload->setIssueReport($report);
            $report->addScreenshot($upload);
            $this->em->persist($upload);
            array_push($files, $upload->getFileName());
        }
        $this->em->persist($report);

        return $this->attemptFlushAndSendResponse($report, $files);
    }

    /* ---------- Flush and Return Data --------------------------------------- */
    /**
     * Attempts to flush all persisted data. If there are no errors, the created/updated
     * data is sent back to the crud form; otherise, an error message is sent back.
     */
    private function attemptFlushAndSendResponse($report, $files)
    {
        try {
            $this->em->flush();
        } catch (\Exception $e) {  //\DBALException|
            return $this->sendErrorResponse($e);
        }

        $response = new JsonResponse();
        $response->setData(['filenames' => $files]);

        return $response;
    }

    /** Logs the error message and returns an error response message. */
    private function sendErrorResponse($e)
    {                                                                           // print("\n\n### Error @ [".$e->getLine().'] = '.$e->getMessage()."\n".$e->getTraceAsString()."\n");
        $this->logger->logError($e);
        $response = new JsonResponse();
        $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        $response->setData($e->getMessage());

        return $response;
    }
}
