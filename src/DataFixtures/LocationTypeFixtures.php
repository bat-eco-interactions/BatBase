<?php

namespace App\DataFixtures;

use App\Entity\LocationType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class LocationTypeFixtures extends Fixture implements FixtureGroupInterface
{
    const array FIXTURE_GROUPS = ['dev', 'test'];

    public function load(ObjectManager $manager): void
    {
        $types = [
            [
                'displayName' => 'Region',
                'ordinal' => 10,
            ],
            [
                'displayName' => 'Country',
                'ordinal' => 20,
            ],
            [
                'displayName' => 'Habitat',
                'ordinal' => 30,
            ],
            [
                'displayName' => 'Area',
                'ordinal' => 40,
            ],
            [
                'displayName' => 'Point',
                'ordinal' => 50,
            ],
        ];

        foreach ($types as $type) {
            $locationType = new LocationType();
            $locationType->setDisplayName($type['displayName']);
            $locationType->setOrdinal($type['ordinal']);
            $manager->persist($locationType);
        }
        $manager->flush();

    }

    public static function getGroups(): array
    {
        return self::FIXTURE_GROUPS;
    }
}
