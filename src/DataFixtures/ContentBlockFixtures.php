<?php

namespace App\DataFixtures;

use App\Entity\ContentBlock;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ContentBlockFixtures extends Fixture implements FixtureGroupInterface, DependentFixtureInterface
{
    const array FIXTURE_GROUPS = ['dev', 'test'];

    public function load(ObjectManager $manager): void
    {
        $admin = $manager->getRepository(User::class)->findOneBy(['username' => 'TestAdmin']);

        $blocks = [
            [
                'name' => 'Home Pg Intro',
                'page' => 'home',
                'content' => 'Home Page - Introduction',
            ],
            [
                'name' => 'Home Pg Second-Col',
                'page' => 'home',
                'content' => 'Home Page - Extra Content',
            ],
            [
                'name' => 'About DB Data',
                'page' => 'about-db',
                'content' => 'About the Database',
            ],
            [
                'name' => 'About DB Caveats',
                'page' => 'about-db',
                'content' => 'Caveats',
            ],
            [
                'name' => 'About DB Data Map',
                'page' => 'about-db',
                'content' => 'Data Map',
            ],
            [
                'name' => 'Definitions Interaction Types',
                'page' => 'definitions',
                'content' => 'Interaction Types',
            ],
            [
                'name' => 'Definitions Habitat Types',
                'page' => 'definitions',
                'content' => 'Habitat Types',
            ],
            [
                'name' => 'About Pg History',
                'page' => 'about',
                'content' => 'Batbase.org History',
            ],
            [
                'name' => 'About Pg Contact',
                'page' => 'about',
                'content' => 'Contact Us',
            ],
            [
                'name' => 'Team Cullen',
                'page' => 'about',
                'content' => 'Cullen Bio',
            ],
            [
                'name' => 'Team Tuli',
                'page' => 'about',
                'content' => 'Tuli Bio',
            ],
            [
                'name' => 'Team Taylor',
                'page' => 'about',
                'content' => 'Taylor Bio',
            ],
            [
                'name' => 'Member Welcome',
                'page' => 'home',
                'content' => 'Welcome - New Member',
            ],
            [
                'name' => 'Team Sarah',
                'page' => 'about',
                'content' => 'Sarah Bio',
            ],
            [
                'name' => 'Future Developments',
                'page' => 'future-developments',
                'content' => 'Future Developments',
            ],
            [
                'name' => 'About DB How To Use',
                'page' => 'about-db',
                'content' => 'About DB - How To Use',
            ],
            [
                'name' => 'Search Tips',
                'page' => 'search',
                'content' => 'Search Tips',
            ],
            [
                'name' => 'Terms and Conditions',
                'page' => '',
                'content' => 'Terms and Conditions',
            ],
            [
                'name' => 'About Pg Acknowledgements',
                'page' => 'about',
                'content' => 'Acknowledgements',
            ],
            [
                'name' => 'About Pg Future Dev',
                'page' => 'about',
                'content' => 'See Future Developments',
            ],
            [
                'name' => 'About DB Tips',
                'page' => 'about-db',
                'content' => 'Database Tips',
            ],
            [
                'name' => 'Future Developments Overview',
                'page' => 'future-developments',
                'content' => 'Read about our Future Developments',
            ],
            [
                'name' => 'Future Developments Announcements',
                'page' => 'future-developments',
                'content' => 'Announcements',
            ],
            [
                'name' => 'About Pg Announcements',
                'page' => 'about',
                'content' => 'Announcements',
            ],
            [
                'name' => 'Future Publications Header',
                'page' => 'future-developments',
                'content' => 'Future Publication Header',
            ],
            [
                'name' => 'How to Cite',
                'page' => 'about-db',
                'content' => 'How to Cite',
            ],
            [
                'name' => 'How to Contribute',
                'page' => 'about-db',
                'content' => 'How to Contribute',
            ],
            [
                'name' => 'New Data Entry',
                'page' => 'about-db',
                'content' => 'Data Entry',
            ],
            [
                'name' => 'Team Kelly',
                'page' => 'about',
                'content' => 'Kelly Bio',
            ],
            [
                'name' => 'Team Chanda',
                'page' => 'about',
                'content' => 'Chanda Bio',
            ],
            [
                'name' => 'About DB Taxon Groups',
                'page' => 'about-db',
                'content' => 'About DB - Taxon Groups',
            ],
            [
                'name' => 'Team Aja',
                'page' => 'about',
                'content' => 'Aja Bio',
            ],
        ];

        foreach ($blocks as $data) {
            $block = new ContentBlock();
            $block->setName($data['name']);
            $block->setPage($data['page']);
            $block->setContent($data['content']);
            $block->setCreatedBy($admin);
            $manager->persist($block);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
        ];
    }

    public static function getGroups(): array
    {
        return self::FIXTURE_GROUPS;
    }
}
