<?php

namespace App\DataFixtures;

use App\Entity\Group;
use App\Entity\GroupRoot;
use App\Entity\Rank;
use App\Entity\Taxon;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TaxonGroupFixtures extends Fixture implements FixtureGroupInterface, DependentFixtureInterface
{
    const array FIXTURE_GROUPS = ['e2e'];

    /**
     * The taxonomic base hierarchy of all taxon groups and their roots.
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager): void
    {
        $admin = $manager->getRepository(User::class)->findOneBy(['username' => 'TestAdmin']);
        $groups = [
            [
                'displayName' => 'Bat',
                'pluralName' => 'Bats',
                'roots' => [
                    [
                        'subRanks' => '[7, 6, 5]',
                        'taxon' => [
                            'name' => 'Chiroptera',
                            'rank' => $manager->getRepository(Rank::class)->findOneBy(['displayName' => 'Order']),
                            // root
                        ]
                    ]
                ],
            ], [
                'displayName' => 'Plant',
                'pluralName' => 'Plants',
                'roots' => [
                    [
                        'subRanks' => '[7, 6, 5]',
                        'taxon' => [
                            'name' => 'Plantae',
                            'rank' => $manager->getRepository(Rank::class)->findOneBy(['displayName' => 'Kingdom']),
                            // root
                        ]
                    ]
                ],
            ], [
                'displayName' => 'Arthropod',
                'pluralName' => 'Arthropods',
                'roots' => [
                    [
                        'subRanks' => '[7, 6, 5, 4, 3]',
                        'taxon' => [
                            'name' => 'Arthropoda',
                            'rank' => $manager->getRepository(Rank::class)->findOneBy(['displayName' => 'Phylum']),
                            // root
                        ]
                    ]
                ],
            ], [
                'displayName' => 'Bacteria',
                'pluralName' => 'Bacteria',
                'roots' => [
                    [
                        'subRanks' => '[7, 6, 5, 4, 3]',
                        'taxon' => [
                            'name' => 'Bacteria',
                            'rank' => $manager->getRepository(Rank::class)->findOneBy(['displayName' => 'Domain']),
                            // root
                        ]
                    ]
                ],
            ], [
                'displayName' => 'Fish',
                'pluralName' => 'Fishes',
                'roots' => [
                    [
                        'description' => 'Ray-finned fish',
                        'subRanks' => '[7, 6, 5, 4]',
                        'taxon' => [
                            'name' => 'Actinopterygii',
                            'rank' => $manager->getRepository(Rank::class)->findOneBy(['displayName' => 'Class']),
                            // root
                        ]
                    ]
                ],
            ], [
                'displayName' => 'Fungi',
                'pluralName' => 'Fungi',
                'roots' => [
                    [
                        'subRanks' => '[7, 6, 5]',
                        'taxon' => [
                            'name' => 'Fungi',
                            'rank' => $manager->getRepository(Rank::class)->findOneBy(['displayName' => 'Kingdom']),
                            // root
                        ]
                    ]
                ],
            ], [
                'displayName' => 'Virus',
                'pluralName' => 'Viruses',
                'roots' => [
                    [
                        'subRanks' => '[7, 6, 5]',
                        'taxon' => [
                            'name' => 'Virus',
                            'rank' => $manager->getRepository(Rank::class)->findOneBy(['displayName' => 'Domain']),
                            // root
                        ]
                    ]
                ],
            ], [
                'displayName' => 'Bird',
                'pluralName' => 'Birds',
                'roots' => [
                    [
                        'subRanks' => '[7, 6, 5]',
                        'taxon' => [
                            'name' => 'Aves',
                            'rank' => $manager->getRepository(Rank::class)->findOneBy(['displayName' => 'Class']),
                            // root
                        ]
                    ]
                ],
            ], [
                'displayName' => 'Reptile',
                'pluralName' => 'Reptiles',
                'roots' => [
                    [
                        'subRanks' => '[7, 6, 5]',
                        'taxon' => [
                            'name' => 'Reptilia',
                            'rank' => $manager->getRepository(Rank::class)->findOneBy(['displayName' => 'Class']),
                            // root
                        ]
                    ]
                ],
            ], [
                'displayName' => 'Amphibian',
                'pluralName' => 'Amphibians',
                'roots' => [
                    [
                        'subRanks' => '[7, 6, 5]',
                        'taxon' => [
                            'name' => 'Amphibia',
                            'rank' => $manager->getRepository(Rank::class)->findOneBy(['displayName' => 'Class']),
                            // root
                        ]
                    ]
                ],
            ], [
                'displayName' => 'Protozoa',
                'pluralName' => 'Parasites',
                'roots' => [
                    [
                        'description' => 'Fungi-like, taxonomy unclear',
                        'subRanks' => '[7, 6, 5]',
                        'taxon' => [
                            'name' => 'Protozoa',
                            'rank' => $manager->getRepository(Rank::class)->findOneBy(['displayName' => 'Kingdom']),
                            // root
                        ]
                    ]
                ],
            ], [
                'displayName' => 'Mammal',
                'pluralName' => 'Mammals',
                'roots' => [
                    [
                        'description' => 'Rodents',
                        'subRanks' => '[7, 6, 5]',
                        'taxon' => [
                            'name' => 'Rodentia',
                            'rank' => $manager->getRepository(Rank::class)->findOneBy(['displayName' => 'Order']),
                            // root
                        ]
                    ], [
                        'description' => 'Even-toed ungulates; hoofed animals- sheep, pigs, cows',
                        'subRanks' => '[7, 6, 5]',
                        'taxon' => [
                            'name' => 'Artiodactyla',
                            'rank' => $manager->getRepository(Rank::class)->findOneBy(['displayName' => 'Order']),
                            // root
                        ]
                    ], [
                        'description' => 'Civets, weasels, dogs',
                        'subRanks' => '[7, 6, 5]',
                        'taxon' => [
                            'name' => 'Carnivora',
                            'rank' => $manager->getRepository(Rank::class)->findOneBy(['displayName' => 'Order']),
                            // root
                        ]
                    ], [
                        'description' => 'Odd-toed ungulates; hoofed animals- horses, tapirs',
                        'subRanks' => '[7, 6, 5]',
                        'taxon' => [
                            'name' => 'Perissodactyla',
                            'rank' => $manager->getRepository(Rank::class)->findOneBy(['displayName' => 'Order']),
                            // root
                        ]
                    ], [
                        'description' => 'Pangolins',
                        'subRanks' => '[7, 6, 5]',
                        'taxon' => [
                            'name' => 'Pholidota',
                            'rank' => $manager->getRepository(Rank::class)->findOneBy(['displayName' => 'Order']),
                            // root
                        ]
                    ]
                ],
            ], [
                'displayName' => 'Worm',
                'pluralName' => 'Worms',
                'roots' => [
                    [
                        'description' => 'Thorny-headed worms',
                        'subRanks' => '[7, 6, 5, 4, 3]',
                        'taxon' => [
                            'name' => 'Acanthocephala',
                            'rank' => $manager->getRepository(Rank::class)->findOneBy(['displayName' => 'Phylum']),
                            // root
                        ]
                    ], [
                        'description' => 'Nematodes',
                        'subRanks' => '[7, 6, 5, 4, 3]',
                        'taxon' => [
                            'name' => 'Nematoda',
                            'rank' => $manager->getRepository(Rank::class)->findOneBy(['displayName' => 'Phylum']),
                            // root
                        ]
                    ], [
                        'description' => 'Flatworms',
                        'subRanks' => '[7, 6, 5, 4, 3]',
                        'taxon' => [
                            'name' => 'Platyhelminthes',
                            'rank' => $manager->getRepository(Rank::class)->findOneBy(['displayName' => 'Phylum']),
                            // root
                        ]
                    ], [
                        'description' => 'Earthworms',
                        'subRanks' => '[7, 6, 5, 4, 3]',
                        'taxon' => [
                            'name' => 'Annelida',
                            'rank' => $manager->getRepository(Rank::class)->findOneBy(['displayName' => 'Phylum']),
                            // root
                        ]
                    ]
                ],
            ], [
                'displayName' => 'Chromista',
                'pluralName' => 'Chromista',
                'roots' => [
                    [
                        'description' => 'Contains protists & plasmodium (ie. malaria)',
                        'subRanks' => '[7, 6, 5]',
                        'taxon' => [
                            'name' => 'Chromista',
                            'rank' => $manager->getRepository(Rank::class)->findOneBy(['displayName' => 'Kingdom']),
                            // root
                        ]
                    ]
                ],
            ],
        ];

        foreach ( $groups as $group ) {
            $groupEntity = new Group();
            $groupEntity->setDisplayName($group['displayName']);
            $groupEntity->setPluralName($group['pluralName']);
            $groupEntity->setCreatedBy($admin);
            // create roots
            foreach ( $group['roots'] as $root ) {
                // create taxon
                $taxon = new Taxon();
                $taxon->setName($root['taxon']['name']);
                $taxon->setRank($root['taxon']['rank']);
                $taxon->setCreatedBy($admin);

                // create group root
                $groupRoot = new GroupRoot();
                $groupRoot->setDescription($root['description']);
                $groupRoot->setSubRanks($root['subRanks']);
                $groupRoot->setGroup($groupEntity);
                $groupRoot->setTaxon($taxon);
                $groupRoot->setCreatedBy($admin);
                $manager->persist($groupRoot);

                // add group-root to taxon
                $taxon->setGroupRoot($groupRoot);
                $taxon->setRoot($groupRoot);
                $manager->persist($taxon);
            }

            $manager->persist($groupEntity);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
            RankFixtures::class,
        ];
    }

    public static function getGroups(): array
    {
        return self::FIXTURE_GROUPS;
    }
}
