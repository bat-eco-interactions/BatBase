<?php

namespace App\DataFixtures;

use App\Entity\Tag;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class TagFixtures extends Fixture implements FixtureGroupInterface
{
    const array FIXTURE_GROUPS = ['dev', 'test'];

    public function load(ObjectManager $manager): void
    {
        $types = [
            'interaction type' => [
                'Seed',
                'Flower',
                'Fruit',
                'Leaf',
                'Arthropod',
                'Bryophyte Fragment',
                'Coroost',
                'Internal',
                'External',
                'Synchronous',
                'Asynchronous',
            ],
            'source' => [
                'Secondary',
            ],
            'season' => [
                'Summer',
                'Dry',
                'Winter',
                'Wet',
                'Spring',
                'Fall'
            ]
        ];

        foreach ($types as $name => $tags) {
            foreach ($tags as $tag) {
                $type = new Tag();
                $type->setDisplayName($tag);
                $type->setType($name);
                $type->setEntity('Interaction');
                $manager->persist($type);
            }
        }

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return self::FIXTURE_GROUPS;
    }
}
