<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture implements FixtureGroupInterface
{
    const array FIXTURE_GROUPS = ['dev', 'test'];

    public function __construct(private UserPasswordHasherInterface $passwordHasher)
    {
    }


    public function load(ObjectManager $manager): void
    {
        $users = [
            [
                'aboutMe' => 'Test super admin',
                'firstName' => 'Test',
                'lastName' => 'SuperAdmin',
                'username' => 'SuperAdmin',
                'role' => 'ROLE_SUPER_ADMIN',
                'email' => 'super.admin@email.com',
                'password' => 'passwordhere',
            ],
            [
                'aboutMe' => 'Test editor',
                'firstName' => 'Test',
                'lastName' => 'Editor',
                'username' => 'TestEditor',
                'role' => 'ROLE_EDITOR',
                'email' => 'editor@email.com',
                'password' => 'passwordhere',
            ],
            [
                'aboutMe' => 'Test user',
                'firstName' => 'Test',
                'lastName' => 'User',
                'username' => 'TestUser',
                'role' => 'ROLE_USER',
                'email' => 'user@email.com',
                'password' => 'passwordhere',
            ],
            [
                'aboutMe' => 'Test admin',
                'firstName' => 'Test',
                'lastName' => 'Admin',
                'username' => 'TestAdmin',
                'role' => 'ROLE_ADMIN',
                'email' => 'admin@email.com',
                'password' => 'passwordhere',
            ],
            [
                'aboutMe' => 'Test contributor',
                'firstName' => 'Test',
                'lastName' => 'Contributor',
                'username' => 'TestContributor',
                'role' => 'ROLE_CONTRIBUTOR',
                'email' => 'contributor@email.com',
                'password' => 'passwordhere',
            ],
            [
                'aboutMe' => 'Test manager',
                'firstName' => 'Test',
                'lastName' => 'Manager',
                'username' => 'TestManager',
                'role' => 'ROLE_SUPER_ADMIN',
                'email' => 'manager@email.com',
                'password' => 'passwordhere',
            ],
        ];

        foreach ($users as $user) {
            $userEntity = new User();
            $userEntity->setAboutMe($user['aboutMe']);
            $userEntity->setFirstName($user['firstName']);
            $userEntity->setLastName($user['lastName']);
            $userEntity->setUsername($user['username']);
            $userEntity->setEmail($user['email']);
            $userEntity->setPassword($this->passwordHasher->hashPassword($userEntity, $user['password']));
            $userEntity->addRole($user['role']);

            $manager->persist($userEntity);
        }

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return self::FIXTURE_GROUPS;
    }
}
