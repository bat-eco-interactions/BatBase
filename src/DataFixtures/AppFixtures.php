<?php

namespace App\DataFixtures;

use App\DataFixtures\Factory\ApiTokenFactory;
use App\DataFixtures\Factory\UserFactory;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    const array FIXTURE_GROUPS = [];

    /**
     * Commands to purge database () and load fixtures is somewhat complicated due
     * to the foreign key restraints. It may end up making sense to build a
     * custom purger at some point. See issue: https://github.com/doctrine/DoctrineFixturesBundle/issues/370
     *
    php bin/console doctrine:schema:drop --force --env=test
    php bin/console doctrine:schema:update --complete --force --env=test
    php bin/console doctrine:fixtures:load -vvv --no-interaction --env=test (as needed: --group=test --group=e2e)
     *
     * When an empty database is not important, the following command is fine:
     *
    php bin/console doctrine:fixtures:load -vvv --append --env=test
     *
     */
    public function load(ObjectManager $manager): void
    {
//        $this->createUserRolesAndTokens();
    }

    /** Creates a user for each user role with an api token with that scope. */
    private function createUserRolesAndTokens(): void
    {
        $roles = User::USER_ROLES;

        foreach ($roles as $role => $desc) {
            // Using Factory::find often returns this error:  No "App\Entity\User" object found for "array".
            // Not sure why and it's not worth tracking down right now
            $user = UserFactory::findBy(['username' => (string) $role]);
            if (!count($user)) {
                $user = UserFactory::createOne([
                    'username' => $role,
                    'roles' => [$role],
                ]);
            } else {
                $user = $user[0];
            }

            $token = ApiTokenFactory::findBy(['ownedBy' => $user]);
            if (!count($token)) {
                ApiTokenFactory::createOne([
                    'ownedBy' => $user,
                    'scopes' => [$role]
                ]);
            }
        }
    }

    public static function getGroups(): array
    {
        return self::FIXTURE_GROUPS;
    }
}
