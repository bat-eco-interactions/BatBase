<?php

namespace App\DataFixtures;

use App\Entity\HabitatType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class HabitatTypeFixtures extends Fixture implements FixtureGroupInterface
{
    const array FIXTURE_GROUPS = ['dev', 'test'];

    public function load(ObjectManager $manager): void
    {
        $types = [ 'Forest', 'Savanna', 'Desert', 'Shrubland', 'Captivity',
            'Artificial Landscape', 'Grassland', 'Wetland', 'Rocky Areas',
            'Caves and Subterranean Areas'
        ];

        foreach ($types as $type) {
            $habitatType = new HabitatType();
            $habitatType->setDisplayName($type);
            $manager->persist($habitatType);
        }

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return self::FIXTURE_GROUPS;
    }
}
