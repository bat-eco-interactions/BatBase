<?php

namespace App\DataFixtures;

use App\Entity\GroupRoot;
use App\Entity\InteractionType;
use App\Entity\Tag;
use App\Entity\Taxon;
use App\Entity\ValidInteraction;
use App\Entity\ValidInteractionTag;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ValidInteractionFixtures extends Fixture implements FixtureGroupInterface, DependentFixtureInterface
{
    const array FIXTURE_GROUPS = ['dev', 'test'];

    /**
     * Includes an arbitrary selection of the possible valid interactions.
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager): void
    {
        $validInteractions = [
            [
                'objectGroupRoot' => 'Order Chiroptera',
                'subjectGroupRoot' => 'Phylum Arthropoda',
                'interactionType' => 'Predation'
            ],
            [
                'objectGroupRoot' => 'Phylum Arthropoda',
                'subjectGroupRoot' => 'Order Chiroptera',
                'interactionType' => 'Host'
            ],
            [
                'tagRequired' => true,
                'objectGroupRoot' => 'Kingdom Plantae',
                'subjectGroupRoot' => 'Order Chiroptera',
                'interactionType' => 'Pollination',
                'validInteractionTags' => [
                    [
                        'tag' => 'Flower',
                        'isRequired' => true
                    ]
                ]
            ],
            [
                'tagRequired' => true,
                'objectGroupRoot' => 'Kingdom Plantae',
                'subjectGroupRoot' => 'Order Chiroptera',
                'interactionType' => 'Visitation',
                'validInteractionTags' => [
                    [
                        'tag' => 'Flower',
                        'isRequired' => true
                    ]
                ]
            ],
            [
                'tagRequired' => true,
                'objectGroupRoot' => 'Kingdom Plantae',
                'subjectGroupRoot' => 'Order Chiroptera',
                'interactionType' => 'Seed Dispersal',
                'validInteractionTags' => [
                    [
                        'tag' => 'Seed',
                        'isRequired' => true
                    ]
                ]
            ],
            [
                'tagRequired' => true,
                'objectGroupRoot' => 'Kingdom Plantae',
                'subjectGroupRoot' => 'Order Chiroptera',
                'interactionType' => 'Consumption',
                'validInteractionTags' => [
                    [
                        'tag' => 'Seed',
                        'isRequired' => false
                    ],
                    [
                        'tag' => 'Flower',
                        'isRequired' => false
                    ],
                    [
                        'tag' => 'Fruit',
                        'isRequired' => false
                    ],
                    [
                        'tag' => 'Leaf',
                        'isRequired' => false
                    ]
                ]
            ],
            [
                'objectGroupRoot' => 'Kingdom Fungi',
                'subjectGroupRoot' => 'Order Chiroptera',
                'interactionType' => 'Consumption'
            ],
            [
                'tagRequired' => true,
                'objectGroupRoot' => 'Kingdom Plantae',
                'subjectGroupRoot' => 'Order Chiroptera',
                'interactionType' => 'Roost',
                'validInteractionTags' => [
                    [
                        'tag' => 'Internal',
                        'isRequired' => false
                    ],
                    [
                        'tag' => 'External',
                        'isRequired' => false
                    ]
                ]
            ],
            [
                'tagRequired' => true,
                'objectGroupRoot' => 'Kingdom Plantae',
                'subjectGroupRoot' => 'Order Chiroptera',
                'interactionType' => 'Transport',
                'validInteractionTags' => [
                    [
                        'tag' => 'Bryophyte Fragment',
                        'isRequired' => true
                    ]
                ]
            ],
            [
                'tagRequired' => true,
                'objectGroupRoot' => 'Phylum Arthropoda',
                'subjectGroupRoot' => 'Order Chiroptera',
                'interactionType' => 'Transport',
                'validInteractionTags' =>[
                    [
                        'tag' => 'Arthropod',
                        'isRequired' => true
                    ]
                ]
            ],
            [
                'objectGroupRoot' => 'Order Chiroptera',
                'subjectGroupRoot' => 'Order Chiroptera',
                'interactionType' => 'Cohabitation',
                'validInteractionTags' => [
                    [
                        'tag' => 'Coroost',
                        'isRequired' => false
                    ]
                ]
            ],
            [
                'objectGroupRoot' => 'Phylum Arthropoda',
                'subjectGroupRoot' => 'Order Chiroptera',
                'interactionType' => 'Cohabitation'
            ],
            [
                'objectGroupRoot' => 'Phylum Arthropoda',
                'subjectGroupRoot' => 'Order Chiroptera',
                'interactionType' => 'Predation'
            ],
            [
                'objectGroupRoot' => 'Kingdom Fungi',
                'subjectGroupRoot' => 'Order Chiroptera',
                'interactionType' => 'Host'
            ],
            [
                'objectGroupRoot' => 'Class Aves',
                'subjectGroupRoot' => 'Order Chiroptera',
                'interactionType' => 'Hematophagy'
            ],
            [
                'objectGroupRoot' => 'Order Chiroptera',
                'subjectGroupRoot' => 'Class Aves',
                'interactionType' => 'Predation'
            ],
            [
                'objectGroupRoot' => 'Order Chiroptera',
                'subjectGroupRoot' => 'Class Reptilia',
                'interactionType' => 'Predation'
            ],
            [
                'objectGroupRoot' => 'Order Chiroptera',
                'subjectGroupRoot' => 'Class Amphibia',
                'interactionType' => 'Predation'
            ],
            [
                'objectGroupRoot' => 'Order Chiroptera',
                'subjectGroupRoot' => 'Class Actinopterygii',
                'interactionType' => 'Predation'
            ],
            [
                'objectGroupRoot' => 'Class Aves',
                'subjectGroupRoot' => 'Order Chiroptera',
                'interactionType' => 'Cohabitation'
            ],
            [
                'objectGroupRoot' => 'Class Reptilia',
                'subjectGroupRoot' => 'Order Chiroptera',
                'interactionType' => 'Cohabitation'
            ]
        ];

        foreach ($validInteractions as $data) {
            $object = $this->getGroupRoot($manager, $data['objectGroupRoot']);
            $subject = $this->getGroupRoot($manager, $data['subjectGroupRoot']);
            $interactionType = $manager->getRepository(InteractionType::class)->findOneBy(['displayName' => $data['interactionType']]);

            $validInteraction = new ValidInteraction();
            $validInteraction->setObjectGroupRoot($object);
            $validInteraction->setSubjectGroupRoot($subject);
            $validInteraction->setInteractionType($interactionType);
            $validInteraction->setTagRequired($data['tagRequired'] ?? false);

            if (isset($data['validInteractionTags'])) {
                foreach ($data['validInteractionTags'] as $tagData) {
                    $tag = $manager->getRepository(Tag::class)->findOneBy(['displayName' => $tagData['tag']]);

                    $validInteractionTag = new ValidInteractionTag();
                    $validInteractionTag->setValidInteraction($validInteraction);
                    $validInteractionTag->setTag($tag);
                    $validInteractionTag->setIsRequired($tagData['isRequired'] ?? false);
                    $validInteraction->addValidInteractionTag($validInteractionTag);
                }
            }

            $manager->persist($validInteraction);
        }

        $manager->flush();
    }

    private function getGroupRoot(ObjectManager $manager, string $name): GroupRoot
    {
        $taxon = $manager->getRepository(Taxon::class)->findOneBy(['displayName' => $name]);
        return $taxon->getGroupRoot();
    }

    public function getDependencies(): array
    {
        return [
            InteractionFixtures::class,
            TaxonGroupFixtures::class,
            TagFixtures::class,
        ];
    }

    public static function getGroups(): array
    {
        return self::FIXTURE_GROUPS;
    }
}
