<?php

namespace App\DataFixtures;

use App\Entity\HabitatType;
use App\Entity\SystemDate;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class SystemDateFixtures extends Fixture implements FixtureGroupInterface
{
    const array FIXTURE_GROUPS = ['dev', 'test'];

    public function load(ObjectManager $manager): void
    {
        $types = [ 'System', 'Author', 'Authority', 'Citation', 'CitationType',
            'ContentBlock', 'Contribution', 'Group', 'Feedback', 'HabitatType',
            'Interaction', 'InteractionType', 'Location', 'Source', 'Tag', 'Taxon',
            'TaxonGroup', 'ValidInteraction', 'ValidInteractionTag', 'User' ];

        foreach ($types as $type) {
            $systemDate = new SystemDate();
            $systemDate->setEntity($type);
            // This date is used in tests to identify changed records, '-3 days' handles timezone edge cases
            $systemDate->setUpdated(new \DateTime('-3 days', new \DateTimeZone('UTC')));
            $manager->persist($systemDate);
        }

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return self::FIXTURE_GROUPS;
    }
}
