<?php

namespace App\DataFixtures\Factory;


use App\Entity\ImageUpload;
use Zenstruck\Foundry\Persistence\PersistentProxyObjectFactory;

/**
 * @extends PersistentProxyObjectFactory<ImageUpload>
 */
final class ImageUploadFactory extends PersistentProxyObjectFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services

     */
    public function __construct()
    {
    }

    public static function class(): string
    {
        return ImageUpload::class;
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function defaults(): array|callable
    {
        return [
            'fileName' => self::faker()->text(),
            'mimeType' => self::faker()->text(),
            'path' => self::faker()->text(),
            'size' => self::faker()->randomFloat(),
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): static
    {
        return $this
            // ->afterInstantiate(function(ImageUpload $imageUpload): void {})
        ;
    }
}
