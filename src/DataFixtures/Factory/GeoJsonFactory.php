<?php

namespace App\DataFixtures\Factory;


use App\Entity\GeoJson;
use Zenstruck\Foundry\Persistence\PersistentProxyObjectFactory;

/**
 * @extends PersistentProxyObjectFactory<GeoJson>
 */
final class GeoJsonFactory extends PersistentProxyObjectFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services

     */
    public function __construct()
    {
    }

    public static function class(): string
    {
        return GeoJson::class;
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function defaults(): array|callable
    {
        return [
            'coordinates' => self::faker()->text(),
            'displayPoint' => self::faker()->text(),
            'type' => 'Point',
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): static
    {
        return $this
            // ->afterInstantiate(function(GeoJson $geoJson): void {})
        ;
    }
}
