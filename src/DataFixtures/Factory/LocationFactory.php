<?php

namespace App\DataFixtures\Factory;

use App\Entity\Location;
use App\Entity\LocationType;
use Zenstruck\Foundry\Persistence\PersistentProxyObjectFactory;
use Zenstruck\Foundry\Persistence\Proxy;

/**
 * @extends PersistentProxyObjectFactory<Location>
 */
final class LocationFactory extends PersistentProxyObjectFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     */
//    public function __construct()
//    {
//    }

    public static function class(): string
    {
        return Location::class;
    }

    /**
     * Default values include required properties and complex relationship handling.
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     */
    protected function defaults(): array|callable
    {
        return [
            'createdBy' => UserFactory::randomOrCreate(),
            'displayName' => self::faker()->words(3, true) . self::faker()->randomNumber(5, true),
            'locationType' => LocationTypeFactory::new(),
        ];
    }

    function withLocationType(string $name): LocationFactory
    {
        $type = LocationTypeFactory::findOrCreate([
            'displayName' => $name,
            'slug' => lcfirst($name)
        ]);

        return $this->with(['locationType' => $type]);
    }

    function withGeoJson(int $lat = 11, int $lng = 44): self
    {
        $geoJson = GeoJsonFactory::createOne([
            'coordinates' => "[$lng, $lat]",
            'displayPoint' => "[$lng, $lat]",
        ]);

        return $this->with([
            'geoJson' => $geoJson,
            'latitude' => $lat,
            'longitude' => $lng
        ]);
    }

    function withParentLocation(Location|Proxy $parentLocation): self
    {
        return $this->with(['parentLocation' => $parentLocation]);
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): static
    {
        return $this// ->afterInstantiate(function(Location $location): void {})
        ;
    }
}
