<?php

namespace App\DataFixtures\Factory;

use App\Entity\LocationType;
use Zenstruck\Foundry\Persistence\PersistentProxyObjectFactory;

/**
 * @extends PersistentProxyObjectFactory<LocationType>
 */
final class LocationTypeFactory extends PersistentProxyObjectFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     */
//    public function __construct()
//    {
//    }

    public static function class(): string
    {
        return LocationType::class;
    }

    /**
     * Default values include required properties and complex relationship handling.
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     */
    protected function defaults(): array|callable
    {
        $name = self::faker()->unique()->words(2, true);
        return [
            'displayName' => $name,
            'ordinal' => self::faker()->unique()->randomNumber(5),
            'slug' => lcfirst($name)
        ];
    }

    function withName(string $name): LocationTypeFactory
    {
        return $this->with([
            'displayName' => $name,
            'slug' => lcfirst($name)
        ]);
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): static
    {
        return $this// ->afterInstantiate(function(LocationType $locationType): void {})
        ;
    }
}
