<?php

namespace App\DataFixtures\Factory;

use App\Entity\Author;
use Zenstruck\Foundry\Persistence\PersistentProxyObjectFactory;

/**
 * @extends PersistentProxyObjectFactory<Author>
 */
final class AuthorFactory extends PersistentProxyObjectFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     */
//    public function __construct()
//    {
//    }

    public static function class(): string
    {
        return Author::class;
    }

    /**
     * Default values include required properties and complex relationship handling.
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     */
    protected function defaults(): array|callable
    {
        $lastName = self::faker()->unique()->words(1, true);
        $createdBy = UserFactory::randomOrCreate();
        return [
            'lastName' => $lastName,
            'source' => SourceFactory::createOne([
                'displayName' => $lastName,
                'createdBy' => $createdBy,
                'sourceType' => SourceTypeFactory::findOrCreate([
                    'displayName' => 'Author',
                ]),
            ]),
            'createdBy' => $createdBy,
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): static
    {
        return $this//->afterInstantiate(function(Author $author): void {})
        ;
    }
}
