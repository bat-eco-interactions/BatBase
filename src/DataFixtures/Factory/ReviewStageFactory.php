<?php

namespace App\DataFixtures\Factory;


use App\Entity\ReviewStage;
use Zenstruck\Foundry\Persistence\PersistentProxyObjectFactory;

/**
 * @extends PersistentProxyObjectFactory<ReviewStage>
 */
final class ReviewStageFactory extends PersistentProxyObjectFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services

     */
    public function __construct()
    {
    }

    public static function class(): string
    {
        return ReviewStage::class;
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function defaults(): array|callable
    {
        return [
            'activeForm' => self::faker()->text(),
            'description' => self::faker()->text(),
            'passiveForm' => self::faker()->text(),
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): static
    {
        return $this
            // ->afterInstantiate(function(ReviewStage $reviewStage): void {})
        ;
    }
}
