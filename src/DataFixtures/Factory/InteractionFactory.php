<?php

namespace App\DataFixtures\Factory;

use App\Entity\Interaction;
use Zenstruck\Foundry\LazyValue;
use Zenstruck\Foundry\Persistence\PersistentProxyObjectFactory;

/**
 * @extends PersistentProxyObjectFactory<Interaction>
 */
final class InteractionFactory extends PersistentProxyObjectFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     */
//    public function __construct()
//    {
//    }

    public static function class(): string
    {
        return Interaction::class;
    }

    /**
     * Default values include required properties and complex relationship handling.
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     */
    protected function defaults(): array|callable
    {
        return [
            'createdBy' => UserFactory::randomOrCreate(),
            'interactionType' => InteractionTypeFactory::randomOrCreate(),
            'location' => LocationFactory::randomOrCreate(),
            'object' => TaxonFactory::randomOrCreate(),
            'source' => CitationFactory::randomOrCreate()->getSource(),
            'subject' => TaxonFactory::randomOrCreate(),
        ];
    }

    public function withInteractionType(string $name): InteractionFactory
    {
        return $this->with([
            'interactionType' => InteractionTypeFactory::findOrCreate([
                'displayName' => $name
            ])
        ]);
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): static
    {
        return $this// ->afterInstantiate(function(Interaction $interaction): void {})
        ;
    }
}
