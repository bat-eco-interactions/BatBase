<?php

namespace App\DataFixtures\Factory;

use App\Entity\ApiToken;

use App\Entity\User;
use Zenstruck\Foundry\LazyValue;
use Zenstruck\Foundry\Persistence\PersistentProxyObjectFactory;

/**
 * @extends PersistentProxyObjectFactory<ApiToken>
 */
final class ApiTokenFactory extends PersistentProxyObjectFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services

     */
//    public function __construct()
//    {
//    }

    public static function class(): string
    {
        return ApiToken::class;
    }

    /**
     * Note: defaults() is called everytime a factory is instantiated (even if you don't end up creating it). Lazy Values allows you to ensure the value is only calculated when/if it's needed.
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function defaults(): array|callable
    {
        // Only created if no 'ownedBy' value is passed to  Factory::new()
        $owner = LazyValue::new((fn() => UserFactory::createOne()));

        return [
            'ownedBy' => $owner,
            'scopes' => [
                self::faker()->randomElement(array_keys(User::USER_ROLES))
            ],
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): static
    {
        return $this
            // ->afterInstantiate(function(ApiToken $apiToken): void {})
        ;
    }
}
