<?php

namespace App\DataFixtures\Factory;

use App\Entity\Taxon;
use Zenstruck\Foundry\LazyValue;
use Zenstruck\Foundry\Persistence\PersistentProxyObjectFactory;

/**
 * @extends PersistentProxyObjectFactory<Taxon>
 */
final class TaxonFactory extends PersistentProxyObjectFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     */
//    public function __construct()
//    {
//    }

    public static function class(): string
    {
        return Taxon::class;
    }

    /**
     * When a parentTaxon value is not passed, a GroupRoot is selected (or created)
     * and its root taxon (created during GroupRoot instantiation) is set as the parent.
     * Default values include required properties and complex relationship handling.
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     */
    protected function defaults(): array|callable
    {
        return [
            'createdBy' => UserFactory::randomOrCreate(),
            'displayName' => self::faker()->unique()->words(3, true),
            'name' => self::faker()->words(1, true),
            'parentTaxon' => LazyValue::memoize(fn() => GroupRootFactory::randomOrCreate()->getTaxon()),
            // Tests depend on this default rank being random and at this ordinal
            // Use withRank() to specify
            'rank' => LazyValue::memoize((fn() => RankFactory::createOne(['ordinal' => 20]))),
        ];
    }

    public function withRank(string $rankName, int $ordinal): TaxonFactory
    {
        $rank = RankFactory::findOrCreate([
            'displayName' => $rankName,
            'ordinal' => $ordinal
        ]);
        return $this->with(['rank' => $rank]);
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): static
    {
        return $this//->afterPersist(function(Taxon $taxon): void {})
        ;
    }
}
