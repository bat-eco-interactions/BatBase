<?php

namespace App\DataFixtures\Factory;


use App\Entity\SourceType;
use Zenstruck\Foundry\Persistence\PersistentProxyObjectFactory;

/**
 * @extends PersistentProxyObjectFactory<SourceType>
 */
final class SourceTypeFactory extends PersistentProxyObjectFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services

     */
//    public function __construct()
//    {
//    }

    public static function class(): string
    {
        return SourceType::class;
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function defaults(): array|callable
    {
        return [
            'displayName' => self::faker()->words(1, true),
            'ordinal' => self::faker()->unique()->randomNumber(5),
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): static
    {
        return $this
            // ->afterInstantiate(function(SourceType $sourceType): void {})
        ;
    }
}
