<?php

namespace App\DataFixtures\Factory;

use App\Entity\Publisher;
use Zenstruck\Foundry\Persistence\PersistentProxyObjectFactory;

/**
 * @extends PersistentProxyObjectFactory<Publisher>
 */
final class PublisherFactory extends PersistentProxyObjectFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     */
//    public function __construct()
//    {
//    }

    public static function class(): string
    {
        return Publisher::class;
    }

    /**
     * Default values include required properties and complex relationship handling.
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     */
    protected function defaults(): array|callable
    {
        $displayName = self::faker()->unique()->words(4, true);
        $createdBy = UserFactory::randomOrCreate();
        return [
            'displayName' => $displayName,
            'source' => SourceFactory::createOne([
                'displayName' => $displayName,
                'createdBy' => $createdBy,
                'sourceType' => SourceTypeFactory::findOrCreate([
                    'displayName' => 'Publisher',
                ]),
            ]),
            'createdBy' => $createdBy,
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): static
    {
        return $this// ->afterInstantiate(function(Publisher $publisher): void {})
        ;
    }
}
