<?php

namespace App\DataFixtures\Factory;


use App\Entity\Source;
use Zenstruck\Foundry\LazyValue;
use Zenstruck\Foundry\Persistence\PersistentProxyObjectFactory;

/**
 * @extends PersistentProxyObjectFactory<Source>
 */
final class SourceFactory extends PersistentProxyObjectFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services

     */
//    public function __construct()
//    {
//    }

    public static function class(): string
    {
        return Source::class;
    }

    /**
     * Default values include required properties and complex relationship handling.
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     */
    protected function defaults(): array|callable
    {
        return [
            'displayName' => self::faker()->text(),
            'sourceType' => LazyValue::memoize(fn() => SourceTypeFactory::new()),
            'createdBy' => LazyValue::memoize(fn() => UserFactory::randomOrCreate()),
        ];
    }

    public function withParentPublication(): self
    {
        $publication = PublicationFactory::createOne();
        $parentSource = $publication->getSource();

        $year = self::faker()->numberBetween(1900, 2021);
        $parentSource->setYear($year);

        return $this->with([
            'parentSource' => $parentSource,
            'year' => $year,
        ]);
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): static
    {
        return $this//->afterInstantiate(function(Source $source): void {})
        ;
    }
}
