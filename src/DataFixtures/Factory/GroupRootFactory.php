<?php

namespace App\DataFixtures\Factory;


use App\Entity\GroupRoot;
use Zenstruck\Foundry\LazyValue;
use Zenstruck\Foundry\Persistence\PersistentProxyObjectFactory;

/**
 * @extends PersistentProxyObjectFactory<GroupRoot>
 */
final class GroupRootFactory extends PersistentProxyObjectFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     */
//    public function __construct()
//    {
//    }

    public static function class(): string
    {
        return GroupRoot::class;
    }

    /**
     * Default values include required properties and complex relationship handling.
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     */
    protected function defaults(): array|callable
    {
        return [
            'group' => LazyValue::memoize((fn() => GroupFactory::createOne())),
            'subRanks' => self::faker()->words(3, true),
            'taxon' => LazyValue::memoize((fn() => TaxonFactory::createOne([
                'parentTaxon' => null,
                'rank' => RankFactory::createOne(['ordinal' => 5])
            ]))),
        ];
    }

    public function withGroupName(string $name): GroupRootFactory
    {
        $group = GroupFactory::findOrCreate([
            'displayName' => $name
        ]);
        return $this->with(['group' => $group]);
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): static
    {
        return $this// ->afterInstantiate(function(GroupRoot $groupRoot): void {})
        ;
    }
}
