<?php

namespace App\DataFixtures\Factory;


use App\Entity\Rank;
use Zenstruck\Foundry\Persistence\PersistentProxyObjectFactory;

/**
 * @extends PersistentProxyObjectFactory<Rank>
 */
final class RankFactory extends PersistentProxyObjectFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     */
//    public function __construct()
//    {
//    }

    public static function class(): string
    {
        return Rank::class;
    }

    /**
     * Default values include required properties and complex relationship handling.
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     */
    protected function defaults(): array|callable
    {
        $displayName = self::faker()->text();
        return [
            'displayName' => $displayName,
            'ordinal' => self::faker()->unique()->randomNumber(3),
            'pluralName' => $displayName . 's',
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): static
    {
        return $this// ->afterInstantiate(function(Rank $rank): void {})
        ;
    }
}
