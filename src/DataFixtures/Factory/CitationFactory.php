<?php

namespace App\DataFixtures\Factory;


use App\Entity\Citation;
use Zenstruck\Foundry\LazyValue;
use Zenstruck\Foundry\Persistence\PersistentProxyObjectFactory;

/**
 * @extends PersistentProxyObjectFactory<Citation>
 */
final class CitationFactory extends PersistentProxyObjectFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     */
//    public function __construct()
//    {
//    }

    public static function class(): string
    {
        return Citation::class;
    }

    /**
     * Default values include required properties and complex relationship handling.
     * Note: contributors must be set after init until Source entity refactored away.
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     */
    protected function defaults(): array|callable
    {
        $createdBy = UserFactory::randomOrCreate();
        $displayName = self::faker()->unique()->words(7, true);

        return [
            'createdBy' => $createdBy,
            'citationType' => CitationTypeFactory::createOne(),
            'displayName' => $displayName,
            'fullText' => self::faker()->text(),
            'source' => LazyValue::new(fn() => SourceFactory::new([
                'createdBy' => $createdBy,
                'displayName' => $displayName,
                'sourceType' => SourceTypeFactory::findOrCreate([
                    'displayName' => 'Citation',
                ]),
                'year' => self::faker()->numberBetween(1900, 2021),
            ])
                ->withParentPublication()
                ->create()
            ),
            'title' => $displayName,
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): static
    {
        return $this//->afterPersist(function (Citation $citation): void {})
        ;
    }
}
