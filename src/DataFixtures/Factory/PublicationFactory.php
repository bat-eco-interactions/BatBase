<?php

namespace App\DataFixtures\Factory;

use App\Entity\Publication;
use Zenstruck\Foundry\LazyValue;
use Zenstruck\Foundry\Persistence\PersistentProxyObjectFactory;

/**
 * @extends PersistentProxyObjectFactory<Publication>
 */
final class PublicationFactory extends PersistentProxyObjectFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     */
//    public function __construct()
//    {
//    }

    public static function class(): string
    {
        return Publication::class;
    }

    /**
     * Default values include required properties and complex relationship handling.
     * Note: contributors must be set after init until Source entity refactored away.
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     */
    protected function defaults(): array|callable
    {
        $createdBy = UserFactory::randomOrCreate();
        $displayName = self::faker()->unique()->words(7, true);
        return [
            'createdBy' => $createdBy,
            'displayName' => $displayName,
            'publicationType' => LazyValue::new(fn() => PublicationTypeFactory::createOne()),
            'source' => LazyValue::new(fn() => SourceFactory::createOne([
                'createdBy' => $createdBy,
                'displayName' => $displayName,
                'sourceType' => SourceTypeFactory::findOrCreate([
                    'displayName' => 'Publication',
                ]),
            ])),
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): static
    {
        return $this//->afterInstantiate(function(Publication $publication): void {})
        ;
    }
}
