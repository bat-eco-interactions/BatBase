<?php

namespace App\DataFixtures;

use App\Entity\SourceType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class SourceTypeFixtures extends Fixture implements FixtureGroupInterface
{
    const array FIXTURE_GROUPS = ['dev', 'test'];

    public function load(ObjectManager $manager): void
    {
        $types = [ 'Publication', 'Citation', 'Author', 'Publisher' ];

        foreach ($types as $name) {
            $type = new SourceType();
            $type->setDisplayName($name);
            $type->setOrdinal(0);
            $manager->persist($type);
        }

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return self::FIXTURE_GROUPS;
    }
}
