<?php

namespace App\DataFixtures;

use App\Entity\ReviewStage;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class ReviewStageFixtures extends Fixture implements FixtureGroupInterface
{
    const array FIXTURE_GROUPS = ['dev', 'test'];

    public function load(ObjectManager $manager): void
    {
        $stages = [
            [
                'passiveForm' => 'Pending',
                'activeForm' => 'Quarantine',
                'description' => 'Unreviewed data from contributor'
            ],
            [
                'passiveForm' => 'Locked',
                'activeForm' => 'Lock',
                'description' => 'Data in an active review process'
            ],
            [
                'passiveForm' => 'Returned',
                'activeForm' => 'Return',
                'description' => 'Returned data to the contributor for further action'
            ],
            [
                'passiveForm' => 'Held',
                'activeForm' => 'Pause',
                'description' => 'Data on hold for a specific data-manager'
            ],
            [
                'passiveForm' => 'Rejected',
                'activeForm' => 'Reject',
                'description' => 'Data are rejected and will be deleted'
            ],
            [
                'passiveForm' => 'Approved',
                'activeForm' => 'Approve',
                'description' => 'Data are approved and entered into the database'
            ],
            [
                'passiveForm' => 'Completed',
                'activeForm' => 'Complete',
                'description' => 'Data are no longer pending review.'
            ]
        ];

        foreach ($stages as $stage) {
            $entity = new ReviewStage();
            $entity->setPassiveForm($stage['passiveForm']);
            $entity->setActiveForm($stage['activeForm']);
            $entity->setDescription($stage['description']);
            $manager->persist($entity);
        }

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return self::FIXTURE_GROUPS;
    }
}
