<?php

namespace App\DataFixtures;

use App\Entity\Rank;
use App\Entity\Taxon;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TaxonChildrenFixtures extends Fixture implements FixtureGroupInterface, DependentFixtureInterface
{
    const array FIXTURE_GROUPS = ['e2e'];

    /**
     * Child taxa used in e2e testing
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager): void
    {
        $admin = $manager->getRepository(User::class)->findOneBy(['username' => 'TestAdmin']);
        $children = [
            [
                'name' => 'Phyllostomidae',
                'parent' => 'Chiroptera',
                'rank' => 'Family',
            ],
            [
                'name' => 'Micronycteris',
                'parent' => 'Phyllostomidae',
                'rank' => 'Genus',
            ],
            [
                'name' => 'Micronycteris hirsuta',
                'parent' => 'Micronycteris',
                'rank' => 'Species',
            ],
            [
                'name' => 'Lepidoptera',
                'parent' => 'Arthropoda',
                'rank' => 'Order',
            ],
            [
                'name' => 'Sphingidae',
                'parent' => 'Lepidoptera',
                'rank' => 'Family',
            ],
            [
                'name' => 'Lauraceae',
                'parent' => 'Plantae',
                'rank' => 'Family',
            ],
            [
                'name' => 'Beilschmiedia',
                'parent' => 'Lauraceae',
                'rank' => 'Genus',
            ],
            [
                'name' => 'Artibeus',
                'parent' => 'Phyllostomidae',
                'rank' => 'Genus',
            ],
            [
                'name' => 'Artibeus lituratus',
                'parent' => 'Artibeus',
                'rank' => 'Species',
            ],
            [
                'name' => 'Anacardiaceae',
                'parent' => 'Plantae',
                'rank' => 'Family',
            ],
            [
                'name' => 'Anacardium',
                'parent' => 'Anacardiaceae',
                'rank' => 'Genus',
            ],
            [
                'name' => 'Anacardium excelsum',
                'parent' => 'Anacardium',
                'rank' => 'Species',
            ],
            [
                'name' => 'Micronycteris schmidtorum',
                'parent' => 'Micronycteris',
                'rank' => 'Species',
            ],
            [
                'name' => 'Fabaceae',
                'parent' => 'Plantae',
                'rank' => 'Family',
            ],
            [
                'name' => 'Mucuna',
                'parent' => 'Fabaceae',
                'rank' => 'Genus',
            ],
            [
                'name' => 'Mucuna andreana',
                'parent' => 'Mucuna',
                'rank' => 'Species',
            ],
            [
                'name' => 'Rhinophylla',
                'parent' => 'Phyllostomidae',
                'rank' => 'Genus',
            ],
            [
                'name' => 'Rhinophylla pumilio',
                'parent' => 'Rhinophylla',
                'rank' => 'Species',
            ],
            [
                'name' => 'Araceae',
                'parent' => 'Plantae',
                'rank' => 'Family',
            ],
            [
                'name' => 'Philodendron',
                'parent' => 'Araceae',
                'rank' => 'Genus',
            ],
            [
                'name' => 'Philodendron sphalerum',
                'parent' => 'Philodendron',
                'rank' => 'Species',
            ],
            [
                'name' => 'Gammaproteobacteria',
                'parent' => 'Bacteria',
                'rank' => 'Class',
            ],
            [
                'name' => 'Spotted',
                'parent' => 'Acanthocephala',
                'rank' => 'Class',
            ],
            [
                'name' => 'Striped',
                'parent' => 'Nematoda',
                'rank' => 'Class',
            ],
            [
                'name' => 'Orange',
                'parent' => 'Protozoa',
                'rank' => 'Family',
            ],
        ];

        foreach ( $children as $child) {
            $parent = $manager->getRepository(Taxon::class)->findOneBy(['name' => $child['parent']]);
            $rank = $manager->getRepository(Rank::class)->findOneBy(['displayName' => $child['rank']]);

            $taxon = new Taxon();
            $taxon->setName($child['name']);
            $taxon->setParentTaxon($parent);
            $taxon->setRank($rank);
            $taxon->setCreatedBy($admin);
            $manager->persist($taxon);
            $manager->flush();
        }
    }

    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
            TaxonGroupFixtures::class
        ];
    }

    public static function getGroups(): array
    {
        return self::FIXTURE_GROUPS;
    }
}
