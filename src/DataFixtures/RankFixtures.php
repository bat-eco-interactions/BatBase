<?php

namespace App\DataFixtures;

use App\Entity\Rank;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class RankFixtures extends Fixture implements FixtureGroupInterface
{
    const array FIXTURE_GROUPS = ['dev', 'test'];

    public function load(ObjectManager $manager): void
    {
        $ranks = [
            [
                'displayName' => 'Domain',
                'ordinal' => 5,
                'pluralName' => 'Domains',
            ],[
                'displayName' => 'Kingdom',
                'ordinal' => 10,
                'pluralName' => 'Kingdoms',
            ],[
                'displayName' => 'Phylum',
                'ordinal' => 30,
                'pluralName' => 'Phyla',
            ],[
                'displayName' => 'Class',
                'ordinal' => 50,
                'pluralName' => 'Classes',
            ],[
                'displayName' => 'Order',
                'ordinal' => 70,
                'pluralName' => 'Orders',
            ],[
                'displayName' => 'Family',
                'ordinal' => 90,
                'pluralName' => 'Families',
            ],[
                'displayName' => 'Genus',
                'ordinal' => 110,
                'pluralName' => 'Genera',
            ],[
                'displayName' => 'Species',
                'ordinal' => 130,
                'pluralName' => 'Species',
            ],
        ];

        foreach ( $ranks as $rank ) {
            $entity = new Rank();
            $entity->setDisplayName($rank['displayName']);
            $entity->setOrdinal($rank['ordinal']);
            $entity->setPluralName($rank['pluralName']);
            $manager->persist($entity);
        }

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return self::FIXTURE_GROUPS;
    }
}
