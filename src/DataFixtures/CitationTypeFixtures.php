<?php

namespace App\DataFixtures;

use App\Entity\CitationType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class CitationTypeFixtures extends Fixture implements FixtureGroupInterface
{
    const array FIXTURE_GROUPS = ['dev', 'test'];

    public function load(ObjectManager $manager): void
    {
        $types = [ 'Article', 'Chapter', 'Book', 'Museum Record', 'Master\'s Thesis',
            'Report', 'Other', 'Ph.D. Dissertation' ];

        foreach ($types as $name) {
            $type = new CitationType();
            $type->setDisplayName($name);
            $manager->persist($type);
        }

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return self::FIXTURE_GROUPS;
    }
}
