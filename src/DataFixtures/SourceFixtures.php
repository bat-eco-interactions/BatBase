<?php

namespace App\DataFixtures;

use App\Entity\Author;
use App\Entity\Citation;
use App\Entity\CitationType;
use App\Entity\Publication;
use App\Entity\PublicationType;
use App\Entity\Publisher;
use App\Entity\Source;
use App\Entity\SourceType;
use App\Entity\User;
use App\Service\DataEntry\ContributionEntry;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class SourceFixtures extends Fixture implements FixtureGroupInterface, DependentFixtureInterface
{
    const array FIXTURE_GROUPS = ['e2e'];

    public function __construct(private readonly ContributionEntry $contributionEntry)
    {
    }

    /**
     * Source entities used in e2e testing
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager): void
    {
        $admin = $manager->getRepository(User::class)->findOneBy(['username' => 'TestAdmin']);

        // Load sources in necessary order
        $this->loadAuthors($manager, $admin);
        $this->loadPublishers($manager, $admin);
        $this->loadPublications($manager, $admin);
        $this->loadCitations($manager, $admin);
    }

    private function loadAuthors(ObjectManager $manager, User $admin): void
    {
        $authors = [
            [
                'displayName' => 'Gardner, Alfred L',
                'firstName' => 'Alfred',
                'middleName' => 'L',
                'lastName' => 'Gardner',
            ],
            [
                'displayName' => 'Bloedel, P',
                'firstName' => 'P',
                'middleName' => '',
                'lastName' => 'Bloedel',
                'fullName' => 'P Bloedel',
            ],
            [
                'displayName' => 'Cockle, Anya',
                'firstName' => 'Anya',
                'middleName' => '',
                'lastName' => 'Cockle',
            ],
            [
                'displayName' => 'Baker, Herbert G',
                'firstName' => 'Herbert',
                'middleName' => 'G',
                'lastName' => 'Baker',
            ],
        ];

        $sourceType = $manager->getRepository(SourceType::class)->findOneBy(['displayName' => 'Author']);

        foreach ($authors as $data) {
            $author = new Author();

            $source = new Source();
            $source->setDisplayName($data['displayName']);
            $source->setSourceType($sourceType);
            $source->setAuthor($author);
            $source->setCreatedBy($admin);

            $author->setFirstName($data['firstName']);
            $author->setMiddleName($data['middleName']);
            $author->setLastName($data['lastName']);
            $author->setCreatedBy($admin);

            $manager->persist($source);
        }

        $manager->flush();
    }

    private function loadPublishers(ObjectManager $manager, User $admin): void
    {
        $publishers = [
            [
                'displayName' => 'Britanica Books',
                'city' => 'Wellingsworth',
                'country' => 'Britan',
                'description' => 'A quaint publisher house.',
            ],
            [
                'displayName' => 'University of Paris VI',
                'city' => 'city',
                'country' => 'country',
                'description' => 'University of Paris VI (Pierre and Marie Curie University)',
            ],
        ];

        $sourceType = $manager->getRepository(SourceType::class)->findOneBy(['displayName' => 'Publisher']);

        foreach ($publishers as $data) {
            $publisher = new Publisher();
            $publisher->setDisplayName($data['displayName']);
            $publisher->setCity($data['city']);
            $publisher->setCountry($data['country']);
            $publisher->setCreatedBy($admin);

            $source = new Source();
            $source->setDisplayName($data['displayName']);
            $source->setSourceType($sourceType);
            $source->setPublisher($publisher);
            $source->setDescription($data['description']);
            $source->setCreatedBy($admin);

            $manager->persist($source);
        }

        $manager->flush();
    }

    private function loadPublications(ObjectManager $manager, User $admin): void
    {
        $publications = [
            [
                'displayName' => 'Biology of bats of the New World family Phyllostomatidae',
                'year' => '1977',
                'type' => 'Book',
                'parentSource' => 'Britanica Books',
                'contributors' => [
                    [
                        'isEditor' => true,
                        'ord' => 1,
                        'author' => 'Bloedel, P',
                    ]
                ]
            ],
            [
                'displayName' => 'Journal of Mammalogy',
                'type' => 'Journal',
            ],
            [
                'displayName' => 'Revista de Biologia Tropical',
                'type' => 'Journal',
            ],
            [
                'displayName' => 'Modalités de dissemination et d\'etablissement de lianes (Cyclanthaceae) et Philodendron) en forêt Guyanaise',
                'year' => '1997',
                'parentSource' => 'University of Paris VI',
                'type' => 'Thesis/Dissertation',
                'contributors' => [
                    [
                        'isEditor' => false,
                        'ord' => 1,
                        'author' => 'Cockle, Anya',
                    ]
                ]
            ],
        ];

        $sourceType = $manager->getRepository(SourceType::class)->findOneBy(['displayName' => 'Publication']);

        foreach ($publications as $data) {
            $publicationType = $manager->getRepository(PublicationType::class)->findOneBy(['displayName' => $data['type']]);

            $publication = new Publication();
            $publication->setDisplayName($data['displayName']);
            $publication->setPublicationType($publicationType);
            $publication->setCreatedBy($admin);

            $source = new Source();
            $source->setDisplayName($data['displayName']);
            $source->setPublication($publication);
            $source->setSourceType($sourceType);
            $source->setYear($data['year']);
            $source->setCreatedBy($admin);

            if ($data['parentSource']) {
                $parentSource = $manager->getRepository(Source::class)->findOneBy(['displayName' => $data['parentSource']]);
                $source->setParentSource($parentSource);
            }

            if ($data['contributors']) {
                $this->addContributors($manager, $source, $data['contributors']);
            }

            $manager->persist($source);
        }

        $manager->flush();
    }

    private function loadCitations(ObjectManager $manager, User $admin): void
    {
        $citations = [
            [
                'displayName' => 'Feeding habits',
                'description' => 'Gardner, A. L. 1977. Feeding habits. Biology of bats of the New World family Phyllostomatidae. Part II (R.J. Baker, J.K. Jones, Jr. & D.C. Carter, ed.), pp. 293-350, Special Publications, The Museum, Texas Tech Univ. Press.',
                'year' => '1977',
                'type' => 'Chapter',
                'parentSource' => 'Biology of bats of the New World family Phyllostomatidae',
                'contributors' => [
                    [
                        'isEditor' => false,
                        'ord' => 1,
                        'author' => 'Gardner, Alfred L',
                    ]
                ],
            ],
            [
                'displayName' => 'Observations on the life histories of Panama bats',
                'type' => 'Book',
                'description' => 'Bloedel, P. 1955. Observations on the life histories of Panama bats. Journal of Mammalogy 36: 232-235.',
                'year' => '1955',
                'parentSource' => 'Journal of Mammalogy',
                'contributors' => [
                    [
                        'isEditor' => false,
                        'ord' => 1,
                        'author' => 'Bloedel, P',
                    ]
                ]
            ],
            [
                'displayName' => 'Two cases of bat pollination in Central America',
                'type' => 'Article',
                'description' => 'Baker, H. G. 1970. Two cases of bat pollination in Central America. Revista de Biologia Tropical. 17: 187-197.',
                'year' => '1970',
                'parentSource' => 'Revista de Biologia Tropical',
                'contributors' => [
                    [
                        'isEditor' => false,
                        'ord' => 1,
                        'author' => 'Baker, Herbert G',
                    ]
                ]
            ],
            [
                'displayName' => 'Modalités de dissemination et d\'etablissement de lianes (Cyclanthaceae) et Philodendron) en forêt Guyanaise(citation)',
                'title' => 'Modalités de dissemination et d\'etablissement de lianes (Cyclanthaceae) et Philodendron) en forêt Guyanaise',
                'type' => 'Ph.D. Dissertation',
                'description' => 'Cockle, A. 1997. Modalités de dissemination et d\'etablissement de lianes (Cyclanthaceae) et Philodendron) en forêt Guyanaise. Ph.D. Dissertation. Université de Paris 6, Paris.',
                'year' => '1997',
                'parentSource' => 'Modalités de dissemination et d\'etablissement de lianes (Cyclanthaceae) et Philodendron) en forêt Guyanaise',
                'contributors' => [
                    [
                        'isEditor' => false,
                        'ord' => 1,
                        'author' => 'Cockle, Anya',
                    ]
                ]
            ]
        ];

        $sourceType = $manager->getRepository(SourceType::class)->findOneBy(['displayName' => 'Citation']);

        foreach ($citations as $data) {
            $citationType = $manager->getRepository(CitationType::class)->findOneBy(['displayName' => $data['type']]);

            $citation = new Citation();
            $citation->setDisplayName($data['displayName']);
            $citation->setTitle($data['title'] ?? $data['displayName']);
            $citation->setFullText('Will be automatically generated');
            $citation->setCitationType($citationType);
            $citation->setCreatedBy($admin);

            $source = new Source();
            $source->setDisplayName($data['displayName']);
            $source->setCitation($citation);
            $source->setSourceType($sourceType);
            $source->setYear($data['year']);
            $source->setCreatedBy($admin);

            if ($data['parentSource']) {
                $parentSource = $manager->getRepository(Source::class)->findOneBy(['displayName' => $data['parentSource']]);
                $source->setParentSource($parentSource);
            }

            if ($data['contributors']) {
                $this->addContributors($manager, $source, $data['contributors']);
            }

            $manager->persist($source);
        }

        $manager->flush();
    }

    private function addContributors($manager, $source, $contributors): void
    {
        $authData = [];
        foreach ($contributors as $contributor) {
            $author = $manager->getRepository(Author::class)->findOneBy(['displayName' => $contributor['author']]);
            $authData[] = [
                'authId' => $author->getId(),
                'isEditor' => $contributor['isEditor'],
                'ord' => $contributor['ord'],
            ];
        }

        $this->contributionEntry->setContributors($authData, $source);
    }

    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
            SourceTypeFixtures::class,
            CitationTypeFixtures::class,
            PublicationTypeFixtures::class,
        ];
    }

    public static function getGroups(): array
    {
        return self::FIXTURE_GROUPS;
    }
}
