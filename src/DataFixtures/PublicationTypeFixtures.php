<?php

namespace App\DataFixtures;

use App\Entity\PublicationType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class PublicationTypeFixtures extends Fixture implements FixtureGroupInterface
{
    const array FIXTURE_GROUPS = ['dev', 'test'];

    public function load(ObjectManager $manager): void
    {
        $types = [ 'Journal', 'Book', 'Thesis/Dissertation', 'Other' ];

        foreach ($types as $name) {
            $type = new PublicationType();
            $type->setDisplayName($name);
            $manager->persist($type);
        }

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return self::FIXTURE_GROUPS;
    }
}
