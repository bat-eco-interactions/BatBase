<?php

namespace App\DataFixtures;

use App\Entity\Interaction;
use App\Entity\InteractionType;
use App\Entity\Location;
use App\Entity\Source;
use App\Entity\Tag;
use App\Entity\Taxon;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class InteractionFixtures extends Fixture implements FixtureGroupInterface, DependentFixtureInterface
{
    const array FIXTURE_GROUPS = ['e2e'];

    /**
     * Interactions used in e2e testing
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager): void
    {
        $admin = $manager->getRepository(User::class)->findOneBy(['username' => 'TestAdmin']);

        $interactions = [
            [
                'source' => 'Feeding habits',
                'interactionType' => 'Host',
                'location' => 'Panama',
                'subject' => 'Micronycteris hirsuta',
                'object' => 'Family Sphingidae',
            ],
            [
                'source' => 'Feeding habits',
                'interactionType' => 'Consumption',
                'location' => 'Panama',
                'subject' => 'Micronycteris hirsuta',
                'object' => 'Genus Beilschmiedia',
                'tags' => ['Seed'],
            ],
            [
                'source' => 'Observations on the life histories of Panama bats',
                'interactionType' => 'Pollination',
                'location' => 'Summit Experimental Gardens',
                'subject' => 'Artibeus lituratus',
                'object' => 'Anacardium excelsum',
            ],
            [
                'source' => 'Feeding habits',
                'interactionType' => 'Transport',
                'location' => 'Costa Rica',
                'subject' => 'Micronycteris schmidtorum',
                'object' => 'Order Lepidoptera',
                'tags' => ['Bryophyte Fragment'],
            ],
            [
                'source' => 'Two cases of bat pollination in Central America',
                'interactionType' => 'Roost',
                'location' => 'Santa Ana-Forest',
                'subject' => 'Family Phyllostomidae',
                'object' => 'Mucuna andreana',
                'tags' => ['Internal'],
            ],
            [
                'source' => 'Modalités de dissemination et d\'etablissement de lianes (Cyclanthaceae) et Philodendron) en forêt Guyanaise(citation)',
                'interactionType' => 'Consumption',
                'location' => 'Les Nouragues',
                'subject' => 'Rhinophylla pumilio',
                'object' => 'Philodendron sphalerum',
                'tags' => ['Fruit'],
            ],
            [
                'source' => 'Modalités de dissemination et d\'etablissement de lianes (Cyclanthaceae) et Philodendron) en forêt Guyanaise(citation)',
                'interactionType' => 'Transport',
                'location' => 'Les Nouragues',
                'subject' => 'Rhinophylla pumilio',
                'object' => 'Order Lepidoptera',
                'tags' => ['Arthropod'],
            ],
            [
                'source' => 'Two cases of bat pollination in Central America',
                'interactionType' => 'Visitation',
                'location' => 'Santa Ana-Forest',
                'subject' => 'Family Phyllostomidae',
                'object' => 'Mucuna andreana',
                'tags' => ['Flower'],
            ],
            [
                'source' => 'Feeding habits',
                'interactionType' => 'Roost',
                'location' => 'Captivity',
                'subject' => 'Genus Micronycteris',
                'object' => 'Family Araceae',
                'tags' => ['External'],
            ],
            [
                'source' => 'Observations on the life histories of Panama bats',
                'interactionType' => 'Visitation',
                'location' => 'Unspecified',
                'subject' => 'Genus Rhinophylla',
                'object' => 'Family Araceae',
            ],
            [
                'source' => 'Observations on the life histories of Panama bats',
                'interactionType' => 'Host',
                'location' => 'Unspecified',
                'subject' => 'Genus Rhinophylla',
                'object' => 'Phylum Nematoda',
            ],
            [
                'source' => 'Observations on the life histories of Panama bats',
                'interactionType' => 'Host',
                'location' => 'Unspecified',
                'subject' => 'Genus Rhinophylla',
                'object' => 'Kingdom Protozoa',
            ],
            [
                'source' => 'Observations on the life histories of Panama bats',
                'interactionType' => 'Host',
                'location' => 'Unspecified',
                'subject' => 'Genus Rhinophylla',
                'object' => 'Class Striped',
            ],
            [
                'source' => 'Observations on the life histories of Panama bats',
                'interactionType' => 'Host',
                'location' => 'Unspecified',
                'subject' => 'Genus Rhinophylla',
                'object' => 'Class Spotted',
            ],
            [
                'source' => 'Observations on the life histories of Panama bats',
                'interactionType' => 'Host',
                'location' => 'Unspecified',
                'subject' => 'Genus Rhinophylla',
                'object' => 'Phylum Annelida',
            ],
            [
                'source' => 'Observations on the life histories of Panama bats',
                'interactionType' => 'Predation',
                'location' => 'Unspecified',
                'subject' => 'Order Chiroptera',
                'object' => 'Family Phyllostomidae',
            ],
        ];

        foreach ($interactions as $data) {
            $source = $manager->getRepository(Source::class)->findOneBy(['displayName' => $data['source']]);
            $interactionType = $manager->getRepository(InteractionType::class)->findOneBy(['displayName' => $data['interactionType']]);
            $location = $manager->getRepository(Location::class)->findOneBy(['displayName' => $data['location']]);
            $subject = $manager->getRepository(Taxon::class)->findOneBy(['displayName' => $data['subject']]);
            $object = $manager->getRepository(Taxon::class)->findOneBy(['displayName' => $data['object']]);

            if (empty($source) || empty($interactionType) || empty($location) || empty($subject) || empty($object)) {
                dump($data);
            }

            $interaction = new Interaction();
            $interaction->setSource($source);
            $interaction->setInteractionType($interactionType);
            $interaction->setLocation($location);
            $interaction->setSubject($subject);
            $interaction->setObject($object);
            $interaction->setUpdated(new \DateTime('-3 days', new \DateTimeZone('UTC')));
            $interaction->setCreatedBy($admin);

            if (!empty($data['tags'])) {
                foreach ($data['tags'] as $tagName) {
                    $tag = $manager->getRepository(Tag::class)->findOneBy(['displayName' => $tagName]);
                    $interaction->addTag($tag);
                }
            }

            $manager->persist($interaction);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
            InteractionTypeFixtures::class,
            TagFixtures::class,
            SourceFixtures::class,
            TaxonChildrenFixtures::class,
            LocationFixtures::class
        ];
    }

    public static function getGroups(): array
    {
        return self::FIXTURE_GROUPS;
    }
}
