<?php

namespace App\DataFixtures;

use App\Entity\InteractionType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class InteractionTypeFixtures extends Fixture implements FixtureGroupInterface
{
    const array FIXTURE_GROUPS = ['dev', 'test'];

    public function load(ObjectManager $manager): void
    {
        $types = [
            [
                'displayName' => 'Visitation',
                'activeForm' => 'visited',
            ],
            [
                'displayName' => 'Consumption',
                'activeForm' => 'consumed',
            ],
            [
                'displayName' => 'Seed Dispersal',
                'activeForm' => 'dispersed seeds of',
            ],
            [
                'displayName' => 'Pollination',
                'activeForm' => 'pollinated',
            ],
            [
                'displayName' => 'Transport',
                'activeForm' => 'transported',
            ],
            [
                'displayName' => 'Cohabitation',
                'activeForm' => 'cohabitated with',
            ],
            [
                'displayName' => 'Host',
                'activeForm' => 'host of',
            ],
            [
                'displayName' => 'Roost',
                'activeForm' => 'roosted in',
            ],
            [
                'displayName' => 'Prey',
                'activeForm' => 'preyed upon by',
            ],
            [
                'displayName' => 'Hematophagy',
                'activeForm' => 'fed on the blood of',
            ],
            [
                'displayName' => 'Predation',
                'activeForm' => 'preyed upon',
            ],
        ];

        foreach ($types as $data) {
            $type = new InteractionType();
            $type->setDisplayName($data['displayName']);
            $type->setActiveForm($data['activeForm']);
            $manager->persist($type);
        }

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return self::FIXTURE_GROUPS;
    }
}
