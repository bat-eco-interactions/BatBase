<?php

namespace App\Service\Utility;

use Psr\Log\LoggerInterface;

/**
 * public:
 *     logError
 */
class LogError
{
    public function __construct(private readonly LoggerInterface $logger)
    {
    }

    /**
     * Serializes entity records. Clears $em memory after each batch of 3000 records.
     */
    public function logError($e): void
    {
        $lineNum = $e->getLine();
        $errMsg = $e->getMessage();
        $trace = $e->getTraceAsString();
        $this->handleLogging($lineNum, $errMsg, $trace);
    }

    /**
     * Logs the error with the line number, error message, and stack track. In
     * DEV environment, the log message is printed as well.
     *
     * @param number $lineNum Error line number
     * @param string $msg     Error message
     * @param string $trace   Error trace-log
     */
    private function handleLogging($lineNum, $msg, $trace): void
    {
        $logMsg = "### Error @ [$lineNum] = $msg\n$trace\n";
        $this->logger->error($logMsg);
        if (getenv('env') === 'prod') {
            return;
        }

        dd($logMsg);
    }
}
