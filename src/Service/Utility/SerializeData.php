<?php

namespace App\Service\Utility;

use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\Mapping\MappingException;
use Symfony\Component\Serializer\Context\Normalizer\ObjectNormalizerContextBuilder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

readonly class SerializeData
{
    /**
     * @param SerializerInterface $serializer
     * @param LogError $logger
     */
    public function __construct(
        private SerializerInterface $serializer,
        private LogError            $logger)
    {
    }

    public function serialize(object $object, string $format = 'json', array $context = []): string
    {
        return $this->serializer->serialize($object, $format, $context);
    }

    /**
     * Serializes entity records. Clears $em memory after each batch of 3000 records.
     * @param array $entities
     * @param EntityManager $em
     * @param 'normal'|'flat' $group serializer group
     *
     * @return object serialized entity records { id: <serialized entity record> }
     * @throws MappingException
     */
    public function serializeRecords(array $entities, EntityManager &$em, string $group = 'normal'): \stdClass
    {
        $data = new \stdClass();
        $count = 0;
        foreach ($entities as $entity) {
            $id = $entity->getId();
            $jsonData = $this->serializeRecord($entity, $group);
            if ($jsonData) {
                $data->$id = $jsonData;
            }
            if ($count < 3000) {
                ++$count;
            } else {
                $em->clear();
                $count = 0;
            }
        }
        $em->clear();

        return $data;
    }

    /**
     * Serializes the entity record. Logs any errors.
     *
     * @param object $entity symfony entity record
     * @param 'normal'|'flat' $group serializer group
     *
     * @return object JSON record
     */
    public function serializeRecord(object $entity, string $group = 'normal'): string
    {
        $rcrd = false;
        try {
            $rcrd = $this->serializer->serialize($entity, 'json', $this->getContext($group));
        } catch (\Throwable|\Exception $e) {
            $this->logger->logError($e);
        }

        return $rcrd;
    }

    private function getContext($group): array
    {
        $config = [AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function (object $object, string $format, array $context): string {
            return $object->getName();
        }
        ];
        $groups = (new ObjectNormalizerContextBuilder())
            ->withGroups($group)
            ->toArray();
        return $group === 'flat' ? array_merge($config, $groups) : $groups;
    }
}
