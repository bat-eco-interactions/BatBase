<?php

namespace App\Service\Utility;

use Psr\Cache\InvalidArgumentException;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

class ProcessCacheManager
{
    public function __construct(
        private CacheInterface    $cache,
        private readonly LogError $logger
    )
    {
    }

    /**
     * Note: Recompute values of 1.0 and higher values mean earlier recompute. Set
     * to 0 to disable early recompute and set to INF to force immediate recompute.
     * @link https://symfony.com/doc/6.4/components/cache.html#cache-contracts
     * @param string $key
     * @param callable $process
     * @param integer $expiration
     * @param float $recompute
     * @return mixed
     */
    public function get(string $key, callable $process, int $expiration = 3600, float $recompute = 1.0): mixed
    {
        try {
            return $this->cache->get($key, function(ItemInterface $item) use ($process, $expiration, $key) {
                $item->expiresAfter($expiration);
                return $process();
            }, $recompute);
        } catch (InvalidArgumentException $e) {
            $this->logger->logError($e);
            return $process();
        }
    }

    public function delete(string $key): void
    {
        try {
            $this->cache->delete($key);
        } catch (InvalidArgumentException $e) {
            $this->logger->logError($e);
        }
    }
}
