<?php

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\SecurityBundle\Security;

class UserActivity
{
    public function __construct(private EntityManagerInterface $em, private Security $security)
    {
    }

    /* ======================== TRACK TIME-UPDATED ============================== */
    public function track(): void
    {
        // returns User object or null if not authenticated
        $user = $this->security->getUser();

        if (($user instanceof User) && !$user->isActiveNow()) {
            $user->setLastActivityAt(new \DateTime('now', new \DateTimeZone('UTC')));
            $this->em->persist($user);
            $this->em->flush();
        }
    }
}
