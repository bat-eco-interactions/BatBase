<?php

namespace App\Service\DataEntry;

use App\Entity\Tag;
use Doctrine\ORM\EntityManagerInterface;

readonly class TagEntry
{
    public function __construct(private EntityManagerInterface $em)
    {
    }

    /**
     * Handles adding and removing tags from the entity.
     * @param $curTags array<int>
     * @param $entity object
     * @return void
     */
    public function handleTags(array $curTags, object &$entity): void
    {
        $prevTags = $entity->getTagIds();
        $this->handleRemoved($prevTags, $curTags, $entity);
        $this->handleAdded($prevTags, $curTags, $entity);
    }

    private function handleRemoved($prevTags, $curTags, $entity): void
    {
        foreach ($prevTags as $id) {
            if (in_array($id, $curTags)) {
                continue;
            }
            $tag = $this->em->find(Tag::class, $id);
            $entity->removeTag($tag);
        }
    }

    private function handleAdded($prevTags, $curTags, $entity): void
    {
        foreach ($curTags as $id) {
            if (in_array($id, $prevTags)) {
                continue;
            }
            $tag = $this->em->find(Tag::class, $id);
            $entity->addTag($tag);
        }
    }
}