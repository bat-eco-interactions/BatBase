<?php

namespace App\Service\DataEntry;

use App\Entity\Contribution;
use App\Entity\Interaction;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\SecurityBundle\Security;

/**
 * public
 *     processEntity.
 *
 * TOC
 *     DETAIL-ENTITY
 *     FLAT DATA
 *     RELATIONAL DATA
 *         SPECIALIZED
 *             CONTRIBUTIONS
 *             TAGS
 *             INTERACTION SOURCE
 *         GENERAL
 *     TRACK REVIEWED
 */
class ProcessEntry
{
    public function __construct(private readonly EntityManagerInterface $em, private readonly EntryUtility $util, private readonly Security $security)
    {
    }

    public function processEntity($data, &$coreEntity, &$returnData)
    {
        try {
            $this->populateEntity($returnData, $coreEntity, $data);
            $this->ifDataManagerTrackReview($returnData);
        } catch (\Exception $e) {
            $returnData->error = $e->getMessage();
        }

        return $returnData;
    }

    private function populateEntity(&$returnData, &$coreEntity, $data): void
    {
        $this->setEntityData($data->core, $coreEntity, $returnData, 'core');
        $this->addEntityName($returnData, $coreEntity);
        $this->setDetailEntity($returnData, $data);
    }

    /* =================== DETAIL-ENTITY ==================================== */
    /**
     * Sets all detail-entity data and returns the entity.
     * Note: Publishers are the only 'sourceType' with no detail-entity.
     */
    private function handleDetailEntity($cData, $data, &$returnData)
    {
        $dName = property_exists($cData->rel, 'SourceType') ?
            lcfirst($cData->rel->SourceType) : 'geoJson';                       // print('detail name = '.$dName);
        $returnData->detail = $dName;
        if (!property_exists($data, 'detail')) {
            return false;
        }
        // if (!property_exists($cData, 'hasDetail')) { return false; }
        $dData = $data->detail;

        return $this->setDetailData($dData, $dName, $returnData);
    }

    private function setDetailData($dData, $dName, &$returnData)
    {
        $dEntity = $this->getDetailEntity($dName, $returnData->detailEdits);
        if ($dName !== 'geoJson') {
            $this->setCoreEntity($returnData->core, $returnData->coreEntity, $dEntity);
        }
        $this->addDetailToCoreEntity($returnData->coreEntity, $dEntity, $dName);
        $this->setEntityData($dData, $dEntity, $returnData, 'detail');

        return $dEntity;
    }

    private function setCoreEntity($coreName, &$coreEntity, &$dEntity): void
    {
        $setCore = 'set' . ucfirst((string)$coreName);
        $dEntity->$setCore($coreEntity);
    }

    /** Returns either a newly created entity or an existing entity to edit. */
    private function getDetailEntity($dName, $edits)
    {
        if ($edits->editing) {
            $curDetail = $this->util->getEntity(ucfirst((string)$dName), $edits->editing);
            if ($curDetail) {
                return $curDetail;
            }
        }
        $dClass = 'App\\Entity\\' . ucfirst((string)$dName);

        return new $dClass();
    }

    /** Adds the detail entity to the core entity. Eg, A Publication to it's Source record. */
    private function addDetailToCoreEntity(&$coreEntity, &$dEntity, $dName): void
    {
        $setMethod = 'set' . ucfirst((string)$dName);
        $coreEntity->$setMethod($dEntity);
        $this->em->persist($coreEntity);
    }

    /**
     * Calls the set method for both types of entity data, flat and relational,
     * and persists the entity.
     */
    private function setEntityData($data, &$entity, &$returnData, $group): void
    {
        $groupEdits = $group . 'Edits';
        $edits = $returnData->$groupEdits;

        $this->setFlatData($data->flat, $entity, $edits);
        $this->setRelatedEntityData($data->rel, $entity, $edits);
        $this->em->persist($entity);
    }

    /* ============================ FLAT DATA =============================== */
    /** Sets all scalar data. */
    private function setFlatData($data, &$entity, &$edits): void
    {
        foreach ($data as $field => $val) {
            $this->setFlatDataAndTrackEdits($entity, $field, $val, $edits);
        }
    }

    /**
     * Checks whether current value is equal to the passed value. If not, the
     * entity is updated with the new value and the field is added to the edits obj.
     */
    private function setFlatDataAndTrackEdits(&$entity, $field, $newVal, &$edits): void
    {
        $setField = 'set' . $field;  // CamelCase
        $getField = 'get' . $field;

        if ($edits->editing) {
            $curVal = $entity->$getField();
            if ($curVal === $newVal) {
                return;
            }
            $edits->$field = ['old' => $curVal, 'new' => $newVal];
        }
        $entity->$setField($newVal);
    }

    /* ========================= RELATIONAL DATA ============================ */
    /** Sets all realtional data. */
    private function setRelatedEntityData($data, &$entity, &$edits): void
    {
        $edgeCases = [
            'Contributor' => function ($ary) use (&$entity, &$edits) {
                $this->handleContributors($ary, $entity, $edits);
            },
            'Tags' => function ($ary) use (&$entity, &$edits) {
                $this->handleTags($ary, $entity, $edits);
            },
            'Source' => function ($id) use (&$entity, &$edits) {
                $this->addInteractionToSource($id, $entity, $edits);
            },
        ];
        foreach ($data as $name => $val) {                                      // print("field [$name] type of [$val][".gettype($val)."]\n");
            if (array_key_exists($name, $edgeCases)) {
                call_user_func($edgeCases[$name], $val);
            } else {
                $relEntity = $this->util->getEntity($name, $val);
                $this->setRelDataAndTrackEdits($entity, $name, $relEntity, $edits);
            }
        }
    }

    /* ---------------------- SPECIALIZED --------------------------------------- */
    /* __________________________ CONTRIBUTIONS _________________________________ */
    private function handleContributors($authData, &$pubSrc, &$edits): void
    {                                                                           // print('  handleContributors ');dump($authData);
        $orgContribs = $pubSrc->getContributorData();
        $this->handleCurrentContributors($authData, $orgContribs, $pubSrc, $edits);
        if (!count($orgContribs)) {
            return;
        } // No ordinals removed
        $this->handleRemovedContributors($orgContribs, $pubSrc, $edits);
    }

    /* ---------- TRACK ------------- */
    /** Tracks changes to a publication/citation contributor, stored by ordinal. */
    private function addContribEdit($ord, $authorUpdate, &$edits): void
    {                                                                          // print("\naddContribEdit ord[$ord] ");print_r($authorUpdate);
        if (!property_exists($edits, 'Contributor')) {
            $edits->Contributor = [];
        }
        $edit = ['authId' => $authorUpdate, 'ord' => $ord];
        array_push($edits->Contributor, $edit);
    }

    /* --------------------- CURRENT ------------------------------ */
    private function handleCurrentContributors($authData, &$orgContribs, &$pubSrc, &$edits): void
    {
        foreach ($authData as $id => $data) {
            $ordContrib = $this->getCurContrib($data->ord, $orgContribs);
            if ($ordContrib) {
                $this->updateContribution($ordContrib, $data, $edits);
                unset($orgContribs[$ordContrib['authId']]);  // Will not be removed
            } else {
                $this->createContribution($data, $pubSrc, $edits);
            }
        }
    }

    private function getCurContrib($ord, $orgContribs)
    {
        foreach ($orgContribs as $id => $data) {
            if ($data['ord'] == $ord) {
                return $data;
            }
        }

        return null;
    }

    /* ---------- UPDATE ----------- */
    /** Updates changes to the auth/editor and the editor status. */
    private function updateContribution($ordData, $newData, &$edits): void
    {
        $contrib = $this->util->getEntity('Contribution', $ordData['contribId']);
        $this->updateEditorStatus($ordData, $newData, $contrib);
        $authorUpdate = $this->updateAuthorSrc($ordData, $newData, $contrib);
        if (!$authorUpdate) {
            return;
        } // Only changes to the author source are tracked
        $this->addContribEdit($ordData['ord'], $authorUpdate, $edits);
        $this->em->persist($contrib);
    }

    private function updateEditorStatus($orgData, $newData, &$contrib): void
    {
        $curIsEd = $orgData['isEditor'];
        $newIsEd = $newData->isEditor;
        if ($curIsEd == $newIsEd) {
            return;
        }
        $contrib->setIsEditor($newIsEd);
    }

    private function updateAuthorSrc($orgData, $newData, &$contrib)
    {
        $orgAuthId = $orgData['authId'];
        $newAuthId = $newData->authId;
        if ($orgAuthId == $newAuthId) {
            return false;
        }
        $contrib->setAuthorSource($this->util->getEntity('source', $newAuthId));  // TODO: SHOULD AUTO ADD TO AUTHOR CONTRIBUTIONS AND REMOVE FROM OLD AUTHOR CONTRIBS

        return ['old' => $orgAuthId, 'new' => $newAuthId];
    }

    /* ---------- CREATE ----------- */
    private function createContribution($data, &$pubSrc, &$edits): void
    {
        $this->addContrib($data, $pubSrc);
        $track = ['new' => $data->authId, 'old' => false];
        $this->addContribEdit($data->ord, $track, $edits);
    }

    private function addContrib($data, &$pubSrc): void
    {
        $authSrc = $this->util->getEntity('Source', $data->authId);
        $contribEntity = $this->createContrib($pubSrc, $authSrc, $data);
        $pubSrc->addContributor($contribEntity);  // $pubSrc persisted later
        $authSrc->addContribution($contribEntity);
        $this->em->persist($authSrc);
    }

    private function createContrib($pubSrc, $authSrc, $data)
    {
        $entity = new Contribution();
        $entity->setWorkSource($pubSrc);
        $entity->setAuthorSource($authSrc);
        $entity->setIsEditor($data->isEditor);
        $entity->setOrd($data->ord);

        $this->em->persist($entity);

        return $entity;
    }

    /* ---------- REMOVE ----------- */
    /** Removes any of the current contributors that are not in the new $authObj. */
    private function handleRemovedContributors($orgContribs, &$pubSrc, &$edits): void
    {
        foreach ($orgContribs as $id => $data) {
            $contrib = $this->util->getEntity('Contribution', $data['contribId']);
            $pubSrc->removeContributor($contrib);
            $authSrc = $contrib->getAuthorSource();
            $authSrc->removeContribution($contrib);                             // print("\ncontrib removed\n");
            $track = ['new' => false, 'old' => $data['authId']];
            $this->addContribEdit($data['ord'], $track, $edits);
            $this->em->persist($authSrc);
            $this->em->remove($contrib);
        }
    }

    /* ___________________________ TAGS _________________________________________ */
    /** Handles adding and removing tags from the entity. */
    private function handleTags($ary, &$entity, &$edits): void
    {
        $curTags = $entity->getTagIds();
        $this->removeFromCollection('Tag', $curTags, $ary, $entity, $edits);
        $this->addToCollection('Tag', $curTags, $ary, $entity, $edits);
        // TODO: Refactor below eventually
        if (!property_exists($edits, 'Tag')) {
            return;
        }
        $edits->Tags = $edits->Tag;
        unset($edits->Tag);
    }

    /** Removes any entities currently in the $coll(ection) that are not in $ary.  */
    private function removeFromCollection($field, $coll, $ary, &$entity, &$edits): void
    {
        $removed = [];
        $removeField = 'remove' . $field;
        foreach ($coll as $id) {
            if (in_array($id, $ary)) {
                continue;
            }
            $collEnt = $this->util->getEntity($field, $id);
            $entity->$removeField($collEnt);
            array_push($removed, $id);
        }
        if (count($removed)) {
            $edits->$field = ['old' => $removed];
        }
    }

    /** Adds each new entity in ary to the collection.  */
    private function addToCollection($field, $coll, $ary, &$entity, &$edits): void
    {
        $added = [];
        $addField = 'add' . $field;
        foreach ($ary as $id) {
            if (in_array($id, $coll)) {
                continue;
            }
            array_push($added, intval($id));
            $collEnt = $this->util->getEntity($field, $id);
            $entity->$addField($collEnt);
        }
        if ($edits->editing && count($added)) {
            $edits->$field = property_exists($edits, $field) ?
                array_merge($edits->$field, ['new' => $added]) : ['new' => $added];
        }
    }

    /* _______________________ INTERACTION SOURCE _______________________________ */
    /** If adding an interaction to a source, ensures it's 'isDirect' flag to true. */
    private function addInteractionToSource($id, $entity, &$edits): void
    {
        $relEntity = $this->util->getEntity('Source', $id);
        $this->setRelDataAndTrackEdits($entity, 'Source', $relEntity, $edits);
        $className = $this->em->getClassMetadata($entity::class)->getName();
        if ($className === Interaction::class && !$relEntity->getIsDirect()) {
            $relEntity->setIsDirect(true);
            $this->em->persist($relEntity);
        }
    }

    /* --------------------------- GENERAL -------------------------------------- */
    /**
     * Checks whether current value is equal to the passed value. If not, the
     * entity is updated with the new value and the field is added to the edits obj.
     */
    private function setRelDataAndTrackEdits(&$entity, $field, $newVal, &$edits): void
    {
        $setField = 'set' . $field;
        $getField = 'get' . $field;

        $prevEntity = null;
        if ($edits->editing) {
            $prevEntity = $entity->$getField();

            if ($newVal === null) {
                if ($prevEntity === null) { // no change
                    return;
                }
            } elseif ($prevEntity?->getId() === $newVal->getId()) {  // no change
                return;
            }

            $edits->$field = ['old' => $prevEntity?->getId(), 'new' => $newVal?->getId()];
        }

        $entity->$setField($newVal);

        if ($prevEntity !== null) {
            $this->em->persist($prevEntity);
        }
    }

    /* ========================= TRACK REVIEWED ================================= */
    private function ifDataManagerTrackReview(&$returnData): void
    {
        if (!$this->security->isGranted('ROLE_MANAGER')) {
            return;
        }
        $user = $this->security->getUser();
        $this->setEntityReviewed($returnData->coreEntity, $user);
        $returnData->coreEdits->reviewed = ['new' => true];
    }

    private function setEntityReviewed(&$entity, $user): void
    {
        $entity->setReviewed(new \DateTime('now', new \DateTimeZone('UTC')));
        $entity->setReviewedBy($user);
        $this->em->persist($entity);
    }

    /* ============================ HELPERS ===================================== */
    private function addEntityName(&$returnData, $coreEntity): void
    {
        if (!method_exists($coreEntity, 'getDisplayName')) {
            return;
        }
        $returnData->name = $coreEntity->getDisplayName();
    }

    private function setDetailEntity(&$returnData, $data): void
    {
        if (!property_exists($data, 'detailEntity')) {
            return;
        }
        $d = $this->handleDetailEntity($data->core, $data, $returnData);
        $returnData->detailEntity = $d;
    }
}
