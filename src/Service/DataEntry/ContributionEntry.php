<?php

namespace App\Service\DataEntry;

use App\Entity\Contribution;
use Doctrine\ORM\EntityManagerInterface;

readonly class ContributionEntry
{
    public function __construct(
        private EntityManagerInterface $em,
        private EntryUtility           $util)
    {
    }

    public function setContributors($authData, $workSrc): void
    {                                                                          // dd($authData);
        $orgContribs = $workSrc->getContributorData();
        $this->handleCurrentContributors($authData, $orgContribs, $workSrc);
        if (!count($orgContribs)) { // No ordinals removed
            return;
        }
        $this->handleRemovedContributors($orgContribs, $workSrc);
    }

    /* --------------------- CURRENT ------------------------------ */
    private function handleCurrentContributors($authData, &$orgContribs, $workSrc): void
    {
        foreach ($authData as $id => $data) {
            $ordContrib = $this->getCurContrib($data['ord'], $orgContribs);
            if ($ordContrib) {
                $this->updateContribution($ordContrib, $data);
                unset($orgContribs[$ordContrib['authId']]);  // Will not be removed
            } else {
                $this->createContribution($data, $workSrc);
            }
        }
    }

    private function getCurContrib($ord, $orgContribs)
    {
        foreach ($orgContribs as $id => $data) {
            if ($data['ord'] == $ord) {
                return $data;
            }
        }

        return null;
    }

    /* ---------- UPDATE ----------- */
    /** Updates changes to the auth/editor and the editor status. */
    private function updateContribution($ordData, $newData): void
    {
        $contrib = $this->util->getEntity('Contribution', $ordData['contribId']);
        $this->updateEditorStatus($ordData, $newData, $contrib);
        $authorUpdate = $this->updateAuthorSrc($ordData, $newData, $contrib);
        if (!$authorUpdate) { // Only changes to the author source are tracked
            return;
        }
        $this->em->persist($contrib);
    }

    private function updateEditorStatus($orgData, $newData, $contrib): void
    {
        $curIsEd = $orgData['isEditor'];
        $newIsEd = $newData['isEditor'] ?? false;
        if ($curIsEd == $newIsEd) {
            return;
        }
        $contrib->setIsEditor($newIsEd);
    }

    private function updateAuthorSrc($orgData, $newData, Contribution $contrib): false|array
    {
        $orgAuthId = $orgData['authId'];
        $newAuthId = $newData['authId'];
        if ($orgAuthId == $newAuthId) {
            return false;
        }
        $contrib->setAuthorSource($this->util->getEntity('source', $newAuthId));  // TODO: SHOULD AUTO ADD TO AUTHOR CONTRIBUTIONS AND REMOVE FROM OLD AUTHOR CONTRIBS

        return ['old' => $orgAuthId, 'new' => $newAuthId];
    }

    /* ---------- CREATE ----------- */
    private function createContribution($data, $workSrc): void
    {
        $author = $this->util->getEntity('Author', $data['authId']);
        $authSrc = $author->getSource();
        $contribEntity = $this->createContrib($workSrc, $authSrc, $data);
        $workSrc->addContributor($contribEntity);  // $workSrc persisted later
        $authSrc->addContribution($contribEntity);
        $this->em->persist($authSrc);
    }

    private function createContrib($workSrc, $authSrc, $data): Contribution
    {
        $entity = new Contribution();
        $entity->setWorkSource($workSrc);
        $entity->setAuthorSource($authSrc);
        $entity->setIsEditor($data['isEditor'] ?? false);
        $entity->setOrd($data['ord']);

        $this->em->persist($entity);

        return $entity;
    }

    /* ---------- REMOVE ----------- */
    /** Removes any of the current contributors that are not in the new $authObj. */
    private function handleRemovedContributors($orgContribs, $workSrc): void
    {
        foreach ($orgContribs as $id => $data) {
            $contrib = $this->util->getEntity('Contribution', $data['contribId']);
            $workSrc->removeContributor($contrib);
            $authSrc = $contrib->getAuthorSource();
            $authSrc->removeContribution($contrib);

            $this->em->persist($authSrc);
            $this->em->remove($contrib);
        }
    }

}
