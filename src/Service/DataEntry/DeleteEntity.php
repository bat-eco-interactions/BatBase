<?php

namespace App\Service\DataEntry;

use Doctrine\ORM\EntityManagerInterface;

/**
 * public
 *     delete.
 *
 * TOC
 *     CHECK FOR DEPENDENTS
 *     HANDLE DELETE
 */
class DeleteEntity
{
    public function __construct(private readonly EntityManagerInterface $em, private readonly EntryUtility $util)
    {
    }

    /* ============================ DELETE ===================================== */
    /**
     * Flags entity for delete. Interactions are soft-deleted and all others have
     * "[DELETED]" added to their name and are automatically hidden from the user.
     */
    public function deleteEntity($coreName, $formData)
    {
        $coreEntity = $this->util->getEntity(ucfirst((string) $coreName), $formData->ids->core);
        $returnData = $this->util->buildReturnDataObj($coreName, $coreEntity);

        if ($this->hasDependentData($coreName, $coreEntity)) {
            $this->handleUnableToDelete($returnData);
        } else {
            $this->flagEntityForDelete($coreName, $coreEntity);
            $this->util->attemptFlushAndLogErrors($returnData);
        }

        return $returnData;
    }

    /* _______________________ CHECK FOR DEPENDENTS _____________________________ */
    private function hasDependentData($classname, $entity)
    {
        if ($classname === 'interaction') {
            return false;
        }
        $getDependentData = $classname.'GetDependentData';

        foreach ($this->$getDependentData($entity) as $type => $entities) {
            if ($this->isDependent($type, $entities)) {
                return true;
            }
        }
    }

    private function locationGetDependentData($entity)
    {
        return [
            'int' => $entity->getInteractions(),
            'sub' => $entity->getChildLocs(),
        ];
    }

    private function sourceGetDependentData($entity)
    {
        return [
            'int' => $entity->getInteractions(),
            'sub' => $entity->getChildSources(),
        ];
    }

    private function taxonGetDependentData($entity)
    {
        return [
            'int' => $entity->getInteractions(),
            'sub' => $entity->getChildTaxa(),
        ];
    }

    private function isDependent($type, $entities)
    {
        $dependents = [];

        foreach ($entities as $key => $entity) {
            if ($this->isDeletedInt($type, $entity)) {
                continue;
            }
            if ($this->isDeletedChild($type, $entity)) {
                continue;
            }
            array_push($dependents, $entity->getId());
        }

        return count($dependents);
    }

    private function isDeletedInt($type, $entity)
    {
        return $type === 'int' ? strpos((string) $entity->getNote(), 'DELETE') : false;
    }

    private function isDeletedChild($type, $entity)
    {
        return $type === 'sub' ? strpos((string) $entity->getDisplayName(), 'DELETE') : false;
    }
    /* _______________________ HANDLE DELETE ____________________________________ */

    private function handleUnableToDelete($returnData): void
    {
        $returnData->fail = [
            'unableToDelete' => true,
        ];
    }

    private function flagEntityForDelete($coreName, $coreEntity): void
    {
        if ($coreName === 'interaction') {
            $coreEntity->setNote($coreEntity->getNote().'[INTERACTION DELETED]');
        } else {
            $coreEntity->setDisplayName($coreEntity->getDisplayName().'[DELETED]');
        }
        $this->em->persist($coreEntity);
    }
}
