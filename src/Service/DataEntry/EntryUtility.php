<?php

namespace App\Service\DataEntry;

use App\Entity\User;
use App\Service\Utility\LogError;
use App\Service\Utility\SerializeData;
use App\Service\UserActivity;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * public
 *     attemptFlushAndLogErrors
 *     buildReturnDataObj
 *     getEntities
 *     getEntity
 *     getRepo
 *     hasErrors
 *     serializeDataResults.
 *
 * TOC
 *     FORENSIC DATA
 *     FLUSH DATA
 *         LOG ERRORS
 *         SERIALIZE DATA-RESULTS
 *         TRACK ENTITY-UPDATE
 *         VALIDATE ENTITY
 *     HELPERS
 *         GET ENTITY
 *         RETURN-DATA BUILDER
 */
readonly class EntryUtility
{
    public function __construct(
        private EntityManagerInterface $em,
        private LogError $logger,
        private SerializeData $serialize,
        private UserActivity $userActivity,
        private ValidatorInterface $validator
    )
    {
    }
    /* ======================= FORENSIC DATA ================================ */

    /**
     * Returns the forensics of the entity to be displayed in the form.
     * @param object $entity
     * @return array
     */
    public function getEntityForensics(object $entity): array
    {
        $createdUser = $this->getUserForensics($entity->getCreatedBy(), 'created');
        $updatedUser = $this->getUserForensics($entity->getUpdatedBy(), 'updated');
        $record = $this->getRecordForensics($entity);

        return array_merge($createdUser, $updatedUser, $record);
    }

    private function getUserForensics(User|null $user, string $action): array
    {
        if (!$user) {
            return [];
        }

        return [
            $action.'By_email' => $user->getEmail(),
            $action.'By_name' => $user->getFullName(),
            $action.'By_username' => $user->getUsername(),
        ];
    }

    private function getRecordForensics($entity): array
    {
        $forensics = ['created' => date_format($entity->getCreated(), 'd M Y')];
        $forensics['updated'] = $this->addDate('updated', $entity);
        $forensics['reviewed'] = $this->addDate('reviewed', $entity);

        return $forensics;
    }

    private function addDate($type, $entity): string
    {
        $getter = 'get'.ucfirst((string) $type);
        $isAvailable = $type === 'reviewed' ? $getter : $getter.'By';

        return $entity->$isAvailable() ? date_format($entity->$getter(), 'd M Y') : '----';
    }
    /* =========================== FLUSH DATA =================================== */
    /**
     * Handles the flush process: If no errors, entity updatedAt and user activity
     * times are tracked. Errors caught during the flush process are logged.
     *
     * @param object $flushData
     *                      ->coreEntity   Entity will be validated before flush. Required.
     * @param bool $isReviewEntry To track the updated ReviewEntry
     */
    public function attemptFlushAndLogErrors(object $flushData, bool $isReviewEntry = false): void
    {                                                                           // dump($flushData); die();
        if ($this->hasErrors($flushData)) {
            return;
        }
        try {
            $validated = $this->validateEntityData($flushData);
            if (!$validated) {
                return;
            }
            $this->em->flush();
            $this->setUpdatedAtTimes($flushData, $isReviewEntry);
            // } catch (\DBALException $e) {
            //     return $this->logError($flushData, $e);
        } catch (\Exception $e) {
            $this->logError($flushData, $e);
        }
    }

    public function hasErrors($flushData): bool
    {
        return property_exists($flushData, 'error') || property_exists($flushData, 'fail');
    }

    /* -------------------- VALIDATE ENTITY ------------------------------------- */
    /**
     * Validates the data against the model. Adds all violation messages to the
     * return data object. Returns true if violations are present and prevents flush.
     * Validation errors are added to the return data.
     */
    private function validateEntityData($flushData): bool
    {
        $msgs = [];
        $this->addValErrMsgs($msgs, $this->validateEntity($flushData->coreEntity));
        if (!count($msgs) && property_exists($flushData, 'detailEntity')) {
            $this->addValErrMsgs($msgs, $this->validateEntity($flushData->detailEntity));
        }
        if (!count($msgs)) {
            return true;
        }
        $flushData->fail = $msgs;
        $this->em->clear();

        return false;
    }

    private function validateEntity($entity): ConstraintViolationListInterface|false
    {
        $errors = $this->validator->validate($entity);
        return count($errors) > 0 ? $errors : false;
    }

    private function addValErrMsgs(&$msgs, $errors): void
    {
        if (!$errors) {
            return;
        }

        foreach ($errors as $violation) {
            $msgs = array_merge(
                $msgs,
                $this->getViolationInfo($violation)
            );
        }
    }

    /** Currently only really handles UniqueEntity constraints. */
    private function getViolationInfo($violation): array
    {
        if (str_contains((string) $violation->getMessage(), 'Duplicate')) {
            return [$violation->getMessage() => $violation->getCause()[0]->getId()];
        } else {
            return [$violation->getMessage() => $violation->getInvalidValue()];
        }
    }

    /* ----------------------- LOG ERRORS --------------------------------------- */
    /** Logs the error message and returns an error response message. */
    private function logError($flushData, $e): void
    {
        if ($this->ifDuplicateEntity($e)) {
            return;
        }   // handled by validator above...
        $this->logger->logError($e);
        $flushData->error = is_string($e) ? $e : $e->getMessage();
    }

    private function ifDuplicateEntity($e): bool
    {
        return str_contains((string) $e->getMessage(), 'Duplicate entry')
            || str_contains((string) $e->getTraceAsString(), 'Duplicate entry');
    }

    /* --------------------- TRACK ENTITY-UPDATE -------------------------------- */
    /**
     * Sets the updatedAt timestamp for modified entities. This will be used to
     * ensure local data stays updated with any changes.
     */
    private function setUpdatedAtTimes($flushData, $isReviewEntry): void
    {
        $this->userActivity->track();
        if ($isReviewEntry) {
            $this->getRepo('SystemDate')->trackUpdate('ReviewEntry');
        }
        if (!property_exists($flushData, 'core')) { // flushing ReviewEntry only
            return;
        }
        if (property_exists($flushData, 'detail')) {
            $this->getRepo('SystemDate')->trackUpdate($flushData->core, $flushData->detail);
        } else {
            $this->getRepo('SystemDate')->trackUpdate($flushData->core);
        }
    }

    /* --------------------- SERIALIZE DATA-RESULTS ----------------------------- */
    public function serializeDataResults($returnData): void
    {
        $serialize = ['core', 'detail'];

        foreach ($serialize as $p) {
            $prop = $p.'Entity';
            if (!property_exists($returnData, $prop) || !$returnData->$prop) {
                continue;
            }
            try {
                $returnData->$prop = $this->serialize->serializeRecord($returnData->$prop);
            } catch (\Throwable|\Exception $e) {
                $this->logError($returnData, $e);
            }
        }
    }

    /* ============================ HELPERS ===================================== */
    /* --------------------------- GET ENTITY ----------------------------------- */
    /** Returns the entity. */
    public function getEntity($name, $val, $prop = false): object | false | null
    {
        if (!$val) {
            return false;
        }
        $className = $this->getEntityClass($name);
        $prop = $prop ?: (is_numeric($val) ? 'id' : 'displayName');

        return $this->getRepo($className)->findOneBy([$prop => $val]);
    }

    /** Handles field-name to class-name translations. */
    private function getEntityClass($name)
    {
        $map = [
            'ParentLocation' => 'Location',
            'ParentSource' => 'Source',
            'ParentTaxon' => 'Taxon',
            'Subject' => 'Taxon',
            'Object' => 'Taxon',
        ];

        return array_key_exists($name, $map) ? $map[$name] : $name;
    }

    public function getEntities($name)
    {
        return $this->getRepo($name)->findAll();
    }
    public function getRepo($name): EntityRepository
    {
        return $this->em->getRepository('App\Entity\\'.$name);
    }

    /* ------------------- RETURN-DATA BUILDER ---------------------------------- */
    public function buildReturnDataObj($coreName, $coreEntity, $formData = null): \stdClass
    {
        $data = new \stdClass();
        $data->core = lcfirst((string) $coreName);
        $data->coreEntity = $coreEntity;
        if ($formData) {
            $data->coreEdits = $this->getEditsObj($formData, 'core');
            $data->detailEdits = $this->getEditsObj($formData, 'detail');
        }

        return $data;
    }

    /**
     * Builds and returns an object that will track any edits made to the entity.
     * The editing prop holds the id of the entity being edited, or false if creating.
     */
    private function getEditsObj($data, $type): \stdClass
    {
        $edits = new \stdClass();
        $edits->editing = property_exists($data, 'ids') ?
            $data->ids->$type : false;

        return $edits;
    }
}
