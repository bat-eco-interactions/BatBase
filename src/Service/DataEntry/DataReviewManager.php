<?php

namespace App\Service\DataEntry;

use App\Entity\ReviewEntry;
use App\Service\Utility\SerializeData;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Manages the data-review process.
 *
 * public
 *     addReviewEntryLogs
 *     setReviewEntryPayload
 *     handleDataReview
 *
 * TOC
 *     REVIEW ACTIONS
 *         APPROVE
 *         COMPLETE
 *         PAUSE/HOLD
 *         QUARANTINE
 *         LOCK
 *         RELEASE
 *         REJECT
 *         RETURN
 *     UTILITY
 *         QUARANTINE DATA-ENTRY
 *         PROCESS DATA-ENTRY
 *         SET AND SERIALIZE ENTITY
 *         ADD LOCAL-STORAGE DATA
 *         UPDATE LOG
 */
class DataReviewManager
{
    private $user;

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly EntryUtility $util,
        private readonly DataEntryManager $dataManager,
        private readonly SerializeData $serialize)
    {
    }

    public function handleDataReview($formData, $user)
    {
        $this->user = $user;
        $handler = $this->getStageHandler($formData);

        return $this->$handler($formData, $formData->review);
    }

    private function getStageHandler($formData): string
    {
        $handlers = [
            'Approve' => 'approveReviewEntry',
            'Complete' => 'completeDataReview',
            'Lock' => 'lockReviewEntry',
            'Pause' => 'holdReviewEntry',
            'Quarantine' => 'createReviewEntry',
            'Reject' => 'rejectReviewEntry',
            'Return' => 'returnReviewEntry',
            'Skip' => 'releaseReviewEntry',
        ];

        return $handlers[$formData->review->stage->name];
    }

    /* ======================= REVIEW ACTIONS =================================== */
    /* -------------------------- APPROVE --------------------------------------- */
    /**
     * Approves the ReviewEntry and modifies the database accordingly.
     *
     * @param  [object] $formData  Submitted form-data
     * @param  [object] $review    Data relevant to the ReviewEntry process
     *
     * @return [object]            Return data
     */
    private function approveReviewEntry($formData, $review)
    {
        $entity = $this->getReviewEntry($review);
        $returnData = $this->enterApprovedData($formData);
        if ($this->util->hasErrors($returnData)) {
            return $returnData;
        }
        $returnData->review = $this->approveAndSerialize($review, $returnData, $entity);

        return $returnData;
    }

    private function enterApprovedData($formData)
    {
        $returnData = $this->processDataEntry($formData);
        $this->util->attemptFlushAndLogErrors($returnData);

        return $returnData;
    }

    private function approveAndSerialize(&$review, &$returnData, &$entity): string
    {
        $entity->setForm(json_encode($review->form));
        $entity->setEntityId($returnData->coreEntity->getId());
        $this->setStage($entity, 'Approved');
        $entity->setManagedBy($this->user);

        return $this->flushAndSerialize($entity, $returnData);
    }

    /* ---------------------------- COMPLETE ------------------------------------ */
    /**
     * Completes the review process. The result has been reviewed by the contributor.
     *
     * @param  [object] $formData  Submitted form-data
     * @param  [object] $review    Data relevant to the ReviewEntry process
     *
     * @return [object]            Return data
     */
    private function completeDataReview($formData, $review): \stdClass
    {
        $returnData = new \stdClass();

        $entity = $this->getReviewEntry($review);
        $entity->setLog($review->log);
        $this->handleCompleteForStage($entity, $returnData);

        $returnData->review = $this->flushAndSerialize($entity, $returnData);

        return $returnData;
    }

    private function handleCompleteForStage($entity, $returnData): void
    {
        $this->ifApprovedAddEntityDataToReturn($entity, $returnData);
        $this->setStage($entity, 'Completed');
    }

    private function ifApprovedAddEntityDataToReturn($entity, &$returnData): void
    {
        if ($entity->getStageName() === 'Rejected') {
            return;
        }
        $id = $entity->getEntityId();
        $returnData->coreEntity = $this->serializeReviewEntity($entity, $id);
    }

    /* --------------------------- PAUSE/HOLD ----------------------------------- */
    /**
     * Holds the ReviewEntry for the data-manager to review further.
     *
     * @param  [object] $formData  Submitted form-data
     * @param  [object] $review    Data relevant to the ReviewEntry process
     *
     * @return [object]            Return data
     */
    private function holdReviewEntry($formData, $review): \stdClass
    {
        $returnData = new \stdClass();

        $entity = $this->getReviewEntry($review);
        $entity->setLog($review->log);
        $this->setStage($entity, 'Held');
        $entity->setManagedBy($this->user);

        $returnData->review = $this->flushAndSerialize($entity, $returnData);

        return $returnData;
    }

    /* --------------------------- QUARANTINE ----------------------------------- */
    /**
     * Creates a ReviewEntry to encapsulate the data-review process. The submitted
     * data will be quarantined to the contributor until approved by a data-manager.
     *
     * @param  [object] $formData  Submitted form-data
     * @param  [object] $review    Data relevant to the ReviewEntry process
     *
     * @return [object]            Return data
     */
    private function createReviewEntry($formData, $review)
    {
        $returnData = $this->getQuarantinedDataEntry($formData);
        if ($this->util->hasErrors($returnData)) {
            return $returnData;
        }
        $returnData->review = $this->processReviewEntry($formData, $review, $returnData);

        return $returnData;
    }

    private function processReviewEntry($formData, $review, &$returnData): string
    {
        return $review->id ?
            $this->updateReviewEntry($formData, $review, $returnData) :
            $this->initReviewEntry($formData, $review, $returnData);
    }

    private function initReviewEntry($formData, $review, &$returnData): string
    {
        $entity = new ReviewEntry();
        $entity->setEntity($returnData->core);
        $this->setReviewEntryBasics($entity, $review);
        $entity->setPayload(json_encode($returnData));

        return $this->flushAndSerialize($entity, $returnData);
    }

    /** After contributor reviews returned-data and resubmits for further review.  */
    private function updateReviewEntry($formData, $review, &$returnData): string
    {
        $entity = $this->getReviewEntry($review);
        $this->setReviewEntryBasics($entity, $review);
        $entity->setPayload(json_encode($returnData));

        return $this->flushAndSerialize($entity, $returnData);
    }

    /* --------------------------- LOCK ----------------------------------------- */
    /**
     * Locks the ReviewEntry to prevent overlapping reviews.
     *
     * @param  [object] $formData  Submitted form-data
     * @param  [object] $review    Data relevant to the ReviewEntry process
     *
     * @return [object]            Return data
     */
    private function lockReviewEntry($formData, $review): \stdClass
    {
        $returnData = new \stdClass();

        $entity = $this->getReviewEntry($review);
        $this->setStage($entity, 'Locked');
        if ($formData->manager) {
            $entity->setManagedBy($this->user);
        }

        $returnData->review = $this->flushAndSerialize($entity, $returnData);

        return $returnData;
    }

    /* --------------------------- RELEASE --------------------------------------- */
    /**
     * Releases the ReviewEntry from a locked state. Reverts to its previous stage.
     * (Data review ended without changes.).
     *
     * @param  [object] $formData  Submitted form-data
     * @param  [object] $review    Data relevant to the ReviewEntry process
     *
     * @return [object]            Return data
     */
    private function releaseReviewEntry($formData, $review): \stdClass
    {
        $returnData = new \stdClass();

        $entity = $this->getReviewEntry($review);
        $entity->setManagedBy(null);
        $this->resetPreviousStage($entity);

        $returnData->review = $this->flushAndSerialize($entity, $returnData);

        return $returnData;
    }

    private function resetPreviousStage(&$entity): void
    {
        $form = json_decode((string) $entity->getForm());
        $this->setStage($entity, $form->stage->name, 'activeForm');
    }

    /* --------------------------- REJECT --------------------------------------- */
    /**
     * Reject the ReviewEntry. If the entry is rejected as a duplicate, the existing
     * entity's ID is added to the return data.
     *
     * @param  [object] $formData Submitted form-data
     * @param  [object] $review    Data relevant to the ReviewEntry process
     *
     * @return [object]           Return data
     */
    private function rejectReviewEntry($formData, $review): \stdClass
    {
        $returnData = new \stdClass();
        $ReviewEntry = $this->handleEntryReject($review);
        $returnData->review = $this->flushAndSerialize($ReviewEntry, $returnData);

        return $returnData;
    }

    private function handleEntryReject($review): object|false|null
    {
        $entity = $this->getReviewEntry($review);
        $entity->setManagedBy($this->user);
        $this->ifDuplicateEntitySetExistingId($review, $entity);
        $this->setReviewEntryBasics($entity, $review, 'Rejected');

        return $entity;
    }

    private function ifDuplicateEntitySetExistingId(&$review, &$entity)
    {
        if (!property_exists($review, 'existingId')) {
            return false;
        }
        $entity->setEntityId($review->existingId);
    }

    /* --------------------------- RETURN --------------------------------------- */
    /**
     * Return the ReviewEntry to the contributor for further action.
     *
     * @param  [object] $formData  Submitted form-data
     * @param  [object] $review    Data relevant to the ReviewEntry process
     *
     * @return [object]            Return data
     */
    private function returnReviewEntry($formData, $review): \stdClass
    {
        $returnData = new \stdClass();

        $entity = $this->getReviewEntry($review);
        $entity->setManagedBy(null);
        $this->setReviewEntryBasics($entity, $review, 'Returned');

        $returnData->review = $this->flushAndSerialize($entity, $returnData);

        return $returnData;
    }

    /* ========================== UTILITY ======================================= */
    private function getReviewEntry(&$review): object|false|null
    {
        return $this->util->getEntity('ReviewEntry', $review->id);
    }

    private function setReviewEntryBasics(&$entity, $review, $stageName = 'Pending'): void
    {
        $entity->setForm(json_encode($review->form));
        $entity->setLog($review->log);
        $this->setStage($entity, $stageName);
    }

    private function setStage(&$entity, $name, $prop = 'passiveForm'): void
    {
        $stage = $this->util->getEntity('ReviewStage', $name, $prop);
        $entity->setReviewStage($stage);
    }

    private function serializeReviewEntity($ReviewEntry, $id): string
    {
        $entityClass = ucfirst((string) $ReviewEntry->getEntity());
        $entity = $this->util->getEntity($entityClass, $id);

        return $this->serialize->serializeRecord($entity);
    }

    /* ----------------------- QUARANTINE DATA-ENTRY ---------------------------- */
    private function getQuarantinedDataEntry($formData)
    {
        $data = null;
        $this->em->getConnection()->beginTransaction(); // suspend auto-commit
        try {
            $data = $this->processDataEntry($formData);
            throw new \Exception('Data quarantined until approved.');
        } catch (\Exception) {
            $this->em->getConnection()->rollBack();
        }
        $this->em->getConnection()->setAutoCommit(true);

        return $data;
    }

    /* ------------------------- PROCESS DATA-ENTRY ----------------------------- */
    private function processDataEntry($formData)
    {
        $coreName = $formData->coreEntity;
        switch ($formData->review->form->action) {
            case 'create':
                return $this->dataManager->createEntity($coreName, $formData);
            case 'edit':
                return $this->dataManager->editEntity($coreName, $formData);
            case 'delete':
                return $this->dataManager->deleteEntity($coreName, $formData);
        }
    }

    /* ----------------------- SET AND SERIALIZE ENTITY ------------------------- */
    /**
     * Handles ReviewEntry flush and error tracking.
     *
     * @param object  &$entity     ReviewEntry
     * @param object &$returnData
     *
     * @return string Serialized entity
     */
    private function flushAndSerialize(&$entity, &$returnData): string
    {
        $errors = $this->flushReviewEntry($entity);
        if ($errors) {
            $returnData->errors = $errors;

            return 'Error serializing entity.';
        }

        return $this->serialize->serializeRecord($entity);
    }

    private function flushReviewEntry(&$entity)
    {
        $flushData = new \stdClass();
        $flushData->coreEntity = $entity;
        $this->em->persist($entity);
        $this->util->attemptFlushAndLogErrors($flushData, true);
        if ($this->util->hasErrors($flushData)) {
            return $flushData;
        }
    }

    /* ----------------------- ADD LOCAL-STORAGE DATA  -------------------------- */
    /**
     * ReviewEntry is returned from the server with temporary data used for the
     * serialization. It is modified to match the locally quarantined data before
     * being stored in the contributor's local database. The resulting entity-data
     * is added here to the ReviewEntry for redownloading and syncing updates.
     */
    public function setReviewEntryPayload($results): \stdClass
    {
        $entity = $this->util->getEntity('ReviewEntry', $results->review->id);
        $entity->setPayload(json_encode($results));
        $this->em->persist($entity);

        $returnData = new \stdClass();
        $returnData->review = $this->flushAndSerialize($entity, $returnData);

        return $returnData;
    }

    /* ----------------------- UPDATE LOG --------------------------------------- */
    /**
     * Logs updates to ReviewEntry when sub-data has been reviewed.
     */
    public function addReviewEntryLogs($updated): \stdClass
    {
        $entity = $this->util->getEntity('ReviewEntry', intval($updated->id));
        $entity->setLog($updated->logs);
        $this->em->persist($entity);

        $returnData = new \stdClass();
        $returnData->review = $this->flushAndSerialize($entity, $returnData);

        return $returnData;
    }
}
