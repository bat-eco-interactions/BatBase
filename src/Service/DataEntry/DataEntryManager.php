<?php

namespace App\Service\DataEntry;

use Doctrine\ORM\EntityManagerInterface;

/**
 * public
 *     createEntity
 *     deleteEntity
 *     editEntity
 *     new
 *     serializeDataResults
 *     updateCitationText.
 *
 * TOC
 *     ACTIONS
 *         CREATE ENTITY
 *         EDIT ENTITY
 *         DELETE ENTITY
 *     UPDATE CITATION TEXT
 */
class DataEntryManager
{
    public function __construct(private readonly EntityManagerInterface $em, private readonly DeleteEntity $delete, private readonly ProcessEntry $processEntry, private readonly EntryUtility $util)
    {
    }

    /* ============================ ACTIONS ===================================== */
    /**
     * Creates a new entity with an array of values separated by handling: flat and rel(ational).
     *
     * @param string $coreName
     * @param array  $dataAry
     *
     * @return void
     */
    public function new($coreName, $dataAry): object
    {
        $data = $this->initializeDataStructure($dataAry);

        return $this->createEntity($coreName, $data);
    }

    private function initializeDataStructure($dataAry): object
    {
        $data = new \stdClass();
        $data->core = new \stdClass();
        $data->core->flat = $this->addToData(new \stdClass(), $dataAry['flat']);

        if (array_key_exists('rel', $dataAry)) {
            $data->core->rel = $this->addToData(new \stdClass(), $dataAry['rel']);
        }

        return $data;
    }

    private function addToData(&$obj, $ary): void
    {
        foreach ($ary as $prop => $value) {
            $obj->$prop = $value;
        }
    }

    /* ------------------------ CREATE ENTITY ----------------------------------- */
    /**
     * Create new entity from data-entry form.
     *
     * @param string $coreName core-Entity class name
     * @param object $data     [ eName: [ // Core name, detail name (optional)
     *                         flat: [ field => val ],
     *                         rel: [ entity => id] ]]
     *
     * @return object Entity data (errors added if present)
     */
    public function createEntity($coreName, $data): object
    {
        $coreEntity = $this->newCoreEntity($coreName);
        $returnData = $this->util->buildReturnDataObj($coreName, $coreEntity, $data);

        try {
            $this->processEntry->processEntity($data, $coreEntity, $returnData);
            $this->removeEditData($returnData->coreEdits, $returnData->detailEdits);
            $this->util->attemptFlushAndLogErrors($returnData);
        } catch (\Exception $e) {
            $returnData->error = $e->getMessage();
        }

        return $returnData;
    }

    private function newCoreEntity($coreName): object
    {
        $coreClass = 'App\\Entity\\'.ucfirst((string) $coreName);

        return new $coreClass();
    }

    /* -------------------------- EDIT ENTITY ----------------------------------- */
    /**
     * Edit existing entity.
     *
     * @param string $coreName core-Entity class name
     * @param object $data     [ eName: [ // Core name, detail name (optional)
     *                         flat: [ field => val ],
     *                         rel: [ entity => id] ]]
     *
     * @return object Entity data (errors added if present)
     */
    public function editEntity($coreName, $data): object
    {
        $coreEntity = $this->util->getEntity(ucfirst($coreName), $data->ids->core);
        $returnData = $this->util->buildReturnDataObj($coreName, $coreEntity, $data);

        $this->processEntry->processEntity($data, $coreEntity, $returnData);
        $this->handleEditFlags($returnData);
        $this->util->attemptFlushAndLogErrors($returnData);

        return $returnData;
    }

    private function handleEditFlags(&$returnData): void
    {
        $this->removeEditingFlag($returnData->coreEdits, $returnData->detailEdits);
        $this->ifNoChangesMadeFlagFail($returnData);
    }

    /** If no changes were made during the edit, this entry "fails" and is returned, */
    private function ifNoChangesMadeFlagFail(&$returnData): void
    {
        if (property_exists($returnData, 'fail')) {
            return;
        } // Data failed previously
        $coreEdited = $returnData->coreEdits != new \stdClass();
        $detailEdited = $returnData->detailEdits != new \stdClass();
        if ($coreEdited || $detailEdited) {
            return;
        }
        $returnData->fail = ['noChanges' => true];
    }

    private function removeEditingFlag(&$coreObj, &$detailObj): void
    {
        unset($coreObj->editing);
        unset($detailObj->editing);
    }

    private function removeEditData(&$core, &$detail): void
    {
        foreach ([$core, $detail] as $obj) {
            foreach ($obj as $key => $value) {
                unset($obj->$key);
            }
        }
    }

    /* -------------------------- DELETE ENTITY --------------------------------- */
    /**
     * Flags entity for delete. Interactions are soft-deleted and all others have
     * "[DELETED]" added to their name and are automatically hidden from the user.
     */
    public function deleteEntity($coreName, $formData)
    {
        return $this->delete->deleteEntity($coreName, $formData);
    }

    /* ===================== UPDATE CITATION TEXT =============================== */
    public function updateCitationText($data)
    {
        $src = $this->util->getEntity('Source', $data->srcId);
        $src->setDescription($data->text);
        $this->em->persist($src);

        $cit = $src->getCitation();
        $cit->setFullText($data->text);
        $this->em->persist($cit);

        $returnData = new \stdClass();
        $returnData->core = 'source';
        $returnData->coreEntity = $src;
        $returnData->detail = 'citation';
        $returnData->detailEntity = $cit;

        $this->util->attemptFlushAndLogErrors($returnData);

        return $returnData;
    }
}
