<?php

namespace App\Service\Aggregator;

use App\Entity\Taxon;
use App\Service\Utility\ProcessCacheManager;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;

/**
 * Aggregates total counts of interactions by type and geographical location
 * (regions/countries) for a given taxon and all its descendants.
 */
readonly class TaxonBranchInteractionAggregator
{
    public function __construct(
        private ProcessCacheManager $cache,
        private Connection          $connection
    )
    {
    }

    /**
     * Retrieves or aggregates the total counts of interactions by type and geographical
     * location (regions/countries) for a given taxon and all its descendants.
     * Results are cached with stampede prevention to avoid redundant calculations.
     * @link https://symfony.com/doc/6.4/components/cache.html#stampede-prevention.
     *
     * @return array{
     *     total: int,
     *     types: array<string, int>,
     *     locations: array<string, array<..., int>
     * }
     * @throws Exception
     */
    public function aggregate(Taxon $taxon): array
    {
        $cacheKey = "aggregate_interactions_" . $taxon->getDisplayName();

        return $this->cache->get($cacheKey, function () use ($taxon) {
            $results = $this->queryBranchInteractions($taxon->getId());
            return $this->aggregateResults($results);
        });
    }

    /**
     * Retrieves and aggregates interaction data for a taxon and it's descendants.
     * Uses temporary tables to efficiently query:
     * 1. Interaction counts grouped by type
     * 2. Location data within region->country->etc hierarchy and interaction counts
     *
     * @param int $targetTaxonId Taxon ID to analyze interactions for
     * @returns array $results Raw query results containing 'types' and 'locations' data
     * @throws Exception
     */
    public function queryBranchInteractions(int $targetTaxonId): array
    {
        $this->createTemporaryTables($targetTaxonId);

        // Aggregate interaction type counts
        $typeSql = <<<SQL
            SELECT 
                it.display_name as interaction_type,
                COUNT(ri.id) as interaction_count
            FROM tmp_relevant_interactions ri
            INNER JOIN interaction_type it ON ri.type_id = it.id
            GROUP BY it.display_name;
        SQL;
        $types = $this->connection->executeQuery($typeSql)->fetchAllAssociative();

        // Select location hierarchy with aggregated interaction counts
        $locationSql = <<<SQL
            SELECT 
                l.id as id,
                l.display_name as name,
                l.parent_loc_id as parent_id,
                lt.slug as type,
                COUNT(ri.id) as interaction_count
            FROM tmp_location_hierarchy lh
            INNER JOIN location l ON l.id = lh.id
            INNER JOIN location_type lt ON l.type_id = lt.id
            LEFT JOIN tmp_relevant_interactions ri ON l.id = ri.location_id
            GROUP BY 
                l.id,
                l.display_name,
                l.parent_loc_id,
                lt.slug
            ORDER BY l.id;
        SQL;
        $locations = $this->connection->executeQuery($locationSql)->fetchAllAssociative();

        $this->dropTemporaryTables();

        return ['types' => $types, 'locations' => $locations];
    }

    /**
     * Aggregates interaction data into a structured summary of totals by type and location.
     * Processes raw query results into three components:
     * - overall interaction total
     * - counts grouped by interaction type
     * - hierarchical geographical breakdown of interactions
     *
     * @param array $results Raw query results containing 'types' and 'locations' data
     * @return array Structured array with 'total', 'types', and 'locations' summaries
     */
    private function aggregateResults(array $results): array
    {
        [$total, $types] = $this->aggregateByType($results['types']);
        $locations = $this->aggregateByCountry($results['locations']);

        return [
            'total' => $total,
            'types' => $types,
            'locations' => $locations,
        ];
    }

    private function aggregateByType(array $types): array
    {
        $totalInteractions = 0;
        $interactionTypes = [];

        foreach ($types as $row) {
            $count = (int)$row['interaction_count'];
            $interactionTypes[$row['interaction_type']] = $count;
            $totalInteractions += $count;
        }

        return [$totalInteractions, $interactionTypes];
    }

    private function aggregateByCountry(array $locations): array
    {
        $locationData = [];

        foreach ($locations as $row) {
            $locationId = $row['id'];

            if (!isset($locationData[$locationId])) {
                $locationData[$locationId] = [
                    'id' => $locationId,
                    'name' => $row['name'],
                    'type' => $row['type'],
                    'parent_id' => $row['parent_id'],
                    'count' => 0,
                ];
            }

            $locationData[$locationId]['count'] += (int)$row['interaction_count'];
        }

        return $this->aggregateGeographicalHierarchy($locationData);
    }


    private function aggregateGeographicalHierarchy(array $locations): array
    {
        $hierarchy = [];
        $childrenMap = [];
        $rootRegions = [];

        // Build parent-child relationships and identify roots
        foreach ($locations as $id => $location) {
            $parentId = $location['parent_id'];

            if ($location['type'] === 'region' && !$parentId) {
                $rootRegions[$id] = $location;
            } elseif ($parentId) {
                if (!isset($childrenMap[$parentId])) {
                    $childrenMap[$parentId] = [];
                }
                $childrenMap[$parentId][$id] = $location;
            }
        }

        // Aggregate counts upward and format structure
        foreach ($rootRegions as $regionId => $region) {
            $hierarchy[$region['name']] = $this->buildLocationNode(
                $regionId,
                $locations,
                $childrenMap
            );
        }

        return $hierarchy;
    }

    /**
     * Recursively constructs a hierarchical location-tree (e.g., region -> sub-region
     * -> country) and their associated interaction counts.
     * Note:
     * 1. Regional nodes aggregate their children's counts and may include an "Unspecified" count
     * 2. Country nodes are treated as leaf nodes with their counts directly included
     * 3. Other location types simply contribute their counts to their parent's total
     *
     * @param int $locationId The ID of the current location node being processed
     * @param array $locations Array mapping location IDs to their data (type, count, etc.)
     * @param array $childrenMap Array mapping parent IDs to arrays of their children's IDs
     *
     * @return array|null The constructed tree node, where:
     *                    - For regions: returns a map of child location names to their subtrees
     *                    - For all other types: returns an array containing just the total count
     *                    - Returns null if the location ID is invalid
     *
     * Example output structure for a region:
     * [
     *     'Africa' => [
     *         'Sub-Saharan Africa' => [
     *             'South Africa' => 15,
     *             'Unspecified' => 5
     *         ],
     *         'Unspecified' => 3
     *     ]
     * ]
     *
     *  Example output for a country or other type:
     *  [
     *      'total' => 42
     *  ]
     */
    private function buildLocationNode(
        int   $locationId,
        array $locations,
        array $childrenMap
    ): ?array
    {
        $location = $locations[$locationId];
        $totalCount = $location['count'];
        $sub_regions = [];

        if (isset($childrenMap[$locationId])) {
            foreach ($childrenMap[$locationId] as $child) {
                $childNode = $this->buildLocationNode($child['id'], $locations, $childrenMap);
                if ($child['type'] === 'region') {
                    $sub_regions[$child['name']] = $childNode;
                } elseif ($child['type'] === 'country') {
                    $sub_regions[$child['name']] = $childNode['total'];
                } else {
                    $totalCount += $childNode['total'];
                }
            }
        }
        if ($location['type'] === 'region') {
            if ($location['count'] > 0) {
                $sub_regions['Unspecified'] = $location['count'];
            }
            return $sub_regions;
        }

        return ['total' => $totalCount];
    }

    /**
     * Creates temporary tables necessary for aggregating interactions data.
     *
     * This function creates three temporary tables:
     * 1. `tmp_taxon_hierarchy`: A recursive hierarchy with the target taxon and descendants.
     * 2. `tmp_relevant_interactions`: Interactions involving any taxa in the hierarchy.
     * 3. `tmp_location_hierarchy`: A hierarchy of locations that are associated with interactions.
     *
     * These tables facilitate efficient querying and aggregation of interaction
     * counts by type and location relevant to the specified taxon.
     *
     * @param int $targetTaxonId The ID of the target taxon for which the hierarchy
     *                           and interactions are to be established.
     * @throws Exception
     */
    private function createTemporaryTables(int $targetTaxonId): void
    {
        // Table 1: Create the taxon branch hierarchical structure:
        $createHierarchySql = <<<SQL
            CREATE TEMPORARY TABLE tmp_taxon_hierarchy AS
                WITH RECURSIVE taxon_hierarchy AS (
                    -- Base case: Start with the target taxon
                    SELECT id, parent_taxon_id, 1 as level
                    FROM taxon
                    WHERE id = :target_id
                    
                    UNION ALL
                
                    -- Recursive case: Join with parent taxa
                    SELECT t.id, t.parent_taxon_id, th.level + 1
                    FROM taxon t
                    INNER JOIN taxon_hierarchy th ON t.parent_taxon_id = th.id
                    WHERE th.level < 10
                )
            SELECT * FROM taxon_hierarchy
        SQL;
        $this->connection->executeStatement($createHierarchySql, ['target_id' => $targetTaxonId]);
        $this->connection->executeStatement('ALTER TABLE tmp_taxon_hierarchy ADD INDEX (id)');

        // Table 2: Get the interactions involving taxa in the hierarchy
        $createRelevantInteractionsSql = <<<SQL
            CREATE TEMPORARY TABLE tmp_relevant_interactions AS
                WITH relevant_interactions AS (
                    SELECT DISTINCT 
                        i.id,
                        i.type_id,
                        i.location_id
                    FROM interaction i
                    
                    INNER JOIN tmp_taxon_hierarchy th 
                    ON (i.subject_taxon_id = th.id OR i.object_taxon_id = th.id)
                    WHERE i.deleted_at IS NULL
                )               
            SELECT * FROM relevant_interactions;
        SQL;
        $this->connection->executeStatement($createRelevantInteractionsSql);
        $this->connection->executeStatement('ALTER TABLE tmp_relevant_interactions ADD INDEX (id)');

        // Table 3: Build location hierarchy
        $createLocHierarchySql = <<<SQL
            -- First create a temporary table for the location hierarchy
            CREATE TEMPORARY TABLE tmp_location_hierarchy AS
                WITH RECURSIVE location_ancestors AS (
                    -- Base: start with locations that have interactions
                    SELECT
                        l.id,
                        l.type_id,
                        l.parent_loc_id,
                        1 as level
                    FROM location l 
                    WHERE l.id IN (SELECT DISTINCT location_id FROM tmp_relevant_interactions)
                    
                    UNION ALL
                    
                    -- Recursive: get all ancestors
                    SELECT
                        l.id,
                        l.type_id,
                        l.parent_loc_id,
                        la.level + 1
                    FROM location l
                    INNER JOIN location_ancestors la ON l.id = la.parent_loc_id
                    WHERE la.level < 10
                )
            SELECT DISTINCT id, type_id, parent_loc_id
            FROM location_ancestors;
        SQL;
        $this->connection->executeStatement($createLocHierarchySql);
        $this->connection->executeStatement('ALTER TABLE tmp_location_hierarchy ADD INDEX (id)');
    }

    /**
     * @throws Exception
     */
    private function dropTemporaryTables(): void
    {
        $this->connection->executeStatement('DROP TEMPORARY TABLE IF EXISTS tmp_taxon_hierarchy');
        $this->connection->executeStatement('DROP TEMPORARY TABLE IF EXISTS tmp_relevant_interactions');
        $this->connection->executeStatement('DROP TEMPORARY TABLE IF EXISTS tmp_location_hierarchy');
    }
}
