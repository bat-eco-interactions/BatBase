<?php

namespace App\Service\Aggregator;

use App\Entity\User;
use App\Service\Utility\Mailer;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Mime\Address;

/**
 * Builds a summary of data changes and emails the digest to site admin.
 *
 * TOC
 *     COMPOSE EMAIL
 *         BUILD
 *         SEND
 *     BUILD DIGEST DATA
 *         DATA ENTRY
 *         SUBMITTED PUBLICATIONS
 *         NEW USER
 *         USER FEEDBACK
 *         HELPERS
 */
class WeeklyDigestManager
{
    private \DateTime $oneWeekAgo;

    public function __construct(
        #[Autowire('%kernel.project_dir%')]
        private readonly string                 $rootPath,
        private readonly EntityManagerInterface $em,
        private readonly LoggerInterface        $logger,
        private readonly Mailer                 $mailer
    )
    {
    }

    /* ======================== COMPOSE EMAIL =================================== */
    /**
     * Sends an email to site admins with various updates from site activity
     * over the last week: Data entry, submitted publications, new users, and new
     * user feedback.
     */
    public function sendAdminWeeklyDigestEmail(): array
    {
        $data = $this->buildWeeklyDigestData();
        $users = $this->em->getRepository(User::class)->findAdmins();

        foreach ($users as $user) {
            $email = $this->getDigestEmail($user, $data);
            $this->handleSendEmail($email);
        }

        return $data;
    }

    /* ----------------------- BUILD -------------------------------------------- */
    /**
     * Build the email object with the weekly digest data and any submitted PDFs.
     *
     * @param string $user User
     * @param array $data Digest data
     */
    private function getDigestEmail($user, $data): TemplatedEmail
    {
        $email = $this->initDigestEmail($this->getAddress($user));
        $this->handleEmailBody($data, $email);
        $this->attachNewPDFs($data['pdfs'], $email);

        return $email;
    }

    private function getAddress($user): Address
    {
        // defining the email address and name as an object
        // (email clients will display the name)
        return new Address($user->getEmail(), $user->getFirstName());
    }

    /**
     * Inits and returns the Swiftmailer email.
     */
    private function initDigestEmail(Address $address): TemplatedEmail
    {
        $email = (new TemplatedEmail())
            ->from('noreply@batbase.org')
            ->to($address)
            ->subject('BatBase Weekly Digest');

        return $email;
    }

    /**
     * Attach PDF files to the email.
     *
     * @param array $pdfs PDF data
     * @param TemplatedEmail $email Email
     */
    private function attachNewPDFs($pdfs, &$email): void
    {
        foreach ($pdfs as $pdf) {
            $path = $this->rootPath . '/public_html/' . $pdf['path'];
            $email->attachFromPath($path, $pdf['title']);
        }
    }

    private function handleEmailBody($data, &$email): void
    {
        $template = 'emails/weekly-digest.html.twig';
        $email
            ->htmlTemplate($template)
            ->context($data);
    }

    /* ----------------------- SEND --------------------------------------------- */
    private function handleSendEmail($email): void
    {
        $this->mailer->sendEmail($email);
    }

    /* ======================== BUILD DIGEST DATA =============================== */
    /**
     * Returns data about site activity over the last week: Data-entry, submitted
     * publications, new users, and new user fedeback.
     *
     * @return array Weekly digest data
     */
    private function buildWeeklyDigestData(): array
    {
        $this->oneWeekAgo = $this->getDate('-7 days');

        return $this->getWeeklyDigestData();
    }

    private function getDate($timeframe): \DateTime
    {
        return new \DateTime($timeframe, new \DateTimeZone('UTC'));
    }

    private function getWeeklyDigestData(): array
    {
        $data = [
            'lastWeek' => $this->oneWeekAgo->format('m-d-Y'),
            // 'yesterday' => $this->getDate('-7 days')->format('m-d-Y'),
            'dataUpdates' => $this->getUpdatedEntityData(),
            'feedback' => $this->getFeedbackData(),
            'pdfs' => $this->getPDFData(),
            'users' => $this->getNewUserData(),
        ];

        return $data;
    }

    /* ----------------------- DATA ENTRY --------------------------------------- */
    /**
     * @return array each entity with updates, wth the totals created and edited
     */
    private function getUpdatedEntityData(): array
    {
        $data = [];
        $reviewEntryCounts = false;
        foreach ($this->getUpdated('SystemDate') as $updated) {
            $entityName = $updated->getEntity();
            if ($this->isNotIncluded($entityName)) {
                continue;
            }
            $updates = $this->countUpdates($entityName);
            if (!$updates) {
                continue;
            }
            if ($entityName === 'ReviewEntry') {
                $reviewEntryCounts = $updates;
                continue;
            }
            array_push($data, $updates);
        }
        if ($reviewEntryCounts) {
            array_push($data, $reviewEntryCounts);
        }

        return $data;
    }

    private function isNotIncluded($entityName): bool
    {
        $skip = ['System', 'FileUpload', 'Feedback', 'Source', 'GeoJson'];

        return in_array($entityName, $skip);
    }

    private function countUpdates($entityName): false|array
    {
        $created = count($this->getCreated($entityName));
        $updated = count($this->getUpdated($entityName));
        $reviewed = count($this->getReviewed($entityName));
        if (!$created && !$updated && !$reviewed) {
            return false;
        }

        return [
            'name' => $entityName,
            'created' => $created,
            'updated' => $updated,
            'reviewed' => $reviewed,
        ];
    }

    /* ------------------- SUBMITTED PUBLICATIONS ------------------------------- */
    /**
     * @return array Data for each publication submitted last week
     */
    private function getPDFData(): array
    {
        $data = [];
        foreach ($this->getCreated('FileUpload') as $pdf) {
            array_push(
                $data, [
                    'path' => $pdf->getPath() . $pdf->getFileName(),
                    'filename' => $pdf->getFileName(),
                    'title' => $pdf->getTitle(),
                    'description' => $pdf->getDescription(),
                    'user' => $pdf->getCreatedBy()->getFullName(),
                    'userEmail' => $pdf->getCreatedBy()->getEmail(),
                ]
            );
        }

        return $data;
    }

    /* ----------------------- NEW USER ----------------------------------------- */
    /**
     * @return array Data for each new user from last week
     */
    private function getNewUserData(): array
    {
        $data = [];
        foreach ($this->getCreated('User') as $user) {
            array_push($data, [
                'name' => $user->getFullName(),
                'about' => $user->getAboutMe(),
            ]);
        }

        return $data;
    }

    /* ----------------------- USER FEEDBACK ------------------------------------ */
    /**
     * NOTE: Not currently reporting updates to feedback.
     *
     * @return array Data for user feedback submitted last week
     */
    private function getFeedbackData(): array
    {
        $feedback = $this->getCreated('Feedback');

        return $this->getCreatedFeedback($feedback);
    }

    private function getCreatedFeedback($created): array
    {
        $data = [];
        foreach ($created as $feedback) {
            array_push($data, [
                'page' => $feedback->getRoute(),
                'topic' => $feedback->getTopic(),
                'feedback' => $feedback->getContent(),
                'user' => $feedback->getCreatedBy()->getFullName(),
                'userEmail' => $feedback->getCreatedBy()->getEmail(),
            ]);
        }

        return $data;
    }

    /* ------------------------ HELPERS ----------------------------------------- */
    private function getCreated($entityName)
    {
        $repo = $this->em->getRepository("\App\Entity\\" . $entityName);
        return $repo->createQueryBuilder('e')
            ->where('e.created > :date')
            ->setParameter('date', $this->oneWeekAgo)
            ->getQuery()
            ->getResult();
    }

    private function getReviewed($entityName)
    {
        if ($entityName === 'ReviewEntry') {
            return [];
        }

        $repo = $this->em->getRepository("\App\Entity\\" . $entityName);
        $query = $repo->createQueryBuilder('e')
            ->where('e.reviewed > :date');
        if ($entityName !== 'SystemDate') {
            $query->andWhere('e.reviewed > e.created');
        }
        $query->setParameter('date', $this->oneWeekAgo);

        return $query->getQuery()->getResult();
    }

    private function getUpdated($entityName)
    {
        $repo = $this->em->getRepository("\App\Entity\\" . $entityName);
        $query = $repo->createQueryBuilder('e')
            ->where('e.updated > :date');
        if ($entityName !== 'SystemDate') {
            $query->andWhere('e.updated > e.created');
        }
        $query->setParameter('date', $this->oneWeekAgo);

        return $query->getQuery()->getResult();
    }
}
