<?php

namespace App\Service\Aggregator;

use App\Entity\Interaction;
use App\Entity\Location;
use App\Entity\Rank;
use App\Entity\Source;
use App\Entity\Taxon;
use App\Entity\User;
use App\Service\Utility\ProcessCacheManager;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Generates relevant project summary strings to display in the header of a page.
 */
readonly class ProjectStatSummary
{
    public function __construct(
        private EntityManagerInterface $em,
        private ProcessCacheManager    $cache
    )
    {
    }

    /**
     * Builds an array of status strings representing current entity metrics.
     *
     * @param string $tag
     * @return array
     */
    public function getProjectStatSummary(string $tag): array
    {
        $map = [
            'all' => fn() => $this->buildAllStatData(),
            'core' => fn() => $this->buildCoreStatData(),
            'db' => fn() => $this->buildAboutDatabaseStatData(),
            'project' => fn() => $this->buildAboutProjectStatData(),
        ];

        return call_user_func($map[$tag]);
    }

    private function buildAllStatData(): array
    {
        $project = $this->buildAboutProjectStatData();
        $db = $this->buildAboutDatabaseStatData();

        return array_merge($project, $db);
    }

    private function buildCoreStatData(): array
    {
        return [
            'bat' => $this->getBatSpeciesStats(),
            'cite' => $this->getCitationStats(),
            'int' => $this->getInteractionsStats(),
            'loc' => $this->getLocationStats(),
        ];
    }

    private function buildAboutDatabaseStatData(): array
    {
        $data = $this->buildCoreStatData();
        $data['nonBat'] = $this->getAllNonBatSpeciesStats();

        return $data;
    }

    private function buildAboutProjectStatData(): array
    {
        $counts = $this->cache->get("count_users", function () {
            $users = $this->em->getRepository(User::class)->findAll();
            $editors = 0;

            foreach ($users as $user) {
                if ($user->hasRole('ROLE_EDITOR') || $user->hasRole('ROLE_CONTRIBUTOR')) {
                    ++$editors;
                }
            }

            return [
                'editors' => $editors,
                'users' => count($users),
            ];
        }, 1800);

        return [
            'editor' => "{$counts['editors']} Editors",
            'user' => "{$counts['users']} Users",
        ];
    }

    /* ---------------------- ENTITY COUNTS --------------------------------- */
    private function getCitationStats(): string
    {
        $cites = $this->cache->get("count_cite_totals", function () {
            return count($this->em->getRepository(Source::class)->findDirectSources());
        }, 1800);

        return "$cites Citations";
    }

    private function getInteractionsStats(): string
    {
        $ints = $this->cache->get("count_int_totals", function () {
            return count($this->em->getRepository(Interaction::class)->findAll());
        }, 1800);

        return "$ints Interactions";
    }

    /**
     * Summarizes the number of locations and countries with interactions.
     * @return string
     */
    private function getLocationStats(): string
    {
        $counts = $this->cache->get("count_locs", function () {
            return $this->countLocations();
        }, 1800);

        return "{$counts['places']} Locations in {$counts['countries']} Countries";
    }

    private function countLocations(): array
    {
        $locs = $this->em->getRepository(Location::class)->findAll();
        $places = 0;
        $countries = []; // names to handle duplication

        foreach ($locs as $loc) {
            $locType = $loc->getLocationType()->getDisplayName();
            if (!count($loc->getInteractions()) || $locType === 'Region') {
                continue;
            }
            if ($locType === 'Country') {
                $countries[] = $loc->getDisplayName();
            } else {
                $places++;
                $countryData = $loc->getCountryData();
                if ($countryData) {
                    $countries[] = $countryData['displayName'];
                }
            }
        }

        return [
            'places' => $places,
            'countries' => count(array_unique($countries)),
        ];
    }

    /* --------------------------- BAT SPECIES ------------------------------ */
    private function getBatSpeciesStats(): string
    {
        $batSpecies = $this->getBatSpeciesCount();
        return "$batSpecies Bat Species";
    }

    private function getBatSpeciesCount(): int
    {
        return $this->cache->get("count_bat_species", function () {
            $species = $this->em->getRepository(Rank::class)->findOneBy(['displayName' => 'Species']);
            $bat = $this->em->getRepository(Taxon::class)->findOneBy(['name' => 'Chiroptera']);
            return $this->countBatSpecies($bat, $species);
        }, 1800);
    }

    private function countBatSpecies(Taxon $taxon, Rank $species): int
    {
        $count = $taxon->getRank() === $species ? 1 : 0;

        foreach ($taxon->getChildTaxa() as $childTaxon) {
            $count += $this->countBatSpecies($childTaxon, $species);
        }

        return $count;
    }

    /* ------------------------- OTHER SPECIES ------------------------------ */
    private function getAllNonBatSpeciesStats(): string
    {
        $nonBatSpecies = $this->cache->get("count_non_bat_species", function () {
            $species = $this->em->getRepository(Rank::class)
                ->findOneBy(['displayName' => 'Species']);
            return count($species->getTaxa()) - $this->getBatSpeciesCount();
        }, 1800);

        return "$nonBatSpecies Other Species";
    }
}
