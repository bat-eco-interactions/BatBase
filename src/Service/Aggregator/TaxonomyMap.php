<?php

namespace App\Service\Aggregator;

use App\Entity\Taxon;

/**
 * Maps taxonomic classifications by creating structured representations.
 */
final class TaxonomyMap
{
    /**
     * Creates a taxonomic map centered on a target taxon.
     *
     * The generated map varies based on whether the target is a root taxon:
     * - For root taxa: Returns a complete map of all descendants
     * - For non-root taxa: Returns a focused map containing only:
     *   1. The direct path from root to target
     *   2. All descendants of the target
     *
     * This selective mapping helps manage complexity by representing only relevant
     * relationships while maintaining the full context above the target and
     * complete detail below it.
     *
     * Example structure for target "Myotis" within the bat group:
     *   Chiroptera (group root)
     *   └── Vespertilionidae
     *       └── Myotis (target)
     *           ├── M. lucifugus (little brown bat)
     *           ├── M. myotis (greater mouse-eared bat)
     *           └── M. sodalis (Indiana bat)
     *
     * @param Taxon $targetTaxon The taxon around which to build the hierarchy
     * @return TaxonomyNode The root node of the generated tree structure
     */
    public function generateMap(Taxon $targetTaxon): TaxonomyNode
    {
        if ($targetTaxon->getIsRoot()) {
            return $this->getTaxonNodeWithAllDescendants($targetTaxon);
        }

        $root = $this->getAncestorMap($targetTaxon);
        $this->insertTargetTaxon($root, $targetTaxon);
        return $root;
    }

    /**
     * Creates a complete map of all descendants for the given taxon.
     *
     * @param Taxon $taxon The starting point for this section of the map
     * @return TaxonomyNode A node representing this portion of the taxonomic map
     */
    private function getTaxonNodeWithAllDescendants(Taxon $taxon): TaxonomyNode
    {
        $childNodes = $this->getChildTaxonomy($taxon);
        return TaxonomyNode::fromTaxon($taxon, $childNodes);
    }

    /**
     * Recursively maps out the descendant portion of the taxonomy.
     *
     * For each child taxon, creates a node and maps its descendants recursively.
     * The resulting array is keyed by display name to maintain consistent
     * access patterns when navigating the map.
     *
     * @param Taxon $taxon The taxon whose children to map
     * @return array<string, TaxonomyNode> Map of display names to child nodes
     */
    private function getChildTaxonomy(Taxon $taxon): array
    {
        $children = [];

        foreach ($taxon->getChildTaxa() as $childTaxon) {
            $node = TaxonomyNode::fromTaxon($childTaxon, $this->getChildTaxonomy($childTaxon));
            $children[$childTaxon->getDisplayName()] = $node;
        }

        return $children;
    }

    /**
     * Maps the direct path from root to the parent of the target taxon, creating
     * a chain of nodes representing only the direct ancestors of the target.
     *
     * Example for target "Myotis lucifugus": Chiroptera -> Vespertilionidae -> Myotis
     *
     * @param Taxon $targetTaxon The taxon whose ancestors to map
     * @return TaxonomyNode The root node of the ancestor chain
     */
    private function getAncestorMap(Taxon $targetTaxon): TaxonomyNode
    {
        $currentNode = null;
        $parentTaxon = $targetTaxon->getParentTaxon();

        while (!$parentTaxon->getIsRoot()) {
            $node = TaxonomyNode::fromTaxon($parentTaxon);

            if ($currentNode) {
                $node->setChildren([$currentNode->getDisplayName() => $currentNode]);
            }

            $currentNode = $node;
            $parentTaxon = $parentTaxon->getParentTaxon();
        }

        $root = TaxonomyNode::fromTaxon($parentTaxon);
        if ($currentNode) {
            $root->setChildren([$currentNode->getDisplayName() => $currentNode]);
        }

        return $root;
    }

    /**
     * Completes the taxonomic map by incorporating the target taxon and its descendants.
     *
     * After mapping the ancestor chain, this method:
     * 1. Locates the insertion point in the ancestor chain (target's parent)
     * 2. Maps out the target's complete descendant structure
     * 3. Connects this new section to the existing map
     *
     * For example, when Myotis is the target:
     * 1. Locates Vespertilionidae in the ancestor chain
     * 2. Maps the complete Myotis structure with all species
     * 3. Connects the Myotis portion to the Vespertilionidae node
     *
     * @param TaxonomyNode $root The root of the existing map
     * @param Taxon $targetTaxon The taxon to add to the map with its descendants
     */
    private function insertTargetTaxon(TaxonomyNode $root, Taxon $targetTaxon): void
    {
        $ancestorNode = $root;

        // The direct parent of the target taxon will have no children
        while (count($ancestorNode->getChildren()) > 0) {
            $ancestorNode = array_values($ancestorNode->getChildren())[0];
        }

        $ancestorNode->setChildren([
            $targetTaxon->getDisplayName() => $this->getTaxonNodeWithAllDescendants($targetTaxon)
        ]);
    }
}

/**
 * Represents a single point in a taxonomic map.
 *
 * TaxonomyNode provides a focused representation of a Taxon entity,
 * containing only the information needed for displaying and navigating
 * the taxonomic map. This separation allows the map structure to be
 * built and traversed efficiently.
 *
 * For example, when representing a bat species:
 * - displayName: "Myotis lucifugus"
 * - id: Database identifier
 * - rank: Position in taxonomic hierarchy
 * - children: Connected nodes below this point in the map
 *
 * The node maintains only the relationships and data needed for
 * effective navigation of the taxonomic structure.
 *
 * @internal
 * @final
 */
final class TaxonomyNode
{
    public function __construct(
        private readonly string $displayName,
        public readonly int     $id,
        public readonly ?int    $rank = null,
        private array           $children = []
    )
    {
    }

    public static function fromTaxon(Taxon $taxon, array $children = []): self
    {
        return new self(
            $taxon->getDisplayName(),
            $taxon->getId(),
            $taxon->getRank()?->getOrdinal(),
            $children
        );
    }

    public function getDisplayName(): string
    {
        return $this->displayName;
    }

    public function setChildren(array $children): void
    {
        $this->children = $children;
    }

    public function getChildren(): array
    {
        return $this->children;
    }
}
