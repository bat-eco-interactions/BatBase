<?php

namespace App\Command;

use App\Service\Aggregator\WeeklyDigestManager as DigestManager;
use Psr\Log\LoggerInterface as Logger;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

// the name of the command (the part after "bin/console")
#[AsCommand(
    name: 'app:send-weekly-digest',
    description: 'Sends admins a weekly digest.',
)]
class SendWeeklyEmailDigestCommand extends Command
{
    /* ____________________ CONSTRUCT/CONFIGURE COMMAND _________________________ */
    public function __construct(private readonly Logger $logger, private readonly DigestManager $digest)
    {
        // best practices recommend to call the parent constructor first and
        // then set your own properties. That wouldn't work in this case
        // because configure() needs the properties set in this constructor
        parent::__construct();
    }

    /**
     * The configure() method is called automatically at the end of the command
     * constructor. If your command defines its own constructor, set the properties
     * first and then call to the parent constructor, to make those properties
     * available in the configure() method.
     */
    // protected function configure(): void
    // {
    // }

    /* ____________________ EXECUTE COMMAND _____________________________________ */
    /** this method must return the "exit status code". 0: success, 1: error */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln(['Creating Weekly Digest', '======================', '']);
        try {
            $data = $this->digest->sendAdminWeeklyDigestEmail();
            $output->writeln(['', 'SENT']);
            foreach ($data as $key => $value) {
                // $output->writeln([$key, print_r($value, true)]);
            }

            return Command::SUCCESS;
        } catch (\Exception $e) {
            $output->writeln(['SEND ERROR', $e->getMessage()]);
            $this->logger->error("\n\n### Error @ [{$e->getLine()}] = {$e->getMessage()}\n");

            return Command::FAILURE;
        }
    }
}
