<?php

namespace App\Mapper;

use App\Dto\CitationDto;
use App\Dto\InteractionDto;
use App\Dto\PublicationDto;
use App\Entity\Citation;
use App\Mapper\Trait\EntityToDtoUtilityTrait;
use Symfonycasts\MicroMapper\AsMapper;
use Symfonycasts\MicroMapper\MapperInterface;
use Symfonycasts\MicroMapper\MicroMapperInterface;

#[AsMapper(from: Citation::class, to: CitationDto::class)]
class CitationEntityToDtoMapper implements MapperInterface
{
    use EntityToDtoUtilityTrait;

    public function __construct(
        private readonly MicroMapperInterface $microMapper,
    )
    {
    }

    public function load(object $from, string $toClass, array $context): object
    {
        $entity = $from;

        $dto = new CitationDto();
        $dto->id = $entity->getId();

        return $dto;
    }

    public function populate(object $from, object $to, array $context): object
    {
        $citation = $from;
        $dto = $to;
        assert($citation instanceof Citation);
        assert($dto instanceof CitationDto);

        $dto->title = $citation->getDisplayName();
        $dto->fullText = $citation->getFullText();
        $dto->abstract = $citation->getAbstract();
        $dto->publicationVolume = $citation->getPublicationVolume();
        $dto->publicationIssue = $citation->getPublicationIssue();
        $dto->publicationPages = $citation->getPublicationPages();
        $dto->citationType = $this->getEntityIdentifiers($citation->getCitationType());
        // multi-step mappers
        $this->handleSourceProperties($citation, $dto);

        return $this->setDtoForensicFields($citation, $dto);
    }

    public function handleSourceProperties(Citation $citation, CitationDto $dto): void
    {
        $source = $citation->getSource();
        $dto->source = $source->getId();
        $dto->displayName = $source->getDisplayName();
        $dto->year = $source->getYear();
        $dto->doi = $source->getDoi();
        $dto->linkUrl = $source->getLinkUrl();
        $dto->isDirect = $source->getIsDirect();
        // relationships
        $dto->authors = $source->getAuthorIds();
        $dto->contributors = $source->getDtoContributors();
        $dto->interactions = $this->mapEntities($source->getInteractions(), InteractionDto::class);
        $dto->publication = $this->mapEntity($source->getParentSource()->getPublication(), PublicationDto::class);
    }
}