<?php

namespace App\Mapper;

use App\Dto\AuthorDto;
use App\Entity\Author;
use App\Entity\Source;
use App\Entity\SourceType;
use App\Repository\AuthorRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfonycasts\MicroMapper\AsMapper;
use Symfonycasts\MicroMapper\MapperInterface;

#[AsMapper(from: AuthorDto::class, to: Author::class)]
class AuthorDtoToEntityMapper implements MapperInterface
{
    public function __construct(
        private readonly AuthorRepository       $repository,
        private readonly EntityManagerInterface $em,
    )
    {
    }

    /**
     * @throws \Exception
     */
    public function load(object $from, string $toClass, array $context): object
    {
        $entity = $from->id ? $this->repository->find($from->id) : new Author();
        if (!$entity) {
            throw new \Exception('%s %d not found', $toClass, $from->id);
        }

        return $entity;
    }

    public function populate(object $from, object $to, array $context): object
    {
        $dto = $from;
        $author = $to;
        assert($dto instanceof AuthorDto);
        assert($author instanceof Author);

        // Must be called before names are set because Source->displayName is
        // updated during author displayName generation (when a name prop is set).
        $this->handleSourceProperties($dto, $author);

        $author->setFirstName($dto->firstName);
        $author->setMiddleName($dto->middleName);
        $author->setLastName($dto->lastName);
        $author->setSuffix($dto->suffix);


        return $author;
    }

    private function handleSourceProperties(AuthorDto $dto, Author $author): void
    {
        $source = $author->getSource() ?? new Source();
        // Display name will be reset when author display-name is generated.
        $source->setDisplayName($dto->lastName);
        $source->setDescription($dto->description);
        $source->setLinkDisplay($dto->linkDisplay);
        $source->setLinkUrl($dto->linkUrl);
        // relationships
        $source->setAuthor($author);
        $source->setSourceType($this->em->getRepository(SourceType::class)->findOneBy(['slug' => 'author']));

        $this->em->persist($source);
    }

}