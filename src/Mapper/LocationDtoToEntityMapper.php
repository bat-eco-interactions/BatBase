<?php

namespace App\Mapper;

use App\Dto\LocationDto;
use App\Entity\GeoJson;
use App\Entity\HabitatType;
use App\Entity\Location;
use App\Entity\LocationType;
use App\Repository\LocationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfonycasts\MicroMapper\AsMapper;
use Symfonycasts\MicroMapper\MapperInterface;

#[AsMapper(from: LocationDto::class, to: Location::class)]
readonly class LocationDtoToEntityMapper implements MapperInterface
{
    public function __construct(
        private LocationRepository     $repository,
        private EntityManagerInterface $em,
    )
    {
    }

    public function load(object $from, string $toClass, array $context): object
    {
        $entity = $from->id ? $this->repository->find($from->id) : new Location();
        if (!$entity) {
            throw new \Exception('%s %d not found', $toClass, $from->id);
        }

        return $entity;
    }

    public function populate(object $from, object $to, array $context): object
    {
        $dto = $from;
        $entity = $to;
        assert($dto instanceof LocationDto);
        assert($entity instanceof Location);

        $entity->setDisplayName($dto->displayName);
        $entity->setDescription($dto->description ?? null);
        // multi-step mappers
        $this->handleElevation($dto, $entity);
        $this->handleGeospatialData($dto, $entity);
        $this->handleParentLocation($dto, $entity);
        $this->handleLocationType($dto, $entity);
        $this->handleHabitatType($dto, $entity);

        return $entity;
    }
    /* ------------ COMPLEX MAPPERS ----------------------------------------- */

    /** @todo - handle possible validation errors:
     *      - Elev max only present with elev
     *      - Elev is lower than max
     */
    private function handleElevation(LocationDto $dto, Location &$entity): void
    {
        $entity->setElevation($dto->elevation);
        $entity->setElevationMax($dto->elevationMax);
        if ($dto->elevation || $dto->elevationMax) {
            $entity->setElevUnitAbbrv();
        }
    }


    /** @todo handle possible validation errors: invalid coords */
    private function handleGeospatialData(LocationDto $dto, Location &$entity): void
    {
        $entity->setLatitude($dto->latitude);
        $entity->setLongitude($dto->longitude);

        if ($dto->latitude && $dto->longitude) {
            $geoJson = $this->getLocationGeoJson($dto->latitude, $dto->longitude, $entity->getGeoJson());
            $entity->setGeoJson($geoJson);
        } else {
            if ($dto->latitude || $dto->longitude) {
                // todo: handle validation error
            } else {
                $entity->setGeoJson(null);
            }
        }
    }

    /**
     * @todo - handle possible validation errors:
     *     - invalid coords
     *     - mismatch between location type and geoJson type
     */
    private function getLocationGeoJson(float $latitude, float $longitude, ?GeoJson $geoJson): GeoJson
    {
        $displayPoint = "[$longitude, $latitude]";

        $geoJson = $geoJson ?? new GeoJson();
        $geoJson->setDisplayPoint($displayPoint);
        $geoJson->setCoordinates($displayPoint);
        //@todo - Why were the previous coordinated being preserved in client side processing? see set-property.js(getCoordValue)
        $geoJson->setType('Point');

        return $geoJson;
    }

    private function handleParentLocation(LocationDto $dto, Location &$entity): void
    {
        if ($dto->parentId) {
            $parentLocation = $this->em->find(Location::class, $dto->parentId);
            // @todo - assert $parentLocation !== null
            $entity->setParentLocation($parentLocation);
        } else {
            // @todo - all modifiable Location's should have parent
        }
    }

    private function handleLocationType(LocationDto $dto, Location &$entity): void
    {
        if ($dto->locationTypeId) {
            $locType = $this->em->find(LocationType::class, $dto->locationTypeId);
            $entity->setLocationType($locType);
        }
    }

    private function handleHabitatType(LocationDto $dto, Location &$entity): void
    {
        if ($dto->habitatTypeId) {
            $habitatType = $this->em->find(HabitatType::class, $dto->habitatTypeId);
            $entity->setHabitatType($habitatType);
        }
    }
}