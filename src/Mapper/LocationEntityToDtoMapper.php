<?php

namespace App\Mapper;

use App\Dto\GeoJsonDto;
use App\Dto\InteractionDto;
use App\Dto\LocationDto;
use App\Entity\Location;
use App\Mapper\Trait\EntityToDtoUtilityTrait;
use Symfonycasts\MicroMapper\AsMapper;
use Symfonycasts\MicroMapper\MapperInterface;
use Symfonycasts\MicroMapper\MicroMapperInterface;

#[AsMapper(from: Location::class, to: LocationDto::class)]
class LocationEntityToDtoMapper implements MapperInterface
{
    use EntityToDtoUtilityTrait;

    public function __construct(
        private readonly MicroMapperInterface $microMapper,
    )
    {
    }

    public function load(object $from, string $toClass, array $context): object
    {
        $entity = $from;

        $dto = new LocationDto();
        $dto->id = $entity->getId();

        return $dto;
    }

    public function populate(object $from, object $to, array $context): object
    {
        $entity = $from;
        $dto = $to;
        assert($entity instanceof Location);
        assert($dto instanceof LocationDto);

        $dto->displayName = $entity->getDisplayName();
        $dto->description = $entity->getDescription();
        $dto->isoCode = $entity->getIsoCode();
        $dto->elevation = $entity->getElevation();
        $dto->elevationMax = $entity->getElevationMax();
        $dto->elevUnitAbbrv = $entity->getElevUnitAbbrv() || 'm';
        $dto->latitude = $entity->getLatitude();
        $dto->longitude = $entity->getLongitude();
        // relationships
        $dto->parent = $this->mapEntity($entity->getParentLocation(), LocationDto::class);
        $dto->children = $this->mapEntities($entity->getChildLocs(), LocationDto::class);
        $dto->locationType = $this->getEntityIdentifiers($entity->getLocationType());
        $dto->habitatType = $this->getEntityIdentifiers($entity->getHabitatType());
        $dto->geoJson = $this->getGeoJson($entity);
        $dto->interactions = $this->mapEntities($entity->getInteractions(), InteractionDto::class);

        return $this->setDtoForensicFields($entity, $dto);
    }

    /**
     * Returns the geoJson for the location. If location represents a habitat of
     * a parent location, the parent's geoJson is returned.
     *
     * @param Location $entity
     * @return array|null
     */
    public function getGeoJson(Location $entity): ?GeoJsonDto
    {
        $geoJson = $entity->getGeoJson();
        if (!$geoJson && $entity->isHabitat()) {
            $geoJson = $entity->getParentLocation()->getGeoJson();
        }

        return $geoJson ? $this->microMapper->map($geoJson, GeoJsonDto::class) : null;
    }
}