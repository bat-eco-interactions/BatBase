<?php

namespace App\Mapper;

use App\Dto\GeoJsonDto;
use App\Entity\GeoJson;
use Symfonycasts\MicroMapper\AsMapper;
use Symfonycasts\MicroMapper\MapperInterface;

#[AsMapper(from: GeoJson::class, to: GeoJsonDto::class)]
class GeoJsonEntityToDtoMapper implements MapperInterface
{
    public function load(object $from, string $toClass, array $context): object
    {
        $entity = $from;

        $dto = new GeoJsonDto();
        $dto->id = $entity->getId();

        return $dto;
    }

    public function populate(object $from, object $to, array $context): object
    {
        $entity = $from;
        $dto = $to;
        assert($entity instanceof GeoJson);
        assert($dto instanceof GeoJsonDto);

        $dto->type = $entity->getType();
        $dto->coordinates = $entity->getCoordinates();
        $dto->displayPoint = $entity->getDisplayPoint();

        return $dto;
    }
}