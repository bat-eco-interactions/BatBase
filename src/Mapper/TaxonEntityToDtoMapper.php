<?php

namespace App\Mapper;

use App\Dto\InteractionDto;
use App\Dto\TaxonDto;
use App\Entity\Taxon;
use App\Mapper\Trait\EntityToDtoUtilityTrait;
use Symfonycasts\MicroMapper\AsMapper;
use Symfonycasts\MicroMapper\MapperInterface;
use Symfonycasts\MicroMapper\MicroMapperInterface;

#[AsMapper(from: Taxon::class, to: TaxonDto::class)]
class TaxonEntityToDtoMapper implements MapperInterface
{
    use EntityToDtoUtilityTrait;

    public function __construct(
        private readonly MicroMapperInterface $microMapper,
    )
    {
    }

    public function load(object $from, string $toClass, array $context): object
    {
        $taxon = $from;

        $dto = new TaxonDto();
        $dto->id = $taxon->getId();

        return $dto;
    }

    public function populate(object $from, object $to, array $context): object
    {
        $taxon = $from;
        $dto = $to;
        assert($taxon instanceof Taxon);
        assert($dto instanceof TaxonDto);

        $dto->displayName = $taxon->getDisplayName();
        $dto->name = $taxon->getName();
        $dto->isRoot = $taxon->getIsRoot();
        // relationships
        $dto->parent = $this->mapEntity($taxon->getParentTaxon(), TaxonDto::class);
        $dto->children = $this->mapEntities($taxon->getChildTaxa(), TaxonDto::class);
        $dto->rank = $this->getEntityIdentifiers($taxon->getRank());
        $dto->group = $taxon->getGroupData();
        $dto->subjectRoles = $this->mapEntities($taxon->getRoleInteractions('subject'), InteractionDto::class);
        $dto->objectRoles = $this->mapEntities($taxon->getRoleInteractions('object'), InteractionDto::class);

        return $this->setDtoForensicFields($taxon, $dto);
    }
}