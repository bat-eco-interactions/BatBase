<?php

namespace App\Mapper\Trait;

use App\Dto\UserDto;
use Symfonycasts\MicroMapper\MicroMapperInterface;

trait EntityForensicsToDtoTrait
{
    public function setDtoForensicFields(object $entity, object $dto): object
    {
        $dto->created = $entity->getCreated()->format('Y-m-d H:i:s');

        $dto->createdBy = $this->microMapper->map($entity->getCreatedBy(), UserDto::class, [
            MicroMapperInterface::MAX_DEPTH => 0
        ]);

        $dto->updated = $entity->getUpdated()?->format('Y-m-d H:i:s');

        $dto->updatedBy = !$entity->getUpdatedBy() ? null :
            $this->microMapper->map($entity->getUpdatedBy(), UserDto::class, [
                MicroMapperInterface::MAX_DEPTH => 0
            ]);

        $dto->reviewed = $entity->getReviewed()?->format('Y-m-d H:i:s');

        $dto->reviewedBy = !$entity->getReviewedBy() ? null :
            $this->microMapper->map($entity->getReviewedBy(), UserDto::class, [
                MicroMapperInterface::MAX_DEPTH => 0
            ]);

        return $dto;
    }
}