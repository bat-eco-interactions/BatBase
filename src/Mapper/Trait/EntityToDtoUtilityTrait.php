<?php

namespace App\Mapper\Trait;

use Doctrine\Common\Collections\Collection;
use Symfonycasts\MicroMapper\MicroMapperInterface;

trait EntityToDtoUtilityTrait
{
    use EntityForensicsToDtoTrait;

    public function __construct(
        private readonly MicroMapperInterface $microMapper,
    )
    {
    }

    public function mapEntities(Collection|array $entities, $classname): array
    {
        return array_map(
            fn(object $entity) => $this->mapEntity($entity, $classname),
            gettype($entities) === "array" ? $entities : $entities->getValues()
        );

    }

    public function mapEntity(?object $entity, string $classname): ?object
    {
        if (!$entity) return null;
        return $this->microMapper->map($entity, $classname, [
            MicroMapperInterface::MAX_DEPTH => 0
        ]);
    }

    public function getEntityIdentifiers(?object $entity): ?array
    {
        if (!$entity) return null;

        return [
            'id' => $entity->getId(),
            'displayName' => $entity->getDisplayName(),
        ];
    }
}