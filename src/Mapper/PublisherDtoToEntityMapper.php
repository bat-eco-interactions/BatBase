<?php

namespace App\Mapper;

use App\Dto\PublisherDto;
use App\Entity\Publisher;
use App\Entity\Source;
use App\Entity\SourceType;
use App\Repository\PublisherRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfonycasts\MicroMapper\AsMapper;
use Symfonycasts\MicroMapper\MapperInterface;

#[AsMapper(from: PublisherDto::class, to: Publisher::class)]
class PublisherDtoToEntityMapper implements MapperInterface
{
    public function __construct(
        private readonly PublisherRepository    $repository,
        private readonly EntityManagerInterface $em,
    )
    {
    }

    /**
     * @throws \Exception
     */
    public function load(object $from, string $toClass, array $context): object
    {
        $entity = $from->id ? $this->repository->find($from->id) : new Publisher();
        if (!$entity) {
            throw new \Exception('%s %d not found', $toClass, $from->id);
        }

        return $entity;
    }

    public function populate(object $from, object $to, array $context): object
    {
        $dto = $from;
        $publisher = $to;
        assert($dto instanceof PublisherDto);
        assert($publisher instanceof Publisher);

        $publisher->setDisplayName($dto->displayName);
        $publisher->setCity($dto->city);
        $publisher->setCountry($dto->country);

        $this->handleSourceProperties($dto, $publisher);

        return $publisher;
    }

    private function handleSourceProperties(PublisherDto $dto, Publisher $publisher): void
    {
        $source = $publisher->getSource() ?? new Source();
        $source->setDisplayName($publisher->getDisplayName());
        $source->setDescription($dto->description);
        $source->setLinkDisplay($dto->linkDisplay);
        $source->setLinkUrl($dto->linkUrl);
        // relationships
        $source->setPublisher($publisher);
        $source->setSourceType($this->em->getRepository(SourceType::class)->findOneBy(['slug' => 'publisher']));

        $this->em->persist($source);
    }

}