<?php

namespace App\Mapper;

use App\Dto\PublicationDto;
use App\Entity\Publication;
use App\Entity\PublicationType;
use App\Entity\Publisher;
use App\Entity\Source;
use App\Entity\SourceType;
use App\Repository\PublicationRepository;
use App\Service\DataEntry\ContributionEntry;
use Doctrine\ORM\EntityManagerInterface;
use Symfonycasts\MicroMapper\AsMapper;
use Symfonycasts\MicroMapper\MapperInterface;

#[AsMapper(from: PublicationDto::class, to: Publication::class)]
readonly class PublicationDtoToEntityMapper implements MapperInterface
{
    public function __construct(
        private PublicationRepository  $repository,
        private EntityManagerInterface $em,
        private ContributionEntry      $contributionEntry,
    )
    {
    }

    /**
     * @throws \Exception
     */
    public function load(object $from, string $toClass, array $context): object
    {
        $entity = $from->id ? $this->repository->find($from->id) : new Publication();
        if (!$entity) {
            throw new \Exception('%s %d not found', $toClass, $from->id);
        }

        return $entity;
    }

    public function populate(object $from, object $to, array $context): object
    {
        $dto = $from;
        $entity = $to;
        assert($dto instanceof PublicationDto);
        assert($entity instanceof Publication);

        $entity->setDisplayName($dto->displayName);
        $this->handlePublicationType($dto, $entity);

        $this->setSourceProperties($dto, $entity);

        return $entity;
    }

    private function handlePublicationType(PublicationDto $dto, Publication $publication): void
    {
        if ($dto->publicationTypeId) {
            $type = $this->em->find(PublicationType::class, $dto->publicationTypeId);
            $publication->setPublicationType($type);
        }
    }

    private function setSourceProperties(PublicationDto $dto, Publication $publication): void
    {
        $source = $publication->getSource() ?? new Source();

        $source->setDisplayName($dto->displayName);
        $source->setDescription($dto->description);
        $source->setYear($dto->year);
        $source->setDoi($dto->doi);
        $source->setLinkUrl($dto->linkUrl);
        // relationships
        $source->setPublication($publication);
        $source->setSourceType($this->em->getRepository(SourceType::class)->findOneBy(['slug' => 'publication']));

        if ($dto->publisherId) {
            $publisher = $this->em->find(Publisher::class, $dto->publisherId);
            $source->setParentSource($publisher->getSource());
        } else {
            $source->setParentSource();
        }

        $this->em->persist($source); // Must be persisted before contributions are handled to prevent error with contribution persist

        if ($dto->contributors) {
            $this->contributionEntry->setContributors($dto->contributors, $source);
        }
    }

}