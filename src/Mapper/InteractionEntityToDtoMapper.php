<?php

namespace App\Mapper;

use App\Dto\CitationDto;
use App\Dto\InteractionDto;
use App\Dto\LocationDto;
use App\Dto\TaxonDto;
use App\Entity\Interaction;
use App\Mapper\Trait\EntityToDtoUtilityTrait;
use Symfonycasts\MicroMapper\AsMapper;
use Symfonycasts\MicroMapper\MapperInterface;
use Symfonycasts\MicroMapper\MicroMapperInterface;

#[AsMapper(from: Interaction::class, to: InteractionDto::class)]
class InteractionEntityToDtoMapper implements MapperInterface
{
    use EntityToDtoUtilityTrait;

    public function __construct(
        private readonly MicroMapperInterface $microMapper,
    )
    {
    }

    public function load(object $from, string $toClass, array $context): object
    {
        $entity = $from;

        $dto = new InteractionDto();
        $dto->id = $entity->getId();

        return $dto;
    }

    public function populate(object $from, object $to, array $context): object
    {
        $entity = $from;
        $dto = $to;
        assert($entity instanceof Interaction);
        assert($dto instanceof InteractionDto);

        $dto->date = $entity->getDate();
        $dto->note = $entity->getNote();
        $dto->pages = $entity->getPages();
        $dto->quote = $entity->getQuote();
        // relationships
        $dto->interactionType = $this->getEntityIdentifiers($entity->getInteractionType());
        $dto->citation = $this->mapEntity($entity->getSource()->getCitation(), CitationDto::class);
        $dto->location = $this->mapEntity($entity->getLocation(), LocationDto::class);
        $dto->tags = $entity->getTagData();
        // multi-step mappers
        $this->setSubjectData($entity, $dto);
        $this->setObjectData($entity, $dto);

        return $this->setDtoForensicFields($entity, $dto);
    }

    function setSubjectData(Interaction $entity, InteractionDto $dto): void
    {
        $taxon = $entity->getSubject();
        $dto->subject = $this->mapEntity($taxon, TaxonDto::class);
        $dto->subjectGroupId = $taxon->getGroupId();
    }

    function setObjectData(Interaction $entity, InteractionDto $dto): void
    {
        $taxon = $entity->getObject();
        $dto->object = $this->mapEntity($taxon, TaxonDto::class);
        $dto->objectGroupId = $taxon->getGroupId();
    }
}