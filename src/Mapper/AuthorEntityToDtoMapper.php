<?php

namespace App\Mapper;

use App\Dto\AuthorDto;
use App\Entity\Author;
use App\Mapper\Trait\EntityToDtoUtilityTrait;
use Doctrine\ORM\EntityManagerInterface;
use Symfonycasts\MicroMapper\AsMapper;
use Symfonycasts\MicroMapper\MapperInterface;
use Symfonycasts\MicroMapper\MicroMapperInterface;

#[AsMapper(from: Author::class, to: AuthorDto::class)]
class AuthorEntityToDtoMapper implements MapperInterface
{
    use EntityToDtoUtilityTrait;

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly MicroMapperInterface   $microMapper,
    )
    {
    }

    public function load(object $from, string $toClass, array $context): object
    {
        $entity = $from;

        $dto = new AuthorDto();
        $dto->id = $entity->getId();

        return $dto;
    }

    public function populate(object $from, object $to, array $context): object
    {
        $author = $from;
        $dto = $to;
        assert($author instanceof Author);
        assert($dto instanceof AuthorDto);

        $dto->firstName = $author->getFirstName();
        $dto->lastName = $author->getLastName();
        $dto->middleName = $author->getMiddleName();
        $dto->suffix = $author->getSuffix();
        $dto->displayName = $author->getDisplayName();
        $dto->fullName = $author->getFullName();
        // multi-step mappers
        $this->handleSourceProperties($author, $dto);

        return $this->setDtoForensicFields($author, $dto);
    }

    public function handleSourceProperties(Author $author, AuthorDto $dto): void
    {
        $source = $author->getSource();
        $dto->source = $source->getId();
        $dto->description = $source->getDescription();
        $dto->linkUrl = $source->getLinkUrl();
        $dto->linkDisplay = $source->getLinkDisplay();
    }

}