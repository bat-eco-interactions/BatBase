<?php

namespace App\Mapper;

use App\Dto\PublisherDto;
use App\Entity\Publisher;
use App\Mapper\Trait\EntityToDtoUtilityTrait;
use Doctrine\ORM\EntityManagerInterface;
use Symfonycasts\MicroMapper\AsMapper;
use Symfonycasts\MicroMapper\MapperInterface;
use Symfonycasts\MicroMapper\MicroMapperInterface;

#[AsMapper(from: Publisher::class, to: PublisherDto::class)]
class PublisherEntityToDtoMapper implements MapperInterface
{
    use EntityToDtoUtilityTrait;

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly MicroMapperInterface   $microMapper,
    )
    {
    }

    public function load(object $from, string $toClass, array $context): object
    {
        $entity = $from;

        $dto = new PublisherDto();
        $dto->id = $entity->getId();

        return $dto;
    }

    public function populate(object $from, object $to, array $context): object
    {
        $publisher = $from;
        $dto = $to;
        assert($publisher instanceof Publisher);
        assert($dto instanceof PublisherDto);

        $dto->displayName = $publisher->getDisplayName();
        $dto->city = $publisher->getCity();
        $dto->country = $publisher->getCountry();
        // multi-step mappers
        $this->handleSourceProperties($publisher, $dto);

        return $this->setDtoForensicFields($publisher, $dto);
    }


    public function handleSourceProperties(Publisher $publisher, PublisherDto $dto): void
    {
        $source = $publisher->getSource();
        $dto->source = $source->getId();
        $dto->description = $source->getDescription();
        $dto->linkUrl = $source->getLinkUrl();
        $dto->linkDisplay = $source->getLinkDisplay();
    }
}