<?php

namespace App\Mapper;

use App\Dto\TaxonDto;
use App\Entity\Rank;
use App\Entity\Taxon;
use App\Repository\TaxonRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfonycasts\MicroMapper\AsMapper;
use Symfonycasts\MicroMapper\MapperInterface;

#[AsMapper(from: TaxonDto::class, to: Taxon::class)]
readonly class TaxonDtoToEntityMapper implements MapperInterface
{
    public function __construct(
        private TaxonRepository        $repository,
        private EntityManagerInterface $em,
    )
    {
    }

    /**
     * @throws \Exception
     */
    public function load(object $from, string $toClass, array $context): object
    {
        $taxon = $from->id ? $this->repository->find($from->id) : new Taxon();
        if (!$taxon) {
            throw new \Exception('%s %d not found', $toClass, $from->id);
        }

        return $taxon;
    }

    public function populate(object $from, object $to, array $context): object
    {
        $dto = $from;
        $taxon = $to;
        assert($dto instanceof TaxonDto);
        assert($taxon instanceof Taxon);

        $taxon->setName($dto->name);
        // multi-step handlers
        $this->handleParentTaxon($dto, $taxon);
        $this->handleRank($dto, $taxon);

        return $taxon;
    }

    function handleParentTaxon(TaxonDto $dto, Taxon $taxon): void
    {
        if ($dto->parentId === null) { // no changes
            return;
        }
        $parent = $this->repository->find($dto->parentId);
        $taxon->setParentTaxon($parent);
    }

    function handleRank(TaxonDto $dto, Taxon $taxon): void
    {
        if ($dto->rankId === null) { // no changes
            return;
        }
        $rank = $this->em->find(Rank::class, $dto->rankId);
        $taxon->setRank($rank);
    }
}
