<?php

namespace App\Mapper;

use App\Dto\PublicationDto;
use App\Dto\PublisherDto;
use App\Entity\Publication;
use App\Mapper\Trait\EntityToDtoUtilityTrait;
use Symfonycasts\MicroMapper\AsMapper;
use Symfonycasts\MicroMapper\MapperInterface;
use Symfonycasts\MicroMapper\MicroMapperInterface;

#[AsMapper(from: Publication::class, to: PublicationDto::class)]
class PublicationEntityToDtoMapper implements MapperInterface
{
    use EntityToDtoUtilityTrait;

    public function __construct(
        private readonly MicroMapperInterface $microMapper,
    )
    {
    }

    public function load(object $from, string $toClass, array $context): object
    {
        $entity = $from;

        $dto = new PublicationDto();
        $dto->id = $entity->getId();

        return $dto;
    }

    public function populate(object $from, object $to, array $context): object
    {
        $publication = $from;
        $dto = $to;
        assert($publication instanceof Publication);
        assert($dto instanceof PublicationDto);

        $dto->displayName = $publication->getDisplayName();
        $dto->publicationType = $this->getEntityIdentifiers($publication->getPublicationType());
        // multi-step mappers
        $this->handleSourceProperties($publication, $dto);

        return $this->setDtoForensicFields($publication, $dto);
    }

    public function handleSourceProperties(Publication $publication, PublicationDto $dto): void
    {
        $source = $publication->getSource();
        $dto->source = $source->getId();
        $dto->year = $source->getYear();
        $dto->doi = $source->getDoi();
        $dto->description = $source->getDescription();
        $dto->linkUrl = $source->getLinkUrl();
        $dto->authors = $source->getAuthorIds();
        $dto->editors = $source->getEditorIds();
        $dto->contributors = $source->getDtoContributors();
        $dto->publisher = $this->mapEntity($source->getParentSource()?->getPublisher(), PublisherDto::class);
    }
}