<?php

namespace App\Mapper;

use App\Dto\InteractionDto;
use App\Entity\Citation;
use App\Entity\Interaction;
use App\Entity\InteractionType;
use App\Entity\Location;
use App\Entity\Taxon;
use App\Repository\InteractionRepository;
use App\Service\DataEntry\TagEntry;
use Doctrine\ORM\EntityManagerInterface;
use Symfonycasts\MicroMapper\AsMapper;
use Symfonycasts\MicroMapper\MapperInterface;

#[AsMapper(from: InteractionDto::class, to: Interaction::class)]
class InteractionDtoToEntityMapper implements MapperInterface
{
    public function __construct(
        private readonly InteractionRepository  $repository,
        private readonly EntityManagerInterface $em,
        private readonly TagEntry               $tagEntry,
    )
    {
    }

    /**
     * @throws \Exception
     */
    public function load(object $from, string $toClass, array $context): object
    {
        $entity = $from->id ? $this->repository->find($from->id) : new Interaction();
        if (!$entity) {
            throw new \Exception('%s %d not found', $toClass, $from->id);
        }

        return $entity;
    }

    public function populate(object $from, object $to, array $context): object
    {
        $dto = $from;
        $entity = $to;
        assert($dto instanceof InteractionDto);
        assert($entity instanceof Interaction);

        $entity->setDate($dto->date);
        $entity->setNote($dto->note);
        $entity->setPages($dto->pages);
        $entity->setQuote($dto->quote);
        // multi-step mappers
        $this->handleInteractionType($dto, $entity);
        $this->handleSourceCitation($dto, $entity);
        $this->handleLocation($dto, $entity, $context['operation']);
        $this->handleTaxa($dto, $entity);
        $this->handleTags($dto, $entity);

        return $entity;
    }

    /* ------------ COMPLEX MAPPERS ----------------------------------------- */
    private function handleInteractionType(InteractionDto $dto, Interaction $entity): void
    {
        if ($dto->interactionTypeId) {
            $type = $this->em->find(InteractionType::class, $dto->interactionTypeId);
            $entity->setInteractionType($type);
        }
    }

    private function handleSourceCitation(InteractionDto $dto, Interaction $entity): void
    {
        if ($dto->citationId) {
            $citation = $this->em->find(Citation::class, $dto->citationId);
            $source = $citation->getSource();
            $entity->setSource($source);
            $source->setIsDirect(true);
        }
    }

    private function handleLocation(InteractionDto $dto, Interaction $entity, string $operation): void
    {
        if ($dto->locationId) {
            $loc = $this->em->find(Location::class, $dto->locationId);
        } else if ($dto->locationId === false || $operation === 'POST') {
            $loc = $this->em->getRepository(Location::class)->findOneBy(['displayName' => 'Unspecified']);
        } else {
            return;
        }
        $entity->setLocation($loc);
    }

    private function handleTaxa(InteractionDto $dto, Interaction $entity): void
    {
        if ($dto->subjectId) {
            $taxon = $this->em->find(Taxon::class, $dto->subjectId);
            $entity->setSubject($taxon);
        }
        if ($dto->objectId) {
            $taxon = $this->em->find(Taxon::class, $dto->objectId);
            $entity->setObject($taxon);
        }
    }

    public function handleTags(InteractionDto $dto, Interaction &$entity): void
    {
        if ($dto->tagIds) {
            $tags = $dto->tagIds;
            $this->tagEntry->handleTags($tags, $entity);
        }
    }
}