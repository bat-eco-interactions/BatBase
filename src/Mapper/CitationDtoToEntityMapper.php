<?php

namespace App\Mapper;

use App\Dto\CitationDto;
use App\Entity\Citation;
use App\Entity\CitationType;
use App\Entity\Publication;
use App\Entity\Source;
use App\Entity\SourceType;
use App\Repository\CitationRepository;
use App\Service\DataEntry\ContributionEntry;
use Doctrine\ORM\EntityManagerInterface;
use Symfonycasts\MicroMapper\AsMapper;
use Symfonycasts\MicroMapper\MapperInterface;

#[AsMapper(from: CitationDto::class, to: Citation::class)]
readonly class CitationDtoToEntityMapper implements MapperInterface
{
    public function __construct(
        private CitationRepository     $repository,
        private EntityManagerInterface $em,
        private ContributionEntry      $contributionEntry,
    )
    {
    }

    /**
     * @throws \Exception
     */
    public function load(object $from, string $toClass, array $context): object
    {
        $entity = $from->id ? $this->repository->find($from->id) : new Citation();
        if (!$entity) {
            throw new \Exception('%s %d not found', $toClass, $from->id);
        }

        return $entity;
    }

    public function populate(object $from, object $to, array $context): object
    {
        $dto = $from;
        $entity = $to;
        assert($dto instanceof CitationDto);
        assert($entity instanceof Citation);

        $entity->setDisplayName($dto->title);
        $entity->setTitle($dto->title);
        $entity->setFullText($dto->fullText);
        $entity->setAbstract($dto->abstract);
        $entity->setPublicationVolume($dto->publicationVolume);
        $entity->setPublicationIssue($dto->publicationIssue);
        $entity->setPublicationPages($dto->publicationPages);
        // multi-step mappers
        $this->handleCitationType($dto, $entity);
        $this->setSourceProperties($dto, $entity);

        return $entity;
    }

    private function handleCitationType(CitationDto $dto, Citation $entity): void
    {
        if ($dto->citationTypeId) {
            $type = $this->em->find(CitationType::class, $dto->citationTypeId);
            $entity->setCitationType($type);
        }
    }

    private function setSourceProperties(CitationDto $dto, Citation $citation): void
    {
        $source = $citation->getSource() ?? new Source();

        $source->setDisplayName($dto->title);
        $source->setYear($dto->year);
        $source->setDoi($dto->doi);
        $source->setLinkUrl($dto->linkUrl);
        // relationships
        $source->setCitation($citation);
        $source->setSourceType($this->em->getRepository(SourceType::class)->findOneBy(['slug' => 'citation']));

        if ($dto->publicationId) {
            $publication = $this->em->find(Publication::class, $dto->publicationId);
            $source->setParentSource($publication->getSource());
            // When an entire work is cited, the source display name is made unique here.
            if ($publication->getDisplayName() === $citation->getTitle()) {
                $uniqName = $citation->getTitle() . '(citation)';
                $source->setDisplayName($uniqName);
            }
        }

        $this->em->persist($source); // Must be persisted before contributions are handled to prevent error with contribution persist

        if ($dto->contributors) {
            $this->contributionEntry->setContributors($dto->contributors, $source);
        }
    }
}