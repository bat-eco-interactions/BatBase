<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class EnvVarExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_env', $this->getEnvironmentVariable(...)),
        ];
    }

    /**
     * Return the value of the requested environment variable.
     *
     * @param string $varname
     */
    public function getEnvironmentVariable($varname): string
    {
        return $_ENV[$varname];
    }
}
