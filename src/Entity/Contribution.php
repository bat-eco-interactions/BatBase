<?php

namespace App\Entity;

use App\Entity\Trait\BlameableTimestampableTrait;
use App\Entity\Trait\IdentifiableTrait;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\UniqueConstraint(name: 'unq_contrib', columns: ['work_src_id', 'auth_src_id'])]
class Contribution implements \Stringable
{
    use BlameableTimestampableTrait;
    use IdentifiableTrait;

    /** citedAs - not currently used */
    #[ORM\Column(nullable: true)]
    private ?string $citedAs = null;

    /** isEditor */
    #[ORM\Column(nullable: true)]
    private ?bool $isEditor = null;

    /** ord */
    #[ORM\Column]
    private ?int $ord = null;

    /** workSource - Source Publication */
    #[ORM\ManyToOne(inversedBy: 'contributors')]
    #[ORM\JoinColumn(name: 'work_src_id', nullable: false)]
    private ?Source $workSource = null;

    /** authorSource - Source Author.*/
    #[ORM\ManyToOne(inversedBy: 'contributions')]
    #[ORM\JoinColumn(name: 'auth_src_id', nullable: false)]
    private ?Source $authorSource = null;

    /**
     * setCitedAs
     *
     * @param string|null $citedAs
     * @return Contribution
     */
    public function setCitedAs(?string $citedAs): self
    {
        $this->citedAs = $citedAs;

        return $this;
    }

    /**
     * getCitedAs
     *
     * @return string|null
     */
    public function getCitedAs(): ?string
    {
        return $this->citedAs;
    }

    /**
     * setIsEditor
     *
     * @param boolean $isEditor
     * @return Contribution
     */
    public function setIsEditor(bool $isEditor): self
    {
        $this->isEditor = $isEditor;

        return $this;
    }

    /**
     * getIsEditor
     *
     * @return boolean
     */
    public function getIsEditor(): bool
    {
        return $this->isEditor || false;
    }

    /**
     * setOrd
     *
     * @param int $ord
     * @return Contribution
     */
    public function setOrd(int $ord): self
    {
        $this->ord = $ord;

        return $this;
    }

    /**
     * getOrd
     *
     * @return int
     */
    public function getOrd(): int
    {
        return $this->ord;
    }

    /**
     * setWorkSource
     *
     * @param Source $workSource
     * @return Contribution
     */
    public function setWorkSource(Source $workSource): self
    {
        $this->workSource = $workSource;

        return $this;
    }

    /**
     * getWorkSource
     *
     * @return Source
     */
    public function getWorkSource(): Source
    {
        return $this->workSource;
    }

    /**
     * setAuthorSource
     *
     * @param Source $authorSource
     * @return Contribution
     */
    public function setAuthorSource(Source $authorSource): self
    {
        $prev = $this->authorSource;
        $prev?->removeContribution($this);

        $this->authorSource = $authorSource;
        $authorSource->addContribution($this);

        return $this;
    }

    /**
     * getAuthorSource
     *
     * @return Source
     */
    public function getAuthorSource(): Source
    {
        return $this->authorSource;
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        $citedAs = $this->getCitedAs();
        if (!is_null($citedAs)) {
            return $citedAs;
        } else {
            return $this->getAuthorSource() . ' - ' . $this->getWorkSource();
        }
    }
}
