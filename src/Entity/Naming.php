<?php

namespace App\Entity;

use App\Entity\Trait\BlameableTimestampableTrait;
use App\Entity\Trait\IdentifiableTrait;
use App\Entity\Trait\SoftDeletableTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', hardDelete: true)]
#[ORM\Entity]
class Naming implements \Stringable
{
    use IdentifiableTrait;
    use BlameableTimestampableTrait;
    use SoftDeletableTrait;

    /** fromDate */
    #[ORM\Column(nullable: true)]
    private ?\DateTime $fromDate = null;

    /** guid */
    #[ORM\Column(nullable: true)]
    private ?string $guid = null;

    /** toDate */
    #[ORM\Column(nullable: true)]
    private ?\DateTime $toDate = null;

    /** taxon */
    #[ORM\ManyToOne(inversedBy: 'namings')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Taxon $taxon = null;

    /** taxonym */
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?Taxonym $taxonym = null;

    /** namingType */
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?NamingType $namingType = null;

    /** authority */
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?Authority $authority = null;

    /** parentTaxon */
    #[ORM\ManyToOne(inversedBy: 'childNamings')]
    #[ORM\JoinColumn]
    private ?Taxon $parentTaxon = null;

    /**
     * setFromDate
     *
     * @param \DateTime|null $fromDate
     * @return Naming
     */
    public function setFromDate(?\DateTime $fromDate): self
    {
        $this->fromDate = $fromDate;

        return $this;
    }

    /**
     * getFromDate
     *
     * @return \DateTime|null
     */
    public function getFromDate(): ?\DateTime
    {
        return $this->fromDate;
    }

    /**
     * setGuid
     *
     * @param string|null $guid
     * @return Naming
     */
    public function setGuid(?string $guid): self
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * getGuid
     *
     * @return string|null
     */
    public function getGuid(): ?string
    {
        return $this->guid;
    }

    /**
     * setToDate
     *
     * @param \DateTime $toDate
     * @return Naming
     */
    public function setToDate(\DateTime $toDate): self
    {
        $this->toDate = $toDate;

        return $this;
    }

    /**
     * getToDate
     *
     * @return \DateTime|null
     */
    public function getToDate(): ?\DateTime
    {
        return $this->toDate;
    }

    /**
     * setTaxon
     *
     * @param Taxon|null $taxon
     * @return Naming
     */
    public function setTaxon(?Taxon $taxon = null): self
    {
        $this->taxon = $taxon;

        return $this;
    }

    /**
     * getTaxon
     *
     * @return Taxon|null
     */
    public function getTaxon(): ?Taxon
    {
        return $this->taxon;
    }

    /**
     * setTaxonym
     *
     * @param Taxonym|null $taxonym
     * @return Naming
     */
    public function setTaxonym(?Taxonym $taxonym = null): self
    {
        $this->taxonym = $taxonym;

        return $this;
    }

    /**
     * getTaxonym
     * @return Taxonym|null
     */
    public function getTaxonym(): ?Taxonym
    {
        return $this->taxonym;
    }

    /**
     * setNamingType
     *
     * @param NamingType|null $namingType
     * @return Naming
     */
    public function setNamingType(?NamingType $namingType = null): self
    {
        $this->namingType = $namingType;

        return $this;
    }

    /**
     * getNamingType
     *
     * @return NamingType|null
     */
    public function getNamingType(): ?NamingType
    {
        return $this->namingType;
    }

    /**
     * setAuthority
     *
     * @param Authority|null $authority
     * @return Naming
     */
    public function setAuthority(?Authority $authority = null): self
    {
        $this->authority = $authority;

        return $this;
    }

    /**
     * getAuthority
     *
     * @return Authority|null
     */
    public function getAuthority(): ?Authority
    {
        return $this->authority;
    }

    /**
     * setParentTaxon
     *
     * @param Taxon|null $parentTaxon
     * @return Naming
     */
    public function setParentTaxon(?Taxon $parentTaxon = null): self
    {
        $this->parentTaxon = $parentTaxon;

        return $this;
    }

    /**
     * getParentTaxon
     *
     * @return Taxon|null
     */
    public function getParentTaxon(): ?Taxon
    {
        return $this->parentTaxon;
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        $auth_name = $this->getAuthority()->getName();

        return $this->getTaxonym()->getName() . ' according to ' . $auth_name;
    }
}
