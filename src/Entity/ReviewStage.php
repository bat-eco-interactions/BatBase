<?php

namespace App\Entity;

use App\Entity\Trait\DescribableTrait;
use App\Entity\Trait\IdentifiableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\Ignore;

#[Groups(['normal'])]
#[ORM\Entity]
class ReviewStage implements \Stringable
{
    use IdentifiableTrait;
    use DescribableTrait;

    /** passiveForm */
    #[ORM\Column]
    private ?string $passiveForm = null;

    /** activeForm */
    #[ORM\Column]
    private ?string $activeForm = null;

    /**
     * reviewEntries
     *
     * @var Collection<int, ReviewEntry>
     */
    #[Ignore]
    #[ORM\OneToMany(mappedBy: 'stage', targetEntity: ReviewEntry::class, fetch: 'EXTRA_LAZY')]
    private Collection $reviewEntries;

    public function __construct()
    {
        $this->reviewEntries = new ArrayCollection();
    }

    /**
     * setActiveForm
     *
     * @param string $activeForm
     * @return ReviewStage
     */
    public function setActiveForm(string $activeForm): self
    {
        $this->activeForm = $activeForm;

        return $this;
    }

    /**
     * getActiveForm
     *
     * @return string
     */
    public function getActiveForm(): string
    {
        return $this->activeForm;
    }

    /**
     * setPassiveForm
     *
     * @param string $passiveForm
     * @return ReviewStage
     */
    public function setPassiveForm(string $passiveForm): self
    {
        $this->passiveForm = $passiveForm;

        return $this;
    }

    /**
     * Get passiveForm.
     */
    public function getPassiveForm(): ?string
    {
        return $this->passiveForm;
    }

    /**
     * addReviewEntry
     *
     * @param ReviewEntry $reviewEntries
     * @return ReviewStage
     */
    public function addReviewEntry(ReviewEntry $reviewEntries): self
    {
        $this->reviewEntries[] = $reviewEntries;

        return $this;
    }

    /**
     * removeReviewEntry
     *
     * @param ReviewEntry $reviewEntries
     * @return ReviewStage
     */
    public function removeReviewEntry(ReviewEntry $reviewEntries): self
    {
        $this->reviewEntries->removeElement($reviewEntries);

        return $this;
    }

    /**
     * getReviewEntries
     *
     * @return Collection
     */
    public function getReviewEntries(): Collection
    {
        return $this->reviewEntries;
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        return $this->getPassiveForm();
    }
}
