<?php

namespace App\Entity;

use App\Entity\Trait\BlameableTimestampableTrait;
use App\Entity\Trait\DescribableTrait;
use App\Entity\Trait\IdentifiableTrait;
use App\Entity\Trait\NameIdentifiableTrait;
use App\Entity\Trait\ReviewableTrait;
use App\Entity\Trait\SluggableTrait;
use App\Entity\Trait\SoftDeletableTrait;
use App\Repository\PublicationRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\SerializedName;

#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', hardDelete: true)]
#[ORM\Entity(repositoryClass: PublicationRepository::class)]
class Publication implements \Stringable
{
    use BlameableTimestampableTrait;
    use DescribableTrait;
    use IdentifiableTrait;
    use NameIdentifiableTrait;
    use ReviewableTrait;
    use SluggableTrait;
    use SoftDeletableTrait;

    /** publicationType */
    #[ORM\ManyToOne(inversedBy: 'publications')]
    #[ORM\JoinColumn(name: 'type_id', nullable: false)]
    private ?PublicationType $publicationType = null;

    /** source */
    #[ORM\OneToOne(inversedBy: 'publication', cascade: ['remove'])]
    #[ORM\JoinColumn(unique: true, nullable: false)]
    private ?Source $source = null;

    /**
     * setPublicationType
     *
     * @param PublicationType $publicationType
     * @return Publication
     */
    public function setPublicationType(PublicationType $publicationType): self
    {
        $this->publicationType = $publicationType;

        return $this;
    }

    /**
     * getPublicationType
     *
     * @return PublicationType|null
     */
    public function getPublicationType(): ?PublicationType
    {
        return $this->publicationType;
    }

    /**
     * getPublicationTypeData
     *
     * Get the type id and displayName.
     * @return array
     */
    #[Groups(['flat', 'normal'])]
    #[SerializedName('publicationType')]
    public function getPublicationTypeData(): array
    {
        return [
            'id' => $this->publicationType->getId(),
            'displayName' => $this->publicationType->getDisplayName(),
        ];
    }

    /**
     * setSource
     *
     * @param Source $source
     * @return Publication
     */
    public function setSource(Source $source): self
    {
        $this->source = $source;

        return $this;
    }

    /**
     * getSource
     *
     * @return Source|null
     */
    public function getSource(): ?Source
    {
        return $this->source;
    }

    /**
     * getSourceId
     *
     * @return int
     */
    #[Groups(['normal'])]
    #[SerializedName('source')]
    public function getSourceId(): int
    {
        return $this->source->getId();
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        return $this->getDisplayName();
    }
}
