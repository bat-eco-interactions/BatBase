<?php

namespace App\Entity;

use App\Entity\Trait\BlameableTimestampableTrait;
use App\Entity\Trait\DescribableTrait;
use App\Entity\Trait\FileUploadTrait;
use App\Entity\Trait\IdentifiableTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

#[ORM\Entity]
#[Vich\Uploadable]
class ImageUpload implements \Stringable
{
    use FileUploadTrait;
    use IdentifiableTrait;
    use BlameableTimestampableTrait;
    use DescribableTrait;

    /** fileName - URL to image|pdf file. */
    #[ORM\Column]
    private ?string $fileName = null;

    /** title */
    #[ORM\Column(unique: true, nullable: true)]
    private ?string $title = null;

    /** image - Used as container for UploadedFile obj */
    #[Vich\UploadableField(mapping: 'issue_image', fileNameProperty: 'fileName', size: 'size', mimeType: 'mimeType')]
    private UploadedFile|File|null $image = null;

    /** issueReport */
    #[ORM\ManyToOne(inversedBy: 'screenshots')]
    #[ORM\JoinColumn(name: 'report_id')]
    private ?IssueReport $issueReport = null;

    /**
     * getFileName
     *
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * setFileName
     *
     * @param string $fileName
     * @return ImageUpload
     */
    public function setFileName(string $fileName): self
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * getTitle
     *
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * setTitle
     *
     * @param string|null $title
     * @return ImageUpload
     */
    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

//    /**
//     * setPath
//     *
//     * @param string $path
//     * @return ImageUpload
//     */
//    public function setPath(string $path = 'uploads/issue_screenshots/'): self
//    {
//        $this->path = $path;
//
//        return $this;
//    }

    /**
     * getImage
     *
     * @return File|UploadedFile|null
     */
    public function getImage(): File|UploadedFile|null
    {
        return $this->image;
    }

    /**
     * setImage
     *
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     * @param File|UploadedFile|null $image
     * @return ImageUpload
     */
    public function setImage(File|UploadedFile $image = null): ImageUpload
    {
        $this->image = $image;

        if (null !== $image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->setPath();
            // Bug with VichUploader that is fixed by setting created datetime asap
            $this->setCreated(new \DateTimeImmutable('now', new \DateTimeZone('UTC')));
        }

        return $this;
    }

    /**
     * getIssueReport
     *
     * @return IssueReport
     */
    public function getIssueReport(): IssueReport
    {
        return $this->issueReport;
    }

    /**
     * setIssueReport
     *
     * @param IssueReport $issueReport
     * @return ImageUpload
     */
    public function setIssueReport(IssueReport $issueReport): self
    {
        $this->issueReport = $issueReport;

        return $this;
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        return $this->fileName;
    }
}
