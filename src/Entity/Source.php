<?php

namespace App\Entity;

use App\Entity\Trait\BlameableTimestampableTrait;
use App\Entity\Trait\DescribableTrait;
use App\Entity\Trait\IdentifiableTrait;
use App\Entity\Trait\NameIdentifiableTrait;
use App\Entity\Trait\ReviewableTrait;
use App\Entity\Trait\SoftDeletableTrait;
use App\Repository\SourceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', timeAware: false)]
#[ORM\Entity(repositoryClass: SourceRepository::class)]
#[UniqueEntity(fields: ['displayName'], message: 'Duplicate Source')]
class Source implements \Stringable
{
    use BlameableTimestampableTrait;
    use DescribableTrait;
    use IdentifiableTrait;

    /**
     * Citation - title, plus "(citation)" as needed
     */
    use NameIdentifiableTrait;
    use ReviewableTrait;
    use SoftDeletableTrait;

    /**
     * Citation - title
     *
     * name
     */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column(nullable: true)]
    private ?string $name = null;

    /**
     * Citation
     * Publication
     * year
     */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column(nullable: true)]
    private ?string $year = null;

    /**
     * Citation
     * Publication
     * doi
     */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column(nullable: true)]
    private ?string $doi = null;

    /**
     * All sources
     * linkDisplay
     */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column(nullable: true)]
    private ?string $linkDisplay = null;

    /**
     * All sources
     * linkUrl
     */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column(nullable: true)]
    #[Assert\Url]
    private ?string $linkUrl = null;

    /** isDirect - Citation source to an interaction */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column(nullable: true)]
    private ?bool $isDirect = null;

    /** parentSource */
    #[Groups(['flat'])]
    #[ORM\ManyToOne(inversedBy: 'childSources')]
    #[ORM\JoinColumn(name: 'parent_src_id', onDelete: 'SET NULL')]
    private ?Source $parentSource = null;

    /**
     * childSources
     *
     * @var Collection<int, Source>
     */
    #[ORM\OneToMany(mappedBy: 'parentSource', targetEntity: self::class, fetch: 'EXTRA_LAZY')]
//    #[ORM\OrderBy(['description' => Order::Ascending])]
    private Collection $childSources;

    /** sourceType */
    #[ORM\ManyToOne(inversedBy: 'sources')]
    #[ORM\JoinColumn(name: 'type_id', nullable: false)]
    private ?SourceType $sourceType = null;

    /**
     * tags
     *
     * @var Collection<int, Tag>
     */
    #[ORM\ManyToMany(targetEntity: Tag::class, inversedBy: 'sources')]
    #[ORM\JoinTable(name: 'source_tag')]
    private Collection $tags;

    /**
     * interactions
     *
     * @var Collection<int, Interaction>
     */
    #[ORM\OneToMany(mappedBy: 'source', targetEntity: Interaction::class, fetch: 'EXTRA_LAZY')]
    private Collection $interactions;

    /** author */
    #[Groups(['flat'])]
    #[ORM\OneToOne(mappedBy: 'source', targetEntity: Author::class, cascade: ['persist'])]
    private ?Author $author = null;

    /** citation */
    #[Groups(['flat'])]
    #[ORM\OneToOne(mappedBy: 'source', cascade: ['persist'])]
    private ?Citation $citation = null;

    /** publication */
    #[Groups(['flat'])]
    #[ORM\OneToOne(mappedBy: 'source', targetEntity: Publication::class, cascade: ['persist'])]
    private ?Publication $publication = null;

    /** publisher */
    #[Groups(['flat'])]
    #[ORM\OneToOne(mappedBy: 'source', targetEntity: Publisher::class, cascade: ['persist'])]
    private ?Publisher $publisher = null;

    /**
     * contributors - A collection of all Authors that contributed to a source work.
     *
     * @var Collection<int, Contribution>
     */
    #[ORM\OneToMany(mappedBy: 'workSource', targetEntity: Contribution::class, orphanRemoval: true)]
    private Collection $contributors;

    /**
     * contributions - A collection of all works an Author source contributed to.
     *
     * @var Collection<int, Contribution>
     */
    #[ORM\OneToMany(mappedBy: 'authorSource', targetEntity: Contribution::class, fetch: 'EXTRA_LAZY', orphanRemoval: true)]
    private Collection $contributions;

    public function __construct()
    {
        $this->contributors = new ArrayCollection();
        $this->contributions = new ArrayCollection();
        $this->childSources = new ArrayCollection();
        $this->interactions = new ArrayCollection();
        $this->tags = new ArrayCollection();
    }

    /**
     * setName
     *
     * @param string|null $name
     * @return Source
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * getName
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * setYear
     *
     * @param string|null $year
     * @return Source
     */
    public function setYear(?string $year): self
    {
        $this->year = $year;

        return $this;
    }

    /**
     * getYear
     *
     * @return string|null
     */
    public function getYear(): ?string
    {
        return $this->year;
    }

    /**
     * setDoi
     *
     * @param string|null $doi
     * @return Source
     */
    public function setDoi(?string $doi): self
    {
        $this->doi = $doi;

        return $this;
    }

    /**
     * getDoi
     *
     * @return string|null
     */
    public function getDoi(): ?string
    {
        return $this->doi;
    }

    /**
     * setLinkDisplay
     *
     * @param string|null $linkDisplay
     * @return Source
     */
    public function setLinkDisplay(?string $linkDisplay): self
    {
        $this->linkDisplay = $linkDisplay;

        return $this;
    }

    /**
     * getLinkDisplay
     *
     * @return string|null
     */
    public function getLinkDisplay(): ?string
    {
        return $this->linkDisplay;
    }

    /**
     * setLinkUrl
     *
     * @param string|null $linkUrl
     * @return Source
     */
    public function setLinkUrl(?string $linkUrl): self
    {
        $this->linkUrl = $linkUrl;

        return $this;
    }

    /**
     * getLinkUrl
     *
     * @return string|null
     */
    public function getLinkUrl(): ?string
    {
        return $this->linkUrl;
    }

    /**
     * setIsDirect
     *
     * @param bool $isDirect
     * @return Source
     */
    public function setIsDirect(bool $isDirect): self
    {
        $this->isDirect = $isDirect;

        return $this;
    }

    /**
     * getIsDirect
     *
     * @return bool|null
     */
    public function getIsDirect(): ?bool
    {
        return $this->isDirect || false;
    }

    /**
     * setParentSource
     *
     * @param Source|null $parentSource
     * @return Source
     */
    public function setParentSource(?Source $parentSource = null): self
    {
        $this->parentSource = $parentSource;

        return $this;
    }

    /**
     * getParentSource
     *
     * @return Source|null
     */
    public function getParentSource(): ?Source
    {
        return $this->parentSource;
    }

    /**
     * getParentSourceId
     *
     * @return int|null
     */
    #[Groups(['normal'])]
    #[SerializedName('parent')]
    public function getParentSourceId(): ?int
    {
        return $this->parentSource?->getId();
    }

    /**
     * addChildSource
     *
     * @param Source $childSource
     * @return Source
     */
    public function addChildSource(Source $childSource): self
    {
        $this->childSources[] = $childSource;
        $childSource->setParentSource($this);

        return $this;
    }

    /**
     * removeChildSource
     *
     * @param Source $childSource
     * @return Source
     */
    public function removeChildSource(Source $childSource): self
    {
        $this->childSources->removeElement($childSource);

        return $this;
    }

    /**
     * getChildSources
     *
     * @return Collection
     */
    public function getChildSources(): Collection
    {
        return $this->childSources;
    }

    /**
     * getChildSourceIds
     *
     * @return array
     */
    #[Groups(['flat', 'normal'])]
    #[SerializedName('children')]
    public function getChildSourceIds(): array
    {
        $childIds = [];
        foreach ($this->childSources as $child) {
            $childIds[] = $child->getId();
        }

        return $childIds;
    }

    /**
     * setSourceType
     *
     * @param SourceType $sourceType
     * @return Source
     */
    public function setSourceType(SourceType $sourceType): self
    {
        $this->sourceType = $sourceType;

        return $this;
    }

    /**
     * getSourceType
     *
     * @return SourceType
     */
    public function getSourceType(): SourceType
    {
        return $this->sourceType;
    }


    /**
     * getSourceTypeData
     *
     * Get the type id and displayName.
     * @return array
     */
    #[Groups(['flat', 'normal'])]
    #[SerializedName('sourceType')]
    public function getSourceTypeData(): array
    {
        return [
            'id' => $this->sourceType->getId(),
            'displayName' => $this->sourceType->getDisplayName(),
        ];
    }

    /**
     * getSourceTypeName
     *
     * @return string
     */
    public function getSourceTypeName(): string
    {
        return $this->sourceType->getDisplayName();
    }

    /**
     * addTag
     *
     * @param Tag $tag
     * @return Source
     */
    public function addTag(Tag $tag): self
    {
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * removeTag
     *
     * @param Tag $tag
     * @return Source
     */
    public function removeTag(Tag $tag): self
    {
        $this->tags->removeElement($tag);

        return $this;
    }

    /**
     * getTags
     *
     * @return Collection
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    /**
     * getTagData
     *
     * Get the id and displayName of each tag.
     * @return array
     */
    #[Groups(['flat', 'normal'])]
    #[SerializedName('tags')]
    public function getTagData(): array
    {
        $tagIds = [];
        foreach ($this->tags as $tag) {
            $tagIds[] = [
                'id' => $tag->getId(),
                'displayName' => $tag->getDisplayName()
            ];
        }
        return $tagIds;
    }

    /**
     * getTagIds
     *
     * @return array
     */
    public function getTagIds(): array
    {
        $tagIds = [];
        foreach ($this->tags as $tag) {
            $tagIds[] = $tag->getId();
        }

        return $tagIds;
    }

    /**
     * addInteraction
     *
     * @param Interaction $interaction
     * @return Source
     */
    public function addInteraction(Interaction $interaction): self
    {
        $this->interactions[] = $interaction;

        return $this;
    }

    /**
     * removeInteraction
     *
     * @param Interaction $interaction
     * @return Source
     */
    public function removeInteraction(Interaction $interaction): self
    {
        $this->interactions->removeElement($interaction);

        return $this;
    }

    /**
     * getInteractions
     *
     * @return Collection
     */
    public function getInteractions(): Collection
    {
        return $this->interactions;
    }

    /**
     * todo: is this only for citations?
     * getInteractionIds
     *
     * @return array
     */
    #[Groups(['normal'])]
    #[SerializedName('interactions')]
    public function getInteractionIds(): array
    {
        $intIds = [];
        foreach ($this->interactions as $interaction) {
            $intIds[] = $interaction->getId();
        }

        return $intIds;
    }

    /**
     * setAuthor
     *
     * @param Author|null $author
     * @return Source
     */
    public function setAuthor(?Author $author): Source
    {
        $this->author = $author;
        $author->setSource($this);

        return $this;
    }

    /**
     * getAuthor
     *
     * @return Author|null
     */
    public function getAuthor(): ?Author
    {
        return $this->author;
    }

    /**
     * getAuthorId
     *
     * @return int|null
     */
    #[Groups(['normal'])]
    #[SerializedName('author')]
    public function getAuthorId(): ?int
    {
        return $this->author?->getId();
    }

    // todo: add author|editor and auto add contribution

    /**
     * setCitation
     *
     * @param Citation|null $citation
     * @return Source
     */
    public function setCitation(?Citation $citation): Source
    {
        $this->citation = $citation;
        $citation->setSource($this);

        return $this;
    }

    /**
     * removeCitation
     *
     * @return Source
     */
    public function removeCitation(): self
    {
        $this->citation = null;

        return $this;
    }

    /**
     * getCitation
     *
     * @return Citation|null
     */
    public function getCitation(): ?Citation
    {
        return $this->citation;
    }

    /**
     * getCitationId
     *
     * @return int|null
     */
    #[Groups(['normal'])]
    #[SerializedName('citation')]
    public function getCitationId(): ?int
    {
        return $this->citation?->getId();
    }

    /**
     * setPublication
     *
     * @param Publication|null $publication
     * @return Source
     */
    public function setPublication(?Publication $publication): Source
    {
        $this->publication = $publication;
        $publication->setSource($this);

        return $this;
    }

    /**
     * removePublication
     *
     * @return Source
     */
    public function removePublication(): self
    {
        $this->publication = null;

        return $this;
    }

    /**
     * getPublication
     *
     * @return Publication|null
     */
    public function getPublication(): ?Publication
    {
        return $this->publication;
    }

    /**
     * getPublicationId
     *
     * @return int|null
     */
    #[Groups(['normal'])]
    #[SerializedName('publication')]
    public function getPublicationId(): ?int
    {
        return $this->publication?->getId();
    }

    /**
     * setPublisher
     *
     * @param Publisher|null $publisher
     * @return Source
     */
    public function setPublisher(?Publisher $publisher): Source
    {
        $this->publisher = $publisher;
        $publisher->setSource($this);

        return $this;
    }

    /**
     * getPublisher
     * @return Publisher|null
     */
    public function getPublisher(): ?Publisher
    {
        return $this->publisher;
    }

    /**
     * getPublisherId
     *
     * @return int|null
     */
    #[Groups(['normal'])]
    #[SerializedName('publisher')]
    public function getPublisherId(): ?int
    {
        return $this->publisher?->getId();
    }

    /**
     * addContributor
     *
     * @param Contribution $contributor
     * @return Source
     */
    public function addContributor(Contribution $contributor): self
    {
        $this->contributors[] = $contributor;

        return $this;
    }

    /**
     * removeContributor
     *
     * @param Contribution $contributor
     * @return Source
     */
    public function removeContributor(Contribution $contributor): self
    {
        $this->contributors->removeElement($contributor);

        return $this;
    }

    /**
     * getContributors
     *
     * @return Collection
     */
    public function getContributors(): Collection
    {
        return $this->contributors;
    }

    /**
     * getAuthorNames
     *
     * NOTE: Used for Interaction show pages.
     * @return array|null
     */
    #[Groups(['flat'])]
    #[SerializedName('contributors')]
    public function getAuthorNames(): ?array
    {
        $contribs = strpos($this->displayName, '(citation)') ?
            $this->parentSource->getContributors() : $this->contributors;

        return $this->getContributorsArray($contribs, 'DisplayName', 'all');
    }

    /**
     * getAuthorIds
     *
     * @return array|null
     */
    #[Groups(['normal'])]
    #[SerializedName('authors')]
    public function getAuthorIds(): ?array
    {
        return $this->getContributorsArray($this->contributors, 'Id', 'author');
    }

    /**
     * getEditorIds
     *
     * @return array|null
     */
    #[Groups(['normal'])]
    #[SerializedName('editors')]
    public function getEditorIds(): ?array
    {
        return $this->getContributorsArray($this->contributors, 'Id', 'editor');
    }

    /**
     * @param Collection $contributors
     * @param string $field - DisplayName | Id
     * @param string $types - editor | author | all
     * @return array|null
     */
    private function getContributorsArray(Collection $contributors, string $field, string $types): ?array
    {
        $getField = 'get' . $field;
        $contribs = [];

        foreach ($contributors as $contributor) {
            $type = $contributor->getIsEditor() ? 'editor' : 'author';
            if ($types !== 'all' && $type !== $types) {
                continue;
            }
            $data = $contributor->getAuthorSource()->$getField(); // displayName for 'all' types, id otherwise
            $ord = $contributor->getOrd();
            $contribs = $contribs + [$ord => $this->getContribData($data, $types, $type)];
        }

        return count($contribs) > 0 ? $contribs : null;
    }

    /**
     * Returns either the id or an array with the type and the display name
     *
     * @param int|string $data
     * @param string $types - editor | author | all
     * @param string $type - editor | author
     * @return array|int
     */
    private function getContribData(int|string $data, string $types, string $type): array|int
    {
        return $types === 'all' ? [$type => $data] : $data;
    }

    /**
     * getContributorData
     *
     * Get the core data for each contributor
     * @return array
     */
    #[Groups(['normal'])]
    #[SerializedName('contributors')]
    public function getContributorData(): array
    {
        $contribs = [];
        foreach ($this->contributors as $contributor) {
            $contribId = $contributor->getId();
            $authId = $contributor->getAuthorSource()->getId();
            $isEd = $contributor->getIsEditor();
            $ord = $contributor->getOrd();
            $contribs = $contribs + [$authId => [
                    'authId' => $authId,
                    'contribId' => $contribId, //todo: is this used anywhere (once no longer needed for data-entry )?
                    'isEditor' => $isEd,
                    'ord' => $ord],
                ];
        }

        return $contribs;
    }

    /**
     * Will replace the above method once the entity is processed view the API DTOs.
     * @return array|array[]
     */
    public function getDtoContributors(): array
    {
        $contribs = [];
        foreach ($this->contributors as $contributor) {
            $contribId = $contributor->getId();
            $authId = $contributor->getAuthorSource()->getAuthor()->getId();
            $isEd = $contributor->getIsEditor();
            $ord = $contributor->getOrd();
            $contribs = $contribs + [$authId => [
                    'authId' => $authId,
                    'contribId' => $contribId, //todo: is this used anywhere (once no longer needed for data-entry )?
                    'isEditor' => $isEd,
                    'ord' => $ord],
                ];
        }

        return $contribs;
    }

    /**
     * addContribution
     *
     * @param Contribution $contribution
     * @return Source
     */
    public function addContribution(Contribution $contribution): self
    {
        $this->contributions[] = $contribution;

        return $this;
    }

    /**
     * removeContribution
     *
     * @param Contribution $contribution
     * @return Source
     */
    public function removeContribution(Contribution $contribution): self
    {
        $this->contributions->removeElement($contribution);

        return $this;
    }

    /**
     * getContributions
     *
     * @return Collection
     */
    public function getContributions(): Collection
    {
        return $this->contributions;
    }

    /**
     * getContributonIds
     *
     * @return array
     */
    #[Groups(['normal'])]
    #[SerializedName('contributions')]
    public function getContributonIds(): array
    {
        $contribIds = [];
        foreach ($this->contributions as $contribution) {
            $contribIds[] = $contribution->getWorkSource()->getId();
        }

        return $contribIds;
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        return $this->getDisplayName();
    }
}
