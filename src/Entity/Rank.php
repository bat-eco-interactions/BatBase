<?php

namespace App\Entity;

use App\Entity\Trait\IdentifiableTrait;
use App\Entity\Trait\NameIdentifiableTrait;
use App\Repository\RankRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\Ignore;

/**  Rank AKA Tier - "rank" is a reserved MySQL term. */
#[ORM\Entity(repositoryClass: RankRepository::class)]
#[ORM\Table(name: 'tier')]
#[Groups(['normal'])]
class Rank implements \Stringable
{
    use IdentifiableTrait;
    use NameIdentifiableTrait;

    /** ordinal */
    #[ORM\Column]
    private ?int $ordinal = null;

    /** pluralName */
    #[ORM\Column]
    private ?string $pluralName = null;

    /**
     * taxa
     *
     * @var Collection<int, Taxon>
     */
    #[Ignore]
    #[ORM\OneToMany(mappedBy: 'rank', targetEntity: Taxon::class, fetch: 'EXTRA_LAZY')]
    private Collection $taxa;

    public function __construct()
    {
        $this->taxa = new ArrayCollection();
    }

    /**
     * setOrdinal
     *
     * @param int $ordinal
     * @return Rank
     */
    public function setOrdinal(int $ordinal): self
    {
        $this->ordinal = $ordinal;

        return $this;
    }

    /**
     * getOrdinal
     *
     * @return int
     */
    public function getOrdinal(): int
    {
        return $this->ordinal;
    }

    /**
     * setPluralName
     *
     * @param string $pluralName
     * @return Rank
     */
    public function setPluralName(string $pluralName): self
    {
        $this->pluralName = $pluralName;

        return $this;
    }

    /**
     * getPluralName
     *
     * @return string
     */
    public function getPluralName(): string
    {
        return $this->pluralName;
    }

    /**
     * addTaxa
     *
     * @param Taxon $taxon
     * @return Rank
     */
    public function addTaxa(Taxon $taxon): self
    {
        $this->taxa[] = $taxon;

        return $this;
    }

    /**
     * removeTaxa
     *
     * @param Taxon $taxon
     * @return Rank
     */
    public function removeTaxa(Taxon $taxon): self
    {
        $this->taxa->removeElement($taxon);

        return $this;
    }

    /**
     * getTaxa
     *
     * @return Collection
     */
    public function getTaxa(): Collection
    {
        return $this->taxa;
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        return $this->getDisplayName();
    }
}
