<?php

namespace App\Entity;

use App\Entity\Trait\DescribableTrait;
use App\Entity\Trait\IdentifiableTrait;
use App\Entity\Trait\NameIdentifiableTrait;
use App\Entity\Trait\ReviewableTrait;
use App\Entity\Trait\SoftDeletableTrait;
use App\Entity\Trait\BlameableTimestampableTrait;
use App\Repository\LocationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\SerializedName;

#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', hardDelete: true)]
#[ORM\Entity(repositoryClass: LocationRepository::class)]
#[UniqueEntity(fields: ['displayName'], message: 'Duplicate Location')]
class Location implements \Stringable
{
    use BlameableTimestampableTrait;
    use DescribableTrait;
    use IdentifiableTrait;
    use NameIdentifiableTrait;
    use ReviewableTrait;
    use SoftDeletableTrait;

    /** isoCode - 2 letter country code */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column(nullable: true)]
    private ?string $isoCode = null;

    /** elevation */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column(nullable: true)]
    private ?int $elevation = null;

    /** elevationMax */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column(nullable: true)]
    private ?int $elevationMax = null;

    /** elevUnitAbbrv */
    /** @todo allow setting in form? */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column(length: 3, nullable: true)]
    private ?string $elevUnitAbbrv = 'm';

    /** latitude */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column(type: Types::DECIMAL, precision: 18, scale: 14, nullable: true)]
    private ?string $latitude = null;

    /** longitude */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column(type: Types::DECIMAL, precision: 18, scale: 14, nullable: true)]
    private ?string $longitude = null;

    /** parentLocation */
    #[ORM\ManyToOne(inversedBy: 'childLocs')]
    #[ORM\JoinColumn(name: 'parent_loc_id', onDelete: 'SET NULL')]
    private ?Location $parentLocation = null;

    /**
     * childLocs
     *
     * @var Collection<int, Location>
     */
    #[ORM\OneToMany(mappedBy: 'parentLocation', targetEntity: self::class, fetch: 'EXTRA_LAZY')]
    private Collection $childLocs;

    /** locationType */
    #[ORM\ManyToOne(inversedBy: 'locations')]
    #[ORM\JoinColumn(name: 'type_id', nullable: false)] // nullable is true by default for join columns
    private ?LocationType $locationType = null;

    /** geoJson */
    #[Groups(['flat'])]
    #[ORM\OneToOne(targetEntity: GeoJson::class, cascade: ['persist'], orphanRemoval: true)]
    #[ORM\JoinColumn(name: 'geo_json')]
    private ?GeoJson $geoJson = null;

    /**
     * interactions
     *
     * @var Collection<int, Interaction>
     */
    #[ORM\OneToMany(mappedBy: 'location', targetEntity: Interaction::class, fetch: 'EXTRA_LAZY')]
    private Collection $interactions;

    /** habitatType */
    #[ORM\ManyToOne(inversedBy: 'locations')]
    #[ORM\JoinColumn(name: 'habitat_type')]
    private ?HabitatType $habitatType = null;

    public function __construct()
    {
        $this->interactions = new ArrayCollection();
        $this->childLocs = new ArrayCollection();
    }

    /**
     * setIsoCode
     *
     * 2 letter country code
     * @param string|null $isoCode
     * @return Location
     */
    public function setIsoCode(?string $isoCode): self
    {
        $this->isoCode = $isoCode;

        return $this;
    }

    /**
     * getIsoCode
     *
     * 2 letter country code
     * @return string|null
     */
    #[Groups(['flat', 'normal'])]
    public function getIsoCode(): ?string
    {
        return $this->isoCode;
    }

    /**
     * setElevation
     *
     * @param int|null $elevation
     * @return Location
     */
    public function setElevation(?int $elevation): self
    {
        $this->elevation = $elevation;

        return $this;
    }

    /**
     * getElevation
     *
     * @return int|null
     */
    public function getElevation(): ?int
    {
        return $this->elevation;
    }

    /**
     * setElevationMax
     *
     * @param int|null $elevationMax
     * @return Location
     */
    public function setElevationMax(?int $elevationMax): self
    {
        $this->elevationMax = $elevationMax;

        return $this;
    }

    /**
     * getElevationMax
     *
     * @return int|null
     */
    public function getElevationMax(): ?int
    {
        return $this->elevationMax;
    }

    /**
     * setElevUnitAbbrv
     *
     * @param string|null $elevUnitAbbrv
     * @return Location
     */
    public function setElevUnitAbbrv(?string $elevUnitAbbrv = 'm'): self
    {
        $this->elevUnitAbbrv = $elevUnitAbbrv;

        return $this;
    }

    /**
     * getElevUnitAbbrv
     *
     * @return string|null
     */
    public function getElevUnitAbbrv(): ?string
    {
        return $this->elevUnitAbbrv;
    }

    /**
     * setLatitude
     *
     * @param string|null $latitude
     * @return Location
     */
    public function setLatitude(?string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * getLatitude
     *
     * @return string|null
     */
    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    /**
     * setLongitude
     *
     * @param string|null $longitude
     * @return Location
     */
    public function setLongitude(?string $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * getLongitude
     *
     * @return string|null
     */
    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    /**
     * setParentLocation
     *
     * @param Location|null $parentLocation
     * @return Location
     */
    public function setParentLocation(?Location $parentLocation = null): self
    {
        $this->parentLocation = $parentLocation;

        $parentLocation->addChildLoc($this);

        return $this;
    }

    /**
     * getParentLocation
     *
     * @return Location|null
     */
    public function getParentLocation(): ?Location
    {
        return $this->parentLocation;
    }

    /**
     * getParentLocationId
     *
     * @return int|null
     */
    #[Groups(['normal'])]
    #[SerializedName('parent')]
    public function getParentLocationId(): ?int
    {
        return $this->parentLocation?->getId();
    }

    /**
     * addChildLoc
     *
     * @param Location $childLoc
     * @return Location
     */
    public function addChildLoc(Location $childLoc): self
    {
        $this->childLocs[] = $childLoc;

        if ($childLoc->getParentLocation() !== $this) {
            $childLoc->setParentLocation($this);
        }

        return $this;
    }

    /**
     * removeChildLoc
     *
     * @param Location $childLoc
     * @return Location
     */
    public function removeChildLoc(Location $childLoc): self
    {
        $this->childLocs->removeElement($childLoc);

        return $this;
    }

    /**
     * getChildLocs
     *
     * @return Collection
     */
    public function getChildLocs(): Collection
    {
        return $this->childLocs;
    }

    /**
     * getChildLocIds
     *
     * @return array
     */
    #[Groups(['normal'])]
    #[SerializedName('children')]
    public function getChildLocIds(): array
    {
        $children = [];
        foreach ($this->childLocs as $child) {
            $children[] = $child->getId();
        }

        return $children;
    }

    /**
     * setLocationType
     *
     * @param LocationType|null $locationType
     * @return Location
     */
    public function setLocationType(?LocationType $locationType = null): self
    {
        $this->locationType = $locationType;

        return $this;
    }

    /**
     * getLocationType
     *
     * @return LocationType|null
     */
    public function getLocationType(): ?LocationType
    {
        return $this->locationType;
    }

    /**
     * getLocationTypeData
     *
     * Get the type id and displayName.
     * @return array
     */
    #[Groups(['flat', 'normal'])]
    #[SerializedName('locationType')]
    public function getLocationTypeData(): array
    {
        return [
            'id' => $this->locationType->getId(),
            'displayName' => $this->locationType->getDisplayName(),
        ];
    }

    /**
     * getRegionData
     *
     * @param bool $comprehensive - Returns both continent and sub-region, if available
     * @return array
     */
    #[Groups(['flat', 'normal'])]
    #[SerializedName('region')]
    public function getRegionData(bool $comprehensive = false): array
    {
        $region = $this->locationType->getSlug() === 'region' ?
            $this : $this->getParentTypeEntity($this, 'region');
        $regionData = $this->getLocObj($region);

        if ($comprehensive) {
            $regionParent = $region->getParentLocation();
            if ($regionParent) { // Eg, continent parent of sub-region location
                $regionData['parentRegion'] = $regionParent->getDisplayName();
            }
        }

        return $regionData;
    }

    public function getRegionName(): string
    {
        $regionData = $this->getRegionData();

        return $regionData['displayName'];
    }

    /**
     * getCountryData
     *
     * Get the Country of this Location.
     * @return array|null
     */
    #[Groups(['flat', 'normal'])]
    #[SerializedName('country')]
    public function getCountryData(): array|null
    {
        $locType = $this->locationType; // print("\n[".$this->displayName."] locationType = [".$locType->getDisplayName()."]");
        if ($locType->getSlug() === 'region') {
            return null; //changed from false... why was that needed?
        }
        if ($locType->getSlug() === 'country') {
            return $this->getLocObj($this);
        }

        return $this->getParentTypeData($this, 'country');
    }

    public function getCountryName(): string
    {
        $country = $this->getCountryData();

        return $country ? $country['displayName'] : 'Unspecified';
    }

    /**
     * getParentTypeData
     *
     * Get summary data for the parent location of the passed type, region or country, if it exists.
     * @param Location $loc
     * @param string $typeSlug
     * @return array|null
     */
    private function getParentTypeData(Location $loc, string $typeSlug): array|null
    {
        $parent = $this->getParentTypeEntity($loc, $typeSlug);
        return $parent ? $this->getLocObj($parent) : null;
    }

    /**
     * getParentTypeEntity
     *
     * Get the parent location of the passed type, region or country, if it exists.
     * @param Location $loc
     * @param string $typeSlug
     * @return array|null
     */
    private function getParentTypeEntity(Location $loc, string $typeSlug): object|null
    {
        $parent = $loc->getParentLocation();
        if (!$parent) {
            return null;
        }
        if ($typeSlug == 'country' && $this->hasRegionParent($parent)) {
            return null;
        }
        if ($parent->getLocationType()->getSlug() === $typeSlug) {
            return $parent;
        }

        return $this->getParentTypeEntity($parent, $typeSlug);
    }

    /**
     * hasRegionParent
     *
     * @param Location $parent
     * @return bool
     */
    private function hasRegionParent(Location $parent): bool
    {
        return $parent->getLocationType()->getSlug() == 'region';
    }

    /**
     * getLocObj
     *
     * @param Location $loc
     * @return array
     */
    private function getLocObj(Location $loc): array
    {
        return [
            'id' => $loc->getId(),
            'displayName' => $loc->getDisplayName()
        ];
    }

    /**
     * setGeoJson
     *
     * @param GeoJson|null $geoJson
     * @return Location
     */
    public function setGeoJson(?GeoJson $geoJson = null): self
    {
        $this->geoJson = $geoJson;

        return $this;
    }

    /**
     * removeGeoJson
     *
     * @return Location
     */
    public function removeGeoJson(): self
    {
        $this->geoJson = null;

        return $this;
    }

    /**
     * getGeoJson
     *
     * @return GeoJson|null
     */
    public function getGeoJson(): ?GeoJson
    {
        return $this->geoJson;
    }

    /**
     * getGeoJsonId
     *
     * @return GeoJson|null
     */
    #[Groups(['flat', 'normal'])]
    #[SerializedName('geoJsonId')]
    public function getGeoJsonId(): ?int
    {
        return $this->geoJson?->getId();
    }

    /**
     * isHabitat
     *
     * @return bool
     */
    public function isHabitat(): bool
    {
        return $this->locationType->getDisplayName() === 'Habitat';
    }

    /**
     * addInteraction
     *
     * @param Interaction $interaction
     * @return Location
     */
    public function addInteraction(Interaction $interaction): self
    {
        $this->interactions[] = $interaction;

        if ($interaction->getLocation() !== $this) {
            $interaction->setLocation($this);
        }

        return $this;
    }

    /**
     * removeInteraction
     *
     * @param Interaction $interaction
     * @return Location
     */
    public function removeInteraction(Interaction $interaction): self
    {
        $this->interactions->removeElement($interaction);

        return $this;
    }

    /**
     * getInteractions
     *
     * @return Collection
     */
    public function getInteractions(): Collection
    {
        return $this->interactions;
    }

    /**
     * getInteractionIds
     *
     * @return array
     */
    #[Groups(['normal'])]
    #[SerializedName('interactions')]
    public function getInteractionIds(): array
    {
        $allIntIds = [];
        foreach ($this->interactions as $interaction) {
            $allIntIds[] = $interaction->getId();
        }

        return $allIntIds;
    }

    /**
     * setHabitatType
     *
     * @param HabitatType|null $habitatType
     * @return Location
     */
    public function setHabitatType(?HabitatType $habitatType = null): self
    {
        $this->habitatType = $habitatType;

        return $this;
    }

    /**
     * getHabitatType
     *
     * @return HabitatType|null
     */
    public function getHabitatType(): ?HabitatType
    {
        return $this->habitatType;
    }

    /**
     * getHabitatTypeData
     *
     * Get the type id and displayName.
     * @return array
     */
    #[Groups(['flat', 'normal'])]
    #[SerializedName('habitatType')]
    public function getHabitatTypeData(): array
    {
        if ($this->habitatType) {
            return [
                'id' => $this->habitatType->getId(),
                'displayName' => $this->habitatType->getDisplayName(),
            ];
        }

        return [];
    }

//    /**
//     * getPlural
//     *
//     * @return string
//     */
//    public function getPlural(): ?string
//    {
//        return 'Locations';
//    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        return $this->getDisplayName();
    }
}
