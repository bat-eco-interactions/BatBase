<?php

namespace App\Entity;

use App\Entity\Trait\BlameableTimestampableTrait;
use App\Entity\Trait\IdentifiableTrait;
use App\Entity\Trait\NameIdentifiableTrait;
use App\Entity\Trait\ReviewableTrait;
use App\Entity\Trait\SoftDeletableTrait;
use App\Repository\CitationRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\SerializedName;

#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', hardDelete: true)]
#[ORM\Entity(repositoryClass: CitationRepository::class)]
class Citation implements \Stringable
{
    use BlameableTimestampableTrait;
    use IdentifiableTrait;

    /** displayName - title + unique string, as needed */
    use NameIdentifiableTrait;
    use ReviewableTrait;
    use SoftDeletableTrait;

    /** title - the title of the work cited */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column]
    private ?string $title = null;

    /** fullText - the formatted citation */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column(type: Types::TEXT)]
    private ?string $fullText = null;

    /** abstract */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $abstract = null;

    /** publicationVolume */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column(nullable: true)]
    private ?string $publicationVolume = null;
//
//    /** publicationVolume */
//    #[Groups(['flat', 'normal'])]
//    #[ORM\Column(type: Types::INTEGER, nullable: true)]
//    private ?int $publicationVolume = null;

    /** publicationIssue */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column(nullable: true)]
    private ?string $publicationIssue = null;

    //    /** publicationIssue */
//    #[Groups(['flat', 'normal'])]
//    #[ORM\Column(type: Types::INTEGER, nullable: true)]
//    private ?int $publicationIssue = null;

    /** publicationPages */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column(nullable: true)]
    private ?string $publicationPages = null;

    /** citationType */
    #[ORM\ManyToOne(inversedBy: 'citations')]
    #[ORM\JoinColumn(name: 'type_id', nullable: false)]
    private ?CitationType $citationType = null;

    /** source */
    #[ORM\OneToOne(inversedBy: 'citation', cascade: ['remove'])]
    #[ORM\JoinColumn(unique: true, nullable: false)]
    private ?Source $source = null;

    /**
     * setTitle
     *
     * @param string $title
     * @return Citation
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * getTitle
     *
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * setFullText
     *
     * @param string $fullText
     * @return Citation
     */
    public function setFullText(string $fullText): self
    {
        $this->fullText = $fullText;

        return $this;
    }

    /**
     * getFullText
     *
     * @return string
     */
    public function getFullText(): string
    {
        return $this->fullText;
    }

    /**
     * setAbstract
     *
     * @param string|null $abstract
     * @return Citation
     */
    public function setAbstract(?string $abstract): self
    {
        $this->abstract = $abstract;

        return $this;
    }

    /**
     * getAbstract
     *
     * @return string|null
     */
    public function getAbstract(): ?string
    {
        return $this->abstract;
    }

    /**
     * setPublicationVolume
     *
     * @param string|null $publicationVolume
     * @return Citation
     */
    public function setPublicationVolume(?string $publicationVolume): self
    {
        $this->publicationVolume = $publicationVolume;

        return $this;
    }

    /**
     * getPublicationVolume
     *
     * @return string|null
     */
    public function getPublicationVolume(): ?string
    {
        return $this->publicationVolume;
    }

    /**
     * setPublicationIssue
     *
     * @param string|null $publicationIssue
     * @return Citation
     */
    public function setPublicationIssue(?string $publicationIssue): self
    {
        $this->publicationIssue = $publicationIssue;

        return $this;
    }

    /**
     * getPublicationIssue
     *
     * @return string|null
     */
    public function getPublicationIssue(): ?string
    {
        return $this->publicationIssue;
    }

    /**
     * setPublicationPages
     *
     * @param string|null $publicationPages
     * @return Citation
     */
    public function setPublicationPages(?string $publicationPages): self
    {
        $this->publicationPages = $publicationPages;

        return $this;
    }

    /**
     * getPublicationPages
     *
     * @return string|null
     */
    public function getPublicationPages(): ?string
    {
        return $this->publicationPages;
    }

    /**
     * setCitationType
     *
     * @param CitationType $citationType
     * @return Citation
     */
    public function setCitationType(CitationType $citationType): self
    {
        $this->citationType = $citationType;

        return $this;
    }

    /**
     * getCitationType
     *
     * @return CitationType
     */
    public function getCitationType(): CitationType
    {
        return $this->citationType;
    }

    /**
     * getCitationTypeData
     *
     * Get the type id and displayName.
     * @return array
     */
    #[Groups(['flat', 'normal'])]
    #[SerializedName('citationType')]
    public function getCitationTypeData(): array
    {
        return [
            'id' => $this->citationType->getId(),
            'displayName' => $this->citationType->getDisplayName(),
        ];
    }

    /**
     * setSource
     *
     * @param Source $source
     * @return Citation
     */
    public function setSource(Source $source): self
    {
        $this->source = $source;

        return $this;
    }

    /**
     * getSource
     *
     * @return Source|null
     */
    public function getSource(): ?Source
    {
        return $this->source;
    }

    /**
     * getSourceId
     *
     * @return int
     */
    #[Groups(['normal'])]
    #[SerializedName('source')]
    public function getSourceId(): int
    {
        return $this->source->getId();
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        return $this->getDisplayName();
    }
}
