<?php

namespace App\Entity;

use App\Entity\Trait\DescribableTrait;
use App\Entity\Trait\IdentifiableTrait;
use App\Entity\Trait\NameIdentifiableTrait;
use App\Entity\Trait\SluggableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\SerializedName;

#[ORM\Entity]
class PublicationType implements \Stringable
{
    use IdentifiableTrait;
    use SluggableTrait;
    use NameIdentifiableTrait;
    use DescribableTrait;

    /**
     * publications
     *
     * @var Collection<int, Publication>
     */
    #[ORM\OneToMany(mappedBy: 'publicationType', targetEntity: Publication::class, fetch: 'EXTRA_LAZY')]
    private Collection $publications;

    public function __construct()
    {
        $this->publications = new ArrayCollection();
    }

    /**
     * addPublication
     *
     * @param Publication $publication
     * @return PublicationType
     */
    public function addPublication(Publication $publication): self
    {
        $this->publications[] = $publication;

        return $this;
    }

    /**
     * removePublication
     *
     * @param Publication $publication
     * @return PublicationType
     */
    public function removePublication(Publication $publication): self
    {
        $this->publications->removeElement($publication);

        return $this;
    }

    /**
     * getPublications
     *
     * @return Collection
     */
    public function getPublications(): Collection
    {
        return $this->publications;
    }

    /**
     * getPublicationIds
     *
     * @return array
     */
    #[Groups(['normal'])]
    #[SerializedName('publications')]
    public function getPublicationIds(): array
    {
        $pubIds = [];
        foreach ($this->publications as $publication) {
            $pubIds[] = $publication->getId();
        }

        return $pubIds;
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        return $this->getDisplayName();
    }
}
