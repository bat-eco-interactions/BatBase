<?php

namespace App\Entity;

use App\Entity\Trait\BlameableTimestampableTrait;
use App\Entity\Trait\IdentifiableTrait;
use App\Entity\Trait\ReviewableTrait;
use App\Entity\Trait\SoftDeletableTrait;
use App\Repository\InteractionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\SerializedName;

#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', hardDelete: true)]
#[ORM\Entity(repositoryClass: InteractionRepository::class)]
class Interaction implements \Stringable
{
    use IdentifiableTrait;
    use BlameableTimestampableTrait;
    use SoftDeletableTrait;
    use ReviewableTrait;

    /**
     * date
     *
     * The date the interaction was cited to occur.
     * Format: 'YYYY-MM-DD to YYYY-MM-DD'.
     */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column(nullable: true)]
    private ?string $date = null;

    /** note */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $note = null;

    /**
     * pages
     *
     * Page number of the source where the interaction is described.
     */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column(nullable: true)]
    private ?string $pages = null;

    /**
     * quote
     *
     * Direct quote from the source where the interaction is described.
     */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $quote = null;

    /** isLikely - not currently used */
//    #[Groups(['flat', 'normal'])]
    #[ORM\Column(nullable: true)]
    private ?bool $isLikely = null;

    /** isOldWorld - not currently used */
//    #[Groups(['flat', 'normal'])]
    #[ORM\Column(nullable: true)]
    private ?bool $isOldWorld = null;

    /** source */
    #[Groups(['flat'])]
    #[ORM\ManyToOne(cascade: ['persist'], inversedBy: 'interactions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Source $source = null;

    /** interactionType */
    #[ORM\ManyToOne(inversedBy: 'interactions')]
    #[ORM\JoinColumn(name: 'type_id', nullable: false)]
    private ?InteractionType $interactionType = null;

    /** location */
    #[Groups(['flat'])]
    #[ORM\ManyToOne(cascade: ['persist'], inversedBy: 'interactions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Location $location = null;

    /** subject */
//    #[Groups(['flat'])]
    #[ORM\ManyToOne(cascade: ['persist'], inversedBy: 'subjectRoles')]
    #[ORM\JoinColumn(name: 'subject_taxon_id', nullable: false)]
    private ?Taxon $subject = null;

    /** object */
//    #[Groups(['flat'])]
    #[ORM\ManyToOne(cascade: ['persist'], inversedBy: 'objectRoles')]
    #[ORM\JoinColumn(name: 'object_taxon_id', nullable: false)]
    private ?Taxon $object = null;

    /**
     * tags
     *
     * Tags are used for secondary sources, for the seasons the interaction was
     * recorded in, and are required for certain interaction-types.
     * @var Collection<int, Tag>
     */
    #[ORM\ManyToMany(targetEntity: Tag::class, inversedBy: 'interactions', cascade: ['persist'])]
    #[ORM\JoinTable(name: 'interaction_tag')]
    private Collection $tags;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    /**
     * getDate
     *
     * The date the interaction was cited to occur.
     * Format: 'YYYY-MM-DD to YYYY-MM-DD'.
     * @return string|null
     */
    public function getDate(): ?string
    {
        return $this->date;
    }

    /**
     * setDate
     *
     * The date the interaction was cited to occur.
     * Format: 'YYYY-MM-DD to YYYY-MM-DD'.
     * @param string|null $date
     * @return Interaction
     */
    public function setDate(?string $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * getNote
     *
     * @return string|null
     */
    public function getNote(): ?string
    {
        return $this->note;
    }

    /**
     * setNote
     *
     * @param string|null $note
     * @return Interaction
     */
    public function setNote(?string $note): self
    {
        $this->note = $note;

        return $this;
    }

    /**
     * getPages
     *
     * Page number of the souce where the interaction is described.
     * @return string|null
     */
    public function getPages(): ?string
    {
        return $this->pages;
    }

    /**
     * setPages
     *
     * Page number of the source where the interaction is described.
     * @param string|null $pages
     * @return Interaction
     */
    public function setPages(?string $pages): self
    {
        $this->pages = $pages;

        return $this;
    }

    /**
     * getQuote
     *
     * Direct quote from the souce where the interaction is described.
     * @return string|null
     */
    public function getQuote(): ?string
    {
        return $this->quote;
    }

    /**
     * setQuote
     *
     * Direct quote from the source where the interaction is described.
     * @param string|null $quote
     * @return Interaction
     */
    public function setQuote(?string $quote): self
    {
        $this->quote = $quote;

        return $this;
    }

    /**
     * setIsLikely
     *
     * @param bool|null $isLikely
     * @return Interaction
     */
    public function setIsLikely(?bool $isLikely): self
    {
        $this->isLikely = $isLikely;

        return $this;
    }

    /**
     * getIsLikely
     *
     * @return bool|null
     */
    public function getIsLikely(): ?bool
    {
        return $this->isLikely;
    }

    /**
     * setIsOldWorld
     *
     * @param bool $isOldWorld
     * @return Interaction
     */
    public function setIsOldWorld(bool $isOldWorld): self
    {
        $this->isOldWorld = $isOldWorld;

        return $this;
    }

    /**
     * getIsOldWorld
     *
     * @return bool|null
     */
    public function getIsOldWorld(): ?bool
    {
        return $this->isOldWorld;
    }

    /**
     * setSource
     *
     * @param Source $source
     * @return Interaction
     */
    public function setSource(Source $source): self
    {
        $this->source = $source;
        $source->setIsDirect(true);

        return $this;
    }

    /**
     * getSource
     *
     * @return Source
     */
    public function getSource(): Source
    {
        return $this->source;
    }

    /**
     * getSourceId
     *
     * @return int
     */
    #[Groups(['normal'])]
    #[SerializedName('source')]
    public function getSourceId(): int
    {
        return $this->source->getId();
    }

    /**
     * setInteractionType
     *
     * @param InteractionType $interactionType
     * @return Interaction
     */
    public function setInteractionType(InteractionType $interactionType): self
    {
        $this->interactionType = $interactionType;

        return $this;
    }

    /**
     * getInteractionType
     *
     * @return InteractionType|null
     */
    public function getInteractionType(): ?InteractionType
    {
        return $this->interactionType;
    }

    /**
     * getInteractionTypeSummaryData
     *
     * @return array
     */
    #[Groups(['normal'])]
    #[SerializedName('interactionType')]
    public function getInteractionTypeSummaryData(): array
    {
        return [
            'id' => $this->interactionType->getId(),
            'displayName' => $this->interactionType->getDisplayName(),
        ];
    }

    /**
     * getAllInteractionTypeData
     *
     * Get the type id, displayName, and the active form of the type.
     * @return array
     */
    #[Groups(['flat'])]
    #[SerializedName('interactionType')]
    public function getAllInteractionTypeData(): array
    {
        return [
            'id' => $this->interactionType->getId(),
            'displayName' => $this->interactionType->getDisplayName(),
            'activeForm' => $this->interactionType->getActiveForm(),
            // 'passiveForm' => $this->interactionType->getPassiveForm()
        ];
    }

    /**
     * setLocation
     *
     * @param Location $location
     * @return Interaction
     */
    public function setLocation(Location $location): self
    {
        $this->location = $location;

        $location->addInteraction($this);

        return $this;
    }

    /**
     * getLocation
     *
     * @return Location|null
     */
    public function getLocation(): ?Location
    {
        return $this->location;
    }

    /**
     * getLocationId
     *
     * @return int|null
     */
    #[Groups(['normal'])]
    #[SerializedName('location')]
    public function getLocationId(): ?int
    {
        return $this->location->getId();
    }

    /**
     * Set subject.
     */
    public function setSubject(Taxon $subject): self
    {
        $this->subject?->removeSubjectRole($this);
        $this->subject = $subject;

        return $this;
    }

    /**
     * getSubject
     *
     * @return Taxon|null
     */
    public function getSubject(): ?Taxon
    {
        return $this->subject;
    }

    /**
     * getSubjectId
     *
     * @return int|null
     */
    #[Groups(['flat', 'normal'])]
    #[SerializedName('subject')]
    public function getSubjectId(): ?int
    {
        return $this->subject->getId();
    }

    /**
     * getSubjectGroupId
     *
     * Get the subject taxon's group id.
     * @return int|null
     */
    #[Groups(['normal'])]
    #[SerializedName('subjGroupId')]
    public function getSubjectGroupId(): ?int
    {
        return $this->subject->getGroupId();
    }

    /**
     * setObject
     *
     * @param Taxon $object
     * @return Interaction
     */
    public function setObject(Taxon $object): self
    {
        $this->object?->removeObjectRole($this);
        $this->object = $object;

        return $this;
    }

    /**
     * getObject
     *
     * @return Taxon|null
     */
    public function getObject(): ?Taxon
    {
        return $this->object;
    }

    /**
     * getObjectId
     *
     * @return int|null
     */
    #[Groups(['flat', 'normal'])]
    #[SerializedName('object')]
    public function getObjectId(): ?int
    {
        return $this->object->getId();
    }

    /**
     * getObjectId
     *
     * Get the object taxon's group id.
     * @return int|null
     */
    #[Groups(['normal'])]
    #[SerializedName('objGroupId')]
    public function getObjectGroupId(): ?int
    {
        return $this->object->getGroupId();
    }

    /**
     * addTag
     *
     * @param Tag $tag
     * @return Interaction
     */
    public function addTag(Tag $tag): self
    {
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * removeTag
     *
     * @param Tag $tag
     * @return Interaction
     */
    public function removeTag(Tag $tag): self
    {
        $this->tags->removeElement($tag);

        return $this;
    }

    /**
     * getTags
     *
     * @return Collection
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    /**
     * getTagData
     *
     * @return array
     */
    #[Groups(['flat', 'normal'])]
    #[SerializedName('tags')]
    public function getTagData(): array
    {
        $tagData = [];
        foreach ($this->tags as $tag) {
            $tagData[] = [
                'id' => $tag->getId(),
                'displayName' => $tag->getDisplayName(),
                'type' => $tag->getType(),
            ];
        }

        return $tagData;
    }

    /**
     * getTagIds
     *
     * @return array
     */
    public function getTagIds(): array
    {
        $tagIds = [];
        foreach ($this->tags as $tag) {
            $tagIds[] = $tag->getId();
        }

        return $tagIds;
    }

    /**
     * getTagNames
     *
     * Get comma separated tag names. If 'Secondary' present, it is listed last.
     * @return string|null
     */
    public function getTagNames(): ?string
    {
        $names = [];
        foreach ($this->tags as $tag) {
            $names[] = $tag->getDisplayName();
        }

        uasort($names, function ($a, $b) {
            if ($a == 'Secondary') {
                return 1;
            }
            if ($b == 'Secondary') {
                return -1;
            }

            return 0;
        });

        return join(', ', $names);
    }


    #[Groups(['normal'])]
    #[SerializedName('serverUpdatedAt')]
    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        $interactionType = $this->getInteractionType();
        $subject_name = $this->getSubject();
        $object_name = $this->getObject();
        if ($interactionType == 'Unspecified') {
            return 'Unspecified Interaction by ' . $subject_name . ' on ' . $object_name;
        } else {
            return $interactionType . ' by ' . $subject_name . ' of ' . $object_name;
        }
    }
}
