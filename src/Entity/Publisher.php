<?php

namespace App\Entity;

use App\Entity\Trait\BlameableTimestampableTrait;
use App\Entity\Trait\DescribableTrait;
use App\Entity\Trait\IdentifiableTrait;
use App\Entity\Trait\NameIdentifiableTrait;
use App\Entity\Trait\ReviewableTrait;
use App\Entity\Trait\SluggableTrait;
use App\Entity\Trait\SoftDeletableTrait;
use App\Repository\PublisherRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\SerializedName;

#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', hardDelete: true)]
#[ORM\Entity(repositoryClass: PublisherRepository::class)]
class Publisher implements \Stringable
{
    use BlameableTimestampableTrait;
    use IdentifiableTrait;
    use DescribableTrait;
    use NameIdentifiableTrait;
    use ReviewableTrait;
    use SoftDeletableTrait;
    use SluggableTrait;

    /** city */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column(nullable: true)]
    private ?string $city = null;

    /** country */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column(nullable: true)]
    private ?string $country = null;

    /** source */
    #[ORM\OneToOne(inversedBy: 'publisher', cascade: ['remove'])]
    #[ORM\JoinColumn(unique: true, nullable: false)]
    private ?Source $source = null;

    /**
     * setCity
     *
     * @param string|null $city
     * @return Publisher
     */
    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * getCity
     *
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * setCountry
     *
     * @param string|null $country
     * @return Publisher
     */
    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * getCountry
     *
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * setSource
     *
     * @param Source $source
     * @return Publisher
     */
    public function setSource(Source $source): self
    {
        $this->source = $source;

        return $this;
    }

    /**
     * getSource
     *
     * @return Source|null
     */
    public function getSource(): ?Source
    {
        return $this->source;
    }

    /**
     * getSourceId
     *
     * @return int
     */
    #[Groups(['normal'])]
    #[SerializedName('source')]
    public function getSourceId(): int
    {
        return $this->source->getId();
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        return $this->getDisplayName();
    }
}
