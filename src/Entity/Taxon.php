<?php /** @noinspection PhpUnhandledExceptionInspection */

/** @noinspection PhpDocMissingThrowsInspection */

namespace App\Entity;

use App\Entity\Trait\BlameableTimestampableTrait;
use App\Entity\Trait\IdentifiableTrait;
use App\Entity\Trait\NameIdentifiableTrait;
use App\Entity\Trait\ReviewableTrait;
use App\Entity\Trait\SluggableTrait;
use App\Entity\Trait\SoftDeletableTrait;
use App\Repository\TaxonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\SerializedName;

#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', hardDelete: true)]
#[ORM\Entity(repositoryClass: TaxonRepository::class)]
#[UniqueEntity(fields: ['name', 'displayName', 'parentTaxon'], message: 'Duplicate Taxon')]
class Taxon implements \Stringable
{
    use BlameableTimestampableTrait;
    use IdentifiableTrait;
    use NameIdentifiableTrait;
    use ReviewableTrait;
    use SoftDeletableTrait;
    use SluggableTrait;

    /** name */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column]
    private ?string $name = null;

    /** defaultGuid */
    #[ORM\Column(nullable: true)]
    private ?string $defaultGuid = null;

    /** isOldWorld */
    #[ORM\Column(nullable: true)]
    private ?bool $isOldWorld = null;

    /** linkDisplay */
    #[ORM\Column(nullable: true)]
    private ?string $linkDisplay = null;

    /** linkUrl */
    #[ORM\Column(nullable: true)]
    private ?string $linkUrl = null;

    /** root - Only set on the root taxon of the group's taxonomy. */
    #[ORM\OneToOne(mappedBy: 'taxon', cascade: ['persist'])]
    private ?GroupRoot $root = null;

    /** groupRoot - The root of the taxonomy the taxon belongs to. */
    #[ORM\ManyToOne(cascade: ['persist'], inversedBy: 'taxa')]
    #[ORM\JoinColumn(name: 'group_root')]
    private ?GroupRoot $groupRoot = null;

    /** rank */
    #[ORM\ManyToOne(inversedBy: 'taxa')]
    #[ORM\JoinColumn(name: 'tier', nullable: false)]
    private ?Rank $rank = null;

    /** parentTaxon */
    #[Groups(['flat'])]
    #[ORM\ManyToOne(inversedBy: 'childTaxa')]
    #[ORM\JoinColumn]
    private ?Taxon $parentTaxon = null;

    /**
     * namings
     *
     * @var Collection<int, Naming>
     */
    #[ORM\OneToMany(mappedBy: 'taxon', targetEntity: Naming::class)]
    private Collection $namings;

    /**
     * childTaxa
     *
     * @var Collection<int, Taxon>
     */
    #[Groups(['flat'])]
    #[ORM\OneToMany(mappedBy: 'parentTaxon', targetEntity: self::class, fetch: 'EXTRA_LAZY')]
//    #[ORM\OrderBy(['displayName' => Order::Ascending])] // todo: not working with foreach in header stats
    private Collection $childTaxa;

    /**
     * childNamings
     *
     * @var Collection<int, Naming>
     */
    #[ORM\OneToMany(mappedBy: 'parentTaxon', targetEntity: Naming::class)]
    private Collection $childNamings;

    /**
     * subjectRoles
     *
     * @var Collection<int, Interaction>
     */
    #[ORM\OneToMany(mappedBy: 'subject', targetEntity: Interaction::class, fetch: 'EXTRA_LAZY')]
    private Collection $subjectRoles;

    /**
     * objectRoles
     *
     * @var Collection<int, Interaction>
     */
    #[ORM\OneToMany(mappedBy: 'object', targetEntity: Interaction::class, fetch: 'EXTRA_LAZY')]
    private Collection $objectRoles;

    public function __construct()
    {
        $this->namings = new ArrayCollection();
        $this->childTaxa = new ArrayCollection();
        $this->childNamings = new ArrayCollection();
        $this->subjectRoles = new ArrayCollection();
        $this->objectRoles = new ArrayCollection();
    }

    /**
     * setName
     *
     * @param string $name
     * @return Taxon
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        if ($this->rank) {
            $this->updateDisplayName();
        }

        return $this;
    }

    /**
     * Sets the taxon's rank and name prepared for display.
     * @return void
     */
    public function updateDisplayName(): void
    {
        $isSpecies = $this->rank->getDisplayName() === 'Species';
        $displayName = $isSpecies ?
            $this->name : $this->rank->getDisplayName() . " " . $this->name;
        $this->displayName = $displayName;
    }

    /**
     * getName
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * setDefaultGuid
     *
     * @param string|null $defaultGuid
     * @return Taxon
     */
    public function setDefaultGuid(?string $defaultGuid): self
    {
        $this->defaultGuid = $defaultGuid;

        return $this;
    }

    /**
     * getDefaultGuid
     *
     * @return string|null
     */
    public function getDefaultGuid(): ?string
    {
        return $this->defaultGuid;
    }

    /**
     * setIsOldWorld
     *
     * @param bool $isOldWorld
     * @return Taxon
     */
    public function setIsOldWorld(bool $isOldWorld = false): self
    {
        $this->isOldWorld = $isOldWorld;

        return $this;
    }

    /**
     * getIsOldWorld
     *
     * @return bool
     */
    public function getIsOldWorld(): bool
    {
        return $this->isOldWorld || false;
    }

    /**
     * setLinkDisplay
     *
     * @param string|null $linkDisplay
     * @return Taxon
     */
    public function setLinkDisplay(?string $linkDisplay): self
    {
        $this->linkDisplay = $linkDisplay;

        return $this;
    }

    /**
     * getLinkDisplay
     *
     * @return string|null
     */
    public function getLinkDisplay(): ?string
    {
        return $this->linkDisplay;
    }

    /**
     * setLinkUrl
     *
     * @param string|null $linkUrl
     * @return Taxon
     */
    public function setLinkUrl(?string $linkUrl): self
    {
        $this->linkUrl = $linkUrl;

        return $this;
    }

    /**
     * getLinkUrl
     *
     * @return string|null
     */
    public function getLinkUrl(): ?string
    {
        return $this->linkUrl;
    }

    /**
     * setRoot
     *
     * Note: set automatically in GroupRoot->setTaxon.
     *
     * @param GroupRoot $root
     * @return Taxon
     */
    public function setRoot(GroupRoot $root): self
    {
        $this->root = $root;

        if ($root->getTaxon() !== $this) {
            $root->setTaxon($this);
        }

        return $this;
    }

    /**
     * getRoot
     *
     * @return GroupRoot|null
     */
    public function getRoot(): ?GroupRoot
    {
        return $this->root;
    }

    /**
     * setGroupRoot
     *
     * Note: set automatically @setParentTaxon.
     *
     * @param GroupRoot|null $groupRoot
     * @return Taxon
     */
    public function setGroupRoot(GroupRoot|null $groupRoot): self
    {
        $this->groupRoot = $groupRoot;

        return $this;
    }

    /**
     * getGroupRoot
     *
     * @return GroupRoot|null
     */
    public function getGroupRoot(): ?GroupRoot
    {
        return $this->groupRoot;
    }

    /**
     * getIsRoot
     *
     * @return bool
     */
    #[Groups(['flat', 'normal'])]
    #[SerializedName('isRoot')]
    public function getIsRoot(): bool
    {
        if ($this->root) {
            return true;
        }

        return false;
    }

    /**
     * getGroupData
     *
     * @todo: Refactor this to return only groupRoot id with 'normal' group.
     * @return array|null
     */
    #[Groups(['flat', 'normal'])]
    #[SerializedName('group')]
    public function getGroupData(): ?array
    {
        $groupRoot = $this->getGroupRoot();

        if (!$groupRoot) {
            return null;
        }
        $group = $groupRoot->getGroup();

        return [
            'id' => $group->getId(),
            'displayName' => $group->getDisplayName(),
            'pluralName' => $group->getPluralName(),
            'root' => [
                'id' => $groupRoot->getId(),
                'name' => $groupRoot->getTaxon()->getName(),
                'taxon' => $groupRoot->getTaxon()->getId()
            ]
        ];
    }

    public function getGroupName(): ?string
    {
        $groupRoot = $this->getGroupRoot();
        return $groupRoot?->getGroup()->getDisplayName();
    }

    /**
     * getGroupId
     *
     * @return int|null
     */
    public function getGroupId(): ?int
    {
        $groupRoot = $this->getGroupRoot();

        return $groupRoot?->getGroup()->getId();

    }

    /**
     * setRank
     *
     * @param Rank $rank
     * @return Taxon
     */
    public function setRank(Rank $rank): self
    {
        $this->rank = $rank;

        if ($this->name) {
            $this->updateDisplayName();
        }

        return $this;
    }

    /**
     * getRank
     *
     * @return Rank|null
     */
    public function getRank(): ?Rank
    {
        return $this->rank;
    }

    /**
     * getRankData
     *
     * Get the rank id and displayName.
     * @return array
     */
    #[Groups(['flat', 'normal'])]
    #[SerializedName('rank')]
    public function getRankData(): array
    {
        return [
            'id' => $this->rank->getId(),
            'displayName' => $this->rank->getDisplayName(),
        ];
    }

    /**
     * setParentTaxon
     *
     * When a parent taxon is set, the group root is updated.
     *
     * @param Taxon|null $taxon
     * @return Taxon
     */
    public function setParentTaxon(?Taxon $taxon = null): self
    {
        $this->parentTaxon = $taxon;

        $parentRoot = $taxon?->getGroupRoot();
        if ($parentRoot) {
            $this->setGroupRoot($parentRoot);
        }

        return $this;
    }

    /**
     * getParentTaxon
     *
     * @return Taxon|null
     */
    public function getParentTaxon(): ?Taxon
    {
        return $this->parentTaxon;
    }

    /**
     * getParentTaxonId
     *
     * @return int|null
     */
    #[Groups(['normal'])]
    #[SerializedName('parent')]
    public function getParentTaxonId(): ?int
    {
        return $this->parentTaxon?->getId();
    }

    /**
     * addNaming
     *
     * @param Naming $namings
     * @return Taxon
     */
    public function addNaming(Naming $namings): self
    {
        $this->namings[] = $namings;

        return $this;
    }

    /**
     * removeNaming
     *
     * @param Naming $namings
     * @return Taxon
     */
    public function removeNaming(Naming $namings): self
    {
        $this->namings->removeElement($namings);

        return $this;
    }

    /**
     * getNamings
     *
     * @return Collection
     */
    public function getNamings(): Collection
    {
        return $this->namings;
    }

    /**
     * addChildTaxa
     *
     * @param Taxon $childTaxon
     * @return Taxon
     */
    public function addChildTaxa(Taxon $childTaxon): self
    {
        $this->childTaxa[] = $childTaxon;

        return $this;
    }

    /**
     * removeChildTaxa
     *
     * @param Taxon $childTaxon
     * @return Taxon
     */
    public function removeChildTaxa(Taxon $childTaxon): self
    {
        $this->childTaxa->removeElement($childTaxon);

        return $this;
    }

    /**
     * getChildTaxa
     *
     * @return Collection
     */
    public function getChildTaxa(): Collection
    {
        return $this->childTaxa;
    }

    /**
     * getChildTaxonIds
     *
     * @return array
     */
    #[Groups(['normal'])]
    #[SerializedName('children')]
    public function getChildTaxonIds(): array
    {
        $childIds = [];
        foreach ($this->childTaxa as $child) {
            $childIds[] = $child->getId();
        }

        return $childIds;
    }

    /**
     * addChildNaming
     *
     * @param Naming $childNamings
     * @return Taxon
     */
    public function addChildNaming(Naming $childNamings): self
    {
        $this->childNamings[] = $childNamings;

        return $this;
    }

    /**
     * removeChildNaming
     *
     * @param Naming $childNamings
     * @return Taxon
     */
    public function removeChildNaming(Naming $childNamings): self
    {
        $this->childNamings->removeElement($childNamings);

        return $this;
    }

    /**
     * getChildNamings
     *
     * @return Collection
     */
    public function getChildNamings(): Collection
    {
        return $this->childNamings;
    }

    /**
     * addSubjectRole
     *
     * @param Interaction $subjectRole
     * @return Taxon
     */
    public function addSubjectRole(Interaction $subjectRole): self
    {
        $this->subjectRoles[] = $subjectRole;

        return $this;
    }

    /**
     * removeSubjectRole
     *
     * @param Interaction $subjectRole
     * @return Taxon
     */
    public function removeSubjectRole(Interaction $subjectRole): self
    {
        $this->subjectRoles->removeElement($subjectRole);
        $this->updated = new \DateTime('now', new \DateTimeZone('UTC'));

        return $this;
    }

    /**
     * getSubjectRoles
     *
     * @return Collection
     */
    public function getSubjectRoles(): Collection
    {
        return $this->subjectRoles;
    }

    /**
     * getSubjectRoleIds
     *
     * Ids for all interactions where the taxon is the subject.
     * @return array
     */
    #[Groups(['normal'])]
    #[SerializedName('subjectRoles')]
    public function getSubjectRoleIds(): array
    {
        $interactions = $this->subjectRoles;

        return $this->getInteractionData($interactions, true);
    }

    /**
     * getFlatSubjectRoles
     *
     * Denormalized interactions where the taxon was the subject.
     * @return array
     */
    #[Groups(['flat'])]
    #[SerializedName('subjectRoles')]
    public function getFlatSubjectRoles(): array
    {
        return $this->flattenTaxonInteractions($this->subjectRoles);
    }

    /**
     * addObjectRole
     *
     * @param Interaction $objectRoles
     * @return Taxon
     */
    public function addObjectRole(Interaction $objectRoles): self
    {
        $this->objectRoles[] = $objectRoles;

        return $this;
    }

    /**
     * removeObjectRole
     *
     * @param Interaction $objectRoles
     * @return Taxon
     */
    public function removeObjectRole(Interaction $objectRoles): self
    {
        $this->objectRoles->removeElement($objectRoles);
        $this->updated = new \DateTime('now', new \DateTimeZone('UTC'));

        return $this;
    }

    /**
     * getObjectRoles
     *
     * @return Collection
     */
    public function getObjectRoles(): Collection
    {
        return $this->objectRoles;
    }

    /**
     * getObjectRoleIds
     *
     * Ids for all interactions where the taxon is the object.
     * @return array
     */
    #[Groups(['normal'])]
    #[SerializedName('objectRoles')]
    public function getObjectRoleIds(): array
    {
        $interactions = $this->objectRoles;

        return $this->getInteractionData($interactions, true);
    }

    /**
     * getFlatObjectRoles
     *
     * Denormalized interactions where the taxon was the object.
     * @return array
     */
    #[Groups(['flat'])]
    #[SerializedName('objectRoles')]
    public function getFlatObjectRoles(): array
    {
        return $this->flattenTaxonInteractions($this->objectRoles);
    }

    /**
     * getInteractions
     *
     * Note: Used during entity DELETE and in migrations
     * @return Interaction[]
     */
    public function getInteractions(): array
    {
        $subj = $this->getInteractionData($this->subjectRoles);
        $obj = $this->getInteractionData($this->objectRoles);

        return array_merge($subj, $obj);
    }

    public function getRoleInteractions(string $role): array
    {
        $ints = $role === 'subject' ? $this->subjectRoles : $this->objectRoles;

        return $this->getInteractionData($ints);
    }

    /**
     * getInteractionData
     *
     * @param Collection $interactions
     * @param bool $getId
     * @return array
     */
    private function getInteractionData(Collection $interactions, bool $getId = false): array
    {
        $data = [];

        foreach ($interactions as $interaction) {
            $int = $getId ? $interaction->getId() : $interaction;
            $data[] = $int;
        }

        return $data;
    }

    /* Used in migrations */
    public function getAllInteractionIds(): array
    {
        return array_merge($this->getObjectRoleIds(), $this->getSubjectRoleIds());
    }

    public function flattenTaxonInteractions($interactions): array
    {
        $flattened = [];
        foreach ($interactions as $int) {  // print("\n    COUNTRY [".$int->getLocation()->getCountryData()."]    \n");
            $flatInt = [
                'country' => !$int->getLocation()->getCountryData() ? 'Unspecified' :
                    explode('[', (string)$int->getLocation()->getCountryData()['displayName'])[0],
                'id' => $int->getId(),
                'interactionType' => $int->getInteractionType()->getDisplayName(),
                'publication' => $int->getSource()->getParentSource()->getDisplayName(),
                'region' => $int->getLocation()->getRegionData()['displayName'],
            ];
            $flattened[] = $flatInt;
        }

        return $flattened;
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        return $this->getDisplayName();
    }
}
