<?php /** @noinspection PhpUnhandledExceptionInspection */

namespace App\Entity;

use App\Entity\Trait\IdentifiableTrait;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\LegacyPasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', hardDelete: true)]
#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
#[UniqueEntity(fields: ['email'], message: 'There is already an account with this email')]
class User implements UserInterface, LegacyPasswordAuthenticatedUserInterface, \Stringable
{
    use IdentifiableTrait;

    protected const string ROLE_DEFAULT = 'ROLE_USER';
    protected const string ROLE_CONTRIBUTOR = 'ROLE_CONTRIBUTOR';
    protected const string ROLE_EDITOR = 'ROLE_EDITOR';
    protected const string ROLE_MANAGER = 'ROLE_MANAGER';
    protected const string ROLE_ADMIN = 'ROLE_ADMIN';
    protected const string ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

    /**
     * All possible user roles. Each role inherits the permissions of the previous roles.
     * @var array|string[]
     */
    public const array USER_ROLES = [
        self::ROLE_DEFAULT => 'User can read interaction data and create limited user-specific data.',
        self::ROLE_CONTRIBUTOR => 'User can contribute interaction data for review.',
        self::ROLE_EDITOR => 'User can edit interaction data directly.',
        self::ROLE_MANAGER => 'User manages and reviews interaction data from contributors.',
        self::ROLE_ADMIN => 'User can manage other users and all types of data records.',
        self::ROLE_SUPER_ADMIN => 'User has max permissions and data access.',
    ];

    /** firstName */
    #[Groups(['flat'])]
    #[ORM\Column]
    protected ?string $firstName = null;

    /** lastName */
    #[Groups(['flat'])]
    #[ORM\Column]
    protected ?string $lastName = null;

    /** aboutMe */
    #[Groups(['flat'])]
    #[ORM\Column(type: Types::TEXT)]
    #[Assert\NotBlank(message: 'Tell us about your interest in bat eco-interactions')]
    #[Assert\Length(min: 2, max: '255', minMessage: 'Tell us more about your interest in bat eco-interactions', maxMessage: 'Limit of 255 characters exceded')]
    protected ?string $aboutMe = null;

    /** education */
    #[Groups(['flat'])]
    #[ORM\Column(nullable: true)]
    protected ?string $education = null;

    /** country */
    #[Groups(['flat'])]
    #[ORM\Column(nullable: true)]
    protected ?string $country = null;

    /** interest */
    #[Groups(['flat'])]
    #[ORM\Column(nullable: true)]
    protected ?string $interest = null;

    /** email */
    #[ORM\Column(length: 180)]
    protected ?string $email = null;

    /** emailCanonical */
    #[ORM\Column(length: 180, unique: true)]
    protected ?string $emailCanonical = null;

    /** username */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column(length: 180)]
    protected ?string $username = null;

    /** usernameCanonical */
    #[ORM\Column(length: 180, unique: true)]
    protected ?string $usernameCanonical = null;

    /** password */
    #[ORM\Column]
    // TODO: Should the assert be on plain password?
    #[Assert\Length(min: 8, max: '255', minMessage: 'Password must be at least 8 characters', maxMessage: 'Limit of 255 characters exceded')]
    protected ?string $password = null;

    /** plainPassword - Used for model validation. Must not be persisted. */
    #[Assert\Length(min: 8, max: '255', minMessage: 'Password must be at least 8 characters', maxMessage: 'Limit of 255 characters exceded')]
    public ?string $plainPassword;

    /** salt - deprecated */
    #[ORM\Column(nullable: true)]
    protected ?string $salt = null;

    /** roles */
    #[ORM\Column(type: Types::ARRAY)]
    protected array $roles = [];

    /** confirmationToken */
    #[ORM\Column(length: 180, unique: true, nullable: true)]
    protected ?string $confirmationToken = null;

    /**
     * @var Collection<int, ApiToken>
     */
    #[ORM\OneToMany(mappedBy: 'ownedBy', targetEntity: ApiToken::class)]
    private Collection $apiTokens;

    /** Scopes given during API authentication. */
    private ?array $accessTokenScopes = null;

    /* --- FORENSIC FIELDS --- */

    /** isVerified */
    #[ORM\Column]
    protected bool $isVerified;

    /** passwordRequestedAt */
    #[ORM\Column(nullable: true)]
    protected ?\DateTime $passwordRequestedAt = null;

    /** lastActivityAt */
    #[ORM\Column(nullable: true)]
    protected ?\DateTime $lastActivityAt = null;

    /** created */
    #[Gedmo\Timestampable(on: 'create')]
    #[ORM\Column]
    protected ?\DateTimeImmutable $created = null;

    /** updated */
    #[Gedmo\Timestampable(on: 'update')]
    #[ORM\Column]
    protected ?\DateTime $updated = null;

    /** deletedAt */
    #[ORM\Column(nullable: true)]
    protected ?\DateTime $deletedAt = null;

    public function __construct()
    {
        $this->isVerified = false;
        $this->roles = [];
        $this->apiTokens = new ArrayCollection();
    }

    /**
     * getUserIdentifier
     *
     * @return string
     */
    public function getUserIdentifier(): string
    {
        return $this->email;
    }

    /**
     * setFirstName
     *
     * @param string|null $firstName
     * @return User
     */
    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * getFirstName
     *
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * setLastName
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * getLastName
     *
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }


    #[Groups(['flat', 'normal'])]
    /**
     * getFullName
     *
     * @return string
     */
    public function getFullName(): string
    {
        if (!$this->firstName) {
            return $this->lastName;
        }
        return $this->firstName . ' ' . $this->lastName;
    }

    /**
     * setAboutMe
     *
     * @param string $aboutMe
     * @return User
     */
    public function setAboutMe(string $aboutMe): self
    {
        $this->aboutMe = $aboutMe;

        return $this;
    }

    /**
     * getAboutMe
     *
     * @return string|null
     */
    public function getAboutMe(): ?string
    {
        return $this->aboutMe;
    }

    /**
     * setCountry
     *
     * @param string|null $country
     * @return User
     */
    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * getCountry
     *
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * setEducation
     *
     * @param string|null $education
     * @return User
     */
    public function setEducation(?string $education): self
    {
        $this->education = $education;

        return $this;
    }

    /**
     * getEducation
     *
     * @return string|null
     */
    public function getEducation(): ?string
    {
        return $this->education;
    }

    /**
     * setInterest
     *
     * @param string|null $interest
     * @return User
     */
    public function setInterest(?string $interest): self
    {
        $this->interest = $interest;

        return $this;
    }

    /**
     * getInterest
     *
     * @return string|null
     */
    public function getInterest(): ?string
    {
        return $this->interest;
    }

    /**
     * setEmail
     *
     * @param string $email
     * @return User
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;
        $this->emailCanonical = $this->canonicalize($email);

        return $this;
    }

    /**
     * getEmail
     *
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * setUsername
     *
     * @param string $username
     * @return User
     */
    public function setUsername(string $username): self
    {
        $this->username = $username;
        $this->usernameCanonical = $this->canonicalize($username);

        return $this;
    }

    /**
     * getUsername
     *
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * canonicalize
     *
     * From the FOSUserBundle package.
     * @param string|null $string
     * @return string
     */
    public function canonicalize(?string $string): string
    {
        if (null === $string) {
            return '';
        }

        $encoding = mb_detect_encoding($string);
        return $encoding
            ? mb_convert_case($string, MB_CASE_LOWER, $encoding)
            : mb_convert_case($string, MB_CASE_LOWER);
    }

    /**
     * getSalt
     *
     * Deprecated
     * @return string|null
     */
    public function getSalt(): ?string
    {
        return $this->salt;
    }

    /**
     * getPassword
     *
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * setPassword
     *
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;
        $this->salt = null; // No longer used in modern hashing algorithms

        return $this;
    }

    /**
     * If you store any temporary, sensitive data on the user, clear it here.
     *
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        $this->plainPassword = null;
    }

    /**
     * getPlainPassword
     *
     * @return string|null
     */
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    /**
     * getRoles
     *
     * @see UserInterface
     * @return array
     */
    public function getRoles(): array
    {
        if (null === $this->accessTokenScopes) { // User authenticated normally, via login form UI
            $roles = $this->roles;
//            $roles[] = 'ROLE_FULL_USER';
        } else { // User authenticated with ApiToken
            $roles = $this->accessTokenScopes;
        }

        // guarantee every user at least has ROLE_USER
        $roles[] = static::ROLE_DEFAULT;

        return array_unique($roles);
    }

    /**
     * setRoles
     *
     * @param array $roles
     * @return User
     */
    public function setRoles(array $roles): self
    {
        $this->roles = [];

        foreach ($roles as $role) {
            $this->addRole($role);
        }

        return $this;
    }

    /**
     * setRoleToDefault
     *
     * @return User
     */
    public function setRoleToDefault(): self
    {
        $this->roles = [];

        // guarantee every user at least has ROLE_USER
        $this->roles[] = static::ROLE_DEFAULT;

        return $this;
    }

    /**
     * addRole
     *
     * @param string $role
     * @return User
     */
    public function addRole(string $role): self
    {
        if ($role === static::ROLE_DEFAULT) {
            return $this;
        }

        if (!in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    /**
     * hasRole
     *
     * @param string $role
     * @return bool
     */
    public function hasRole(string $role): bool
    {
        return in_array(strtoupper($role), $this->getRoles(), true);
    }

    /**
     * getUserRoleShortName
     *
     * Get short name of user role
     * @return string
     */
    #[Groups(['flat', 'normal'])]
    #[SerializedName('role')]
    public function getUserRoleShortName(): string
    {
        $roles = $this->getRoles();
        $shortNames = [
            'ROLE_SUPER_ADMIN' => 'super',
            'ROLE_ADMIN' => 'admin',
            'ROLE_QA_MANAGER' => 'manager',
            'ROLE_MANAGER' => 'manager',
            'ROLE_QA_EDITOR' => 'editor',
            'ROLE_EDITOR' => 'editor',
            'ROLE_QA_CONTRIBUTOR' => 'contributor',
            'ROLE_CONTRIBUTOR' => 'contributor',
            'ROLE_USER' => 'user',
        ];

        return $shortNames[$roles[0]];
    }

    /**
     * @return Collection<int, ApiToken>
     */
    public function getApiTokens(): Collection
    {
        return $this->apiTokens;
    }

    public function getValidTokenStrings(): array
    {
        return $this->getApiTokens()
            ->filter(fn(ApiToken $token) => $token->isValid())
            ->map(fn(ApiToken $token) => $token->getToken())
            ->toArray();
    }

    public function markAsTokenAuthenticated(array $scopes): void
    {
        $this->accessTokenScopes = $scopes;
    }

    public function addApiToken(ApiToken $apiToken): static
    {
        if (!$this->apiTokens->contains($apiToken)) {
            $this->apiTokens->add($apiToken);
            $apiToken->setOwnedBy($this);
        }

        return $this;
    }

    public function removeApiToken(ApiToken $apiToken): static
    {
        if ($this->apiTokens->removeElement($apiToken)) {
            // set the owning side to null (unless already changed)
            if ($apiToken->getOwnedBy() === $this) {
                $apiToken->setOwnedBy(null);
            }
        }

        return $this;
    }

    /* --- FORENSIC FIELDS --- */

    /**
     * setLastActivityAt
     *
     * @param \DateTime $lastActivityAt
     * @return User
     */
    public function setLastActivityAt(\DateTime $lastActivityAt): self
    {
        $this->lastActivityAt = $lastActivityAt;

        return $this;
    }

    /**
     * getLastActivityAt
     *
     * @return \DateTime|null
     */
    public function getLastActivityAt(): ?\DateTime
    {
        return $this->lastActivityAt;
    }

    /**
     * isActiveNow
     *
     * @return bool
     */
    public function isActiveNow(): bool
    {   // Delay during which the user is considered active
        $delay = new \DateTime('5 minutes ago', new \DateTimeZone('UTC'));

        return $this->getLastActivityAt() > $delay;
    }

    /**
     * isVerified
     *
     * @return bool
     */
    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    /**
     * setIsVerified
     *
     * @param bool $isVerified
     * @return User
     */
    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    /**
     * setCreated
     *
     * Note: test fixtures use this method.
     * @param \DateTimeImmutable $time
     * @return User
     */
    public function setCreated(\DateTimeImmutable $time): self
    {
        $this->created = $time;

        return $this;
    }

    /**
     * getCreated
     *
     * @return \DateTimeImmutable
     */
    public function getCreated(): \DateTimeImmutable
    {
        return $this->created;
    }

    /**
     * setUpdated
     *
     * Note: test fixtures use this method.
     * @param \DateTime $time
     * @return User
     */
    public function setUpdated(\DateTime $time): self
    {
        $this->updated = $time;

        return $this;
    }

    /**
     * getUpdated
     *
     * @return \DateTime|null
     */
    public function getUpdated(): ?\DateTime
    {
        return $this->updated;
    }

    /**
     * setDeletedAt
     *
     * @param \DateTime|null $deletedAt
     * @return User
     */
    public function setDeletedAt(?\DateTime $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * getDeletedAt
     *
     * @return \DateTime|null
     */
    public function getDeletedAt(): ?\DateTime
    {
        return $this->deletedAt;
    }

    public function __toString(): string
    {
        return $this->getUsername();
    }
}
