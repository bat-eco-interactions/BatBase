<?php

namespace App\Entity;

use App\Entity\Trait\BlameableTimestampableTrait;
use App\Entity\Trait\IdentifiableTrait;
use App\Entity\Trait\NameIdentifiableTrait;
use App\Entity\Trait\SluggableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\SerializedName;

#[ORM\Entity]
#[ORM\Table(name: '`group`')]
class Group implements \Stringable
{
    use BlameableTimestampableTrait;
    use IdentifiableTrait;
    use NameIdentifiableTrait;
    use SluggableTrait;

    /** pluralName */
    #[Groups(['normal'])]
    #[ORM\Column]
    private ?string $pluralName = null;

    /**
     * roots
     *
     * @var Collection<int, GroupRoot>
     */
    #[ORM\OneToMany(
        mappedBy: 'group',
        targetEntity: GroupRoot::class,
        cascade: ['remove'],
        fetch: 'EXTRA_LAZY',
        orphanRemoval: true
    )]
    private Collection $roots;

    public function __construct()
    {
        $this->roots = new ArrayCollection();
    }

    /**
     * setPluralName
     *
     * @param string $pluralName
     * @return Group
     */
    public function setPluralName(string $pluralName): self
    {
        $this->pluralName = $pluralName;

        return $this;
    }

    /**
     * getPluralName
     *
     * @return string
     */
    public function getPluralName(): string
    {
        return $this->pluralName;
    }

    /**
     * addRoots
     *
     * @param GroupRoot $groupRoot
     * @return Group
     */
    public function addRoot(GroupRoot $groupRoot): self
    {
        $this->roots[] = $groupRoot;

        return $this;
    }

    /**
     * removeRoots
     *
     * @param GroupRoot $groupRoot
     * @return Group
     */
    public function removeRoot(GroupRoot $groupRoot): self
    {
        $this->roots->removeElement($groupRoot);

        return $this;
    }

    /**
     * getRoots
     *
     * @return Collection
     */
    public function getRoots(): Collection
    {
        return $this->roots;
    }

    /**
     * getGroupRoots
     *
     * Builds an array of all group roots and their core data.
     * @todo: use as example to try to replace tags where the jms bug was
     * @return array
     */
    #[Groups(['normal'])]
    #[SerializedName('roots')]
    public function getGroupRoots(): array
    {
        $roots = [];

        foreach ($this->roots as $root) {
            $taxon = $root->getTaxon();
            $roots = array_merge(
                $roots, [
                    $root->getId() => [
                        'id' => $root->getId(),
                        'name' => $taxon->getName(),
                        'subRanks' => $root->getSubRanks(),
                        'taxon' => $taxon->getId(),
                    ],
                ]
            );
        }
        sort($roots);

        return $roots;
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        return $this->getDisplayName();
    }
}
