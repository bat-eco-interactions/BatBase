<?php

namespace App\Entity;

use App\Entity\Trait\BlameableTimestampableTrait;
use App\Entity\Trait\IdentifiableTrait;
use App\Entity\Trait\SoftDeletableTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', hardDelete: true)]
#[ORM\Entity]
class Taxonym implements \Stringable
{
    use BlameableTimestampableTrait;
    use IdentifiableTrait;
    use SoftDeletableTrait;

    /** name */
    #[ORM\Column]
    private ?string $name = null;

    /**
     * setName
     *
     * @param string|null $name
     * @return Taxonym
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * getName
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        return $this->getName();
    }
}
