<?php

namespace App\Entity;

use App\Entity\Trait\IdentifiableTrait;
use App\Entity\ValidInteractionTag as ValidTag;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\SerializedName;

#[ORM\Entity]
class ValidInteraction implements \Stringable
{
    use IdentifiableTrait;

    /** tagRequired */
    #[Groups(['normal'])]
    #[ORM\Column]
    private bool $tagRequired = false;

    /** interactionType */
    #[ORM\ManyToOne(inversedBy: 'validInteractions')]
    private ?InteractionType $interactionType = null;

    /** objectGroupRoot */
    #[ORM\ManyToOne(cascade: ['persist'], inversedBy: 'validObjectInteractions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?GroupRoot $objectGroupRoot = null;

    /** subjectGroupRoot */
    #[ORM\ManyToOne(cascade: ['persist'], inversedBy: 'validSubjectInteractions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?GroupRoot $subjectGroupRoot = null;

    /**
     * validInteractionTags
     *
     * @var Collection<int, ValidInteractionTag>
     */
    #[ORM\OneToMany(mappedBy: 'validInteraction', targetEntity: ValidTag::class, cascade: ['persist'], orphanRemoval: true)]
    private Collection $validInteractionTags;

    public function __construct()
    {
        $this->validInteractionTags = new ArrayCollection();
    }

    /**
     * setTagRequired
     *
     * @param bool $tagRequired
     * @return ValidInteraction
     */
    public function setTagRequired(bool $tagRequired): self
    {
        $this->tagRequired = $tagRequired;

        return $this;
    }

    /**
     * getTagRequired
     *
     * @return bool
     */
    public function getTagRequired(): bool
    {
        return $this->tagRequired;
    }

    /**
     * setInteractionType
     *
     * @param InteractionType $interactionType
     * @return ValidInteraction
     */
    public function setInteractionType(InteractionType $interactionType): self
    {
        $this->interactionType = $interactionType;

        return $this;
    }

    /**
     * getInteractionType
     *
     * @return InteractionType
     */
    public function getInteractionType(): InteractionType
    {
        return $this->interactionType;
    }

    /**
     * getInteractionTypeId
     *
     * @return int
     */
    #[Groups(['normal'])]
    #[SerializedName('interactionType')]
    public function getInteractionTypeId(): int
    {
        return $this->interactionType->getId();
    }

    /**
     * setObjectGroupRoot
     *
     * @param GroupRoot $objectGroupRoot
     * @return ValidInteraction
     */
    public function setObjectGroupRoot(GroupRoot $objectGroupRoot): self
    {
        $this->objectGroupRoot = $objectGroupRoot;

        return $this;
    }

    /**
     * getObjectGroupRoot
     *
     * @return GroupRoot
     */
    public function getObjectGroupRoot(): GroupRoot
    {
        return $this->objectGroupRoot;
    }

    /**
     * getObjectGroupRootId
     *
     * @return int
     */
    #[Groups(['normal'])]
    #[SerializedName('objectGroupRoot')]
    public function getObjectGroupRootId(): int
    {
        return $this->objectGroupRoot->getId();
    }

    /**
     * setSubjectGroupRoot
     *
     * @param GroupRoot $subjectGroupRoot
     * @return ValidInteraction
     */
    public function setSubjectGroupRoot(GroupRoot $subjectGroupRoot): self
    {
        $this->subjectGroupRoot = $subjectGroupRoot;

        return $this;
    }

    /**
     * getSubjectGroupRoot
     *
     * @return GroupRoot
     */
    public function getSubjectGroupRoot(): GroupRoot
    {
        return $this->subjectGroupRoot;
    }

    #[Groups(['normal'])]
    #[SerializedName('subjectGroupRoot')]
    /**
     * getSubjectGroupRootId
     *
     * @return int
     */
    public function getSubjectGroupRootId(): int
    {
        return $this->subjectGroupRoot->getId();
    }

    /**
     * addValidInteractionTag
     *
     * @param ValidInteractionTag $validInteractionTag
     * @return ValidInteraction
     */
    public function addValidInteractionTag(ValidTag $validInteractionTag): self
    {
        $this->validInteractionTags[] = $validInteractionTag;

        return $this;
    }

    /**
     * removeValidInteractionTag
     *
     * @param ValidInteractionTag $validInteractionTag
     * @return ValidInteraction
     */
    public function removeValidInteractionTag(ValidTag $validInteractionTag): self
    {
        $this->validInteractionTags->removeElement($validInteractionTag);

        return $this;
    }

    /**
     * getValidInteractionTags
     *
     * @return Collection
     */
    public function getValidInteractionTags(): Collection
    {
        return $this->validInteractionTags;
    }

    /**
     * getValidInteractionTagData
     *
     * For each tag, get id, displayName, and isRequired.
     * TODO: Is this format still necessary?
     * Note: Due to a bug with the JMS serializer (that seems to only affect this
     * entity and this property), tags are serialized as an array and are restored
     * to an object keyed by tag ID before being stored in the client's Local Storage.
     * @return array|null
     */
    #[Groups(['normal'])]
    #[SerializedName('tags')]
    public function getValidInteractionTagData(): ?array
    {
        $tags = [];

        foreach ($this->validInteractionTags as $validTag) {
            $tag = $validTag->getTag();
            $tags = array_merge(
                $tags, [
                    $tag->getId() => [
                        'id' => $tag->getId(),
                        'displayName' => $tag->getDisplayName(),
                        'isRequired' => $validTag->getIsRequired(),
                    ],
                ]
            );
        }

        return $tags;
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        $subjTaxonGroup = $this->subjectGroupRoot->getTaxon()->getDisplayName();
        $objTaxonGroup = $this->objectGroupRoot->getTaxon()->getDisplayName();
        $intType = $this->interactionType->getDisplayName();
        $string = "$subjTaxonGroup - $objTaxonGroup - $intType";

        $tags = [];

        foreach ($this->validInteractionTags as $validTag) {
            $tags[] = $validTag->getTag()->getDisplayName();
        }

        if (count($tags) > 0) {
            $string .= '(' . implode(", ", $tags) . ')';
        }

        return $string;
    }
}
