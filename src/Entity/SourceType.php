<?php

namespace App\Entity;

use App\Entity\Trait\DescribableTrait;
use App\Entity\Trait\IdentifiableTrait;
use App\Entity\Trait\NameIdentifiableTrait;
use App\Entity\Trait\SluggableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\SerializedName;
use Symfony\Component\Serializer\Attribute\Ignore;

#[ORM\Entity]
#[Groups(['normal'])]
class SourceType implements \Stringable
{
    use DescribableTrait;
    use IdentifiableTrait;
    use NameIdentifiableTrait;
    use SluggableTrait;

    /** ordinal */
    #[ORM\Column]
    private ?int $ordinal = null;

    /**
     * sources
     *
     * @var Collection<int, Source>
     */
    #[Ignore]
    #[ORM\OneToMany(mappedBy: 'sourceType', targetEntity: Source::class, fetch: 'EXTRA_LAZY')]
    private Collection $sources;

    public function __construct()
    {
        $this->sources = new ArrayCollection();
    }

    /**
     * setOrdinal
     *
     * @param int $ordinal
     * @return SourceType
     */
    public function setOrdinal(int $ordinal): self
    {
        $this->ordinal = $ordinal;

        return $this;
    }

    /**
     * getOrdinal
     *
     * @return int
     */
    public function getOrdinal(): int
    {
        return $this->ordinal;
    }

    /**
     * addSource
     *
     * @param Source $source
     * @return SourceType
     */
    public function addSource(Source $source): self
    {
        $this->sources[] = $source;

        return $this;
    }

    /**
     * removeSource
     *
     * @param Source $source
     * @return SourceType
     */
    public function removeSource(Source $source): self
    {
        $this->sources->removeElement($source);

        return $this;
    }

    /**
     * getSources
     *
     * @return Collection
     */
    public function getSources(): Collection
    {
        return $this->sources;
    }

    /**
     * getSourceIds
     *
     * @return array
     */
    #[Groups(['normal'])]
    #[SerializedName('sources')]
    public function getSourceIds(): array
    {
        $srcIds = [];
        foreach ($this->sources as $source) {
            $srcIds[] = $source->getId();
        }

        return $srcIds;
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        return $this->getDisplayName();
    }
}
