<?php

namespace App\Entity;

use App\Entity\Trait\BlameableTimestampableTrait;
use App\Entity\Trait\DescribableTrait;
use App\Entity\Trait\IdentifiableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\SerializedName;

#[ORM\Entity]
class GroupRoot implements \Stringable
{
    use BlameableTimestampableTrait;
    use DescribableTrait;
    use IdentifiableTrait;

    /**
     * subRanks
     *
     * JSON array with the rank IDs for each sub-rank in the sub-group.
     * eg, "[7, 6, 5]"
     * -- Tier is used in the database, as "rank" is a reserved MySQL term
     */
    #[Groups(['normal'])]
    #[SerializedName('subRanks')]
    #[ORM\Column(name: 'sub_tiers')]
    private ?string $subRanks = null;

    /** group */
    #[ORM\ManyToOne(cascade: ['persist'], inversedBy: 'roots')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Group $group = null;

    /** taxon */
    #[ORM\OneToOne(inversedBy: 'root', cascade: ['persist'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Taxon $taxon = null;

    /**
     * taxa
     *
     * @var Collection<int, Taxon>
     */
    #[ORM\OneToMany(mappedBy: 'groupRoot', targetEntity: Taxon::class, fetch: 'EXTRA_LAZY')]
    private Collection $taxa;

    /**
     * validObjectInteractions
     *
     * @var Collection<int, ValidInteraction>
     */
    #[ORM\OneToMany(mappedBy: 'objectGroupRoot', targetEntity: ValidInteraction::class)]
    private Collection $validObjectInteractions;

    /**
     * validSubjectInteractions
     *
     * @var Collection<int, ValidInteraction>
     */
    #[ORM\OneToMany(mappedBy: 'subjectGroupRoot', targetEntity: ValidInteraction::class)]
    private Collection $validSubjectInteractions;

    public function __construct()
    {
        $this->taxa = new ArrayCollection();
        $this->validObjectInteractions = new ArrayCollection();
        $this->validSubjectInteractions = new ArrayCollection();
    }

    /**
     * setDescription
     *
     * @param string|null $description
     * @return GroupRoot
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * getDescription
     * @todo - how to overwrite a trait??
     *
     * @return string|null
     */
    public function getDescription(): ?string
    {
        if (null === $this->description && null !== $this->group) {
            return $this->group->getPluralName();
        }
        return $this->description;
    }

    /**
     * setSubRanks
     *
     * JSON array with the rank IDs for each sub-rank in the sub-group.
     * eg, "[7, 6, 5]"
     * @param string $subRanks
     * @return GroupRoot
     */
    public function setSubRanks(string $subRanks): self
    {
        $this->subRanks = $subRanks;

        return $this;
    }

    /**
     * getSubRanks
     *
     * @return string
     */
    public function getSubRanks(): string
    {
        return $this->subRanks;
    }

    /**
     * setGroup
     *
     * @param Group $group
     * @return GroupRoot
     */
    public function setGroup(Group $group): self
    {
        $this->group = $group;

        $group->addRoot($this);

        return $this;
    }

    /**
     * getGroup
     *
     * @return Group
     */
    public function getGroup(): Group
    {
        return $this->group;
    }

    /**
     * getGroupId
     *
     * @return int
     */
    #[Groups(['normal'])]
    #[SerializedName('group')]
    public function getGroupId(): int
    {
        return $this->group->getId();
    }

    /**
     * setTaxon
     *
     * @param Taxon $taxon
     * @return GroupRoot
     */
    public function setTaxon(Taxon $taxon): self
    {
        $this->taxon = $taxon;

        $taxon->setGroupRoot($this);
        $taxon->setRoot($this);

        return $this;
    }

    /**
     * getTaxon
     *
     * @return Taxon
     */
    public function getTaxon(): Taxon
    {
        return $this->taxon;
    }

    /**
     * getTaxonId
     *
     * @return int
     */
    #[Groups(['normal'])]
    #[SerializedName('taxon')]
    public function getTaxonId(): int
    {
        return $this->taxon->getId();
    }

    /**
     * addTaxa
     *
     * @param Taxon $taxon
     * @return GroupRoot
     */
    public function addTaxa(Taxon $taxon): self
    {
        $this->taxa[] = $taxon;

        return $this;
    }

    /**
     * removeTaxa
     *
     * @param Taxon $taxon
     * @return GroupRoot
     */
    public function removeTaxa(Taxon $taxon): self
    {
        $this->taxa->removeElement($taxon);

        return $this;
    }

    /**
     * getTaxa
     *
     * @return Collection
     */
    public function getTaxa(): Collection
    {
        return $this->taxa;
    }


    /**
     * addValidObjectInteractions
     *
     * @param ValidInteraction $validInt
     * @return GroupRoot
     */
    public function addValidObjectInteractions(ValidInteraction $validInt): self
    {
        $this->validObjectInteractions[] = $validInt;

        return $this;
    }

    /**
     * removeValidObjectInteractions
     *
     * @param ValidInteraction $validInt
     * @return GroupRoot
     */
    public function removeValidObjectInteractions(ValidInteraction $validInt): self
    {
        $this->validObjectInteractions->removeElement($validInt);

        return $this;
    }

    /**
     * getValidObjectInteractions
     *
     * @return Collection
     */
    public function getValidObjectInteractions(): Collection
    {
        return $this->validObjectInteractions;
    }

    /**
     * addValidSubjectInteractions
     *
     * @param ValidInteraction $validInt
     * @return GroupRoot
     */
    public function addValidSubjectInteractions(ValidInteraction $validInt): self
    {
        $this->validSubjectInteractions[] = $validInt;

        return $this;
    }

    /**
     * removeValidSubjectInteractions
     *
     * @param ValidInteraction $validInt
     * @return GroupRoot
     */
    public function removeValidSubjectInteractions(ValidInteraction $validInt): self
    {
        $this->validSubjectInteractions->removeElement($validInt);

        return $this;
    }

    /**
     * getValidSubjectInteractions
     *
     * @return Collection
     */
    public function getValidSubjectInteractions(): Collection
    {
        return $this->validSubjectInteractions;
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        return $this->group->getDisplayName() . ' ' . $this->taxon->getDisplayName();
    }
}
