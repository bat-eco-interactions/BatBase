<?php

namespace App\Entity\Trait;

use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\SerializedName;

trait ReviewableTrait
{
    /** reviewed */
    #[Groups(['normal'])]
    #[SerializedName('reviewedAt')]
    #[ORM\Column(nullable: true)]
    private ?\DateTime $reviewed = null;

    /** reviewedBy */
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(name: 'reviewed_by')]
    private ?User $reviewedBy = null;

    /**
     * setReviewed
     *
     * @param \DateTime|null $reviewedAt
     * @return self
     */
    public function setReviewed(?\DateTime $reviewedAt): self
    {
        $this->reviewed = $reviewedAt;

        return $this;
    }

    /**
     * getReviewed
     *
     * @return \DateTime|null
     */
    public function getReviewed(): ?\DateTime
    {
        return $this->reviewed;
    }

    /**
     * setReviewedBy
     *
     * @param User|null $user
     * @return self
     */
    public function setReviewedBy(?User $user): self
    {
        $this->reviewedBy = $user;

        return $this;
    }

    /**
     * getReviewedBy
     *
     * @return User|null
     */
    public function getReviewedBy(): ?User
    {
        return $this->reviewedBy;
    }

    /**
     * getReviewedByUsername
     *
     * @return string|null
     */
    #[Groups(['normal'])]
    #[SerializedName('reviewedBy')]
    public function getReviewedByUsername(): ?string
    {
        return $this->reviewedBy?->getUsername();
    }
}