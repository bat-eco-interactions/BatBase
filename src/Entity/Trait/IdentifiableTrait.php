<?php

namespace App\Entity\Trait;

use Symfony\Component\Serializer\Attribute\Groups;
use Doctrine\ORM\Mapping as ORM;

trait IdentifiableTrait
{
    /** id */
    #[ORM\Column]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[Groups(['flat', 'normal'])]
    private ?int $id = null;

    /**
     * getId
     * @todo - once source is removed, this should always return int
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }
}
