<?php

namespace App\Entity\Trait;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

trait FileUploadTrait
{
    /** path */
    #[ORM\Column]
    private ?string $path = null;

    /** mimeType */
    #[ORM\Column]
    private ?string $mimeType = null;

    /** size */
    #[ORM\Column(type: Types::DECIMAL)]
    private ?string $size = null;

    /**
     * getPath
     *
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * setPath
     *
     * @todo: customize default value?
     *
     * @param string $path
     * @return self
     */
    public function setPath(string $path = 'uploads/publications/'): self
    {
        $this->path = $path;

        return $this;
    }

    /**
     * getMimeType
     *
     * @return string
     */
    public function getMimeType(): string
    {
        return $this->mimeType;
    }

    /**
     * setMimeType
     *
     * @param string $mimeType
     * @return self
     */
    public function setMimeType(string $mimeType): self
    {
        $this->mimeType = $mimeType;

        return $this;
    }

    /**
     * getSize
     *
     * @return string
     */
    public function getSize(): string
    {
        return $this->size;
    }

    /**
     * setSize
     *
     * @param string $size
     * @return self
     */
    public function setSize(string $size): self
    {
        $this->size = $size;

        return $this;
    }

}