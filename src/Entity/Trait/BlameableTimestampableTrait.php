<?php

namespace App\Entity\Trait;

use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\SerializedName;

trait BlameableTimestampableTrait
{
    /** created */
    #[Gedmo\Timestampable(on: 'create')]
    #[ORM\Column]
    private ?\DateTimeImmutable $created = null;

    /** createdBy */
    #[Gedmo\Blameable(on: 'create')]
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(name: 'created_by')]
    private ?User $createdBy = null;

    /** updated */
    #[Gedmo\Timestampable(on: 'update')]
    #[ORM\Column]
    private ?\DateTime $updated = null;

    /** updatedBy */
    #[Gedmo\Blameable(on: 'update')]
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(name: 'updated_by')]
    private ?User $updatedBy = null;

    /**
     * setCreated
     *
     * Note: test fixtures use this method.
     * @param \DateTimeImmutable $time
     * @return self
     */
    public function setCreated(\DateTimeImmutable $time): self
    {
        $this->created = $time;

        return $this;
    }

    /**
     * getCreated
     *
     * @return \DateTimeImmutable
     */
    public function getCreated(): \DateTimeImmutable
    {
        return $this->created;
    }

    /**
     * setCreatedBy
     *
     * @param User $user
     * @return self
     */
    public function setCreatedBy(User $user): self
    {
        $this->createdBy = $user;

        return $this;
    }

    /**
     * getCreatedBy
     *
     * @return User
     */
    public function getCreatedBy(): User
    {
        return $this->createdBy;
    }

    /**
     * setUpdated
     *
     * Note: test fixtures use this method.
     * @param \DateTime $time
     * @return self
     */
    public function setUpdated(\DateTime $time): self
    {
        $this->updated = $time;

        return $this;
    }

    /**
     * getUpdated
     *
     * @return \DateTime|null
     */
    public function getUpdated(): ?\DateTime
    {
        return $this->updated;
    }

    /**
     * setUpdatedBy
     *
     * @param User $user
     * @return self
     */
    public function setUpdatedBy(User $user): self
    {
        $this->updatedBy = $user;

        return $this;
    }

    /**
     * getUpdatedBy
     *
     * @return User|null
     */
    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }

    /**
     * getUpdatedByFirstName
     *
     * @return string
     */
    #[Groups(['flat', 'normal'])]
    #[SerializedName('updatedBy')]
    public function getUpdatedByFirstName(): string
    {
        $user = $this->updatedBy ?: $this->createdBy;

        return $user ? $user->getFirstName() : '';
    }
}