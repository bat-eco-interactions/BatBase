<?php

namespace App\Entity\Trait;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Attribute\Groups;

trait SluggableTrait
{
    /** slug */
    #[Gedmo\Slug(fields: ['displayName'])]
    #[Groups('normal')]
    #[ORM\Column(unique: true)]
    private ?string $slug = null;

    /**
     * getSlug
     *
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * setSlug
     *
     * Note: test fixtures use this method.
     * @param string $slug
     * @return self
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }
}