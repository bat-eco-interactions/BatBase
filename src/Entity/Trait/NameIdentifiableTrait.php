<?php

namespace App\Entity\Trait;

use Symfony\Component\Serializer\Attribute\Groups;
use Doctrine\ORM\Mapping as ORM;

trait NameIdentifiableTrait
{
    /** displayName - Unique name of the self */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column(unique: true)]
    private ?string $displayName = null;

    /**
     * setDisplayName
     *
     * @param string $displayName
     * @return self
     */
    public function setDisplayName(string $displayName): self
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * getDisplayName
     *
     * @return string
     */
    public function getDisplayName(): string
    {
        return $this->displayName;
    }
}