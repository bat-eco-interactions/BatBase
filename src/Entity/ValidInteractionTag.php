<?php

namespace App\Entity;

use App\Entity\Trait\IdentifiableTrait;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class ValidInteractionTag implements \Stringable
{
    use IdentifiableTrait;

    /** isRequired */
    #[ORM\Column(name: 'required', options: ['default' => '0'])]
    private bool $isRequired = false;

    /** validInteraction */
    #[ORM\ManyToOne(cascade: ['persist'], inversedBy: 'validInteractionTags')]
    #[ORM\JoinColumn(nullable: false)]
    private ?ValidInteraction $validInteraction = null;

    /** tag */
    #[ORM\ManyToOne(cascade: ['persist'], inversedBy: 'validInteractionTags')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Tag $tag = null;

    /**
     * setIsRequired
     *
     * @param bool $isRequired
     * @return ValidInteractionTag
     */
    public function setIsRequired(bool $isRequired): self
    {
        $this->isRequired = $isRequired;

        return $this;
    }

    /**
     * getIsRequired
     *
     * @return bool
     */
    public function getIsRequired(): bool
    {
        return $this->isRequired;
    }

    /**
     * setValidInteraction
     *
     * @param ValidInteraction $validInteraction
     * @return ValidInteractionTag
     */
    public function setValidInteraction(ValidInteraction $validInteraction): self
    {
        $this->validInteraction = $validInteraction;

        return $this;
    }

    /**
     * getValidInteraction
     *
     * @return ValidInteraction
     */
    public function getValidInteraction(): ValidInteraction
    {
        return $this->validInteraction;
    }

    /**
     * setTag
     *
     * @param Tag $tag
     * @return ValidInteractionTag
     */
    public function setTag(Tag $tag): self
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * getTag
     *
     * @return Tag
     */
    public function getTag(): Tag
    {
        return $this->tag;
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        return 'Valid Interaction Tag: ' . $this->getTag()->getDisplayName();
    }
}
