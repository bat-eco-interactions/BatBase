<?php

namespace App\Entity;

use App\Entity\Trait\DescribableTrait;
use App\Entity\Trait\IdentifiableTrait;
use App\Entity\Trait\NameIdentifiableTrait;
use App\Entity\Trait\SluggableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\SerializedName;

#[ORM\Entity]
class CitationType implements \Stringable
{
    use DescribableTrait;
    use IdentifiableTrait;
    use NameIdentifiableTrait;
    use SluggableTrait;

    /**
     * citations
     *
     * @var Collection<int, Citation>
     */
    #[ORM\OneToMany(mappedBy: 'citationType', targetEntity: Citation::class, fetch: 'EXTRA_LAZY')]
    private Collection $citations;

    public function __construct()
    {
        $this->citations = new ArrayCollection();
    }

    /**
     * addCitation
     *
     * @param Citation $citation
     * @return CitationType
     */
    public function addCitation(Citation $citation): self
    {
        $this->citations[] = $citation;

        return $this;
    }

    /**
     * removeCitation
     *
     * @param Citation $citation
     * @return CitationType
     */
    public function removeCitation(Citation $citation): self
    {
        $this->citations->removeElement($citation);

        return $this;
    }

    /**
     * getCitations
     *
     * @return Collection
     */
    public function getCitations(): Collection
    {
        return $this->citations;
    }

    /**
     * getCitationIds
     *
     * @return array
     */
    #[Groups(['normal'])]
    #[SerializedName('citations')]
    public function getCitationIds(): array
    {
        $ids = [];

        foreach ($this->citations as $cit) {
            $ids[] = $cit->getId();
        }

        return $ids;
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        return $this->getDisplayName();
    }
}
