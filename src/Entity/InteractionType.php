<?php

namespace App\Entity;

use App\Entity\Trait\IdentifiableTrait;
use App\Entity\Trait\NameIdentifiableTrait;
use App\Entity\Trait\SluggableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity]
class InteractionType implements \Stringable
{
    use IdentifiableTrait;
    use SluggableTrait;
    use NameIdentifiableTrait;

    /** isSymmetric - not currently used */
    #[Groups(['normal'])]
    #[ORM\Column(nullable: true)]
    private ?bool $isSymmetric = null;

    /** activeForm */
    #[Groups(['normal'])]
    #[ORM\Column]
    private ?string $activeForm = null;

    /**
     * interactions
     *
     * @var Collection<int, Interaction>
     */
    #[ORM\OneToMany(mappedBy: 'interactionType', targetEntity: Interaction::class, fetch: 'EXTRA_LAZY')]
    private Collection $interactions;

    /**
     * validInteractions
     *
     * @var Collection<int, ValidInteraction>
     */
    #[ORM\OneToMany(mappedBy: 'interactionType', targetEntity: ValidInteraction::class)]
    private Collection $validInteractions;

    public function __construct()
    {
        $this->interactions = new ArrayCollection();
        $this->validInteractions = new ArrayCollection();
    }
    /**
     * setIsSymmetric
     *
     * @param bool|null $isSymmetric
     * @return InteractionType
     */
    public function setIsSymmetric(?bool $isSymmetric): self
    {
        $this->isSymmetric = $isSymmetric;

        return $this;
    }

    /**
     * getIsSymmetric
     *
     * @return bool|null
     */
    public function getIsSymmetric(): ?bool
    {
        return $this->isSymmetric;
    }

    /**
     * setActiveForm
     *
     * @param string $activeForm
     * @return InteractionType
     */
    public function setActiveForm(string $activeForm): self
    {
        $this->activeForm = $activeForm;

        return $this;
    }

    /**
     * getActiveForm
     *
     * @return string
     */
    public function getActiveForm(): string
    {
        return $this->activeForm;
    }

    /**
     * addInteraction
     *
     * @param Interaction $interactions
     * @return InteractionType
     */
    public function addInteraction(Interaction $interactions): self
    {
        $this->interactions[] = $interactions;

        return $this;
    }

    /**
     * removeInteraction
     *
     * @param Interaction $interactions
     * @return InteractionType
     */
    public function removeInteraction(Interaction $interactions): self
    {
        $this->interactions->removeElement($interactions);

        return $this;
    }

    /**
     * getInteractions
     *
     * @return Collection
     */
    public function getInteractions(): Collection
    {
        return $this->interactions;
    }

    /**
     * addValidInteraction
     *
     * @param ValidInteraction $validInteraction
     * @return InteractionType
     */
    public function addValidInteraction(ValidInteraction $validInteraction): self
    {
        $this->validInteractions[] = $validInteraction;

        return $this;
    }

    /**
     * removeValidInteraction
     *
     * @param ValidInteraction $validInteraction
     * @return InteractionType
     */
    public function removeValidInteraction(ValidInteraction $validInteraction): self
    {
        $this->validInteractions->removeElement($validInteraction);

        return $this;
    }

    /**
     * getValidInteractions
     *
     * @return Collection
     */
    public function getValidInteractions(): Collection
    {
        return $this->validInteractions;
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        return $this->getDisplayName();
    }
}
