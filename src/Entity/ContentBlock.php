<?php

namespace App\Entity;

use App\Entity\Trait\BlameableTimestampableTrait;
use App\Entity\Trait\IdentifiableTrait;
use App\Entity\Trait\NameIdentifiableTrait;
use App\Entity\Trait\SluggableTrait;
use App\Entity\Trait\SoftDeletableTrait;
use App\Repository\ContentBlockRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Attribute\Groups;

#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', hardDelete: true)]
#[ORM\Entity(repositoryClass: ContentBlockRepository::class)]
class ContentBlock implements \Stringable
{
    use BlameableTimestampableTrait;
    use IdentifiableTrait;
//    use NameIdentifiableTrait;
    use SluggableTrait;
    use SoftDeletableTrait;

    /** slug */
    #[Gedmo\Slug(fields: ['name'])]
    #[Groups('normal')]
    #[ORM\Column(unique: true)]
    private ?string $slug = null;

    /** todo: name -> displayName */

    #[ORM\Column(unique: true)]
    private ?string $name = null;

    /** page */
    #[ORM\Column(length: 100, nullable: true)]
    private ?string $page = null;

    /** content */
    #[ORM\Column(type: Types::TEXT)]
    private ?string $content = null;

    /**
     * setName
     *
     * @param string $name
     * @return ContentBlock
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * getName
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * setPage
     *
     * @param string|null $page
     * @return ContentBlock
     */
    public function setPage(?string $page): self
    {
        $this->page = $page;

        return $this;
    }

    /**
     * getPage
     *
     * @return string|null
     */
    public function getPage(): ?string
    {
        return $this->page;
    }

    /**
     * setContent
     *
     * @param string $content
     * @return ContentBlock
     */
    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * getContent
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        return $this->getName();
    }
}
