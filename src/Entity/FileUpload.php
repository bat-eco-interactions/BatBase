<?php

namespace App\Entity;

use App\Entity\Trait\BlameableTimestampableTrait;
use App\Entity\Trait\DescribableTrait;
use App\Entity\Trait\FileUploadTrait;
use App\Entity\Trait\IdentifiableTrait;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * PDF File Upload.
 */
#[ORM\Entity]
#[UniqueEntity(fields: ['title'], message: 'A publication with this title has already been submitted.')]
#[Vich\Uploadable]
class FileUpload implements \Stringable
{
    use BlameableTimestampableTrait;
    use DescribableTrait;
    use FileUploadTrait;
    use IdentifiableTrait;

    /** filename - URL to image|pdf file. */
    #[ORM\Column]
    private ?string $fileName = null;

    /** title */
    #[ORM\Column(unique: true)]
    private ?string $title = null;

    /** pdfFile - Used as container for UploadedFile obj */
    #[Vich\UploadableField(mapping: 'pdf_image', fileNameProperty: 'fileName', size: 'size')]
    private UploadedFile|File|null $pdfFile;

    /**
     * getFileName
     *
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * setFileName
     *
     * @param string $fileName
     * @return FileUpload
     */
    public function setFileName(string $fileName): self
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * getTitle
     *
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * setTitle
     *
     * @param string $title
     * @return FileUpload
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * getPdfFile
     *
     * @return File|UploadedFile|null
     */
    public function getPdfFile(): File|UploadedFile|null
    {
        return $this->pdfFile;
    }

    /**
     * setPdfFile
     *
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update.
     * @param File|UploadedFile|null $pdfFile
     * @return FileUpload
     */
    public function setPdfFile(File|UploadedFile $pdfFile = null): FileUpload
    {
        $this->pdfFile = $pdfFile;

        if (null !== $pdfFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->setUpdated(new \DateTime('now', new \DateTimeZone('UTC')));
        }

        return $this;
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        return $this->description;
    }
}
