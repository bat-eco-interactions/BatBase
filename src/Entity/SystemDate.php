<?php /** @noinspection PhpUnhandledExceptionInspection */

namespace App\Entity;

use App\Entity\Trait\IdentifiableTrait;
use App\Repository\SystemDateRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

#[ORM\Entity(repositoryClass: SystemDateRepository::class)]
class SystemDate implements \Stringable
{
    use IdentifiableTrait;

    /** entity */
    #[ORM\Column(name: 'entity', length: '30')]
    private ?string $entity = null;

    /** updated */
    #[ORM\Column]
    private ?\DateTime $updated = null;

    /** updatedBy */
    #[Gedmo\Blameable(on: 'update')]
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(name: 'updated_by')]
    private ?User $updatedBy = null;

    /**
     * setEntity
     *
     * @param string $entity
     * @return SystemDate
     */
    public function setEntity(string $entity): self
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * getEntity
     *
     * @return string
     */
    public function getEntity(): string
    {
        return $this->entity;
    }

    /* --- FORENSIC FIELDS --- */

    /**
     * setUpdated
     *
     * The time an instance of the entity class was updated.
     * Note: the 'System' entity will be updated as well, handled elsewhere.
     * @param \DateTime|null $updatedAt
     * @return SystemDate
     */
    public function setUpdated(\DateTime $updatedAt = null): self
    {
        if (!$updatedAt) {
            $updatedAt = new \DateTime('now', new \DateTimeZone('UTC'));
        }
        $this->updated = $updatedAt;

        return $this;
    }

    /**
     * getUpdated
     *
     * @return \DateTime|null
     */
    public function getUpdated(): ?\DateTime
    {
        return $this->updated;
    }

    /**
     * setUpdatedBy
     *
     * @param User|null $user
     * @return SystemDate
     */
    public function setUpdatedBy(?User $user): self
    {
        $this->updatedBy = $user;

        return $this;
    }

    /**
     * getUpdatedBy
     *
     * @return User|null
     */
    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        return $this->getEntity();
    }
}
