<?php

namespace App\Entity;

use App\Entity\Trait\BlameableTimestampableTrait;
use App\Entity\Trait\IdentifiableTrait;
use App\Entity\Trait\SoftDeletableTrait;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', hardDelete: true)]
#[ORM\Entity]
class Feedback implements \Stringable
{
    use BlameableTimestampableTrait;
    use IdentifiableTrait;
    use SoftDeletableTrait;

    /** status */
    #[ORM\Column]
    private ?int $status = null;

    /** topic */
    #[ORM\Column]
    private ?string $topic = null;

    /** route */
    #[ORM\Column]
    private ?string $route = null;

    /** content */
    #[ORM\Column(type: Types::TEXT)]
    private ?string $content = null;

    /** adminNotes */
    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $adminNotes = null;

    /** assignedUser */
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(name: 'assigned_user')]
    private ?User $assignedUser = null;

    /**
     * setStatus
     *
     * @param int $status
     * @return Feedback
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * getStatus
     *
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * getStatusStr
     *
     * @return string
     */
    public function getStatusStr(): string
    {
        $statusAry = ['Closed', 'Follow-Up', 'Read', 'Unread'];
        $status = $this->status;

        return $statusAry[$status];
    }

    /**
     * setRoute
     *
     * @param string $route
     * @return Feedback
     */
    public function setRoute(string $route): self
    {
        $this->route = $route;

        return $this;
    }

    /**
     * getRoute
     *
     * @return string
     */
    public function getRoute(): string
    {
        return $this->route;
    }

    /**
     * setTopic
     *
     * @param string $topic
     * @return Feedback
     */
    public function setTopic(string $topic): self
    {
        $this->topic = $topic;

        return $this;
    }

    /**
     * getTopic
     *
     * @return string
     */
    public function getTopic(): string
    {
        return $this->topic;
    }

    /**
     * setContent
     *
     * @param string $content
     * @return Feedback
     */
    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * getContent
     *
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * setAdminNotes
     *
     * @param string|null $adminNotes
     * @return Feedback
     */
    public function setAdminNotes(?string $adminNotes): self
    {
        $this->adminNotes = $adminNotes;

        return $this;
    }

    /**
     * getAdminNotes
     *
     * @return string|null
     */
    public function getAdminNotes(): ?string
    {
        return $this->adminNotes;
    }

    /**
     * setAssignedUser
     *
     * @param User|null $assignedUser
     * @return Feedback
     */
    public function setAssignedUser(?User $assignedUser = null): self
    {
        $this->assignedUser = $assignedUser;

        return $this;
    }

    /**
     * getAssignedUser
     *
     * @return User|null
     */
    public function getAssignedUser(): ?User
    {
        return $this->assignedUser;
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        return $this->getTopic();
    }
}
