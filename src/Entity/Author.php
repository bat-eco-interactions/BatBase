<?php

namespace App\Entity;

use App\Entity\Trait\IdentifiableTrait;
use App\Entity\Trait\NameIdentifiableTrait;
use App\Entity\Trait\ReviewableTrait;
use App\Entity\Trait\SluggableTrait;
use App\Entity\Trait\SoftDeletableTrait;
use App\Entity\Trait\BlameableTimestampableTrait;
use App\Repository\AuthorRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\SerializedName;

#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', hardDelete: true)]
#[ORM\Entity(repositoryClass: AuthorRepository::class)]
#[UniqueEntity(fields: ['displayName'], message: 'Duplicate Author')]
class Author implements \Stringable
{
    use BlameableTimestampableTrait;
    use IdentifiableTrait;
    use ReviewableTrait;
    use SluggableTrait;
    use SoftDeletableTrait;

    /**
     * displayName - Last, First Middle Suffix
     * Note: When part of the Author name changes, the display name is rebuilt.
     */
    use NameIdentifiableTrait;

    /** firstName */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column(nullable: true)]
    private ?string $firstName = null;

    /** middleName */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column(nullable: true)]
    private ?string $middleName = null;

    /** lastName */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column]
    private ?string $lastName = null;

    /** suffix */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column(length: 20, nullable: true)]
    private ?string $suffix = null;

    /**
     * fullName - First Middle Last Suffix
     * Note: When part of the Author name changes, the full name is rebuilt.
     */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column(nullable: true)]
    private ?string $fullName = null;

    /** source */
    #[ORM\OneToOne(inversedBy: 'author', cascade: ['remove'])]
    #[ORM\JoinColumn(name: 'source_id', unique: true, nullable: false)]
    private ?Source $source = null;

    /**
     * setFirstName
     *
     * @param string|null $firstName
     * @return Author
     */
    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;
        $this->setWholeNames();

        return $this;
    }

    /**
     * getFirstName
     *
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * setMiddleName
     *
     * @param string|null $middleName
     * @return Author
     */
    public function setMiddleName(?string $middleName): self
    {
        $this->middleName = $middleName;
        $this->setWholeNames();

        return $this;
    }

    /**
     * getMiddleName
     *
     * @return string|null
     */
    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    /**
     * setLastName
     *
     * @param string $lastName
     * @return Author
     */
    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;
        $this->setWholeNames();

        return $this;
    }

    /**
     * getLastName
     *
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * setSuffix
     *
     * @param string|null $suffix
     * @return Author
     */
    public function setSuffix(?string $suffix): self
    {
        $this->suffix = $suffix;
        $this->setWholeNames();

        return $this;
    }

    /**
     * getSuffix
     *
     * @return string|null
     */
    public function getSuffix(): ?string
    {
        return $this->suffix;
    }

    /**
     * setFullName - First Middle Last Suffix
     * Note: Set automatically when part of the Author name changes @setWholeNames.
     *
     * @param string $fullName
     * @return Author
     */
    public function setFullName(string $fullName): self
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * getFullName - First Middle Last Suffix
     *
     * @return string|null
     */
    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    /**
     * When part of the Author name changes, full name and display name are rebuilt.
     * Full Name - First Middle Last Suffix
     * Display Name - Last, First Middle Suffix
     *
     * @return self
     */
    private function setWholeNames(): self
    {
        $this->updateFullName();
        $this->updateDisplayName();

        return $this;
    }

    /** updateFullName - First Middle Last Suffix */
    private function updateFullName(): void
    {
        $parts = [
            $this->firstName,
            $this->middleName,
            $this->lastName,
            $this->suffix
        ];
        $this->setFullName($this->combineNameParts($parts));
    }

    /** updateDisplayName - Last, First Middle Suffix || Last */
    private function updateDisplayName(): void
    {
        if (!$this->firstName) {
            if (!$this->lastName) { // allows setting name-props in any order
                return;
            }
            $this->setDisplayName($this->lastName);
            return;
        }
        $parts = [
            $this->lastName . ',',
            $this->firstName,
            $this->middleName,
            $this->suffix
        ];
        $displayName = $this->combineNameParts($parts);
        $this->setDisplayName($displayName);
        $this->source->setDisplayName($displayName);
    }

    private function combineNameParts(array $parts): string
    {
        return implode(' ', array_filter($parts, fn($part) => !empty($part)));
    }

    /**
     * setSource
     *
     * @param Source $source
     * @return Author
     */
    public function setSource(Source $source): self
    {
        $this->source = $source;

        return $this;
    }

    /**
     * getSource
     *
     * @return Source|null
     */
    public function getSource(): ?Source
    {
        return $this->source;
    }

    /**
     * getSourceId
     *
     * @return int
     */
    #[Groups(['normal'])]
    #[SerializedName('source')]
    public function getSourceId(): int
    {
        return $this->source->getId();
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        return $this->getDisplayName();
    }
}
