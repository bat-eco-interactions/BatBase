<?php

namespace App\Entity;

use App\Entity\Trait\DescribableTrait;
use App\Entity\Trait\IdentifiableTrait;
use App\Entity\Trait\BlameableTimestampableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * This class represents an authority on a naming of a taxon.
 * Not yet implemented.
 */
#[ORM\Entity]
class Authority implements \Stringable
{
    use BlameableTimestampableTrait;
    use DescribableTrait;
    use IdentifiableTrait;

    /** name */
    #[ORM\Column]
    private ?string $name = null;

    /** priority */
    #[ORM\Column(nullable: true)]
    private ?int $priority = null;

    /**
     * setName
     *
     * @param string|null $name
     * @return Authority
     */
    public function setName($name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * getName
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set priority.
     *
     * @param int $priority
     */
    public function setPriority($priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority.
     */
    public function getPriority(): ?int
    {
        return $this->priority;
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        return $this->getName();
    }
}
