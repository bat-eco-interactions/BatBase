<?php

namespace App\Entity;

use App\Entity\Trait\DescribableTrait;
use App\Entity\Trait\IdentifiableTrait;
use App\Entity\Trait\NameIdentifiableTrait;
use App\Entity\Trait\SluggableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\SerializedName;

#[ORM\Entity]
class LocationType implements \Stringable
{
    use DescribableTrait;
    use IdentifiableTrait;
    use NameIdentifiableTrait;
    use SluggableTrait;

    /** ordinal */
    #[Groups(['normal'])]
    #[ORM\Column(nullable: true)]
    private ?int $ordinal = null;

    /**
     * locations
     *
     * @var Collection<int, Location>
     */
    #[ORM\OneToMany(mappedBy: 'locationType', targetEntity: Location::class, fetch: 'EXTRA_LAZY')]
    private Collection $locations;

    public function __construct()
    {
        $this->locations = new ArrayCollection();
    }

    /**
     * setOrdinal
     *
     * @param int $ordinal
     * @return LocationType
     */
    public function setOrdinal(int $ordinal): self
    {
        $this->ordinal = $ordinal;

        return $this;
    }

    /**
     * getOrdinal
     *
     * @return int|null
     */
    public function getOrdinal(): ?int
    {
        return $this->ordinal;
    }

    /**
     * addLocation
     *
     * @param Location $locations
     * @return LocationType
     */
    public function addLocation(Location $locations): self
    {
        $this->locations[] = $locations;

        return $this;
    }

    /**
     * removeLocation
     *
     * @param Location $locations
     * @return LocationType
     */
    public function removeLocation(Location $locations): self
    {
        $this->locations->removeElement($locations);

        return $this;
    }

    /**
     * getLocations
     *
     * @return Collection
     */
    public function getLocations(): Collection
    {
        return $this->locations;
    }

    /**
     * getLocationIds
     *
     * @return array
     */
    #[Groups(['normal'])]
    #[SerializedName('locations')]
    public function getLocationIds(): array
    {
        $ids = [];
        foreach ($this->locations as $loc) {
            $ids[] = $loc->getId();
        }

        return $ids;
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        return $this->getDisplayName();
    }
}
