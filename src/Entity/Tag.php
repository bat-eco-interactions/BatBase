<?php

namespace App\Entity;

use App\Entity\Trait\DescribableTrait;
use App\Entity\Trait\IdentifiableTrait;
use App\Entity\Trait\NameIdentifiableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\SerializedName;

#[ORM\Entity]
class Tag implements \Stringable
{
    use IdentifiableTrait;
    use NameIdentifiableTrait;
    use DescribableTrait;

    /** entity - The valid entity for this tag. */
    #[Groups(['normal'])]
    #[ORM\Column]
    private ?string $entity = null;

    /** type - eg, 'season' or 'interaction type'. */
    #[Groups(['normal'])]
    #[ORM\Column]
    private ?string $type = null;

    /**
     * interactions
     *
     * @var Collection<int, Interaction>
     */
    #[ORM\ManyToMany(targetEntity: Interaction::class, mappedBy: 'tags', fetch: 'EXTRA_LAZY')]
    #[ORM\JoinTable(name: 'interaction_tag')]
    private Collection $interactions;

    /**
     * validInteractionTags
     *
     * @var Collection<int, ValidInteractionTag>
     */
    #[ORM\OneToMany(mappedBy: 'tag', targetEntity: ValidInteractionTag::class, orphanRemoval: true)]
    private Collection $validInteractionTags;

    /**
     * sources
     *
     * @var Collection<int, Source>
     */
    #[ORM\ManyToMany(targetEntity: Source::class, mappedBy: 'tags', fetch: 'EXTRA_LAZY')]
    #[ORM\JoinTable(name: 'source_tag')]
    private Collection $sources;

    public function __construct()
    {
        $this->interactions = new ArrayCollection();
        $this->sources = new ArrayCollection();
        $this->validInteractionTags = new ArrayCollection();
    }

    /**
     * setEntity
     *
     * @param string $entity
     * @return Tag
     */
    public function setEntity(string $entity): self
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * getEntity
     *
     * @return string
     */
    public function getEntity(): string
    {
        return $this->entity;
    }

    /**
     * setType
     *
     * @param string $type
     * @return Tag
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * getType
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * addInteraction
     *
     * @param Interaction $interaction
     * @return Tag
     */
    public function addInteraction(Interaction $interaction): self
    {
        $this->interactions[] = $interaction;

        return $this;
    }

    /**
     * removeInteraction
     *
     * @param Interaction $interaction
     * @return Tag
     */
    public function removeInteraction(Interaction $interaction): self
    {
        $this->interactions->removeElement($interaction);

        return $this;
    }

    /**
     * getInteractions
     *
     * @return Collection
     */
    public function getInteractions(): Collection
    {
        return $this->interactions;
    }

    /**
     * getInteractionIds
     *
     * @return array
     */
    #[Groups(['normal'])]
    #[SerializedName('interactions')]
    public function getInteractionIds(): array
    {
        $intIds = [];
        foreach ($this->interactions as $interaction) {
            $intIds[] = $interaction->getId();
        }

        return $intIds;
    }

    /**
     * addValidInteractionTag
     *
     * @param ValidInteractionTag $validInteraction
     * @return Tag
     */
    public function addValidInteractionTag(ValidInteractionTag $validInteraction): self
    {
        $this->validInteractionTags[] = $validInteraction;

        return $this;
    }

    /**
     * removeValidInteractionTag
     *
     * @param ValidInteractionTag $validInteraction
     * @return Tag
     */
    public function removeValidInteractionTag(ValidInteractionTag $validInteraction): self
    {
        $this->validInteractionTags->removeElement($validInteraction);

        return $this;
    }

    /**
     * getValidInteractionTags
     *
     * @return Collection
     */
    public function getValidInteractionTags(): Collection
    {
        return $this->validInteractionTags;
    }

    /**
     * addSource
     *
     * @param Source $source
     * @return Tag
     */
    public function addSource(Source $source): self
    {
        $this->sources[] = $source;

        return $this;
    }

    /**
     * removeSource
     *
     * @param Source $source
     * @return Tag
     */
    public function removeSource(Source $source): self
    {
        $this->sources->removeElement($source);

        return $this;
    }

    /**
     * getSources
     *
     * @return Collection
     */
    public function getSources(): Collection
    {
        return $this->sources;
    }

    /**
     * getSourceIds
     *
     * @return array
     */
    #[Groups(['normal'])]
    #[SerializedName('sources')]
    public function getSourceIds(): array
    {
        $srcIds = [];
        foreach ($this->sources as $source) {
            $srcIds[] = $source->getId();
        }

        return $srcIds;
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        return $this->getDisplayName();
    }
}
