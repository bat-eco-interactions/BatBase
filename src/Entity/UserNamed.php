<?php

namespace App\Entity;

use App\Entity\Trait\BlameableTimestampableTrait;
use App\Entity\Trait\DescribableTrait;
use App\Entity\Trait\IdentifiableTrait;
use App\Entity\Trait\NameIdentifiableTrait;
use App\Entity\Trait\SoftDeletableTrait;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Attribute\Groups;

#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', hardDelete: true)]
#[ORM\Entity]
class UserNamed implements \Stringable
{
    use IdentifiableTrait;
    use NameIdentifiableTrait;
    use DescribableTrait;
    use BlameableTimestampableTrait;
    use SoftDeletableTrait;

    /** type - Filter or Interaction */
    #[Groups(['normal'])]
    #[ORM\Column]
    private ?string $type = null;


    /** details */
    #[Groups(['normal'])]
    #[ORM\Column(type: Types::TEXT)]
    private ?string $details = null;

    /** lastLoaded */
    #[Groups(['normal'])]
    #[ORM\Column(nullable: true)]
    protected ?\DateTime $lastLoaded = null;

    /**
     * setType
     *
     * Filter or Interaction
     * @param string $type
     * @return UserNamed
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * getType
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * setDetails
     *
     * JSON list object. Eg:
     *     LIST: JSON ARRAY - Interaction IDs
     *     SET: JSON OBJ - (See JS module) <database->table->filter->set>
     * @param string $details
     * @return UserNamed
     */
    public function setDetails(string $details): self
    {
        $this->details = $details;

        return $this;
    }

    /**
     * getDetails
     *
     * JSON list object. Eg:
     *     LIST: JSON ARRAY - Interaction IDs
     *     SET: JSON OBJ - (See JS module) <database->table->filter->set>
     * @return string
     */
    public function getDetails(): string
    {
        return $this->details;
    }

    /* --- FORENSIC FIELDS --- */

    /**
     * setLastLoaded
     *
     * @param \DateTime|null $lastLoaded
     */
    public function setLastLoaded(?\DateTime $lastLoaded): void
    {
        $this->lastLoaded = $lastLoaded;
    }

    /**
     * getLastLoaded
     *
     * @return \DateTime|null
     */
    public function getLastLoaded(): ?\DateTime
    {
        return $this->lastLoaded;
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        return $this->getDisplayName();
    }
}
