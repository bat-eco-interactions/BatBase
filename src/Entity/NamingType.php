<?php

namespace App\Entity;

use App\Entity\Trait\BlameableTimestampableTrait;
use App\Entity\Trait\IdentifiableTrait;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class NamingType implements \Stringable
{
    use IdentifiableTrait;
    use BlameableTimestampableTrait;

    /** name */
    #[ORM\Column]
    private ?string $name = null;

    /**
     * setName
     *
     * @param string|null $name
     * @return NamingType
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * getName
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        return $this->getName();
    }
}
