<?php

namespace App\Entity;

use App\Entity\Trait\BlameableTimestampableTrait;
use App\Entity\Trait\IdentifiableTrait;
use App\Entity\Trait\SoftDeletableTrait;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\SerializedName;

#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', hardDelete: true)]
#[ORM\Entity]
class ReviewEntry implements \Stringable
{
    use BlameableTimestampableTrait;
    use IdentifiableTrait;
    use SoftDeletableTrait;

    /** entity */
    #[Groups(['normal'])]
    #[ORM\Column]
    private ?string $entity = null;

    /** entityId - Set once entry is approved or rejected.  */
    #[Groups(['normal'])]
    #[ORM\Column(nullable: true)]
    private ?string $entityId = null;

    /** form - JSON form-data */
    #[Groups(['normal'])]
    #[ORM\Column(type: Types::TEXT)]
    private ?string $form = null;

    /** log - JSON review-log */
    #[Groups(['normal'])]
    #[ORM\Column(type: Types::TEXT)]
    private ?string $log = null;

    /** payload - JSON, entry record being reviewed */
    #[Groups(['normal'])]
    #[ORM\Column(type: Types::TEXT)]
    private ?string $payload = null;

    /** stage - Pending, Locked, Returned, Held/Paused, Rejected, Approved. */
    #[ORM\ManyToOne(inversedBy: 'reviewEntries')]
    #[ORM\JoinColumn(nullable: false)]
    private ?ReviewStage $stage = null;

    /** managedBy - When payload is locked/held by a form-manager. */
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(name: 'managed_by', nullable: true)]
    private ?User $managedBy = null;

    /**
     * setEntity
     *
     * @param string $entity
     * @return ReviewEntry
     */
    public function setEntity(string $entity): self
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * getEntity
     *
     * @return string
     */
    public function getEntity(): string
    {
        return $this->entity;
    }

    /**
     * setEntityId
     *
     * @param string|null $entityId
     * @return ReviewEntry
     */
    public function setEntityId(?string $entityId): self
    {
        $this->entityId = $entityId;

        return $this;
    }

    /**
     * getEntityId
     *
     * @return string|null
     */
    public function getEntityId(): ?string
    {
        return $this->entityId;
    }

    /**
     * setForm
     *
     * JSON form-data
     * @param string $form
     * @return ReviewEntry
     */
    public function setForm(string $form): self
    {
        $this->form = $form;

        return $this;
    }

    /**
     * getForm
     *
     * JSON form-data
     * @return string
     */
    public function getForm(): string
    {
        return $this->form;
    }

    /**
     * setLog
     *
     * JSON review-log
     * @param $log
     * @return ReviewEntry
     */
    public function setLog($log): self
    {
        $this->log = $log;

        return $this;
    }

    /**
     * getLog
     *
     * JSON review-log
     * @return string
     */
    public function getLog(): string
    {
        return $this->log;
    }

    /**
     * setPayload
     *
     * JSON, entry record being reviewed
     * @param string $payload
     * @return ReviewEntry
     */
    public function setPayload(string $payload): self
    {
        $this->payload = $payload;

        return $this;
    }

    /**
     * getPayload
     *
     * JSON, entry record being reviewed
     * @return string
     */
    public function getPayload(): string
    {
        return $this->payload;
    }

    /**
     * setReviewStage
     *
     * Pending, Locked, Returned, Held/Paused, Rejected, Approved.
     * @param ReviewStage $stage
     * @return ReviewEntry
     */
    public function setReviewStage(ReviewStage $stage): self
    {
        $this->stage = $stage;

        return $this;
    }

    /**
     * getReviewStage
     *
     * Pending, Locked, Returned, Held/Paused, Rejected, Approved.
     * @return ReviewStage
     */
    public function getReviewStage(): ReviewStage
    {
        return $this->stage;
    }

    /**
     * getStageData
     *
     * Get the stage id and displayName.
     * @return array
     */
    #[Groups(['normal'])]
    #[SerializedName('stage')]
    public function getStageData(): array
    {
        return [
            'id' => $this->stage->getId(),
            'name' => $this->stage->getPassiveForm(),
        ];
    }

    /**
     * getStageName
     *
     * Get the passive-form name of the review stage.
     * @return string
     */
    public function getStageName(): string
    {
        return $this->stage->getPassiveForm();
    }

    /**
     * setManagedBy
     *
     * @param User|null $user
     * @return ReviewEntry
     */
    public function setManagedBy(?User $user = null): self
    {
        $this->managedBy = $user;

        return $this;
    }

    /**
     * getManagedBy
     *
     * @return User|null
     */
    public function getManagedBy(): ?User
    {
        return $this->managedBy;
    }

    /**
     * getManagedByData
     *
     * If the entry is locked to a specific manager, return their id and displayName.
     * @return array|false
     */
    #[Groups(['normal'])]
    #[SerializedName('managedBy')]
    public function getManagedByData(): array|false
    {
        return !$this->managedBy ? false : [
            'id' => $this->managedBy->getId(),
            'displayName' => $this->managedBy->getFullName(),
        ];
    }

    /**
     * getCreatedByData
     *
     * @return array
     */
    #[Groups(['normal'])]
    #[SerializedName('createdBy')]
    public function getCreatedByData(): array
    {
        return [
            'id' => $this->createdBy->getId(),
            'displayName' => $this->createdBy->getFullName(),
        ];
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        return $this->getForm();
    }
}
