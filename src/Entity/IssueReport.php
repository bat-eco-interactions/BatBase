<?php

namespace App\Entity;

use App\Entity\Trait\BlameableTimestampableTrait;
use App\Entity\Trait\DescribableTrait;
use App\Entity\Trait\IdentifiableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

#[ORM\Entity]
#[Vich\Uploadable]
class IssueReport implements \Stringable
{
    use IdentifiableTrait;
    use DescribableTrait;
    use BlameableTimestampableTrait;

    /** stepsToReproduce */
    #[ORM\Column(type: Types::TEXT)]
    private ?string $stepsToReproduce = null;

    /** miscInfo */
    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $miscInfo = null;

    /**
     * screenshots
     *
     * @var Collection<int, ImageUpload>
     */
    #[ORM\OneToMany(
        mappedBy: 'issueReport',
        targetEntity: ImageUpload::class,
        fetch: 'EXTRA_LAZY'
    )]
    private Collection $screenshots;

    public function __construct()
    {
        $this->screenshots = new ArrayCollection();
    }

    /**
     * getStepsToReproduce
     *
     * @return string|null
     */
    public function getStepsToReproduce(): ?string
    {
        return $this->stepsToReproduce;
    }

    /**
     * setStepsToReproduce
     *
     * @param string $stepsToReproduce
     * @return IssueReport
     */
    public function setStepsToReproduce(string $stepsToReproduce): self
    {
        $this->stepsToReproduce = $stepsToReproduce;

        return $this;
    }

    /**
     * getMiscInfo
     *
     * @return string|null
     */
    public function getMiscInfo(): ?string
    {
        return $this->miscInfo;
    }

    /**
     * setMiscInfo
     *
     * @param string|null $miscInfo
     * @return IssueReport
     */
    public function setMiscInfo(?string $miscInfo): self
    {
        $this->miscInfo = $miscInfo;

        return $this;
    }

    /**
     * addScreenshot
     *
     * @param ImageUpload $screenshots
     * @return IssueReport
     */
    public function addScreenshot(ImageUpload $screenshots): self
    {
        $this->screenshots[] = $screenshots;

        return $this;
    }

    /**
     * removeScreenshot
     *
     * @param ImageUpload $screenshots
     * @return IssueReport
     */
    public function removeScreenshot(ImageUpload $screenshots): self
    {
        $this->screenshots->removeElement($screenshots);

        return $this;
    }

    /**
     * getScreenshots
     *
     * @return Collection
     */
    public function getScreenshots(): Collection
    {
        return $this->screenshots;
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        return $this->description;
    }
}
