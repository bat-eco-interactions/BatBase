<?php

namespace App\Entity;

use App\Entity\Trait\BlameableTimestampableTrait;
use App\Entity\Trait\IdentifiableTrait;
use App\Repository\GeoJsonRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: GeoJsonRepository::class)]
class GeoJson implements \Stringable
{
    use BlameableTimestampableTrait;
    use IdentifiableTrait;

    /** type - MultiPolygon, Point, Polygon */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column]
    private ?string $type = null;

    /** coordinates - "[[ lng, lat ], ...]" (GeoJson format) */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column(type: Types::TEXT)]
    private ?string $coordinates = null;

    /** displayPoint - "[ "long", "lat" ]" (GeoJson format) */
    #[Groups(['flat', 'normal'])]
    #[ORM\Column]
    private ?string $displayPoint = null;

    /**
     * setType - MultiPolygon, Point, Polygon
     *
     * @param string $type
     * @return GeoJson
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * getType
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * setCoordinates
     *
     * "[ "long", "lat" ]" (GeoJson format)
     * @param string $coordinates
     * @return GeoJson
     */
    public function setCoordinates(string $coordinates): self
    {
        $this->coordinates = $coordinates;

        return $this;
    }

    /**
     * getCoordinates
     *
     * "[ "long", "lat" ]" (GeoJson format)
     * @return string
     */
    public function getCoordinates(): string
    {
        return $this->coordinates;
    }

    /**
     * setDisplayPoint
     *
     * "[ "long", "lat" ]" (GeoJson format)
     * @param string $displayPoint
     * @return GeoJson
     */
    public function setDisplayPoint(string $displayPoint): self
    {
        $this->displayPoint = $displayPoint;

        return $this;
    }

    /**
     * getDisplayPoint
     *
     * "[ "long", "lat" ]" (GeoJson format)
     * @return string
     */
    public function getDisplayPoint(): string
    {
        return $this->displayPoint;
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        return 'GeoJson ' . $this->getType();
    }
}
