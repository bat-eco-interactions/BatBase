<?php

namespace App\Entity;

use App\Entity\Trait\IdentifiableTrait;
use App\Entity\Trait\NameIdentifiableTrait;
use App\Entity\Trait\SluggableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\SerializedName;

#[ORM\Entity]
class HabitatType implements \Stringable
{
    use IdentifiableTrait;
    use SluggableTrait;
    use NameIdentifiableTrait;

    /**
     * locations
     *
     * @var Collection<int, Location>
     */
    #[ORM\OneToMany(mappedBy: 'habitatType', targetEntity: Location::class, fetch: 'EXTRA_LAZY')]
    private Collection $locations;

    public function __construct()
    {
        $this->locations = new ArrayCollection();
    }

    /**
     * addLocation
     *
     * @param Location $locations
     * @return HabitatType
     */
    public function addLocation(Location $locations): self
    {
        $this->locations[] = $locations;

        return $this;
    }

    /**
     * removeLocation
     *
     * @param Location $locations
     * @return HabitatType
     */
    public function removeLocation(Location $locations): self
    {
        $this->locations->removeElement($locations);

        return $this;
    }

    /**
     * getLocations
     *
     * @return Collection
     */
    public function getLocations(): Collection
    {
        return $this->locations;
    }

    /**
     * getLocationIds
     *
     * @return array
     */
    #[Groups(['normal'])]
    #[SerializedName('locations')]
    public function getLocationIds(): array
    {
        $locIds = [];
        foreach ($this->locations as $loc) {
            $locIds[] = $loc->getId();
        }

        return $locIds;
    }

    /**
     * Get string representation of object.
     */
    public function __toString(): string
    {
        return $this->getDisplayName();
    }
}
