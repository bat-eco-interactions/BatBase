<?php

namespace App\Dto;

use ApiPlatform\Doctrine\Orm\State\Options;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Dto\Trait\AuditableDtoTrait;
use App\Entity\Interaction;
use App\State\EntityClassDtoStateProcessor;
use App\State\EntityToDtoStateProvider;
use Symfony\Component\Serializer\Attribute\Groups;

#[ApiResource(
    shortName: 'Interaction',
    description: 'todo...',
    operations: [
        new Get(),
        new GetCollection(),
        new Post(security: 'is_granted("ROLE_EDITOR")'),
        new Patch(security: 'is_granted("ROLE_EDITOR")'),
        new Delete(security: 'is_granted("ROLE_EDITOR")')
    ],
    normalizationContext: ['groups' => ['read']],
    denormalizationContext: ['groups' => ['write']],
    paginationItemsPerPage: 5,
    provider: EntityToDtoStateProvider::class,
    processor: EntityClassDtoStateProcessor::class,
    stateOptions: new Options(entityClass: Interaction::class),
)]
class InteractionDto
{
    use AuditableDtoTrait;

    #[ApiProperty(identifier: true)]
    #[Groups('read')]
    public ?int $id = null;

    // todo: validate pattern
    /**
     * The date the interaction was cited to occur.
     * Format: 'YYYY-MM-DD to YYYY-MM-DD'.
     * @var string|null
     */
    #[Groups(['read', 'write'])]
    public ?string $date = null;

    /** @var string|null */
    #[Groups(['read', 'write'])]
    public ?string $note = null;

    // todo: num - num || num
    // todo: num1 < num2
    /**
     * Page number of the source where the interaction is described.
     * @var string|null
     */
    #[Groups(['read', 'write'])]
    public ?string $pages = null;

    /**
     * Direct quote from the source where the interaction is described.
     * @var string|null
     */
    #[Groups(['read', 'write'])]
    public ?string $quote = null;

    //todo: validate against ValidInteraction
    /**
     * <code>
     * [
     *     'id'      => 1,
     *     'displayName'   => 'Consumption'
     * ];
     * </code>
     * @var array{id:int, displayName:string}|null
     */
    #[Groups('read')]
    public ?array $interactionType = null;

    #[Groups('write')]
    public ?int $interactionTypeId = null;

    /**
     * The citation for the interaction.
     * @var CitationDto
     */
    #[ApiProperty(readableLink: false, writableLink: false)]
    #[Groups('read')]
    public CitationDto $citation;

    #[Groups('write')]
    public ?int $citationId = null;

    /**
     * @var LocationDto
     */
    #[ApiProperty(readableLink: false, writableLink: false)]
    #[Groups('read')]
    public LocationDto $location;

    /**
     * todo: check that null passed in patch action is distinct from no value passed
     *
     * Note: Pass boolean FALSE to remove the location, a null value will be ignored.
     * @var int|bool|null
     */
    #[Groups('write')]
    public int|bool|null $locationId = null;

    /**
     * @var TaxonDto
     */
    #[ApiProperty(readableLink: false, writableLink: false)]
    #[Groups('read')]
    public TaxonDto $subject;

    #[Groups('write')]
    public ?int $subjectId = null;

    /**
     * @var int
     */
    #[Groups('read')]
    public int $subjectGroupId;

    /**
     * @var TaxonDto
     */
    #[ApiProperty(readableLink: false, writableLink: false)]
    #[Groups('read')]
    public TaxonDto $object;

    #[Groups('write')]
    public ?int $objectId = null;

    /**
     * @var int
     */
    #[Groups('read')]
    public int $objectGroupId;

    /**
     * <code>
     * [
     *     'id'      => 1,
     *     'displayName'   => 'Wet',
     *     'type'    =>  'season'
     *
     * ];
     * </code>
     * @var array{id:int, displayName:string, type:string}|null
     */
    #[Groups('read')]
    public ?array $tags = null;

    //todo: validate against ValidInteraction
    /**
     * Note: to remove all tags pass boolean FALSE or an empty array, a null value will be ignored.
     * @var array<int>|false
     */
    #[Groups('write')]
    public array|false|null $tagIds = null;
}