<?php

namespace App\Dto;

use ApiPlatform\Doctrine\Orm\State\Options;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use App\Entity\GeoJson;
use App\State\EntityToDtoStateProvider;
use Symfony\Component\Serializer\Attribute\Groups;

/** @note - create/update/delete handled through Location */
#[ApiResource(
    uriTemplate: 'geojson',
    shortName: 'GeoJson',
    description: 'Spatial data representing geographic features of a Location.',
    operations: [
        new Get(
            uriTemplate: 'geojson/{id}',
            normalizationContext: ['groups' => ['read_full']],
        ),
        new GetCollection(
            normalizationContext: ['groups' => ['read_full']],
        )
    ],
    normalizationContext: ['groups' => ['read']],
    paginationItemsPerPage: 5,
    provider: EntityToDtoStateProvider::class,
    stateOptions: new Options(entityClass: GeoJson::class),
)]
class GeoJsonDto
{
    #[ApiProperty(identifier: true)]
    #[Groups(['read', 'read_full'])]
    public ?int $id = null;

    /**
     * Point, Polygon, MultiPolygon. Note: polygon types have extended coordinate values.
     * @var string
     */
    #[Groups(['read', 'read_full'])]
    public string $type;

    /* @todo - refactor client storage to fetch coordinates as needed. */
    /**
     * JSON array of float coordinates in geojson format:
     *      "[[ lng, lat ], ...]" (polygons)
     *      "[ lng, lat ]" (Point)
     * @var string
     */
    #[Groups('read_full')]
    public string $coordinates;

    /**
     * JSON array of float coordinates in geojson format: [ longitude, latitude ]
     * @var string "[ lng, lat ]" (GeoJson format)
     */
    #[Groups(['read', 'read_full'])]
    public string $displayPoint;
}