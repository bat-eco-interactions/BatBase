<?php

namespace App\Dto;

use ApiPlatform\Doctrine\Orm\State\Options;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Dto\Trait\AuditableDtoTrait;
use App\Entity\Citation;
use App\State\EntityClassDtoStateProcessor;
use App\State\EntityToDtoStateProvider;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    shortName: 'Citation',
    description: 'The citation where interactions have been documented and published.',
    operations: [
        new Get(),
        new GetCollection(),
        new Post(security: 'is_granted("ROLE_EDITOR")'),
        new Patch(security: 'is_granted("ROLE_EDITOR")'),
        new Delete(security: 'is_granted("ROLE_EDITOR")')
    ],
    normalizationContext: ['groups' => ['read']],
    denormalizationContext: ['groups' => ['write']],
    paginationItemsPerPage: 5,
    provider: EntityToDtoStateProvider::class,
    processor: EntityClassDtoStateProcessor::class,
    stateOptions: new Options(entityClass: Citation::class),
)]
class CitationDto
{
    use AuditableDtoTrait;

    #[ApiProperty(identifier: true)]
    #[Groups('read')]
    public ?int $id = null;

    /* todo: should not be needed... if there is a use, maybe isIndirect is better? */
    /**
     * Citation source to an interaction.
     * @var bool
     */
    #[Groups('read')]
    public bool $isDirect = false;

    // todo: assert value set
    /**
     * The title of the work cited.
     * @var string|null
     */
    #[Groups(['read', 'write'])]
    public ?string $title = null;

    /**
     * The title of the work cited, ensured unique among publications and citations.
     * @var string
     */
    #[Groups('read')]
    public string $displayName;

    // todo (what format?)
    // todo: assert valid format
    /**
     * The fully formated citation.
     * @var string|null
     */
    #[Groups(['read', 'write'])]
    public ?string $fullText = null;

    /**
     * A brief summary of the work being cited.
     * @var string|null
     */
    #[Groups(['read', 'write'])]
    public ?string $abstract = null;

    /**
     * @var int|null
     */
    #[Groups(['read', 'write'])]
    public ?int $publicationVolume = null;

    /**
     * @var int|null
     */
    #[Groups(['read', 'write'])]
    public ?int $publicationIssue = null;

    // todo: assert num - num || num
    // todo: assert num1 < num2
    /**
     * @var string|null
     */
    #[Groups(['read', 'write'])]
    public ?string $publicationPages = null;

    /**
     * @var int|null
     */
    #[Groups(['read', 'write'])]
    #[Assert\Length(4)]
    public ?int $year = null;

    /* todo - regex doi pattern */
    /**
     * A unique permanent web address (URL) used to identify an article or document.
     * @var string|null
     */
    #[Groups(['read', 'write'])]
    public ?string $doi = null;

    /* todo - validate url pattern */
    /**
     * Citation website (non-DOI)
     * @var string|null
     */
    #[Groups(['read', 'write'])]
    public ?string $linkUrl = null;

    // todo: validate matches with publication type
    /**
     * <code>
     * [
     *     'id'      => 1,
     *     'displayName'   => 'Article'
     * ];
     * </code>
     * @var array{id:int, displayName:string}|null
     */
    #[Groups('read')]
    public ?array $citationType = null;

    #[Groups('write')]
    public ?int $citationTypeId = null;

    /** The publication being cited. */
    #[Groups('read')]
    #[ApiProperty(readableLink: false, writableLink: false)]
    public ?PublicationDto $publication = null;

    #[Groups('write')]
    public ?int $publicationId = null;

    /* todo: refactor away, derive from contributors */
    /**
     * <code>
     * [
     *     '1'   =>  23,
     *     '2'   =>  24
     * ];
     * </code>
     * @var array{ord:int, id:int}|null
     */
    #[Groups('read')]
    public ?array $authors = null;

    /* todo: fix input|output serialization inconsistencies */
    /**
     * <code>
     * [ 1 =>
     *     [
     *         'authId'   =>  int,
     *         'isEditor' => false,
     *         'ord' => 1
     *     ]
     * ];
     * </code>
     * @var array{authId:int, array{authId:int, isEditor:bool, ord: int}}|null
     */
    #[Groups(['read', 'write'])]
    public ?array $contributors = null;

    /**
     * Interactions associated with the Citation.
     * @var array<int, InteractionDto>
     */
    #[ApiProperty(readableLink: false, writableLink: false)]
    #[Groups('read')]
    public ?array $interactions = null;

    /**
     * Source entity ID
     * @var string|null
     */
    #[Groups('read')]
    public ?string $source = null;
}