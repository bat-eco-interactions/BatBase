<?php

namespace App\Dto;

use ApiPlatform\Doctrine\Orm\State\Options;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Dto\Trait\AuditableDtoTrait;
use App\Entity\Publisher;
use App\State\EntityClassDtoStateProcessor;
use App\State\EntityToDtoStateProvider;
use Symfony\Component\Serializer\Attribute\Groups;

#[ApiResource(
    shortName: 'Publisher',
    description: 'todo...',
    operations: [
        new Get(),
        new GetCollection(),
        new Post(security: 'is_granted("ROLE_EDITOR")'),
        new Patch(security: 'is_granted("ROLE_EDITOR")'),
        new Delete(security: 'is_granted("ROLE_EDITOR")')
    ],
    normalizationContext: ['groups' => ['read']],
    denormalizationContext: ['groups' => ['write']],
    paginationItemsPerPage: 5,
    provider: EntityToDtoStateProvider::class,
    processor: EntityClassDtoStateProcessor::class,
    stateOptions: new Options(entityClass: Publisher::class),
)]
class PublisherDto
{
    use AuditableDtoTrait;

    #[ApiProperty(identifier: true)]
    #[Groups('read')]
    public ?int $id = null;

    /** @var string|null */
    #[Groups(['read', 'write'])]
    public ?string $displayName = null;

    /** @var string|null */
    #[Groups(['read', 'write'])]
    public ?string $city = null;

    /** @var string|null */
    #[Groups(['read', 'write'])]
    public ?string $country = null;

    /** @var string|null */
    #[Groups(['read', 'write'])]
    public ?string $description = null;

    // todo: validate pattern
    /**
     * Publisher website.
     * @var string|null
     */
    #[Groups(['read', 'write'])]
    public ?string $linkUrl = null;
    /**
     * Source entity ID
     * @var string
     */
    #[Groups('read')]
    public string $source;
}