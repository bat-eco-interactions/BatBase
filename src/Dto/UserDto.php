<?php

namespace App\Dto;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;

#[ApiResource(
    shortName: 'User',
    operations: [
        new Get(
            uriTemplate: 'users/{id}',
            security: 'is_granted("ROLE_ADMIN")'
        ),
        new GetCollection(security: 'is_granted("ROLE_ADMIN")'),
    ],
)]
class UserDto
{
    #[ApiProperty(identifier: true)]
    public int $id;

    public string $email;

    public string $firstName;

    public string $username;
}