<?php

namespace App\Dto;

use ApiPlatform\Doctrine\Orm\State\Options;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Dto\Trait\AuditableDtoTrait;
use App\Entity\Location;
use App\State\EntityClassDtoStateProcessor;
use App\State\EntityToDtoStateProvider;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    shortName: 'Location',
    description: 'A location is the place where an interaction is cited to occur.',
    operations: [
        new Get(),
        new GetCollection(),
        new Post(security: 'is_granted("ROLE_EDITOR")'),
        new Patch(security: 'is_granted("ROLE_EDITOR")'),
        new Delete(security: 'is_granted("ROLE_EDITOR")')
    ],
    normalizationContext: ['groups' => ['read']],
    denormalizationContext: ['groups' => ['write']],
    paginationItemsPerPage: 5,
    provider: EntityToDtoStateProvider::class,
    processor: EntityClassDtoStateProcessor::class,
    stateOptions: new Options(entityClass: Location::class),
)]
class LocationDto
{
    use AuditableDtoTrait;

    #[ApiProperty(identifier: true)]
    #[Groups('read')]
    public ?int $id = null;

    /** @var string Unique name of the location. */
    #[Groups(['read', 'write'])]
    public string $displayName;

    #[Groups(['read', 'write'])]
    public ?string $description = null;

    /** @var int|null The elevation of the location. The start of a range, if elevationMax is set. */
    #[Groups(['read', 'write'])]
    public ?int $elevation = null;

    // todo - validate max is more than elev
    /** @var int|null The maximum of an elevation range. */
    #[Groups(['read', 'write'])]
    public ?int $elevationMax = null;

    #[Groups('read')]
    public ?string $elevUnitAbbrv = null;

//@todo
//    #[Assert\Regex(
//        pattern: '/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/',
//        message: 'Invalid Latitude'
//    )]
    #[Groups(['read', 'write'])]
    public ?float $latitude = null;

//@todo
//    #[Assert\Regex(
//        pattern: '/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/',
//        message: 'Invalid Longitude'
//    )]
    #[Groups(['read', 'write'])]
    public ?float $longitude = null;

    /**
     * <code>
     * [
     *     'id'      => 1,
     *     'displayPoint'   => '[ (float) longitude, latitude ]',
     *     'type'    => '<Point|Polygon|MultiPolygon>',
     * ];
     * </code>
     * @var GeoJsonDto|null
     */
    #[Groups('read')]
    public ?GeoJsonDto $geoJson = null;

    /** @var string|null 2-letter country code. */
    #[Groups('read')]
    public ?string $isoCode = null;

    /** The closest containing Location, eg: Point->Country->Region */
    #[ApiProperty(readableLink: false, writableLink: false)]
    #[Groups('read')]
    public ?LocationDto $parent = null;

    #[Groups('write')]
    public ?int $parentId = null;

    /**
     * Locations contained within this location, eg: A region's countries
     * @var array<int, LocationDto>
     */
    #[ApiProperty(readableLink: false, writableLink: false)]
    #[Groups('read')]
    public array $children;

    /**
     * <code>
     * [
     *     'id'      => 1,
     *     'displayName'   => 'Forest'
     * ];
     * </code>
     * @var array{id:int, displayName:string}|null
     */
    #[Groups('read')]
    public ?array $habitatType = null;

    #[Groups('write')]
    public ?int $habitatTypeId = null;

    // todo: ensure POST only possible for Point type
    /**
     * Eg: Region, Country, Point
     *  <code>
     *  [
     *      'id'      => 1,
     *      'displayName'   => 'Point'
     *  ];
     *  </code>
     * @var array{ 'id': int, 'displayName': string }
     */
    #[Groups('read')]
    public array $locationType;

    // todo: ensure POST only possible for Point type
    /** Ex: Region, Country, Habitat, Point */
    #[Assert\Type(Integer::class, groups: ['write'])]
    #[Groups('write')]
    public ?int $locationTypeId = null;

    /**
     * Interactions at this Location.
     * @var array<int, InteractionDto>
     */
    #[ApiProperty(readableLink: false, writableLink: false)]
    #[Groups('read')]
    public array $interactions;
}