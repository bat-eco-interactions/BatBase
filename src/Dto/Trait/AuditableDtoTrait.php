<?php

namespace App\Dto\Trait;

use App\Dto\UserDto;
use Symfony\Component\Serializer\Attribute\Groups;

trait AuditableDtoTrait
{
    #[Groups("read")]
    public string $created;

    #[Groups("read")]
    public UserDto $createdBy;

    #[Groups("read")]
    public ?string $updated = null;

    #[Groups("read")]
    public ?UserDto $updatedBy = null;

    #[Groups("read")]
    public ?string $reviewed = null;

    #[Groups("read")]
    public ?UserDto $reviewedBy = null;
}