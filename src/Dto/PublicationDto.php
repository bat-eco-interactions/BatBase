<?php

namespace App\Dto;

use ApiPlatform\Doctrine\Orm\State\Options;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Dto\Trait\AuditableDtoTrait;
use App\Entity\Publication;
use App\State\EntityClassDtoStateProcessor;
use App\State\EntityToDtoStateProvider;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    shortName: 'Publication',
    description: 'todo',
    operations: [
        new Get(),
        new GetCollection(),
        new Post(security: 'is_granted("ROLE_EDITOR")'),
        new Patch(security: 'is_granted("ROLE_EDITOR")'),
        new Delete(security: 'is_granted("ROLE_EDITOR")')
    ],
    normalizationContext: ['groups' => ['read']],
    denormalizationContext: ['groups' => ['write']],
    paginationItemsPerPage: 5,
    provider: EntityToDtoStateProvider::class,
    processor: EntityClassDtoStateProcessor::class,
    stateOptions: new Options(entityClass: Publication::class),
)]
class PublicationDto
{
    use AuditableDtoTrait;

    #[ApiProperty(identifier: true)]
    #[Groups('read')]
    public ?int $id = null;

    /**
     * The title of the publication.
     * @var string|null
     */
    #[Groups(['read', 'write'])]
    public ?string $displayName = null;

    /**
     * @var string|null
     */
    #[Groups(['read', 'write'])]
    #[Assert\Length(4)]
    public ?string $year = null;

    //todo: assert correct format
    /**
     * A unique permanent web address (URL) used to identify an article or document.
     * @var string|null
     */
    #[Groups(['read', 'write'])]
    public ?string $doi = null;

    /**
     * Publication website.
     * @var string|null
     */
    #[Groups(['read', 'write'])]
    public ?string $linkUrl = null;

    /** @var string|null */
    #[Groups(['read', 'write'])]
    public ?string $description = null;

    /**
     * <code>
     * [
     *     'id'      => 1,
     *     'displayName'   => 'Journal'
     * ];
     * </code>
     * @var array{id:int, displayName:string}|null
     */
    #[Groups('read')]
    public ?array $publicationType = null;

    #[Groups('write')]
    public ?int $publicationTypeId = null;

    #[Groups('read')]
    #[ApiProperty(readableLink: false, writableLink: false)]
    public ?PublisherDto $publisher = null;

    #[Groups('write')]
    public ?int $publisherId = null;

    /* todo: refactor away, derive from contributors */
    /**
     * <code>
     * [
     *      '1'   =>  23,
     *      '2'   =>  24
     *  ];
     *  </code>
     * @var array{ord:int, id:int}|null
     */
    #[Groups('read')]
    public ?array $authors = null;

    /* todo: refactor away, derive from contributors */
    /**
     * <code>
     * [
     *      '1'   =>  23,
     *      '2'   =>  24
     *  ];
     *  </code>
     * @var array{ord:int, id:int}|null
     */
    #[Groups('read')]
    public ?array $editors = null;

    /* todo: fix input|output serialization inconsistencies */
    /**
     * <code>
     * [ 1 =>
     *     [
     *         'authId'   =>  int,
     *         'isEditor' => false,
     *         'ord' => 1
     *     ]
     * ];
     * </code>
     * @var array{authId:int, array{authId:int, isEditor:bool, ord: int}}|null
     */
    #[Groups(['read', 'write'])]
    public ?array $contributors = null;

    /**
     * Source entity ID
     * @var string|null
     */
    #[Groups('read')]
    public ?string $source = null;
}