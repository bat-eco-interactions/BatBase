<?php

namespace App\Dto\Csv;

use ApiPlatform\Doctrine\Orm\State\Options;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\OpenApi\Model;
use ApiPlatform\Metadata\Post;
use App\Dto\Csv\Trait\CitationCsvTrait;
use App\Dto\Csv\Trait\LocationCsvTrait;
use App\Entity\Interaction;
use App\State\EntityClassDtoStateProcessor;
use Symfony\Component\Serializer\Attribute\Groups;

#[ApiResource(
    shortName: 'Interaction',
    operations: [
        new Post(
            uriTemplate: 'interaction-csv',
            formats: ['csv' => 'text/csv'],
            openapi: new Model\Operation(
                responses: [
                    201 => new Model\Response(
                        content: new \ArrayObject([
                                'application/json' => [
                                    'schema' => [
                                        'type' => 'object',
                                        'properties' => [
                                            'status' => ['type' => 'string'],
                                            'description' => ['type' => 'string']
                                        ]
                                    ],
                                    'example' => [
                                        'status' => 'success',
                                        'description' => 'Interaction submitted for review.',
                                    ]
                                ]
                            ]
                        )
                    )
                ],
                summary: 'Submits for review and potential entry.',
                description: 'Submits for review and potential entry.',
            ),
        ),
    ],
    formats: ['csv' => 'text/csv'],
    denormalizationContext: ['groups' => ['csv'], 'openapi_definition_name' => 'write_csv'],
    security: 'is_granted("ROLE_USER")',
    processor: EntityClassDtoStateProcessor::class,
    stateOptions: new Options(entityClass: Interaction::class),
)]
class InteractionCsvDto //24 props (29 trait props)
{
    use CitationCsvTrait;
    use LocationCsvTrait;

    /**
     * Interaction types have a valid sub-set of interaction tags.
     * @var string See batbase.org/definitions for possible interaction types.
     */
    #[Groups('csv')]
    public string $interactionType;

    //todo: handle internal csv
    /**
     * Interaction tags must be valid with the interaction type.
     * @var string|null See batbase.org/definitions for possible interaction tags.
     */
    #[Groups('csv')]
    public ?string $interactionTags = null;

    //todo: handle internal csv
    /**
     * Seasons the interaction occurred in. Ie: wet, dry, spring, winter, etc
     * @var string|null See batbase.org/definitions for possible interaction tags.
     */
    #[Groups('csv')]
    public ?string $season = null;

    /**
     * The pages of the source where the interaction can be found.
     * @var string|null
     */
    #[Groups('csv')]
    public ?string $interactionPages = null;

    /** @var string|null */
    #[Groups('csv')]
    public ?string $notes = null;

    /**
     * The date the interaction is cited to have occurred.
     * @var string|null
     */
    #[Groups('csv')]
    public ?string $date = null;

    /**
     * The direct quote from the source of the interaction.
     * @var string|null
     */
    #[Groups('csv')]
    public ?string $quote = null;

    /**
     * True if the interaction is cited in a secondary source.
     * @var string|null
     */
    #[Groups('csv')]
    public ?string $secondarySource = null;

    /* ------ SUBJECT -------- */

    /**
     * Taxon domain name.
     * @var string|null
     */
    #[Groups('csv')]
    public ?string $subjectDomain = null;

    /** @var string|null */
    #[Groups('csv')]
    public ?string $subjectKingdom = null;

    /** @var string|null */
    #[Groups('csv')]
    public ?string $subjectPhylum = null;

    /** @var string|null */
    #[Groups('csv')]
    public ?string $subjectClass = null;

    /** @var string|null */
    #[Groups('csv')]
    public ?string $subjectOrder = null;

    /** @var string|null */
    #[Groups('csv')]
    public ?string $subjectFamily = null;

    /** @var string|null */
    #[Groups('csv')]
    public ?string $subjectGenus = null;

    /** @var string|null */
    #[Groups('csv')]
    public ?string $subjectSpecies = null;

    /* ------ OBJECT -------- */

    /** @var string|null */
    #[Groups('csv')]
    public ?string $objectDomain = null;

    /** @var string|null */
    #[Groups('csv')]
    public ?string $objectKingdom = null;

    /** @var string|null */
    #[Groups('csv')]
    public ?string $objectPhylum = null;

    /** @var string|null */
    #[Groups('csv')]
    public ?string $objectClass = null;

    /** @var string|null */
    #[Groups('csv')]
    public ?string $objectOrder = null;

    /** @var string|null */
    #[Groups('csv')]
    public ?string $objectFamily = null;

    /** @var string|null */
    #[Groups('csv')]
    public ?string $objectGenus = null;

    /** @var string|null */
    #[Groups('csv')]
    public ?string $objectSpecies = null;
}