<?php

namespace App\Dto\Csv;

use ApiPlatform\Doctrine\Orm\State\Options;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\OpenApi\Model;
use ApiPlatform\Metadata\Post;
use App\Dto\Csv\Trait\CitationCsvTrait;
use App\Entity\Citation;
use App\State\EntityClassDtoStateProcessor;

#[ApiResource(
    shortName: 'Citation',
    operations: [
        new Post(
            uriTemplate: 'citation-csv',
            formats: ['csv' => 'text/csv'],
            openapi: new Model\Operation(
                responses: [
                    201 => new Model\Response(
                        content: new \ArrayObject([
                                'application/json' => [
                                    'schema' => [
                                        'type' => 'object',
                                        'properties' => [
                                            'status' => ['type' => 'string'],
                                            'description' => ['type' => 'string']
                                        ]
                                    ],
                                    'example' => [
                                        'status' => 'success',
                                        'description' => 'Citation submitted for review.',
                                    ]
                                ]
                            ]
                        )
                    )
                ],
                summary: 'Submits for review and potential entry.',
                description: 'Submits for review and potential entry.'
            ),
        ),
    ],
    denormalizationContext: ['groups' => ['csv'], 'openapi_definition_name' => 'write_csv'],
    security: 'is_granted("ROLE_USER")',
    processor: EntityClassDtoStateProcessor::class,
    stateOptions: new Options(entityClass: Citation::class),
)]
class CitationCsvDto
{
    use CitationCsvTrait;
}