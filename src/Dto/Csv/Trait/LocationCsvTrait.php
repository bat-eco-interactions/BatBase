<?php

namespace App\Dto\Csv\Trait;

use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Validator\Constraints as Assert;

trait LocationCsvTrait
{
    /** @var string Unique name of the location. */
    #[Groups('csv')]
    #[Assert\NotBlank]
    public string $locationName;

    #[Groups('csv')]
    public ?string $locationDescription = null;

    /** @var int|null The elevation of the location. The start of a range, if elevationMax is set. */
    #[Groups('csv')]
    public ?int $elevation = null;

    /** @var int|null The maximum of an elevation range. */
    #[Groups('csv')]
    public ?int $elevationMax = null;

    /** @var float|null Decimal latitude */
    #[Groups('csv')]
    public ?float $latitude = null;

    /** @var float|null Decimal longitude */
    #[Groups('csv')]
    public ?float $longitude = null;

    /** @var string|null See batbase.org/definitions for possible habitats. */
    #[Groups('csv')]
    public ?string $habitatType = null;

    /** @var string|null 2-letter country code. */
    #[Groups('csv')]
    public ?string $isoCode = null;

    /** @var string|null https://www.iucnredlist.org/resources/country-codes */
    #[Groups('csv')]
    public ?string $locationCountry = null;

    /** @var string https://www.iucnredlist.org/resources/country-codes */
    #[Groups('csv')]
    #[Assert\NotBlank]
    public string $locationRegion;
}