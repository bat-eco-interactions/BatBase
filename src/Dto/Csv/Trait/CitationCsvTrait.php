<?php

namespace App\Dto\Csv\Trait;

use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Validator\Constraints as Assert;

trait CitationCsvTrait
{
    /**
     * The title of the work cited.
     * @var string
     */
    #[Groups('csv')]
    #[Assert\NotBlank]
    public string $citationTitle;

    /**
     * A brief summary of the work being cited.
     * @var string|null
     */
    #[Groups('csv')]
    public ?string $citationAbstract = null;

    /**
     * @var int
     */
    #[Groups('csv')]
    #[Assert\Length(4)]
    #[Assert\NotBlank]
    public int $citationYear;

    /* todo - regex doi pattern */
    /**
     * A unique permanent web address (URL) used to identify an article or document.
     * @var string|null
     */
    #[Groups('csv')]
    public ?string $citationDoi = null;

    /* todo - validate url pattern */
    /**
     * Website URL (non-DOI)
     * @var string|null
     */
    #[Groups('csv')]
    public ?string $citationUrl = null;

    // todo: validate matches with publication type
    /**
     * Citation type must be a valid type of the publication:
     * todo: list types
     * @var string
     */
    #[Groups('csv')]
    public string $citationType;

    /**
     * todo: question this format , alternatives?
     * Last, First Middle Suffix + Last, First Middle Suffix + ...
     * Note: Order matters. Separate author names with + symbol.
     */
    #[Groups('csv')]
    public ?array $citationAuthors = null;

    /* ------ PUBLICATION -------- */

    /**
     * The title of the work cited.
     * @var string
     */
    #[Groups('csv')]
    #[Assert\NotBlank]
    public string $publicationTitle;

    // todo: validate matches with publication type
    /**
     * Publication types have a valid sub-set of citation types.
     * todo: list types
     * @var string
     */
    #[Groups('csv')]
    public string $publicationType;

    /* todo - regex doi pattern */
    /**
     * A unique permanent web address (URL) used to identify an article or document.
     * @var string|null
     */
    #[Groups('csv')]
    public ?string $publicationDoi = null;

    /* todo - validate url pattern */
    /**
     * Website URL (non-DOI)
     * @var string|null
     */
    #[Groups('csv')]
    public ?string $publicationUrl = null;

    /**
     * @var int|null
     */
    #[Groups('csv')]
    public ?int $publicationVolume = null;

    /**
     * @var int|null
     */
    #[Groups('csv')]
    public ?int $publicationIssue = null;

    // todo: assert num - num || num
    // todo: assert num1 < num2
    /**
     * @var string|null
     */
    #[Groups('csv')]
    public ?string $publicationPages = null;

    /**
     * Last, First Middle Suffix + Last, First Middle Suffix + ...
     */
    #[Groups('csv')]
    public ?array $publicationEditors = null;

    /* ------ PUBLISHER -------- */

    #[Groups('csv')]
    public ?string $publisherName = null;

    #[Groups('csv')]
    public ?string $publisherCity = null;

    #[Groups('csv')]
    public ?string $publisherCountry = null;

    /* todo - validate url pattern */
    /**
     * Website URL (non-DOI)
     * @var string|null
     */
    #[Groups('csv')]
    public ?string $publisherUrl = null;

}