<?php

namespace App\Dto;

use ApiPlatform\Doctrine\Orm\State\Options;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Dto\Trait\AuditableDtoTrait;
use App\Entity\Taxon;
use App\State\EntityClassDtoStateProcessor;
use App\State\EntityToDtoStateProvider;
use Symfony\Component\Serializer\Attribute\Groups;

#[ApiResource(
    shortName: 'Taxon',
    description: '...todo',
    operations: [
        new Get(),
        new GetCollection(),
        new Post(security: 'is_granted("ROLE_EDITOR")'),
        new Patch(security: 'is_granted("ROLE_EDITOR")'),
        new Delete(security: 'is_granted("ROLE_EDITOR")')
    ],
    normalizationContext: ['groups' => ['read']],
    denormalizationContext: ['groups' => ['write']],
    paginationItemsPerPage: 5,
    provider: EntityToDtoStateProvider::class,
    processor: EntityClassDtoStateProcessor::class,
    stateOptions: new Options(entityClass: Taxon::class),
)]
class TaxonDto
{
    use AuditableDtoTrait;

    #[ApiProperty(identifier: true)]
    #[Groups('read')]
    public ?int $id = null;

    /** @var string|null todo */
    #[Groups(['read'])]
    public ?string $displayName = null;

    /** @var string|null todo */
    #[Groups(['read', 'write'])]
    public ?string $name = null;

    /** @var bool True when the taxon is the root of a taxonomic group, eg: Order Chiroptera is the root of the group 'Bats'. */
    #[Groups('read')]
    public bool $isRoot;

    /**
     * Direct descendants of the Taxon.
     * @var array<int, TaxonDto>
     */
    #[ApiProperty(readableLink: false, writableLink: false)]
    #[Groups('read')]
    public array $children;

    #[Groups('read')]
    #[ApiProperty(readableLink: false, writableLink: false)]
    public ?TaxonDto $parent;

    // todo: validate value set in mapper
    /** @var int|null The ID of the direct ancestor. */
    #[Groups('write')]
    public ?int $parentId = null;

    // todo: validate value set in mapper
    /** @var int|null The ID of the taxonomic rank. */
    #[Groups('write')]
    public ?int $rankId = null;

    /**
     * <code>
     * [
     *     'id'      => 1,
     *     'displayName'   => 'Order'
     * ];
     * </code>
     * @var array{id:int, displayName:string}
     */
    #[Groups('read')]
    public array $rank;

    /**
     * The taxonomy group the taxon belongs to.
     * <code>
     * [
     *     'id'      => 1,
     *     'displayName'   => 'Bat',
     *     'pluralName'    => 'Bats',
     *      'root' => [
     *          'id' => 1,
     *          'name' => 'Chiroptera',
     *          'taxon' => 1
     *      ]
     * ];
     * </code>
     * @var array|null
     */ // Note: null for Kingdom Animalia and in some test cases.
    #[Groups('read')]
    public ?array $group;

    /**
     * Taxon is the subject (active role) of the Interactions.
     * @var array<int, InteractionDto>
     */
    #[ApiProperty(readableLink: false, writableLink: false)]
    #[Groups('read')]
    public array $subjectRoles;

    /**
     * Taxon is the object (passive role) of the Interactions.
     * @var array<int, InteractionDto>
     */
    #[ApiProperty(readableLink: false, writableLink: false)]
    #[Groups('read')]
    public array $objectRoles;
}
