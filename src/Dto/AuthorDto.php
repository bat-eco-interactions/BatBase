<?php

namespace App\Dto;

use ApiPlatform\Doctrine\Orm\State\Options;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Dto\Trait\AuditableDtoTrait;
use App\Entity\Author;
use App\State\EntityClassDtoStateProcessor;
use App\State\EntityToDtoStateProvider;
use Symfony\Component\Serializer\Attribute\Groups;

#[ApiResource(
    shortName: 'Author',
    description: 'todo...',
    operations: [
        new Get(),
        new GetCollection(),
        new Post(security: 'is_granted("ROLE_EDITOR")'),
        new Patch(security: 'is_granted("ROLE_EDITOR")'),
        new Delete(security: 'is_granted("ROLE_EDITOR")')
    ],
    normalizationContext: ['groups' => ['read']],
    denormalizationContext: ['groups' => ['write']],
    paginationItemsPerPage: 5,
    provider: EntityToDtoStateProvider::class,
    processor: EntityClassDtoStateProcessor::class,
    stateOptions: new Options(entityClass: Author::class),
)]
class AuthorDto
{
    use AuditableDtoTrait;

    #[ApiProperty(identifier: true)]
    #[Groups('read')]
    public ?int $id = null;

    /** @var string|null */
    #[Groups(['read', 'write'])]
    public ?string $firstName = null;

    /** @var string|null */
    #[Groups(['read', 'write'])]
    public ?string $middleName = null;

    /** @var string|null */
    #[Groups(['read', 'write'])]
    public ?string $lastName = null;

    /** @var string|null */
    #[Groups(['read', 'write'])]
    public ?string $suffix = null;

    /** @var string|null */
    #[Groups(['read', 'write'])]
    public ?string $description = null;

    /**
     * Author website.
     * @var string|null
     */
    #[Groups(['read', 'write'])]
    public ?string $linkUrl = null;

    /**
     * Last, First Middle Suffix OR Last
     * @var string
     */
    #[Groups('read')]
    public string $displayName;

    /**
     * First Middle Last Suffix
     * @var string
     */
    #[Groups('read')]
    public string $fullName;

    /**
     * Source entity ID
     * @var string
     */
    #[Groups('read')]
    public string $source;
}