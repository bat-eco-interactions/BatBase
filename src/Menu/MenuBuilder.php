<?php

namespace App\Menu;

use Knp\Menu\FactoryInterface;

// use Symfony\Component\DependencyInjection\ContainerAwareInterface;
// use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class MenuBuilder
{
    /**
     * @param FactoryInterface $factory
     *
     * Add any other dependency you need
     */
    public function __construct(private readonly FactoryInterface $factory)
    {
    }
    // use ContainerAwareTrait;

    public function createMainMenu(array $options)
    {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttributes(['id' => 'oimenu']);
        $menu->addChild('Home', ['route' => 'app_home']);
        $menu->addChild('About', ['uri' => '#']);
        $menu['About']->setAttribute('class', 'closed smtrigger');
        $menu['About']->addChild('Project', ['route' => 'app_about']);
        $menu['About']->addChild('Database', ['route' => 'app_db_top']);
        $menu['About']->addChild('Definitions', ['route' => 'app_definitions']);
        $menu['About']->addChild('Bibliography', ['route' => 'app_biblio']);
        $menu['About']->addChild('Coming Soon', ['route' => 'app_future_dev']);
        $menu->addChild('Explore', ['route' => 'app_explore_page']);

        $user_name = $options['username'];
        if ($options['userrole'] !== 'visitor') {
            $menu->addChild($user_name, ['uri' => '#']);
            $menu[$user_name]->setAttribute('class', 'closed smtrigger');
            $menu[$user_name]->addChild('Submit Publication', ['route' => 'app_submit_pub']);
            if ($this->_isAdmin($options['userrole'])) {
                $menu[$user_name]->addChild('View Submissions', ['route' => 'app_file_upload_list']);
                $menu[$user_name]['View Submissions']->setAttribute('class', 'admin-menu');
                $menu[$user_name]->addChild('View Feedback', ['route' => 'app_feedback']);
                $menu[$user_name]['View Feedback']->setAttribute('class', 'admin-menu');
            }
            if ($this->_isSuper($options['userrole'])) {
                $menu[$user_name]->addChild('Online Users', ['route' => 'super_user_online']);
                $menu[$user_name]->addChild('Content Blocks', ['route' => 'admin_content_block']);
                $menu[$user_name]['Online Users']->setAttribute('class', 'super-admin-menu');
                $menu[$user_name]['Content Blocks']->setAttribute('class', 'super-admin-menu');
            }
            $menu[$user_name]->addChild('View Profile', ['route' => 'user_profile']);
            $menu[$user_name]->addChild('Log Out', ['route' => 'app_logout']);
        } else {
            $menu->addChild('Login', ['route' => 'app_login']);
            $menu->addChild('Participate', ['route' => 'app_register']);
        }

        return $menu;
    }

    private function _isAdmin($user_role)
    {
        $show_for_roles = ['admin', 'super'];
        if (in_array($user_role, $show_for_roles)) {
            return true;
        }

        return false;
    }

    private function _isSuper($user_role)
    {
        $show_for_roles = ['super'];
        if (in_array($user_role, $show_for_roles)) {
            return true;
        }

        return false;
    }
}
