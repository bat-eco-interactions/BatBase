<?php

namespace App\Repository;

use App\Entity\ContentBlock;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ContentBlockRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ContentBlock::class);
    }

    public function findBySlugs($slugAry): array
    {
        return $this->createQueryBuilder('c')
            ->where('c.slug IN (:slugAry)')
            ->setParameter('slugAry', $slugAry)
            ->getQuery()
            ->getResult();
    }

    public function findPageContent($page): array
    {
        $result = $this->createQueryBuilder('c')
            ->where('c.page = :val')
            ->setParameter('val', $page)
            ->select('c.slug', 'c.content')
            ->getQuery()
            ->getResult();

        return array_combine(
            array_column($result, 'slug'),
            array_column($result, 'content')
        );
    }
}
