<?php

namespace App\Repository;

use App\Entity\Taxon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class TaxonRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Taxon::class);
    }
    public function findAllNonBatTaxa($batGroup)
    {
        return $this->createQueryBuilder('taxon')
            ->leftJoin('taxon.group', 'group_taxon')
            ->andWhere('group_taxon.group != :batGroup')
            ->setParameter('batGroup', $batGroup)
            ->getQuery()
            ->execute();
    }

    //todo: get all bat species for header stats
}
