<?php

namespace App\Repository;

use App\Entity\Source;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class SourceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Source::class);
    }
    public function findDirectSources()
    {
        return $this->createQueryBuilder('s')
            ->where('s.isDirect = :isDirect')
            ->setParameter('isDirect', true)
            ->getQuery()
            ->getResult();
    }
}
