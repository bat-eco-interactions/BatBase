<?php

namespace App\Repository;

use App\Entity\Rank;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class RankRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Rank::class);
    }

    public function getOrderedRankDisplayNames(): array
    {
        $result = $this->createQueryBuilder('r')
            ->select('r.displayName')
            ->orderBy('r.ordinal', 'ASC')
            ->getQuery()
            ->getResult();

        return array_column($result, 'displayName');
    }

    public function getAllRankNamesByOrdinal(): array
    {
        $result = $this->createQueryBuilder('r')
            ->select('r.displayName', 'r.ordinal')
            ->getQuery()
            ->getResult();

        return array_combine(
            array_column($result, 'ordinal'),
            array_column($result, 'displayName')
        );
    }

}
