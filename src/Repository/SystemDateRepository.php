<?php

namespace App\Repository;

use App\Entity\SystemDate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class SystemDateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SystemDate::class);
    }
    public function getLastUpdatedState($skip = ['Feedback'])
    {
        $result = $this->createQueryBuilder('s')
            ->select('s.entity', 's.updated')
            ->where('s.entity NOT IN (:skip)')
            ->setParameter('skip', $skip)
            ->getQuery()
            ->getResult();

        return array_combine(
            array_column($result, 'entity'),
            array_map(function($item) {
                return $item['updated']->format('Y-m-d H:i:s');
            }, $result)
        );
    }

    public function findUpdatedEntityData($entityName, $lastUpdatedAt)
    {
        $repo = $this->getEntityManager()->getRepository("\App\Entity\\".$entityName);

        return $repo->createQueryBuilder('e')
            ->where('e.updated > :lastUpdated')
            ->setParameter('lastUpdated', $lastUpdatedAt)
            ->getQuery()
            ->getResult();
    }

    /**
     * Updates the datetime updated for the entity and the system. Does not flush.
     * @todo: this should perhaps be handled in a listener, but doctrine events can
     * not handle the additional entity and finding alternatives grew time consuming.
     *
     * @param string $entityName
     * @return void
     */
    public function trackUpdate($coreName, $detailName = null)
    {
        $names = [ucfirst($coreName), ucfirst($detailName), 'System'];

        $result = $this->createQueryBuilder('s')
            ->where('s.entity IN (:names)')
            ->setParameter('names', $names)
            ->getQuery()
            ->getResult();

        foreach ($result as $entity) {
            $entity->setUpdated();
            $this->getEntityManager()->persist($entity);
        }

        $this->getEntityManager()->flush();
    }

}
