<?php

namespace App\Repository;

use App\Entity\Citation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CitationRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Citation::class);
    }

    public function findValidCitations() {
        return $this->createQueryBuilder('c')
            ->where('c.title NOT LIKE :val')
            ->setParameter('val', '%[DELETED]%')
            ->orderBy('c.fullText', 'ASC')
            ->getQuery()
            ->getResult();

    }
}
