<?php

namespace App\Repository;

use App\Entity\GeoJson;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class GeoJsonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GeoJson::class);
    }
    public function findByLocation($locId)
    {
        return $this->createQueryBuilder('g')
            ->where('g.loc_id = :id')
            ->setParameter('id', $locId)
            ->getQuery()
            ->getResult();
    }
}
