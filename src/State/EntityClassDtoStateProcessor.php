<?php

namespace App\State;

use ApiPlatform\Doctrine\Common\State\PersistProcessor;
use ApiPlatform\Doctrine\Common\State\RemoveProcessor;
use ApiPlatform\Doctrine\Orm\State\Options;
use ApiPlatform\Metadata\DeleteOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfonycasts\MicroMapper\MicroMapperInterface;

class EntityClassDtoStateProcessor implements ProcessorInterface
{
    public function __construct(
        #[Autowire(service: PersistProcessor::class)] private readonly ProcessorInterface $persistProcessor,
        #[Autowire(service: RemoveProcessor::class)] private readonly ProcessorInterface  $removeProcessor,
        private readonly MicroMapperInterface                                             $microMapper,
        private readonly EntityManagerInterface                                           $em,
    )
    {
    }

    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): mixed
    {
        $stateOptions = $operation->getStateOptions();
        assert($stateOptions instanceof Options);
        $entityClass = $stateOptions->getEntityClass();

        if ($operation instanceof DeleteOperationInterface) {
            $entity = $this->em->getRepository($entityClass)->find($data->id);
            $this->removeProcessor->process($entity, $operation, $uriVariables, $context);
            return null;
        }

        $entity = $this->mapDtoToEntity($data, $entityClass, $operation->getMethod());

        $this->persistProcessor->process($entity, $operation, $uriVariables, $context);

        return $this->microMapper->map($entity, $operation->getClass(), [
            MicroMapperInterface::MAX_DEPTH => 2 // allows related entities to be mapped fully (if set in entity mappers)
        ]);
    }

    private function mapDtoToEntity(object $dto, string $entityClass, string $operation): object
    {
        $context = [ // 'POST' or 'PATCH'
            'operation' => $operation,
        ];
        return $this->microMapper->map($dto, $entityClass, $context);
    }
}
