<?php

namespace App\Form;

use App\Entity\ImageUpload;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ImageUploadType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('taxon')
            ->add('file', FileType::class, ['label' => 'Upload image file'])
            ->add('caption')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => ImageUpload::class]);
    }

    public function getBlockPrefix(): string
    {
        return 'App_image_upload';
    }
}
