<?php

namespace App\EventSubscriber;

use App\Service\UserActivity;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\HttpKernel;

/**
 * Listener that updates the last activity of the authenticated user.
 */
class ActivityListener
{
    public function __construct(protected $tokenContext, private readonly UserActivity $userActivity)
    {
    }

    /**
     * Update the user "lastActivity" on each request.
     */
    public function onCoreController(ControllerEvent $event): void
    {
        // API requests are stateless and attempting to access the session will throw an exception.
        if (str_contains($event->getRequest()->getPathInfo(), 'api')) {
            return;
        }

        // Check that the current request is a "MASTER_REQUEST"
        // Ignore any sub-request
        if ($event->getRequestType() !== HttpKernel::MASTER_REQUEST) {
            return;
        }
        // Check token authentication availability
        if ($this->tokenContext->getToken()) {
            $this->userActivity->track();
        }
    }
}
