/**
 * Loads the core structure and features of the Eco-Interactions website.
 */
import { initSiteCore } from './core/core-main';

initSiteCore();
