/**
 * Submitted Publications' Page: Manage submitted publication pdfs. [Admin+]
 *
 * TOC:
 *    DELETE
 *    OPEN
 *        LOAD AND STYLE POPUP
 *        UPDATE LAST VIEWED BY
 */
import { hidePagePopup, showPagePopup, showSaveModal } from '@elems';
import { sendAjaxQuery } from '@util';
import * as O from 'fp-ts/lib/Option';
import { pipe } from 'fp-ts/lib/function';

if ( window.location.pathname.includes( 'pdfs' ) ) {
    $( 'input[name="delete-pdf' ).on( 'click', handleDeletePDF );
    $( 'input[name="view-pdf' ).on( 'click', handleOpenPDF );
}
/* ============================ DELETE ====================================== */
function handleDeletePDF ( ev: JQuery.TriggeredEvent ): void {
    const id: string = $( ev.target ).data( 'id' );
    const config = {
        html: 'Are you sure you want to delete?',
        position: 'left' as const,
        selector: `input[name="delete-pdf"][data-id="${ id }"]`,
        title: 'Delete Publication PDF'
    };
    const button = { onConfirm: () => deletePDF( id ) };
    showSaveModal( config, button );
}
function deletePDF ( id: string ): void {
    const url = `upload/pub/${ id }/delete`;
    sendAjaxQuery( null, url, () => location.reload() );
}
/* ============================ OPEN ======================================== */
/**
 * Clones hidden pdf object element and adds to the hidden page-popup elem. Then
 * the PDF is loaded in the iframe.
 * Note: Couldn't figure out how to get webpack to work with files users upload
 * on the server, so using twig to create the elems. There is probably a better way to do this.
 */
function handleOpenPDF ( ev: JQuery.TriggeredEvent ): void {
    pipe(
        $( ev.target ).attr( 'data-filename' ), //attr must be used to load the pdf
        O.fromNullable,
        O.map( openPdf ),
        O.getOrElse( showAlert )
    );
    updateLastViewedBy( $( ev.target ).data( 'id' ) );
}
function openPdf( fileName: string ) {
    const afterLoad = () => afterOpenPDF( fileName );
    showPagePopup( 'pdf-popup', null, getPDF( fileName ), afterLoad );
}
function afterOpenPDF ( fileName: string ): void {
    $( '#' + fileName ).fadeIn( 500 );
    bindEscEvents();
}
function showAlert() {
    showPagePopup( 'pdf-popup', null, '<div>Error loading PDF.</div>', bindEscEvents );
}
/* -------------------- LOAD AND STYLE POPUP -------------------------------- */
function getPDF ( fileName: string ): HTMLElement | string {
    const $pdf = $( '.' + fileName ).clone();
    $pdf.css( 'display', 'inherit' );
    return pipe(
        $pdf[ 0 ],
        O.fromNullable,
        O.map( el => setPdfElemId( el, fileName ) ),
        O.getOrElseW( () => '<div>Error loading PDF.</div>' )
    )
}
function setPdfElemId( el:HTMLElement, fileName:string ): HTMLElement {
    el.id = fileName;
    return el;
}
function bindEscEvents (): void {
    $( document ).on( 'keyup', function ( evt ) {
        if ( evt.keyCode == 27 ) closePDF();
    } );
    $( "#b-overlay" ).on( 'click', closePDF );
    $( "#b-overlay-popup" ).on( 'click', stopEventPropagation );
}
function stopEventPropagation ( e: JQuery.Event ): void {
    e.stopPropagation();
}
function closePDF (): void {
    hidePagePopup();
    unbindEscEvents();
}
function unbindEscEvents (): void {
    $( document ).off( 'keyup', closePDF );
    $( "#b-overlay" ).off( 'click', stopEventPropagation );
}
/* -------------------- UPDATE LAST VIEWED BY ------------------------------- */
function updateLastViewedBy ( id: string ): void {
    const url = `upload/pub/${ id }/update`;
    sendAjaxQuery( null, url, () => updateTableViewedBy( id ) );
}
function updateTableViewedBy ( id: string ): void {
    $( `#${ id }-viewed` ).text( $( 'body' ).data( 'user-name' ) );
}