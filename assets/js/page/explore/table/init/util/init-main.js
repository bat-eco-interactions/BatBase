/**
 * Handles the focus-entity's data-table (re)build.
 *
 * Note: Refactor to using a formal stack array for table builds to be able to
 * cancel table build if another table-rebuild action is initated. The asynchronous
 * local database calls make this process difficult to cancel.
 *
 * TOC
 *     ENTITY TABLE
 *         LOCATION
 *         SOURCE
 *         TAXON
 *     FORMAT DATA
 *         DATA TREE
 *         AGGRID ROW-DATA
 *     INIT AGGRID TABLE
 *     TABLE REBUILD
 *     UTILITY
 */

import { table, ui } from '@explore';
import { getSelVal } from '@elems/combo';
import { getDetachedRcrd, setData } from '@localdata';
import { buildLocTable, buildSrcTable, buildTxnTable } from '..';
/* ==================== TABLE REBUILD ======================================= */
export function reloadTableWithCurrentFilters() {                   /*dbug-log*///console.log('-- reloadTableWithCurrentFilters');
    const filters = table.getFilterState();
    buildTable( getSelVal( 'Focus' ), getSelVal( 'View' ) )
        .then( () => table.onTableReloadCompleteApplyFilters( filters ) );
}
/**
 * Table-rebuild entry point after local database updates, filter clears, and
 * after edit-form close.
 */
export function resetDataTable ( focus ) {                          /*dbug-log*///console.log( '   //resetting explore table. Focus ? [%s]', focus );
    table.resetTableState();
    return buildTable( focus )
        .then( () => ui.updateUiForTableView() );
}
export function buildTable ( f, view = false ) {
    if ( f === '' ) { return Promise.resolve(); } //Combobox cleared by user
    const focus = f ? f : getSelVal( 'Focus' );                     /*dbug-log*///console.log( "   //buildTable focus[%s], view?[%s]", focus, view );
    const prevFocus = table.getState( 'curFocus' );
    table.resetTableState();
    return updateFocusAndBuildTable( focus, view, prevFocus );
}
/** Updates the top sort (focus) of the data table: 'taxa', 'locs' or 'srcs'. */
function updateFocusAndBuildTable ( focus, view, prevFocus ) {      /*dbug-log*///console.log( "updateFocusAndBuildTable newFocus[%s] lastFocus[%s] view[%s]", focus, prevFocus, view )
    if ( focus === prevFocus ) { return buildDataTable( focus, view ); }
    return onFocusChanged( focus, view )
        .then( () => buildDataTable( focus, view ) );
}
function onFocusChanged ( focus, view ) {
    setData( 'curFocus', focus );
    setData( 'curView', view );
    resetFilterPanel( focus );
    return table.resetFocusState( focus );
}
function resetFilterPanel ( focus ) {
    ui.updateFilterPanelHeader( focus );
    $( '#focus-filters' ).empty();
}
function buildDataTable ( focus, view ) {
    const builders = {
        locs: buildLocTable,
        srcs: buildSrcTable,
        taxa: buildTxnTable
    };
    return builders[ focus ]( view );
}
/* ================== UTILITY =============================================== */
/** Sorts the all levels of the data tree alphabetically. */
export function sortDataTree ( tree ) {
    const sortedTree = {};
    const keys = Object.keys( tree ).sort( alphaBranchNames );

    for ( var i = 0; i < keys.length; i++ ) {
        sortedTree[ keys[ i ] ] = sortNodeChildren( tree[ keys[ i ] ] );
    }
    return sortedTree;

    function sortNodeChildren ( node ) {
        if ( node.children ) {
            node.children = node.children.sort( alphaEntityNames );
            node.children.forEach( sortNodeChildren );
        }
        return node;
    }
    function alphaBranchNames ( a, b ) {
        if ( a.includes( 'Unspecified' ) ) { return 1; }
        var x = a.toLowerCase();
        var y = b.toLowerCase();
        return x<y ? -1 : x>y ? 1 : 0;
    }
}
/** Alphabetizes array via sort method. */
function alphaEntityNames ( a, b ) {                                               //console.log("alphaSrcNames a = %O b = %O", a, b);
    var x = a.displayName.toLowerCase();
    var y = b.displayName.toLowerCase();
    return x<y ? -1 : x>y ? 1 : 0;
}
export function getTreeRcrds ( idAry, rcrds, entity ) {                                   //console.log('getTreeRcrds. args = %O', arguments);
    return idAry.map( id => getDetachedRcrd( id, rcrds, entity ) ).filter( r => r );
}