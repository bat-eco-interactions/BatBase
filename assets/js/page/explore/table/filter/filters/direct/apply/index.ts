export {
    getRowDataForCurrentFilters,
    onFilterChangeUpdateRowData,
} from './apply-filters.js';