/**
 * Filters that will rebuild the table completely when applied.
 */
export {
    loadLocFilters,
} from './loc-filters.js';

export {
    loadSrcFilters,
} from './src-filters.js';

export {
    loadTxnFilters,
    updateTaxonComboboxes,
} from './taxon';