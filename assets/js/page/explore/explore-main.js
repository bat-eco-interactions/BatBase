/**
 * The Explore page entry point. The data table is built to display the
 * eco-interaction records organized by a selected "focus": taxa (grouped further
 * by view: bat, plant, etc), locations, or sources (authors, publications, or
 * publishers). The data map displays interactions geographically. Filtered
 * interactions can be viewed in either form.
 */
import { initDb } from '@localdata';
import { afterLocalStorageReadyInitExploreTable, initPageUi } from './ui/init';

initExplorePage();
/** ==================== PAGE INIT ========================================== */
/** Initializes the Explore page, also syncing local storage, unless on mobile device. */
function initExplorePage () {
    if ( $( '.mobile-opt-overlay' ).length ) {console.log( 'stopping DB load' ); return;} //Popup shown in oi.js
    initLocalStorageAndThenExploreTable();
    initPageUi();
}
function initLocalStorageAndThenExploreTable() {
    initDb()
    .then( afterLocalStorageReadyInitExploreTable );
}