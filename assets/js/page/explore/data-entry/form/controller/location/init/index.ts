export {
    initCreateForm,
    initEditForm,
    initReviewForm,
} from './build-form.js';

export {
    addMapToLocForm,
    focusParentAndShowChildLocs,
    autofillCoordinateFields,
} from './form-map.js';
