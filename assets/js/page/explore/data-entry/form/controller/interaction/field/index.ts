/* ------------------ LOCATION ---------------------------------------------- */
export {
    addLocationSelectionMethodsNote,
    enableCountryRegionField,
    onLocSelection,
    resetLocCombo,
    selectLoc,
    setUnspecifiedLocation,
/* ------------------ MISC -------------------------------------------------- */
    clearInteractionDate,
    initMiscFields,
/* ------------------ SOURCE ------------------------------------------------ */
    clearCitationCombo,
    fillCitationCombo,
/* ------------------ TAXON ROLES ------------------------------------------- */
    addRoleTaxonFocusListeners,
    buildOptAndUpdateCombo,
    enableTaxonFieldCombos,
    onTaxonFieldSelection,
    selectFieldTaxon,
/* ------------------ TYPE -------------------------------------------------- */
    initTypeField,
    onTypeSelection,
    resetTypeData,
} from './controller';
/* ------------------ UTIL -------------------------------------------------- */
export {
    finishFieldFill,
    focusPinAndEnableSubmitIfFormValid,
    initCombos,
} from './util';