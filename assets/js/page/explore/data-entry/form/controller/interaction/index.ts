/**
 * Contains code specific to the interaction form. From within many of the fields
 * the editor can create new entities of the field-type by selecting the 'add...'
 * option from the field's combobox and completing the appended sub-form. Code
 * relevant to the interaction-form's fields are in the fields folder. Code
 * relevant to any create sub-form is located in the dedicated entity folder
 * (the same level where the interaction folder is).
 */
/** ====================== FIELDS =========================================== */
export {
/* ------------------ LOCATION ---------------------------------------------- */
    selectLoc,
/* ------------------ SOURCE ------------------------------------------------ */
    clearCitationCombo,
    fillCitationCombo,
/* ------------------ TAXON ROLES ------------------------------------------- */
    buildOptAndUpdateCombo,
    enableTaxonFieldCombos,
    onTaxonFieldSelection,
    selectFieldTaxon,
/* ------------------ TYPE -------------------------------------------------- */
    resetTypeData,
} from './field';
/** ======================= BUILD FORM ====================================== */
export {
    initCreateForm,
    initEditForm,
    initReviewForm,
} from './init';
