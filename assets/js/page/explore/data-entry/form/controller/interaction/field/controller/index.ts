/* ------------------ LOCATION ---------------------------------------------- */
export {
    addLocationSelectionMethodsNote,
    enableCountryRegionField,
    onCntryRegSelection,
    onLocSelection,
    resetLocCombo,
    selectLoc,
    setUnspecifiedLocation
} from './location.js';
/* ------------------ MISC -------------------------------------------------- */
export {
    clearInteractionDate,
    initMiscFields,
    onSourceTypeChange
} from './misc.js';
/* ------------------ SOURCE ------------------------------------------------ */
export {
    clearCitationCombo,
    fillCitationCombo,
    onCitSelection,
    onPubSelection
} from './source.js';
/* ------------------ TAXON ROLES ------------------------------------------- */
export {
    addRoleTaxonFocusListeners,
    buildOptAndUpdateCombo,
    enableTaxonFieldCombos,
    onTaxonFieldSelection,
    selectFieldTaxon
} from './taxon.js';
/* ------------------ TAG --------------------------------------------------- */
export {
    onTagSelection
} from './tag.js';
/* ------------------ TYPE -------------------------------------------------- */
export {
    initTypeField,
    onTypeSelection,
    resetTypeData
} from './type.js';
