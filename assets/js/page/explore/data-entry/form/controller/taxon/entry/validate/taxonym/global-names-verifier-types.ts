export type MatchType = "NoMatch" | "PartialFuzzy" | "PartialExact" | "Fuzzy" | "Exact" | "Virus" | "FacetedSearch";



export type ApiResponse = {
    "metadata": {
        "namesNumber": number,
        "withAllSources"?: boolean,
        "withAllMatches"?: boolean,
        "withStats"?: boolean,
        "withCapitalization"?: boolean,
        "withSpeciesGroup"?: boolean,
        "withUninomialFuzzyMatch"?: boolean,
        "dataSources"?: number[],
        "mainTaxonThreshold"?: 0.5 | 0.6 | 0.7 | 0.8 | 0.9 | 1.0,
        "statsNamesNum": number,
        "mainTaxon"?: string,
        "mainTaxonPercentage"?: number,
        "kingdom"?: string,
        "kingdomPercentage"?: number,
        "Kingdoms"?: [
            {
                "name": string,
                "namesNum": number,
                "percentage": number
            }
        ]
    },
    "names": [
        {
          "id": string,
          "name": string,
          "matchType": MatchType;
          "bestResult"?: VerifierResult,
          "results"?: VerifierResult[],
          "dataSourcesNum": number,
          "curation": string,
          "overloadDetected": string,
          "error": string
        }
    ]
};
export type VerifierResult = {
    "dataSourceId"?: number,
    "dataSourceTitleShort": string,
    "curation": string,
    "recordId": string,
    "globalId"?: string,
    "localId"?: string,
    "outlink"?: string,
    "entryDate": string,
    "sortScore": number,
    "matchedName": string,
    "matchedCardinality": number,
    "matchedCanonicalSimple"?: string,
    "matchedCanonicalFull"?: string,
    "currentRecordId": string,
    "currentName": string,
    "currentCardinality"?: number,
    "currentCanonicalSimple"?: string,
    "currentCanonicalFull"?: string,
    "isSynonym": boolean,
    "classificationPath"?: string,
    "classificationRanks"?: string,
    "editDistance": number,
    "editDistanceStem"?: number,
    "matchType": MatchType,
    "scoreDetails": {
        "cardinalityScore": number,
        "infraSpecificRankScore": number,
        "fuzzyLessScore": number,
        "curatedDataScore": number,
        "authorMatchScore": number,
        "acceptedNameScore": number,
        "parsingQualityScore": number
    }
};