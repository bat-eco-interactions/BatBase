export {
    getRankName,
    onRankSelection,
    populateAllRankCombos,
} from './rank-main.js';