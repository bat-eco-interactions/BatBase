/** ------------------------ INTERACTION ------------------------------------ */
export {
    buildOptAndUpdateCombo,
    clearCitationCombo,
    enableTaxonFieldCombos,
    fillCitationCombo,
    onTaxonFieldSelection,
    resetTypeData,
    selectFieldTaxon,
    selectLoc,
    /* --- INIT FORM --- */
    initEditForm as interactionEditForm,
    initCreateForm as interactionCreateForm,
    initReviewForm as interactionReviewForm,
} from './interaction';
/** ------------------------- LOCATION -------------------------------------- */
export {
    addMapToLocForm,
    autofillCoordinateFields,
    focusParentAndShowChildLocs,
    /* --- INIT FORM --- */
    initEditForm as locationEditForm,
    initCreateForm as locationCreateForm,
    initReviewForm as locationReviewForm,
} from './location';
/** -------------------------- TAXON ---------------------------------------- */
export {
    getRankName,
    getSelectedTaxon,
    initFieldTaxonSelect,
    /* --- INIT FORM --- */
    initEditForm as taxonEditForm,
    initCreateForm as taxonCreateForm,
    initReviewForm as taxonReviewForm,
} from './taxon';
/** -------------------------- SOURCE --------------------------------------- */
export {
    finishSrcFieldLoad,
    onCitationFieldChange,
    rmvExtraMultiFields,
    selectExistingAuthsOrEds,
    setFinalCitation,
    /* --- INIT FORM --- */
    initEditForm as sourceEditForm,
    initCreateForm as sourceCreateForm,
    initReviewForm as sourceReviewForm,
} from './source';