/**
 * Once the required fields are filled, the citation text is generated and the
 * empty fields are highlighted, prompting the user to continue adding all
 * available data.
 *
 * TOC
 *      RE-GENERATE CITATION
 */
import { model, state } from '@dataentry';
import { generateCitationText } from '@util';
import { ifReqFieldsFilledHighlightEmptyAndPrompt } from './highlight-empty-fields';
import { buildCitTextAndUpdateField, getDataForCitation } from './regen-citation';

let timeout: number | null = null;
/* ======================= ON FIELD CHANGE ================================== */
export function onCitationFieldChange( fLvl: model.FormGroup ): void {
    if ( timeout || isPublicationForm( fLvl ) ) return;
    timeout = window.setTimeout( () => updateUiAfterFieldChange( fLvl ), 750 );
}
/** Prevents calls from the author code when in publication forms. */
function isPublicationForm( fLvl: model.FormGroup ): boolean {
    return state.getFormState( fLvl, 'name' ) === 'Publication';
}
function updateUiAfterFieldChange( fLvl: model.FormGroup ): void {
    timeout = null;
    ifReqFieldsFilledHighlightEmptyAndPrompt( fLvl );
    generateCitation( fLvl );
}
/* ======================= RE-GENERATE CITATION ============================= */
/**
 * Citation text is auto-generated after changes to related data in citation forms.
 * Note: to prevent multiple rebuilds, a timeout is used.
 */
function generateCitation( fLvl: model.FormGroup ): void {
    timeout = null;
    if ( state.isFormActive() ) buildCitTextAndUpdateField( fLvl );
}
/* -------------------- GENERATE FINAL CITATION ----------------------------- */
export function setFinalCitation( fLvl: model.FormGroup ): void {
    const text = generateCitationText( getDataForCitation( fLvl, false ) );
    state.setFieldValue( fLvl, 'Description', text );
}