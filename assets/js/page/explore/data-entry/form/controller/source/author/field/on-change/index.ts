
export {
    buildNewAuthorSelect,
    removeAuthField,
} from './dynamic-field';

export {
    alertBlank,
} from './val-order';

export {
    ifNoneStillSelectedEnableOtherType,
    toggleOtherAuthorType,
} from './toggle-other-type';

export {
    onAuthAndEdSelection,
} from './on-change';

export {
    rmvExtraMultiFields,
} from './on-clear-field';