export {
    /* --- DYNAMIC COMBOS --- */
    buildNewAuthorSelect,
    removeAuthField,
    /* --- ON FIELD CHANGE --- */
    alertBlank,
    ifNoneStillSelectedEnableOtherType,
    onAuthAndEdSelection,
    rmvExtraMultiFields,
    toggleOtherAuthorType,
} from './on-change';

export {
    selectExistingAuthsOrEds,
} from './util'
