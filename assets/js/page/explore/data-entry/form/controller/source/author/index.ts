export {
    /* --- DYNAMIC COMBOS --- */
    buildNewAuthorSelect,
    removeAuthField,
    /* --- SELECT AUTHORS|EDITORS --- */
    selectExistingAuthsOrEds,
    /* --- ON FIELD CHANGE --- */
    alertBlank,
    onAuthAndEdSelection,
    rmvExtraMultiFields,
    toggleOtherAuthorType,
} from './field';

export {
    initCreateForm,
    initEditForm,
    initReviewForm,
} from './form';
