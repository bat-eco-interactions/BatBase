/* ========================= INIT FORM ====================================== */
export {
    finishFieldLoad,
    initCreateForm,
    initEditForm,
    loadCitTypeFields,
    initReviewForm,
} from './init';
/* ============================= UTIL ======================================= */
/* ----------------------- AUTO-GENERATE CITATION --------------------------- */
export {
    onCitationFieldChange,
    setFinalCitation,
} from './util';