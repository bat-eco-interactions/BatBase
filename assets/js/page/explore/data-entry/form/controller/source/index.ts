/* ========================== SOURCE ======================================== */
export {
    finishSrcFieldLoad,
    getSourceDetailType,
    initCombos,
    initCreateForm,
    initEditForm,
    initReviewForm,
} from './source-main.js';
/* -------------------------- AUTHOR ---------------------------------------- */
export {
    selectExistingAuthsOrEds,
    rmvExtraMultiFields,
} from './author';
/* ------------------------- CITATION --------------------------------------- */
export {
    onCitationFieldChange,
    setFinalCitation,
} from './citation';
/* ----------------------- PUBLICATION -------------------------------------- */
/* ------------------------ PUBLISHER --------------------------------------- */
/* ========================== UTIL ========================================== */
export {
    loadSrcTypeFields,
    showSrcSubmitModal,
} from './util';