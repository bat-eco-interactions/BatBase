/**
 * Custom data-entry alerts. (Forms utilize HTML5 validation where possible.)
 *
 * TOC
 *     ALERT FACADE
 *     SHOW DATA-ENTRY ALERT
 *     SHOW ALERT
 *     CLEAR ALERT
 *         DEFAULT
 */
import { components } from '@dataentry';
import * as entity from './entity-alerts.js';
import * as form from './form-alerts.js';
/* ====================== SHOW DATA-ENTRY ALERT ============================= */
/**
 * Handles the validation issue and displays an alert. The alert can be cleared
 * manually with a close button or automatically after a timeout, or on field change.
 */
export function showFormValAlert( fieldName, tag, fLvl ) {          /*perm-log*/console.log( "       --show[%s]FormValAlert [%s][%s]", fLvl, fieldName, tag );
    const el = getAlertElem( fieldName, fLvl );
    const handlers = getFieldValAlertHandler( tag )
    showAlert( fLvl, el, handlers.show( el, fLvl ), handlers.clear );
}
function getFieldValAlertHandler( tag ) {
    const map = {
        invalidRange: {
            show: form.handleInvalidRangeAndReturnAlertMsg
        },
        /* --- INTERACTION --- */
        noValidInts: {
            show: entity.handleNoValidIntsAndReturnAlertMsg
        },
        needsTypeTag: {
            show: entity.handleNeedsTypeTagAndReturnAlertMsg
        },
        /* --- SOURCE --- */
        fillAuthBlanks: {
            show: entity.handleAuthBlanksAndReturnAlertMsg
        },
        /* --- TAXON --- */
        isGenusPrnt: {
            show: entity.handleIsGenusPrntAndReturnAlertMsg,
            clear: entity.clrTaxonRankAlert
        },
        needsGenusName: {
            show: entity.handleNeedsGenusNameAndReturnAlertMsg
        },
        needsGenusPrnt: {
            show: entity.handleNeedsGenusParentAndReturnAlertMsg,
            clear: entity.clrTaxonParentAlert
        },
        needsHigherRankPrnt: {
            show: entity.handleNeedsHigherRankPrntAndReturnAlertMsg,
            clear: entity.clrTaxonParentAlert
        },
        needsHigherRank: {
            show: entity.handleNeedsHigherRankAndReturnAlertMsg,
            clear: entity.clrTaxonRankAlert
        },
        needsLowerRank: {
            show: entity.handleNeedsLowerRankAndReturnAlertMsg,
            clear: entity.clrTaxonRankAlert
        },
        needsName: {
            show: form.getRequiredFieldsEmptyAleryMsg
        },
        noFamily: {
            show: entity.handleNoFamilyAndReturnAlertMsg
        },
        noGenus: {
            show: entity.handleNoGenusAndReturnAlertMsg
        },
        rankNotAvailableInNewGroup: {
            show: entity.handleRankNotAvailableInNewGroupAndReturnAlertMsg,
            clear: entity.clrTaxonParentAlert
        }
    };
    return map[ tag ];
}
/* ===================== SHOW ALERT ========================================= */
/** Returns the validation alert container for the passed field|form. */
export function getAlertElem( fieldName, fLvl ) {                     /*dbug-log*///console.log("getAlertElem for %s", fieldName);
    const field = fieldName ? fieldName.split( ' ' ).join( '' ) : fLvl;  //[fLvl]_alert for form-validation alerts
    const elem = $( '#'+field+'_alert' )[ 0 ];
    $( elem ).addClass( fLvl+'-active-alert' );
    return elem;
}
export function showAlert( fLvl, elem, msg, onClear ) {               /*dbug-log*///console.log('-- showAlert. args[%O]', arguments)
    elem.innerHTML = msg;
    if ( onClear ) { $( elem ).append( getAlertExitBttn( onClear, elem, fLvl ) ); }
    components.toggleSubmitBttn( fLvl, false );
    toggleFieldAlertBackgroudColor( elem );
}
function getAlertExitBttn( onClear, elem, fLvl ) {
    const onExit = clearFieldAlert.bind( null, onClear, elem, fLvl );
    const bttn = components.getExitButton( onExit );
    bttn.className += ' alert-exit';
    return bttn;
}
function toggleFieldAlertBackgroudColor( elem, enable = true ) {
    const highlightEl = isFormLevelAlert( elem ) ? elem : elem.parentElement;
    if ( enable ) return $( highlightEl ).addClass( 'alert-bkgrnd' );
    $( highlightEl ).removeClass( 'alert-bkgrnd' );
}
function isFormLevelAlert( elem ) {
    return elem.parentElement.id.includes( 'form' );
}
/* ===================== CLEAR ALERT ======================================== */
function clearFieldAlert( fieldHandler, elem, fLvl ) {             /*dbug-log*///console.log( ' --clearFieldAlert. elem[%O] fLvl[%s]', elem, fLvl );
    if ( fieldHandler ) fieldHandler( elem, fLvl );
    clearAlert( elem, fLvl );
}
export function clrFormLvlAlert( elem, fLvl ) {                    /*dbug-log*///console.log( ' --clrFormLvlAlert. elem[%O] fLvl[%s]', elem, fLvl );
    $( '#'+fLvl+'_alert' ).empty();
    components.ifFormValidClearAlertsAndEnableSubmit( fLvl );
}
export function setOnCloseClearAlert( elem, fLvl ) {
    $( `#${ fLvl }-form` ).bind( 'destroyed', clearAlert.bind( null, elem, fLvl ) );
}
export function setOnChangeClearAlert( field, elem, fLvl, hndlr ) {   /*dbug-log*///console.log(' --setOnChangeClearAlert field[%s][%s] elem[%O] hndlr[%O]', field, fLvl, elem, hndlr);
    const input = `#${ fLvl }-form #${ field }_f .f-input`;
    if ( !hndlr ) { hndlr = clearAlert.bind( null, elem, fLvl ); }
    $( input ).change( hndlr );
}
/* ------------------------ DEFAULT ----------------------------------------- */
export function clearAlert( elem, fLvl, enableSubmit = true ) {     /*dbug-log*///console.log( '   --clearAlert. elem[%O] enableSubmit?[%s]', elem, enableSubmit );  console.trace()
    clearAlertElem( elem, fLvl );
    enableSubmitIfFormReady( fLvl, enableSubmit );
}
function clearAlertElem( elem, fLvl ) {                             /*dbug-log*///console.log( '   --clearAlertElem. elem[%O] fLvl[%s]', elem, fLvl );
    $( elem ).removeClass( fLvl+'-active-alert' );
    if ( elem.innerHTML ) { elem.innerHTML = ''; }
    $( elem ).fadeTo( 0, 1 );
    toggleFieldAlertBackgroudColor( elem, false )
}
function enableSubmitIfFormReady( fLvl, enableSubmit ) {
    if ( !enableSubmit || isFormInInvalidState( fLvl ) ) return;
    components.ifFormValidClearAlertsAndEnableSubmit( fLvl );
}
function isFormInInvalidState( fLvl ) {
    return !$( `#${ fLvl }-form` ).length || components.hasOpenSubForm( fLvl );
}
/** External-access alert-clear method */
export function clearActiveAlert( fLvl, field, enableSubmit = true ) {
    const alerted = field || fLvl;                                  /*dbug-log*///console.log( '   --clearActiveAlert. fLvl[%s] field?[%s] alerted[%s] enableSubmit?[%s]', fLvl, field, alerted, enableSubmit );
    if ( !$( `.${ fLvl }-active-alert` ).length ) return;
    clearAlert( $( `#${ alerted }_alert` )[ 0 ], fLvl, enableSubmit );
}