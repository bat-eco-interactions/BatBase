# Data Entry Component Module

A JavaScript based system for complex scientific data entry managing taxonomic and ecological data, featuring dynamic forms, real-time validation, and review workflows. The system captures complex hierarchical relationships with support for citations, locations, and taxonomic classifications, resulting in robust and comprehensive ecological interaction data.

[About our Data Model](https://batbase.org/db) | [Field Definitions](https://batbase.org/definitions)

## Design Principles

- **Intuitive and Efficient**: Designed for ease of use, with a focus on simplicity and efficiency.
- **Robust and Comprehensive**: Built to handle complex data entry requirements, with a focus on data integrity and quality.
- **Flexible and Extensible**: Designed to be flexible and extensible, with a focus on modularity and reusability.

## Key Features

- [Automated real-time Chicago Manual of Style citation formatting](https://www.chicagomanualofstyle.org/tools_citationguide.html)
- Hierarchical taxonomic validation ensuring data integrity across classifications
- Dynamic form generation allows for flexible, extensible, and intuitive data entry
- Feedback and guidance provided in response to user actions
- Context aware field data reduces data entry errors and improves user experience
- Data validation and error handling for improved data quality
- Contextual information displays to simplify the data entry process
- Multi-stage review and approval workflow for data quality assurance

## Notable Dependencies

- [fp-ts](https://gcanti.github.io/fp-ts/) - Functional programming utilities
- [Selectize.js](https://selectize.dev/) - Enhanced select inputs
- [Flatpickr](https://flatpickr.js.org/) - Date picker

## Form State Architecture
A hierarchical state management system that supports nested forms, field state tracking, record caching, and review workflows with a data quarantine system providing rollback support. It features a centralized state manager with clear separation of concerns between state access and modification, while handling form lifecycles, field dependencies, and data review processes through a well-defined API.

## Future Features

- [ ] Extract and publish as a standalone npm library
- [ ] Create developer guides and architectural documentation
- [ ] Migrate codebase to TypeScript (in progress)
- [ ] Implement unit tests focusing on data validation and state changes
- [ ] Document the existing E2E test suite covering critical user flows

## Library Structure

```
/data-entry
├── alerts/                  # Form alert management
├── components/
│   ├── core/                # Base component architecture
│   │   ├── button/          # Form control elements
│   │   ├── field/           # Field generation and management
│   │   ├── form/            # Form assembly system
│   │   └── side-panel/      # Contextual information display
│   └── review/              # Data Review workflow components
├── form/
│   ├── controller/          # Form logic controllers
│   │   ├── interaction/     # Taxa interaction management
│   │   ├── location/        # Geographic data handling
│   │   ├── source/          # Citation and publication system
│   │   └── taxon/           # Taxonomic hierarchy management
│   └── init/                # Form initialization
├── model/                   # Data modeling and validation
├── state/                   # Application state management
├── submit/                  # Form submission processing
└── transformers/            # Data transformation layer
    ├── input/               # Incoming data processing
    └── output/              # Response formatting
```

## Data Review System

Dynamic multi-stage approval process built on top of the core data entry components that allows for users from various backgrounds to contribute to the data collection process, while ensuring data quality and consistency.

### Key Features

- Custom components facilitate communication between data managers and data contributors
- Integrated data quarantine system with rollback support
- Audit trail generation for review actions and data state changes
- Intuitive wizard-based review process simplifies complex data review processes
- Field-level and form-level notes for detailed feedback and context
