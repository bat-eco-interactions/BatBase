/**
 * Processes the form-field data and adds to the entity's class property object.
 *
 * EXPORT
 *     prepareAndSetFieldData
 *
 * TOC
 *     DATA HANDLERS
 *         GENERAL
 *         AUTHOR
 *         CITATION
 *         INTERACTION
 *         LOCATION/GEOJSON
 *         PUBLICATION
 *         TAXON
 */
import { form, state } from '@dataentry';
import { getEntityName, isNotNumber, stripString, ucfirst } from '@util';
import * as util from './util.js';

export function prepareDataAndSetEntityProperty ( v, fKey, fConfig ) {/*dbug-log*///console.log( '   --prepareDataAndSetEntityProperty fConfig[%s][%O] v[%O]', fConfig.name, fConfig, v );
    if ( fConfig.server?.prep ) return handleDataPreparation( fKey, fConfig, v );
    const [ eType, prop ] = getEntityPropParams( fConfig );
    util.setEntityProp( fKey, prop, v, eType );
}
function getEntityPropParams( fConfig ) {
    if ( !fConfig.server?.prop ) return [ 'core', ucfirst( fConfig.name ) ];
    const eType = Object.keys( fConfig.server.prop )[ 0 ];
    const prop = ucfirst( fConfig.server.prop[ eType ] );
    return [ eType, prop ];
}
/* ========================= DATA HANDLERS ================================== */
/** Loops through the field's prep methods to handle data-preparation. */
function handleDataPreparation( fKey, fConfig, v ) {
    for ( let handler in fConfig.server.prep ) handleDataPrep( handler );

    function handleDataPrep( h ) {
        getPrepMethod( h )( fKey, fConfig, v, ...fConfig.server.prep[ h ] );
    }
}
/**
 * @param  {string} h  Data-prep method-name, specified in the fields's confg.
 * @return {function}  Data-prep method
 */
function getPrepMethod( h ) {
    const map = {
        buildTaxonDisplayName: buildTaxonDisplayName,
        handleAuthorNames: handleAuthorNames,
        handleSeasonTags: handleSeasonTags,
        handleSecondaryTag: handleSecondaryTag,
        renameField: renameField,
        setCitationPages: setCitationPages,
        setCitationTitle: setCitationTitle,
        setContributors: setContributors,
        setCoreAndDetail: setCoreAndDetail,
        setCoreData: setCoreData,
        setCoreType: setCoreType,
        setDetailData: setDetailData,
        setDetailEntity: setDetailEntity,
        setElevationRange: setElevationRange,
        setGeoJsonData: setGeoJsonData,
        setParent: setParent,
        setPublicationTitle: setPublicationTitle,
        setSuffix: setSuffix,
        validateTags: validateTags
    };
    return map[ h ];
}
/* --------------------------- GENERAL -------------------------------------- */
function setCoreData( g, fConfig, v ) {                                /*dbug-log*///console.log('               --setCoreData [%s] fConfig[%O]', g, fConfig);
    util.setEntityProp( g, fConfig.name, v );
}
function setDetailData( g, fConfig, v ) {                              /*dbug-log*///console.log('               --setDetailData [%s] fConfig[%O]', g, fConfig);
    util.setEntityProp( g, fConfig.name, v, 'detail' );
}
function renameField( g, fConfig, v, name, dKey = 'core' ) {           /*dbug-log*///console.log('               --renameField [%s]entity[%s] fConfig[%O]', name, dKey, g, fConfig);
    util.setEntityProp( g, name, v, dKey );
}
function setCoreType( g, fConfig, v ) {                                /*dbug-log*///console.log('               --setCoreType [%s] fConfig[%O]', g, fConfig);
    if ( typeof v !== 'string' ) { return util.trackFailure( fConfig.name, v ); }
    util.setEntityProp( g, fConfig.entity, v );
}
function setParent( g, fConfig, v, entity ) {                          /*dbug-log*///console.log('               --setParent [%s]entity[%s] fConfig[%O]', g, entity, fConfig);
    const prop = 'Parent' + entity;
    if ( isNotNumber( v ) ) { return util.trackFailure( prop, v ); }
    util.setEntityProp( g, prop, v );
}
function setDetailEntity( g, fConfig, v ) {                            /*dbug-log*///console.log('               --setDetailEntity [%s] fConfig[%O]', g, fConfig);
    if ( isNotNumber( v ) ) { return util.trackFailure( fConfig.name, v ); }
    util.setEntityProp( g, fConfig.name, v, 'detail' );
}
function setCoreAndDetail( g, fConfig, v ) {                        /*dbug-log*///console.log( '               --setCoreAndDetail [%s] fConfig[%O] v[%s]', g, fConfig, v );
    [ 'core', 'detail' ].forEach( e => util.setEntityProp( g, fConfig.name, v, e ) );
}
/* --------------------------- AUTHOR --------------------------------------- */
/** Handles Author names */
function handleAuthorNames( g, fConfig, v ) {
    const names = getAuthNameValues( state.getFormState( fConfig.group, 'fields' ) );
    util.setEntityProp( 'flat', 'DisplayName', buildAuthDisplayName( names ) );
    util.setEntityProp( 'flat', 'DisplayName', buildAuthDisplayName( names ), 'detail' );
    util.setEntityProp( 'flat', 'FullName', buildAuthFullName( names ), 'detail' );
}
function getAuthNameValues( fields ) {                                /*dbug-log*///console.log('--getAuthNameValues fields[%O]', fields);
    const sufx = fields.Suffix.value;
    return {
        first: fields.FirstName.value,
        middle: fields.MiddleName.value,
        last: fields.LastName.value,
        suffix: sufx && sufx[ sufx.length-1 ] !== '.' ? sufx+'.' : sufx
    };
}
// todo: handled with api DTO
function buildAuthDisplayName( names ) {                              /*dbug-log*///console.log('--buildAuthDisplayName names[%O]', names);
    if ( !names.first ) return names.last;
    const name = [ names.last+',', names.first, names.middle, names.suffix ];
    return name.filter( n => n ).join( ' ' );
}
// todo: handled with api DTO
function buildAuthFullName( names ) {                                 /*dbug-log*///console.log('--buildAuthFullName names[%O]', names);
    return Object.values( names ).filter( n => n ).join( ' ' );
}
/** Ensure value saved without punctuation */
function setSuffix( g, fConfig, v ) {
    const sufx = v && v[ v.length-1 ] == '.' ? v.slice( 0, -1 ) : v;
    util.setEntityProp( 'flat', 'Suffix', sufx, 'detail' );
}
/** Creates an object with contributor server-data.  */
function setContributors( g, fConfig, v ) {                         /*dbug-log*///console.log( '           --setContributors fConfig[%O] v[%O]', fConfig, v );
    if ( !v || !Object.keys( v ).length ) return;
    const isEditor = fConfig.name === 'Editor';
    util.setEntityProp( g, 'Contributor', getContribs( v, isEditor ) );
}
function getContribs( vals, isEditor = false ) {                      /*dbug-log*///console.log('           --getContributorData editors?[%s] vals[%O]', isEditor, vals);
    const data = {};
    for ( let ord in vals ) buildContributorData( ord );
    return data;

    function buildContributorData( ord ) {
        const contrib = {
            authId: vals[ ord ],
            isEditor: isEditor,
            ord: ord,
        };                                                          /*dbug-log*///console.log('              --buildContributorData [%O]', contrib);
        data[ vals[ ord ] ] = contrib;
    }
}
/* --------------------------- CITATION --------------------------------------- */
function setCitationTitle( g, fConfig, v ) {                           /*dbug-log*///console.log('--setCitationTitle title[%s]', v)
    const title = stripString( v, true );
    const unqName = getUniqueCitationName( fConfig.group, title );
    util.setEntityProp( 'flat', 'Title', title, 'detail' );
    util.setEntityProp( 'flat', 'DisplayName', unqName, 'detail' );
    util.setEntityProp( 'flat', 'DisplayName', unqName );
}
// todo: handled with api DTO
function getUniqueCitationName( fLvl, citationTitle ) {
    const pub = getEntityName( getPublicationTitle( fLvl ) ); /*dbug-log*///console.log( '--getUniqueCitationName citationTitle[%s] pubTitle[%s]', citationTitle, pub )
    return pub == citationTitle ? citationTitle + '(citation)' : citationTitle;
}
function getPublicationTitle( fLvl ) {
    const pubSrcId = state.getFieldValue( fLvl, 'ParentSource' );
    const pubSrc = state.getRecords( 'source', pubSrcId );
    const pub = state.getRecords( 'publication', pubSrc.publication );
    return pub.displayName;
}
/** Validates page range. */
function setCitationPages( g, fConfig, v ) {
    if ( v && isInvalidPageFormat( v ) ) return util.trackFailure( 'Pages' );
    util.setEntityProp( 'flat', 'PublicationPages', v, 'detail' );
}
function isInvalidPageFormat( v ) {
    const range = v.split( '-' ).map( i => parseInt( i ) );
    return range.length > 2 || ( range[ 1 ] && range[ 0 ] > range[ 1 ] );
}
/* --------------------------- INTERACTION ---------------------------------- */
function getTagsValue() {
    const tags = util.getEntityProp( 'rel', 'Tags' );
    return !tags ? [] : tags;
}
function handleSecondaryTag( g, fConfig, v ) {                         /*dbug-log*///console.log('--handleSecondaryTag v[%s] add?[%s]', v, v === 'Secondary');
    const tags = getTagsValue();
    updateTags( getTagId( 'Secondary' ), tags, v === 'Secondary' );
    util.setEntityProp( 'rel', 'Tags', tags );
}
function updateTags( id, tags, adding = true ) {                      /*dbug-log*///console.log('--updateTags id[%s] tags[%O] adding?[%s]', id, tags, adding);
    if ( adding ) {
        tags.push( id );
    } else if ( tags.indexOf( id ) !== -1 ){
        tags.splice( tags.indexOf( id ), 1 );
    }
}
function getTagId( name ) {
    const tags = state.getRecords( 'tag' );
    return Object.values( tags ).find( t => t.displayName === name ).id;
}
function handleSeasonTags( g, fConfig, v ) {                           /*dbug-log*///console.log('  -- handleSeasonTags seasons[%O]', v);
    const seasons = v ? v : [];
    const tags = getTagsValue();
    tags.push( ...seasons );
    util.setEntityProp( 'rel', 'Tags', tags );
}
/** Interaction-type tags: Validates that required tags are selected. */
function validateTags( g, fConfig, v ) {                               /*dbug-log*///console.log('  -- validateTags fConfig[%O]', fConfig);
    const val = getTagsValue().concat( v ? v : [] );
    if ( fConfig.required ) { validateTypeTags( fConfig, val ); }
    util.setEntityProp( 'rel', 'Tags', val );
}
function validateTypeTags( fConfig, val ) {
    if ( !val.length ) { return failTags( val ); }
    ensureTypeTagSelected( fConfig.misc.typeTags, val );
}
function ensureTypeTagSelected( typeTags, selected ) {                /*dbug-log*///console.log('--ensureTypeTagSelected tags[%O] selected[%O]', typeTags, selected);
    const fails = Object.values( typeTags ).find( isTagMissing );
    if ( fails ) { failTags( selected ); }

    function isTagMissing( tag ) {                                    /*dbug-log*///console.log('   --isRequiredTagSelected tag[%O]', tag);
        const isSelected = selected.indexOf( String( tag.id ) ) !== -1;
        return tag.isRequired && !isSelected;
    }
}
function failTags( val ) {
    util.trackFailure( 'InteractionTags', val )
}
/* ----------------------- LOCATION/GEOJSON --------------------------------- */
// todo: handled with api DTO
/** Handles detail-entity data */
function setGeoJsonData( g, fConfig, v ) {                             /*dbug-log*///console.log('               --setGeoJsonData [%s] fConfig[%O]', g, fConfig);
    const displayPoint = getDisplayCoordinates( v, fConfig.group );
    util.setEntityProp( 'flat', 'DisplayPoint', displayPoint, 'detail' );
    util.setEntityProp( 'flat', 'Type', 'Point', 'detail' );
    util.setEntityProp( 'flat', 'Coordinates', getCoordValue( displayPoint ), 'detail' );
}
function getDisplayCoordinates( lat, fLvl ) {
    const lng = state.getFieldValue( fLvl, 'Longitude' )
    return JSON.stringify( [ lng, lat ] );
}
function getCoordValue( displayPoint ) {
    const geoJson = state.getFormState( 'top', 'geoJson' );
    return geoJson ? geoJson.coordinates : displayPoint;
}
/** Validates and sets elevation range. */
function setElevationRange( g, fConfig, v ) {                          /*dbug-log*///console.log('               -- setElevationRange [%s] fConfig[%O]', g, fConfig);
    const elevLow = state.getFieldValue( fConfig.group, 'Elevation' );
    if ( v < elevLow ) { return util.trackFailure( 'Elevation' ); }
    util.setEntityProp( 'flat', fConfig.name, v );
}
/* --------------------------- PUBLICATION ---------------------------------- */
function setPublicationTitle( g, fConfig, v ) {
    const title = stripString( v );
    util.setEntityProp( 'flat', 'DisplayName', title, 'detail' );
    util.setEntityProp( 'flat', 'DisplayName', title );
}
/* --------------------------- TAXON ---------------------------------------- */
function buildTaxonDisplayName( g, fConfig, v ) {
    const rank = form.getRankName( fConfig.group );
    if ( !rank ) return; //Form closed.
    const dName = rank === 'Species' ? v : rank +' '+ v;
    util.setEntityProp( 'flat', 'DisplayName', dName );               /*dbug-log*///console.log('--buildTaxonDisplayName rank[%s] name[%s]', rank, dName);
}