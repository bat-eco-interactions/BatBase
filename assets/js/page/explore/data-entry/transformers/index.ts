/**
 * Handles the data transformations for the input/output of the data-entry forms.
*/
/* =========================== INPUT ======================================== */
export {
    /* --- INIT VALUES --- */
    setFieldModelInitValues,
    /* --- REVIEW --- */
    clearSkippedRecordsMemory,
    getNextSubReviewEntry,
    getPendingRootReviewEntry,
    prepPayloadForClient,
    updateReviewEntryFields,
} from './input';
/* ============================ OUTPUT ====================================== */
export {
    /* --- ENTRY --- */
    prepareFormData,
    /* --- REVIEW --- */
    buildFormReviewData,
    /* --- TEMP --- */
    addToReviewLog,
    pushUpdatedLogs,
} from './output';