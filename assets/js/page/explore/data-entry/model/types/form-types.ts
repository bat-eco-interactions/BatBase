/**\ Module for the main types associated with form-state. */
//todo2: refactor to 'model'
import * as model from '@dataentry/model'; //todo2: decouple
import * as _t from '@types';
/* ========================== INIT PARAMS =================================== */
export type InitFormParams = InitCreateParams | InitEditParams | InitReviewParams;
//todo rename to FormParamsComposite

type InitBaseParams = {
    action: FormAction;
    group: FormGroup;
    initCombos?: () => void;
    name: string;
    style: string;
    submit?: () => void;
    type?: string;
}
export type InitCreateParams = InitBaseParams & {
    appendForm?: ( form: HTMLElement ) => void;
    combo?: string;
    count?: number;
    onformClose?: () => void;
    afterFormClose?: () => void;
    vals?: { [key: string]: any; };
}
export type InitEditParams = InitBaseParams & {
    id: number | string;
}
export type InitEntryParams = InitCreateParams | InitEditParams;
export type InitReviewParams = InitEntryParams & {
    review: FormReviewEntry;
}
export type InitSelectParams = Required<InitBaseParams> & Required<InitCreateParams> & {
    cancel: () => void;
};
/* ========================== FORM STATE ==================================== */
//todo: includes 'geoJson' in detail entity, but that will never be a form entity
export type FormEntity = _t.CoreEntity | _t.DetailEntity;

export interface FormState {
    group: FormGroup;
    handlers: Record<string, () => void>;
    name: FormEntity;
    type?: string;
    fields: Record<string, FormField>;
}
export type EntityFormFields<T extends string> = {
    [key in T]: FormField;
};
export type FormGroup = 'top' | 'sub' | 'sub2';
export type FormAction = 'create' | 'edit' | 'review' | 'select';

export type FormConfig = {
    fields: {
        [name: string]: FormField;
    };
};
export type FormField = model.FormFieldModel & {
    review?: _t.ReviewEntry | Record<number, _t.ReviewEntry>;
    replacedReview?: _t.ReviewEntry | Record<number, _t.ReviewEntry>;
    entity?: string;
    entityId?: number; //Present when approved ReviewEntry still in data-review process
};
/* ====================== FORM DATA ========================================= */
/* --------------------- REVIEW ENTRY --------------------------------------- */
export type FormReviewEntry = _t.ReviewEntry & {
    existingId?: number;
    inst: {
        group: FormGroup;
        field: string;
    };
    rvwAction?: { id?: number, name: _t.ReviewStage['activeForm'] };
    pChild?: boolean; //Used in select-forms when the selected record has ReviewEntry parents that will be reviewed first.
}