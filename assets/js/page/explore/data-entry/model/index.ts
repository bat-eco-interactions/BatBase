/* =========================== FORM CONFIG ================================== */
export {
    getEntityFormConfig,
    CitationFormFields,
    FieldServerConfig,
    FormFieldModel,
    MultiFieldModel,
} from './config';

export {
    /* --- INIT --- */
    initFormModel,
    /* --- UPDATE --- */
    onLayoutChangeUpdateModel,
    onToggleFieldsUpdateModel,
} from './builders';

export * from './util';
/* =========================== TYPES ======================================== */
export * from './types';
