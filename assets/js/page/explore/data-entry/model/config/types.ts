/**
 * This file contains types for the config files used to generate the data-entry forms.
 *
 * TOC
 *    CONFIG
 *        FIELD
 *        LAYOUT
 */
/* =========================== CONFIG ======================================= */
export interface FormConfig {
    fields: ModelFormFields;  // Form-field configuration. Will be merged with detail-entity and entity-type configs, if present.
    layouts: LayoutConfig; // Field layout configuration for a view (eg, 'all', 'simple', 'create', 'edit')
    name: string;   // The name of the form entity
    records: {     // The entity records to store in memory for the form action
        create?: string[];
        edit?: string[];
    };
    types?: ViewTypeConfig;  // Entity detail/sub-type or form-type (eg, create, edit) config
    style?: string;  // The style class for the form
}
export interface DetailEntityFormConfig extends FormConfig {
    core: string;   // The core-entity for the form, ie, Source (ucfirst)
    type?: string | null;  // Holds detail-sub-type config once type is selected (e.g. 'book' for 'publication')
}
/* --------------------------- FIELD ---------------------------------------- */
export interface ModelFormFields<T extends FormFieldModel = FormFieldModel> {
    [key: string]: T;
}
export type FormFieldModel = FieldModel | MultiFieldModel;
//todo1: refactor dataClasses and prop to be a single object "serverEntityConfig"
export interface FieldModel {
    class?: string;   //Style class for the field
    entity?: string;    //The relational entity for the field (e.g. 'Contributor' for 'Author')
    id?: string;  //Used for id when the label has an invalid selector character
    info?: FieldInfoTipStrings;
    label?: string | false;   //The UI label and the default used for field id. False when no label is needed (multiFields)
    misc?: FieldMiscellaneous;
    name?: string; //The name of the field. Always present in final field-model
    required?: boolean;  //Set if the field is required
    server?:FieldServerConfig;
    shown?: boolean;    //Set if the field is displayed in the form currently
    type?: string | null;   //The field type (e.g. 'text', 'select', 'multiField')
    value?: any;     //The field value
}
/** Field information and tips show in field tooltips and form walkthroughs. */
interface FieldInfoTipStrings {
    tooltip?: string;  //The UI tooltip and the default used for the intro.js tutorial
    intro?: string;   //Text for the intro.js tutorial (includes links and html formatting)
}
/** Container for miscellaneous field-data specific to the field. */
interface FieldMiscellaneous {    //Miscellaneous data for the field
    [key: string]: any;
    customValueStore?: true; //The field value is stored using a custom method
}
/**
 * Entity - The field's server-entity names
 * Prep - Custom transformers to prepare the field-data for server submission.
 * Prop - The field's server-entity properties
 */
export interface FieldServerConfig {
    entity?: {  //The field's entity-class name
        core?: string;  //Set if the field's core-entity is different than fConfig.entity
        detail?: string;  //The field's detail-entity name.
    };
    //Note: Prep is merged with detail entities, so only put methods in the core config that apply to all detail entities
    prep?: { //Prevents the default prep methods and runs the included methods instead
        [key: string]: string[];  //Custom method to run on the field data before sending to server
    };
    prop?: {  //The entity-class's property-name. If not present, field is skipped during server prep and push
        core?: string | false; //false to remove previous prop when state changes
        detail?: string | false;
    };
}
export interface MultiFieldModel extends FieldModel {
    count: number;  //The number of fields current loaded in the multi-field UI
    value: { [key: number]: string }
}
/* --------------------------- LAYOUT --------------------------------------- */
/** Config specific to a detail-entity sub-type  (e.g. 'book' for 'publication') */
export interface ViewTypeConfig {
    [type: string]: {
        fields?: {   // Config will overwrite and merge into the default field config
            [field: string]: Partial<FormFieldModel>;
        };
        layouts?: LayoutConfig;  // Config will be merged into the default layout config
    };
}
/** Field layout configuration for a form view (eg, 'all', 'simple', 'create', 'edit') */
interface LayoutConfig {
    all: LayoutSection[];   // All possible fields. (Default display for edit and review forms)
    simple?: LayoutSection[];   // A subset of fields for a simple display. (If present, default display for create forms)
}
/**
 * Field names in a multi-dimensional array, formatted for display.
 * string: Single field in row cell
 * string[]: Multiple fields in row cell
 * { class?: string; fields: LayoutSection }: Vertical sub-section, 'stacked' fields, in row cell
 */
export type LayoutSection = (
    string      // Single field in row cell
    | string[]  // Multiple fields in row cell
    | { class?: string; fields: LayoutSection }  // Vertical sub-section in row cell
)[];