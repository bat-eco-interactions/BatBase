/**
 * Form-field builder.  rowDiv>(alertDiv, fieldDiv>(label, input, [pin]))
 * - onChange:
 *     -- Update value in field-state
 *     -- If required-field, the form submit-button is toggled as needed.
 *     -- If citation form, citation-text auto-generated as needed.
 */
import { cloneObj } from '@util';
import { buildMultiSelectField, getFieldElems, getFieldInput, isMultiField } from '@elems';
import { state } from '@dataentry';
import * as change from './on-field-change.js';
import * as fUtil from './util-button/util-button-main.js';
/* ====================== BUILD SINGLE-FIELD ================================ */
export function buildFormField( config ) {                          /*dbug-log*///console.log( '--buildFormField config[%O]', config );
    const fieldState = state.getFieldState( config.group, config.name );
    const fieldModel = { ...fieldState, ...config };                /*dbug-log*///console.log( '       --fieldModel name[%s] fieldModel[%O]', fieldModel.name, fieldModel );
    return getFieldInput( fieldModel )
        .then( input => buildField( fieldModel, input ) );
}
function buildField( fieldModel, input ) {
    const fieldState = updateFormFieldStateModel( fieldModel );
    return buildFormFieldElems( fieldState, input );
}
function buildFormFieldElems( f, input ) {                          /*dbug-log*///console.log( '--buildFormFieldElems f[%s][%O] value?[%s]', f.name, f, input.value );
    change.setFieldChangeListeners( f, input );
    const fieldContainer = isMultiField( f ) ? input : getFieldElems( f, input );
    fUtil.addFieldUtilElem( f, fieldContainer );
    return fieldContainer;
}
/* ====================== BUILD MULTI-FIELD ================================= */
export function buildDynamicFormField( fieldModel ) {               /*dbug-log*///console.log( '+--buildDynamicFormField f[%O]', fieldModel );
    return buildMultiSelectField( fieldModel )
        .then( addChangeEventAndReturnField );

    function addChangeEventAndReturnField( fieldContainer ) {
        fUtil.addFieldUtilElem( fieldModel, fieldContainer );
        return fieldContainer;
    }
}
/** Note: This method is the first form-method after the util elem-build. */
function updateFormFieldStateModel( f ) {                        /*dbug-log*///console.log( '       --updateFormFieldStateModel f[%s][%O]', f.name, cloneObj( f ) );
    const newState = {
        class: f.class,
        combo: isComboField( f.type ),
        id: f.id || f.name,
        label: f.label,
        shown: true,
    };
    if ( isMultiFieldInit( f ) ) newState.value = {};           /*dbug-log*///console.log( '       --updateFormFieldStateModel newState[%O]', newState );
    return state.setFieldState( f.group, f.name, newState );
}
function isComboField( type ) {
    return [ 'multiSelect', 'select', 'tags' ].includes( type );
}
/** Multi-fields must have an object set to hold their values. */
function isMultiFieldInit( f ) {
    return f.type === 'multiSelect' && !f.value;
}