/**
 *
 */
import * as model from '@dataentry/model'; //todo2: decouple

export function focusField( fLvl: model.FormGroup, field: string ): void {
    const selector = `#${ fLvl }-form #${ field }_f .f-input`;
    $( selector ).focus();
}