/**
 * Returns a container with the form's walkthrough button, if walkthrough set.
 *
 * Export
 *     getFormHelpElems
 */
import { getElem, showFormTutorialModal } from '@elems';
/* ===================== FORM WALKTHROUGH =================================== */
export function getFormHelpElems ( fLvl, fields ) {                 /*dbug-log*///console.log('+--getFormHelpElems fLvl[%s] fields[%O]', fLvl, fields);
    const cntnr = getElem( 'div', { id: fLvl + '-help', class: 'flex-row' } );
    $( cntnr ).append( getFormWalkthroughBttn( fLvl, getInfoStepCount( fields ) ) );
    return cntnr;
}
function getFormWalkthroughBttn ( fLvl, infoSteps ) {               /*dbug-log*///console.log( '   --getFormWalkthroughBttn fLvl[%s] infoSteps[%s]', fLvl, infoSteps );
    if ( !infoSteps ) { return $( '<div>' )[ 0 ]; }
    const titleInfo = "Hover your mouse over any field and it's help popup will show, if it has one.";
    const bttn = buildWalkthroughButton( fLvl, titleInfo );
    $( bttn ).on( 'click', showFormTutorialModal.bind( null, fLvl ) );
    setIntroWalkthroughAttrs( bttn, titleInfo, ++infoSteps, fLvl );
    return bttn;
}
function buildWalkthroughButton ( fLvl, titleInfo ) {
    const attr = {
        id: fLvl + '-walkthrough',
        class: 'hlp-bttn',
        title: titleInfo,
        type: 'button',
        value: '?',
    };
    return getElem( 'input', attr );
}
function setIntroWalkthroughAttrs ( bttn, titleInfo, infoSteps, fLvl ) {
    $( bttn ).attr( {
        'data-intro': titleInfo,
        'data-step': infoSteps
    } );
}
function getInfoStepCount( fields ) {
    return Object.values( fields ).reduce( ( count, f ) => f.info ? ++count : count, 0 );
}