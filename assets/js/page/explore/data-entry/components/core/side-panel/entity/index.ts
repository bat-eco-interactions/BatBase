export {
    clearSidePanelDetails,
    fillEditEntitySidePanel,
    getEntityDetailElems,
    setSubEntityDetails,
} from './entity-details-main.js';