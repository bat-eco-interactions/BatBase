/**
 * The form side-panel is used to display additional details about the entity
 * record - showing all related entities, or the contained data within an
 * interaction (source and location details).
 */
/* =========================== ENTITY ======================================= */
export {
    clearSidePanelDetails,
    fillEditEntitySidePanel,
    setSubEntityDetails,
} from './entity';
/* ============================ INIT ========================================= */
export {
    getSidePanelElems,
} from './init/init-main';
/* =========================== REVIEW ======================================= */
export {
    initSubEntityDataReviewElems,
    resetReviewSidePanel,
} from './review';