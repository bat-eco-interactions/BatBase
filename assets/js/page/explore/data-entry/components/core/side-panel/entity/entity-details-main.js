/**
 * The form side-panel is used to display additional data about the entity
 * record - showing all related entities, or the contained data within an
 * interaction (source and location details).
 */
import { state } from '@dataentry'
import * as int from './interaction-details.js';
import * as rel from './relational-details.js';
/* ===================== INIT DETAIL-ELEMS ================================== */
export function getEntityDetailElems( fConfig ) {                    /*dbug-log*///console.log("--getEntityDetailElems fConfig[%O]", fConfig);
    const builder = state.isEditForm( fConfig.group ) && fConfig.name !== 'Interaction' ?
        rel.getSubEntityEditDetailElems : int.getInteractionDetailElems;
    return builder( fConfig.name );
}
/* ================== INTERACTION-FORM DETAILS ============================== */
export function setSubEntityDetails() {
    return int.setSubEntityDetails( ...arguments );
}
export function clearSidePanelDetails() {
    return int.clearSidePanelDetails( ...arguments );
}
/* ================== EDIT FORM RELATIONAL DETAILS ========================== */
export function fillEditEntitySidePanel() {
    rel.fillEditEntitySidePanel( ...arguments );
}