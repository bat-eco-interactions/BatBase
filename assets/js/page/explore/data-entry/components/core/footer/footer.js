/**
 * Returns row with a checkbox that will toggle optional form fields on the left
 * and, on the right, the submit/cancel buttons and, in review forms, an actions combo.
 *
 * Export
 *     getFormFooter
 *
 * TOC
 *     LEFT-ALIGNED ELEMS
 *         DELETE BUTTON
 *     RIGHT-ALIGNED ELEMS
 *         SUBMIT AND CANCEL BUTTONS
 */
import { getElem, showSaveModal } from '@elems';
import { components, state, submit } from '@dataentry';

//todo1: refactore with getFormFooter
export function getFormFooter ( entity, fLvl, action ) {            /*dbug-log*///console.log("+--getFormFooter [%s][%s][%s]", entity, fLvl, action);
    const cntnr = getFooterRowContainer();
    $( cntnr ).append( ...buildFooterElems( entity, fLvl, action ) );
    return cntnr;
}
function getFooterRowContainer () {
    return getElem( 'div', { class: 'flex-row bttn-cntnr' } );
}
function buildFooterElems ( entity, fLvl, action ) {
    const lElems = getLeftAlignedFooterElems( entity, fLvl, action );
    const spacer = $( '<div></div>' ).css( "flex-grow", 2 );
    const rElems = components.buildSubmitAndCancelBttns( fLvl, action );
    return [ lElems, spacer, rElems ];
}
/** Returns a (submit or cancel) button for the form fLvl. */
function buildFormButton ( actn, lvl, val ) {
    const attr = { id: lvl + '-' + actn, type: 'button', value: val };
    return getElem( 'input', attr );
}
/* ================== LEFT-ALIGNED ELEMS ==================================== */
function getLeftAlignedFooterElems ( entity, fLvl, action ) {
    return action === 'edit' ? getDeleteBttn( fLvl ) :
    components.ifMultipleDisplaysGetToggle( entity, fLvl );
}
/* --------------------- DELETE BUTTON -------------------------------------- */
function getDeleteBttn ( fLvl ) {
    if ( fLvl !== 'top' ) { return null; }
    const bttn = buildFormButton( 'delete', 'top', 'Delete' );
    $( bttn ).on( 'click', confirmDelete );
    return bttn;
}
function confirmDelete () {                                          /*dbug-log*///console.log('    -- confirmDelete');
    showSaveModal( buildModalConfig(), getModalButton(), getModalEvents() );
    $( `#top-submit` ).css( { opacity: .5, cursor: 'not-allowed' } );
}
function buildModalConfig () {
    return {
        html: 'Are you sure the data are ready to be deleted?',
        position: 'right',
        selector: `#top-delete`,
        title: 'Delete Entity'
    };
}
function getModalButton() {
    return {
        onConfirm: () => {
            state.setFormState( 'top', { action: 'delete' } );
            submit.submitForm( 'top' );
        }
    };
}
function getModalEvents() {
    return {
        onexit: () => components.ifFormValidClearAlertsAndEnableSubmit( 'top' ),
    };
}