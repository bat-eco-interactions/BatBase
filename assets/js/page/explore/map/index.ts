export {
    addFormMapPin,
    initFormMap,
    showAllLocsOnMap,
    showLocOnMap,
    showIntsOnMap,
} from './actions';

export {
    clearMemory,
} from './manager.js';