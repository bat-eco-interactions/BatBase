export {
    geocodeInForm,
    geocodeOnFormMapClick,
} from './geocode-form.js';

export {
    addFormMapPin,
    initFormMap,
} from './form-map-main.js';

export {
    addCountToLegend,
} from './map-elems.js';

