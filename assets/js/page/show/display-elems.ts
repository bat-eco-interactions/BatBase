import * as d from "./format-data";
import * as _t from "@types";
import { getElem } from "@elems";

export function displayEntityData ( displayConfig: d.DisplaySection[] ): void {
    const sections = displayConfig.map( buildDataSection );
    $( '#entity-show' ).empty().append( sections );
}

function buildDataSection ( config: d.DisplaySection, i: number ): HTMLDivElement {
    const rows = config.rows.map( row => getSectionRow( row, i ) );
    return getDataSect( ++i, config.section, rows );
}

function getSectionRow ( row: d.DisplayRow, i: number ): HTMLDivElement {
    return buildDataRow( ++i, row.map( getRowCell ).filter( _t.isNonNullable ) );
}

function getRowCell ( cell: d.DataCell ): HTMLDivElement | null {
    if ( cell.length == 1 ) return ifDataGetCell( cell[ 0 ] );
    return getRowGroupSect( cell.map( ifDataGetCell ).filter( _t.isNonNullable ) );
}

function ifDataGetCell ( display: d.PropData | null | undefined ): HTMLDivElement | null {
    return _t.isNonNullable( display ) ? getDataCell( display ) : null;
}

/* ------------------------- HTML BUILDERS ---------------------------------- */
function getDataSect ( cnt: number, section: d.DisplaySection["section"], rows: HTMLDivElement[] ): HTMLDivElement {
    const hdr = getElem( 'h3', { text: section.name } );
    const id = 'data-sect-' + cnt;
    const classes = 'data-sect flex-col gap-5 ' + ( section.classes || '' );
    return getDivWithContent( id, classes, [ hdr, ...rows ] );
}

function buildDataRow ( cnt: number, rowCells: HTMLDivElement[] ): HTMLDivElement {
    return getDivWithContent( `sect-row${ cnt }`, 'sect-row flex-row gap-5', rowCells );
}

function getRowGroupSect ( colCells: HTMLDivElement[] ): HTMLDivElement {
    return getDivWithContent( '', `group-col flex-col gap-5`, colCells );
}

function getDataCell ( display: d.PropData ): HTMLDivElement {
    const lbl = getFieldLabel( display.field, display.label );
    const data = getDivWithContent( display.field + '-data', '', display.content );
    const classes = 'flex-row cell-data ' + ( display.classes ?? '' );
    const elems = [ lbl, data ].filter( _t.isNonNullable );
    return getDivWithContent( display.field + '-cell', classes, elems );
}

/** Note: If label is set to FALSE in config, no label is built. */
function getFieldLabel ( field: d.PropData["field"], label: d.PropData["label"] ): HTMLLabelElement | null {
    return label === false ? null : getElem<HTMLLabelElement>( 'label', { text: field + ':' } );
}

/* ------------ base ------------------- */
function getDivWithContent ( id: string, classes: d.PropData['classes'], content: d.PropData['content'] ): HTMLDivElement {
    const div = getElem<HTMLDivElement>( 'div', { class: classes, id: id } );
    const html = content ? content : '[ NONE ]';
    $( div ).append( html );
    return div;
}
