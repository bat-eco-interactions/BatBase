import * as _t from "@types";

export type TaxonomyOverview = {
    displayName: string;
    id: number;
    rank: number;
    children: Record<string, TaxonomyOverview>;
}

export type TaxonInteractionCounts = {
    total: number
    types: AggregatedCounts;
    locations: Record<string, AggregatedCounts | number>;
}
type AggregatedCounts = Record<string, number>;

export type InteractionOverview = {
    country: string;
    id: number;
    interactionType: _t.EntityBones<_t.InteractionTypeName>;
    location: _t.Location;
    note?: string;
    object: number;
    serverUpdatedAt: string;
    region: string;
    source: FlatInteractionSource;
    subject: number;
    tags: _t.InteractionTag[];
    updatedBy: string;
}

type FlatSourceProps = Lowercase<_t.SourceType> | 'contributors';

interface FlatSource extends Omit<_t.Source, FlatSourceProps> {
    author?: _t.Author;
    citation?: _t.Citation;
    // contributors: { id: { authorType: authorDisplayName }}
    contributors: { [key: number]: { [key in Lowercase<_t.AuthorType>]: string } };
    publisher?: _t.Publisher;
    publication?: _t.Publication;
}
type FlatInteractionSource = Required<Pick<FlatSource, 'citation' | 'contributors'>>
    & Omit<FlatSource, 'citation' | 'contributors'>;
