import { TaxonomyOverview } from "./show-types";

let showId: number;
let group: 'parent' | 'full';
let ranks: Record<number, string>[];
let taxonomy: TaxonomyOverview;

/**
 * Returns the taxonomy-display html for the group:
 *     full - All related taxa from the root down with the core taxon bolded
 *     parent - From the core taxon up through the group
 */
export function formatTaxonomy (
    i: number,
    t: TaxonomyOverview,
    g: 'parent' | 'full',
    r: Record<number, string>[]
): string {
    showId = i;
    group = g;
    ranks = r;
    taxonomy = group === 'full' ? t : getTaxonomyWithoutDescendants( { ...t } );
    return formatForDisplay();
}

/**
 * Taxonomy display for subject and object of an interaction start with the target taxon in the role and
 * display direct ancestors.
 * @param orgTaxonomy
 */
function getTaxonomyWithoutDescendants ( orgTaxonomy: TaxonomyOverview ): TaxonomyOverview {
    let currentTaxon = orgTaxonomy;
    while ( currentTaxon.id !== showId ) {
        currentTaxon = Object.values( currentTaxon.children )[ 0 ] as TaxonomyOverview;
    }
    currentTaxon.children = {};
    return orgTaxonomy;
}

/**
 * Formats the taxonomy into a hierarchical display with each taxon on a line with
 * an arrow to it's direct ancestor with indentation to clearly distinguish each rank.
 */
function formatForDisplay (): string {
    const displayOrder = buildTaxonDisplayOrder( taxonomy );
    if ( group === 'parent' ) displayOrder.reverse();
    const indentLvls = calcRankIndentLevels( displayOrder[ 0 ]! );
    return displayOrder.reduce( ( html: string, t: TaxonomyOverview ) => {
        return html + formatTaxonForDisplay( indentLvls[ t.rank ]!, t )
    }, "" );
}

function buildTaxonDisplayOrder ( taxon: TaxonomyOverview ): TaxonomyOverview[] {
    const children = Object.values( taxon.children ).flatMap( buildTaxonDisplayOrder );
    return [ taxon, ...children ];
}

function calcRankIndentLevels ( taxon: TaxonomyOverview ): Record<number, number> {
    const indentAmount = {};
    indentAmount[ taxon.rank ] = 0;
    const rankOrder = Object.keys( ranks ).sort( ( a, b ) => Number( a ) - Number( b ) );
    const baseIndex = rankOrder.indexOf( String( taxon.rank ) );
    rankOrder.forEach( ( r, idx ) => indentAmount[ r ] = Math.abs( baseIndex - idx ) );
    return indentAmount;
}

function formatTaxonForDisplay ( indentLvl: number, taxon: TaxonomyOverview ): string {
    const prefix = getIndentation( indentLvl );
    const html = taxon.id === showId ?
        getEntityLinkHtml( taxon.id, `<strong>${ taxon.displayName }</strong>` ) :
        getEntityLinkHtml( taxon.id, taxon.displayName );
    return prefix + html;
}

function getIndentation ( indentLvl: number ): string {
    const indent = !indentLvl ? '' : '<br>' + '&emsp;'.repeat( indentLvl );
    return `${ indent }${ String.fromCharCode( 8627 ) }&nbsp`;
}

function getEntityLinkHtml ( id: number, displayTxt: string | undefined ): string {
    const link = $( 'body' ).data( 'base-url' ) + `taxon/${ id }`;
    return `<a href="${ link }">${ displayTxt }</a>`;
}
