/**
 * Returns the entity's data formatted for display and grouped by sections, rows,
 * and cells.
 * Ex: [{
 *        section: { classes, name },
 *        rows: [
 *            (row1)[
 *                (cell) [
 *                    { field: 'name', content: data }
 *                ], (cell)...
 *            ], (row2)...
 *         ]
 *      }, { section2 }]
 *
 * TOC:
 *    ENTITY-SHOW CONFIG
 *    FIELD-DATA HANDLERS
 */
import { ucfirst } from '@util';
import * as _t from '@types';
import { formatTaxonomy } from './format-taxonomy';
import {
    InteractionOverview,
    TaxonInteractionCounts,
    TaxonomyOverview
} from "./show-types";
/* ========================== TYPE DEFINITIONS ============================== */
export type PropData = {
    classes?: string;
    content: HTMLElement[] | string | null;
    field: string;
    label?: string | false;
};
export type DataCell = ( PropData | null )[];
export type DisplayRow = DataCell[];
export type DisplaySection = {
    rows: DisplayRow[];
    section: {
        classes?: string,
        name: string;
    };
};
/* =================== ENTITY-SHOW CONFIG =================================== */
export type ShowEntityName = "interaction" | "taxon";

export type TaxonOverviewResponse = {
    counts: TaxonInteractionCounts
    ranks: Record<number, string>[]
    entity: _t.ApiTaxon
    taxonomy: TaxonomyOverview
}

export type InteractionOverviewResponse = {
    ranks: Record<number, string>[],
    subjectTaxonomy: TaxonomyOverview
    objectTaxonomy: TaxonomyOverview
    entity: InteractionOverview
}

export function getEntityDisplayData (
    classname: ShowEntityName,
    id: number,
    data: TaxonOverviewResponse | InteractionOverviewResponse
): DisplaySection[] {
    return classname === 'interaction'
        ? getIntDisplayData( data as InteractionOverviewResponse )
        : getTxnDisplayData( id, data as TaxonOverviewResponse );
}

function getIntDisplayData ( data: InteractionOverviewResponse ): DisplaySection[] {
    const entity = data.entity;
    return [
        {
            section: {
                name: 'Interaction Details',
            },
            rows: [
                [  //row 1
                    [ //cell 1
                        {
                            field: 'Subject',
                            content: formatTaxonomy( entity.subject, data.subjectTaxonomy, 'parent', data.ranks )
                        }
                    ],
                    [  //cell 2
                        {
                            field: 'Object',
                            content: formatTaxonomy( entity.object, data.objectTaxonomy, 'parent', data.ranks )
                        }
                    ],
                    [  //cell 3
                        {
                            field: 'Type',
                            content: entity.interactionType.displayName
                        },
                        {
                            field: 'Tag',
                            content: getTagData( entity.tags )
                        },
                    ]
                ],
                [ //row 2
                    [  //cell 1
                        {
                            field: 'Note',
                            content: getIfSet( entity.note )
                        }
                    ]
                ]
            ]
        }, {
            section: {
                name: 'Source',
            },
            rows: [
                [  //row 1
                    [
                        getContributorFieldData( entity.source.contributors )
                    ], [
                    { field: 'Citation', content: getCitationDisplay( entity.source ) },
                ]
                ], [
                    [
                        { field: 'Abstract', content: getIfSet( entity.source.citation.abstract ) }
                    ]
                ]
            ]
        }, {
            section: {
                name: 'Location',
            },
            rows: [
                [  //row 1
                    [
                        { field: 'Name', content: entity.location.displayName },
                        { field: 'Coordinates', content: getCoordinates( entity.location ), classes: 'max-content' },
                    ], [
                    { field: 'Country', content: getNameIfSet( entity.location, "country" ) },
                    { field: 'Region', content: entity.location.region.displayName, classes: 'max-content' },
                ], [
                    { field: 'Habitat', content: getNameIfSet( entity.location, "habitatType" ) },
                    { field: 'Elevation(m)', content: getElevRange( entity.location ) },
                ], [
                    { field: 'Description', content: getIfSet( entity.location.description ) },
                ]
                ]
            ]
        }
    ];
}

function getTxnDisplayData ( id: number, data: TaxonOverviewResponse ): DisplaySection[] {
    return [
        {
            section: {
                name: `${ data.entity.group.displayName } Hierarchy - ${ data.entity.name }`,
            },
            rows: [
                [  //row 1
                    [
                        {
                            field: 'Taxonomy',
                            label: false,
                            content: formatTaxonomy( id, data.taxonomy, 'full', data.ranks )
                        },
                    ],
                ]
            ]
        }, {
            section: {
                classes: 'flex-grow flex-wrap',
                name: `${ data.counts.total } Interactions`,
            },
            rows: [
                [  //row 1
                    [
                        {
                            field: 'By Type',
                            content: getIntTypeCounts( data.counts.types ),
                            classes: 'max-content'
                        },
                    ],
                    [
                        {
                            field: 'By Country',
                            content: getIntRegionCounts( data.counts.locations ),
                            classes: 'max-content'
                        },
                    ],
                ]
            ]
        }
    ];
}

/* ================== FIELD-DATA HANDLERS =================================== */
function getTagData ( tags: InteractionOverview['tags'] ): string | null {
    if ( !tags.length ) return null;
    return tags.map( t => t.displayName! ).sort( moveSecondaryTag ).join( ', ' );
}

function moveSecondaryTag ( a: string, b: string ): number {
    return b === 'Secondary' ? -1 : 1;
}

function getNameIfSet ( entity: _t.EntityRecord, field: string ): string | null {
    return entity[ field ] ? entity[ field ].displayName : null;
}

function getIfSet ( value: string | undefined ): string | null {
    return value ? value : null
}

/* ------------------------------- TAXON ------------------------------------ */

/* ---------------------------- SOURCE -------------------------------------- */
function getContributorFieldData ( authors: InteractionOverview['source']['contributors'] ): PropData | null {
    if ( hasNoAuthors( authors ) ) return null;
    const authorType = getAuthorType( authors );
    return buildAuthorDisplayData( authors, authorType );
}

function hasNoAuthors ( authors: InteractionOverview['source']['contributors'] ): boolean {
    return !authors || !Object.keys( authors ).length;
}

function getAuthorType ( authors: InteractionOverview['source']['contributors'] ): Lowercase<_t.AuthorType> {
    const first = _t.objectValues( authors )[ 0 ];
    return first ? _t.objectKeys( first )[ 0 ]! : 'author';
}

function buildAuthorDisplayData ( authors: InteractionOverview['source']['contributors'], authorType: Lowercase<_t.AuthorType> ) {
    const names = concatAuthorNames( authors, authorType );
    const fieldName = `${ ucfirst( authorType ) }s`;
    return { field: fieldName, content: names, classes: 'max-content' };
}

function concatAuthorNames ( authors: InteractionOverview['source']['contributors'], authorType: Lowercase<_t.AuthorType> ): string {
    return _t.objectValues( authors ).map( a => a[ authorType ] ).join( "<br>" );
}

function getCitationDisplay ( source: InteractionOverview['source'] ): string {
    const type = source.citation.citationType.displayName;
    const doi = getDoiLink( source.doi );
    const url = getCitationWebsite( source.linkUrl, type );
    return source.citation.fullText + doi + url;
}

function getDoiLink ( doi: string | undefined ): string {
    return doi ? ` <a href="${ doi }" target="_blank">DOI</a>` : '';
}

function getCitationWebsite ( url: string | undefined, type: string ): string {
    return url ? ` <a href="${ url }"" target="_blank">Read ${ type }</a>` : '';
}

/* ---------------------------- LOCATION ------------------------------------ */
function getElevRange ( location: _t.Location ): string | null {
    const elev = location.elevationMax && location.elevation ?
        ( location.elevation + ' - ' + location.elevationMax ) :
        ( location.elevation || location.elevationMax ); //Some locations have a max but not the base of the range. Will fix in data soon.
    return elev ? String( elev ) : null;
}

function getCoordinates ( location: _t.Location ): string {
    return location.latitude && location.longitude ?
        ( location.latitude.toString() + ', ' + location.longitude.toString() ) :
        '';
}

/* -------------------------- INTERACTIONS ---------------------------------- */
function getIntTypeCounts ( totals: TaxonOverviewResponse['counts']['types'] ): string {
    const sorted = Object.entries( totals ).sort( ( a:any, b:any ) => b[ 1 ] - a[ 1 ] );
    return sorted.map( ( [ type, count ] ) => {
        return `<span class="show-int-cnt">${ count }</span><span>${ type }</span>`;
    } ).join( '<br>' );
}

function getIntRegionCounts (
    totals: TaxonOverviewResponse['counts']['locations'],
    depth: number = 0
): string {
    const sorted = Object.entries( totals ).sort( ( a:any, b:any ) => b[ 1 ] - a[ 1 ] );
    const prefix = getIndentation( depth );
    return sorted.flatMap( ( [ key, value ] ) => {
        if ( typeof value === 'number' ) {
            return `${ prefix }<span class="show-int-cnt">${ value }</span><span>${ key }</span><br>`
        }
        const region = `${ prefix }<i>${ key }</i><br>`;
        return [ region, getIntRegionCounts( value, depth + 1 ) ];
    } ).join( '' );
}
function getIndentation ( lvl: number ): string {
    if ( !lvl ) return '';
    const indent = '&emsp;'.repeat( lvl );
    return `${ indent }${ String.fromCharCode( 8627 ) }&nbsp`;
}
