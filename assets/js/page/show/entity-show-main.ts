/**
 * Builds and displays entity-data for the entity's show-page.
 */
import { startNewLoadingAnimation } from '@elems';
import { parseData } from '@localdata';
import { sendAjaxQuery } from '@util';
import { displayEntityData } from "./display-elems";
import {
    getEntityDisplayData,
    ShowEntityName,
    InteractionOverviewResponse,
    TaxonOverviewResponse
} from "./format-data";

initShowPage();

function initShowPage (): void {
    require( 'styles/pages/entity-show.styl' );
    handleEntityDataDisplay();
    addCsvDownloadButton();
}

function handleEntityDataDisplay (): void {
    const spinner = startNewLoadingAnimation( '#detail-block' );
    downloadAndDisplayEntityData()
        .then( () => spinner.stop() );
}

function downloadAndDisplayEntityData (): JQuery.Promise<ApiOverviewResponse> {
    const [ entity, id ] = getEntityFromUrl();
    const url = `api/v1/${ entity }/${ id }/overview`;
    const onSuccess = handleEntityDisplay.bind( null, entity, Number( id ), );
    return sendAjaxQuery( null, url, onSuccess, undefined, 'GET' );
}

function getEntityFromUrl (): [ entity: ShowEntityName, url: string ] {
    const URL = $( 'body' ).data( 'this-url' ).split( '/' );
    const id = URL.pop();
    const entity = URL.pop();
    return [ entity, id ];
}

type ApiOverviewResponse = InteractionOverviewResponse | TaxonOverviewResponse;

function handleEntityDisplay ( name: ShowEntityName, id: number, data: ApiOverviewResponse ): void {
    const displayConfig = getEntityDisplayData( name, id, parseData( data ) );
    displayEntityData( displayConfig );
}

/* ======================== CSV DOWNLOAD ==================================== */
function addCsvDownloadButton (): void {
    const attrs = {
        id: 'entity-csv',
        name: 'csv',
        text: 'CSV',
        title: 'Visit Explore page for CSV download. CSV Download Coming Soon.',
    };
    $( '#headline-right' ).append( $( '<button/>', attrs ) );
}
