import { initPopupOverlay } from './elems/misc/popup-util';
import { initPrototypeExtensions } from './utils/misc-util';

export function initUtil () {
    initPrototypeExtensions();
    initPopupOverlay();
}