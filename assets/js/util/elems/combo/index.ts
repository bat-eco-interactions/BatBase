
/* ==== COMBOBOX ==== */
export {
    /* SET */
    addOpt,
    /* --- UTIL --- */
    destroySelectizeInstance,
    enableCombobox,
    enableComboboxes,
    enableFirstCombobox,
    focusCombobox,
    /* --- OPTIONS --- */
    /* GET */
    getOptionTextForValue,
    getOptionTotal,
    getOptionValueForText,
    getSelTxt,
    /* --- SELECTED --- */
    /* GET */
    getSelVal,
    /* --- INIT --- */
    initCombobox,
    isComboActive,
    removeOptions,
    replaceSelOpts,
    resetCombobox,
    /* SET */
    setSelVal,
    triggerComboChangeReturnPromise,
    updateComboChangeEvent,
    /* --- PLACEHOLDER --- */
    updatePlaceholderText,
} from './combobox-main';

export {
    ComboboxConfig,
} from './Combobox'

/* ==== OPTIONS ==== */
export {
    /* --- UTIL --- */
    alphabetizeOpts,
    /* --- ENTITY --- */
    getCitationTypeOptsForPublicationType,
    getComboEntityDisplayName,
    getGroupRootOpts,
    getTaxonGroupFieldOpts,
    /* --- FIELDS --- */
    getFieldOptions,
    getOptions,
    getOptsFromStoredData,
    getOptsFromStringArray,
    getRcrdOpts,
    getDetailRecordOpts,
    getTaxonOpts,
    initOptsWithCreate,
} from './get-options';
