export {
    hasEdits,
    removeInvalidatedData,
} from './rmv-config';