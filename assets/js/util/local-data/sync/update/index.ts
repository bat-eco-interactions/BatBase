/* --------------------------- EXECUTE -------------------------------------- */
export {
    clearFailedMemory,
    ifFailuresSendReport,
    ifFailuresReportAndAddToReturnData,
    retryFailedUpdates,
    retryIssuesAndReportFailures,
} from './execute-update';
/* --------------------------- ADD ------------------------------------------ */
export {
    addCoreEntityData,
    addDetailEntityData,
} from './add';
/* -------------------------- REMOVE ---------------------------------------- */
export {
    hasEdits,
    removeInvalidatedData,
} from './rmv';