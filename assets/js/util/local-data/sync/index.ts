/* ====================== DATA ENTRY ======================================== */
export {
    DataEntryResults,
    EditObj,
    EditedIds,
    FormReturnData,
    handleLocalDataUpdate,
    syncLocalDataAfterDataEntry,
    updateUserNamedList,
} from './data-entry';
/* ------------------------ REVIEW ------------------------------------------ */
export {
    getParsedReviewEntry,
    syncReviewEntries,
    updateReviewEntryStorage,
} from './review';
/* ====================== PAGE LOAD ========================================= */
export {
    syncLocalDbWithServer,
} from './db-pg-load';
/* ========================= UPDATE ========================================= */
export {
    /* --- EXECUTE --- */
    clearFailedMemory,
    ifFailuresSendReport,
    ifFailuresReportAndAddToReturnData,
    retryIssuesAndReportFailures,
    /* --- ADD --- */
    addCoreEntityData,
    addDetailEntityData,
    /* --- REMOVE --- */
    hasEdits,
    removeInvalidatedData,
} from './update';