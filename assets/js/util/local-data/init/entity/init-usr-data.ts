/**
 * Modifies and sorts user-data for local storage.
 * - savedFilters - an object with each filter's id (k) and data (v)
 * - savedFiltersNames - an object with each filter's name(k) and { id, focus-view }
 * - dataLists - an object with each list's id (k) and data (v)
 * - dataListNames - an object with each list's name (k) and id (v).
 * - user - current user's data or { name & role (k) 'visitor' (v) }
 *
 * - editorNames - an object with each editor's name(k) and id(v)
 *
 * Export
 *     getFilterListGroupString
 *     modifyUsrDataForLocalDb
 *
 * TOC
 *     CUSTOM USER LIST AND FILTER DATA
 *     USER ENTITY
 */
import { removeData, storeData } from '@localdata/util';
import * as _t from '@types';
import { getNameObj } from '../init-helpers';
/* ====================== CUSTOM USER LIST AND FILTER DATA ================== */
type UserServerData = {
    lists: _t.EntityRecord[];
    users: _t.EntityRecords;
};
export function modifyUsrDataForLocalDb ( data: UserServerData ): void {
    if ( !data.lists || !data.users ) return console.error( 'Invalid user data downloaded' );
    storeUserNamedData( data.lists as _t.EntityRecord[] );
    storeData( 'editorNames', getEditors( data.users ) );
    setCurrentUserData( Object.values( data.users ) );
}
function storeUserNamedData ( lists: _t.EntityRecord[] ): void {
    const filters: _t.EntityRecords = {};
    const filterIds: number[] = [];
    const intLists: _t.EntityRecords = {};
    const intListIds: number[] = [];

    lists.forEach( addToDataObjs );
    storeData( 'savedFilters', filters );
    storeData( 'savedFilterNames', getFilterOptionGroupObj( filterIds, filters ) );
    storeData( 'dataLists', intLists );
    storeData( 'dataListNames', getNameObj( intListIds, intLists ) );
    removeData( 'list' );

    function addToDataObjs ( l: _t.EntityRecord ): void {
        const entities = l.type == 'filter' ? filters : intLists;
        const idAry = l.type == 'filter' ? filterIds : intListIds;
        entities[ l.id ] = l;
        idAry.push( l.id );
    }
}
function getFilterOptionGroupObj (
    ids: number[],
    filters: _t.EntityRecords
): { [name: string]: { value: number; group: string; }; } {
    const data: { [name: string]: { value: number, group: string; }; } = {};
    ids.forEach( buildOptObj );
    return data;

    function buildOptObj ( id: number ): void {
        const filter = filters[ id ];
        if ( filter?.displayName ) {
            data[ filter.displayName ] = {
                value: id, group: getFilterListGroupString( filter )
            };
        } else {
            console.error( 'Filter [%s] error', id );
        }
    }
}
export function getFilterListGroupString ( list: _t.EntityRecord ): string {
    list.details = JSON.parse( list.details );
    const map = {
        srcs: 'Source',
        locs: 'Location',
        taxa: 'Taxon',
    } as const;
    const view = list.details.view ? list.details.view.text : false;
    const focus = map[ list.details.focus as keyof typeof map ];
    return !view ? focus : `${ focus } - ${ view }`;
}
/* =========================== USER ENTITY ================================== */

/**
 * In order to handle user-impersonation, the UI user is the user local data uses
 */
function setCurrentUserData ( users: _t.EntityRecord[] ): void {
    const name = $( 'body' ).data( 'user-name' );
    const user = !name ? getVisitor() : users.find( u => u.username === name );
    if ( user ){
        storeData( 'user', user );
    } else {
        console.error( 'Could not find user data for [%s]', name );
    }
}
function getVisitor () {
    return { username: null, role: 'visitor' };
}
function getEditors ( users: _t.EntityRecords ): _t.IdsByName {
    const editorIds = _t.objectKeys( users ).filter( isEditor );
    const data: _t.IdsByName = {};
    editorIds.forEach( ( id: number ) => {
        const username: string = users[ id ]?.fullname;
        data[ username ] = id;
    } );
    return data;

    function isEditor ( id: number ): boolean {
        return users[ id ]?.role == 'editor';
    }
}