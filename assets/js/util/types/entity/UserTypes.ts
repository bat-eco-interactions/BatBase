import { OptionObject } from "../types-main";
/* ========================= USER =========================================== */
export type User = {
    id?: number;
    fullName?: string;
    role: Role,
    username: string;
};
export type UserRole =
    | 'super'
    | 'admin'
    | 'manager'
    | 'editor'
    | 'contributor'
    | 'user';
export type Role = UserRole | 'visitor';
/* ==================== USER-NAMED LIST ===================================== */
export type UserNamedList = {
    id: number;
    user: number;
    type: 'interaction' | 'filter';
    displayName: string;
    description: string;
    details: FilterSet | InteractionList;
};

export type FilterSet = {
    direct: object;
    focus: string;
    rebuild: object;
    table: object;
    view: OptionObject;
}
export type InteractionList = number[];