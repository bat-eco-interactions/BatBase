/** Tracks last updated times for database entities */
export type EntityStateDates = { [entity: string]: string; };