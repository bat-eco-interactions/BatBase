import { EntityBones } from "../types-main";
/* ========================= INTERACTION ==================================== */
export type Interaction = {
    id: number;
    interactionType: EntityBones<InteractionTypeName>;
    location: number;
    objGroupId: string;
    object: number;
    serverUpdatedAt: string;
    source: number;
    subjGroupId: string;
    subject: number;
    tags: InteractionTag[];
    updatedBy: string;
}
export type InteractionType = {
    id: number;
    displayName: InteractionTypeName;
    activeForm: InteractionTypeMap[InteractionTypeName];
}
type InteractionTypeMap = {
    'Seed Dispersal': 'dispersed seeds of',
    Consumption: 'consumed',
    Pollination: 'pollinated',
    Visitation: 'visited',
    Transport: 'transported',
    Roost: 'roosted in',
    Host: 'host of',
    Cohabitation: 'cohabitated with',
    Prey: 'preyed upon by',
    Hematophagy: 'fed on the blood of',
    Predation: 'preyed upon'
  };

export type InteractionTypeName = keyof InteractionTypeMap;
/* ====================== VALID INTERACTION ================================= */
export type ValidInteraction =  {
    interactionType: number;
    objectGroupRoot: number;
    subjectGroupRoot: number;
    tagRequired: boolean;
    tags: {
        [key: number]: ValidTag;
    };
}
export type ValidTag = {
    id: number;
    displayName: string;
    isRequired: boolean;
}
/* ============================ TAG ========================================= */
export type Tag = {
    displayName: string;
    entity: string;
    id: number;
    interactions: number[];
    sources: number[];
    type: 'interaction' | 'source';
}
export type InteractionTag = {
    id: number;
    displayName: string;
    type: 'interaction type' | 'source';
}
