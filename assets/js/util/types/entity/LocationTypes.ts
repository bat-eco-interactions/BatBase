import { EntityBones } from "../types-main";
/* ============================ LOCATION ==================================== */
export interface Location {
    children: number[];
    country: EntityBones;
    description?: string;
    displayName: string;
    elevationMax?: number;
    elevation?: number;
    geoJsonId: number;
    habitatType: EntityBones<HabitatTypeName>;
    id: number;
    interactions: number[];
    latitude?: number;
    locationType: EntityBones<LocationTypeName>;
    longitude?: number;
    parent: number;
    region: EntityBones;
    serverUpdatedAt: string;
    totalInts: number;
    updatedBy: string;
}
type LocationTypeName =
    | 'Region'
    | 'Country'
    | 'Habitat'
    | 'Area'
    | 'Point'
    | 'City';
/* ========================== HABITAT TYPE ================================== */
export type HabitatType = {
    displayName: string;
    id: number;
    locations: number[];
    slug: string;
}
type HabitatTypeName =
    | 'Savanna'
    | 'Desert'
    | 'Shrubland'
    | 'Captivity'
    | 'Artificial Landscape'
    | 'Grassland'
    | 'Wetlands'
    | 'Rocky Areas'
    | 'Caves and Subterranean Areas';
/* ============================ GEOJSON ===================================== */
export type GeoJson = {
    coordinates: string;
    displayPoint: string;
    id: number;
    serverUpdatedAt: string;
    type: string;
}