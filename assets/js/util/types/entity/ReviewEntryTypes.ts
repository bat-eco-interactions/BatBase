import { FieldServerConfig, FormAction, FormGroup } from "@dataentry/model";
import { DataEntryResults } from '@localdata/sync';
import { EntityBones } from "../types-main";
/* ======================== REVIEW ENTRY ==================================== */
export type ReviewEntry = {
    createdBy: EntityBones;
    completed?: true;   //Only used in client-side local-storage for the review-process
    form: ReviewFormData;
    entity: string;
    entityId?: number;  //Set when entry is connected to fully approved data (eg, rejected for a duplicate)
    id: number;
    log: string;
    managedBy: EntityBones;
    payload: ReviewEntryPayload;    //Entry formatted to be added to local storage, quarantined to its contributor
    serverUpdatedAt: string;
    stage: { id: number, name: PassiveForm };
}
export type ReviewFormData = {
    action: FormAction;
    count?: number;
    editId?: number;
    fields: {
        [key: string]: ReviewField;
    };
    group: FormGroup;
    id?: number | null;
    stage: { name: ActiveForm };
}
export interface ReviewField {
    notes?: string;
    prop?: FieldServerConfig['prop'];
    replacedReview?: QuarantinedReviewEntryBones;
    review?: QuarantinedReviewEntryBones;
    value: any;
}
export interface MultiReviewField extends Omit<ReviewField, 'replacedReview' | 'review'> {
    replacedReview?: Record<number, QuarantinedReviewEntryBones>;
    review: Record<number, QuarantinedReviewEntryBones>;
}
//todo: when does the below get stage added and become r.ReviewEntryBones ?
export type QuarantinedReviewEntryBones = {
    id: number;
    entity: string;
    qId: number;
}
export type ReviewEntryBones = {
    id: number;
    stage: ReviewStage['passiveForm']
}
export interface ReviewEntryPayload extends Omit<DataEntryResults, 'coreEntity' | 'detailEntity'>  {
    coreEntity: DataEntryResults['coreEntity'] & { review: ReviewEntryBones },
    coreId: number;        //Added during quarantined-data init
    detailEntity?: DataEntryResults['detailEntity'] & { review: ReviewEntryBones },
    detailId?: number;     //Added during quarantined-data init
    review: ReviewEntryBones;
}

export const isReviewEntry = ( rEntry: any ): rEntry is ReviewEntry => rEntry && rEntry.form;

/* ========================== REVIEW STAGE ================================== */
export type ReviewStage = {
    id: number;
    passiveForm: PassiveForm;
    activeForm: ActiveForm;
    description: string;
};
type PassiveForm =
    | 'Pending'
    | 'Locked'
    | 'Returned'
    | 'Held'
    | 'Rejected'
    | 'Approved'
    | 'Completed';

type ActiveForm =
    | 'Quarantine'
    | 'Lock'
    | 'Return'
    | 'Pause'
    | 'Reject'
    | 'Approve'
    | 'Complete';
