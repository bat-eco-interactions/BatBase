import { EntityBones } from "../types-main";
/* =========================== SOURCE ======================================= */
export interface Source {
    children: number[];
    citation?: number;
    contributions: number[];
    contributors: { [key: number]: Contributor };
    displayName: string;
    doi?: string;
    id: number;
    interactions: number[];
    linkUrl?: string;
    publication?: number;
    publisher?: number;
    serverUpdatedAt: string;
    sourceType: EntityBones<SourceType>;
    tags: number[];
    updatedBy: string;
}
export type SourceType =
    | 'Publisher'
    | 'Publication'
    | 'Author'
    | 'Editor'
    | 'Citation';

/* =========================== AUTHOR ======================================= */
export interface AuthorSource extends Source {
    author: number;
}
export type Author = {
    displayName: string;
    firstName: string;
    fullName: string;
    id: number;
    lastName: string;
    middleName: string;
    serverUpdatedAt: string;
    source: number;
}
export type Contributor = {
    authId: number;
    contribId: number;
    isEditor: boolean;
    ord: number;
}
export type AuthorType = 'Author' | 'Editor';
/* ========================== CITATION ====================================== */
export interface CitationSource extends Source {
    authors: { [key: number]: string;}
    citation: number;
    contributors: { [key: number]: Contributor};
    isDirect?: true;
    parent: number;
    year: string;
}
export type Citation = {
    abstract?: string;
    citationType: EntityBones<CitationType>;
    displayName: string;
    fullText: string;
    id: number;
    publicationIssue: string;
    publicationPages: string;
    publicationVolume: string;
    serverUpdatedAt: string;
    source: number;
    title: string;
}
export type CitationType =
    | 'Article'
    | 'Chapter'
    | 'Book'
    | 'Museum record'
    | "Master's Thesis"
    | 'Report'
    | 'Other'
    | 'Ph.D. Dissertation';
/* ========================= PUBLICATION ==================================== */
export interface PublicationSource extends Source {
    authors?: { [key: number]: string;}
    editors?: { [key: number]: string;}
    parent?: number;
    publication: number;
    year?: string;
}
export type Publication = {
    displayName: string;
    id: number;
    publicationType: EntityBones<PublicationType>;
    serverUpdatedAt: string;
    source: number;
}
export type PublicationType =
    | 'Other'
    | 'Book'
    | 'Journal'
    | 'Thesis/Dissertation';
/* ========================== PUBLISHER ===================================== */
export interface PublisherSource extends Source {
    publisher: number;
}
export type Publisher = {
    city: string;
    country: string;
    displayName: string;
    id: number;
    serverUpdatedAt: string;
    source: number;
}
