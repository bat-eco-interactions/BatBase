import { EntityBones } from "../types-main";
/* ======================== TAXON =========================================== */
export type Taxon = {
    children: number[];
    displayName: string;
    id: number;
    isRoot: boolean;
    name: string;
    objectRoles: number[];
    parent?: number;
    rank: EntityBones<RankName>;
    serverUpdatedAt: string;
    subjectRoles: number[];
    updatedBy: string;
};
export type ApiTaxon = {
    childTaxa: string[]; //iri
    displayName: string;
    group: Group;
    id: number;
    isRoot: boolean;
    name: string;
    objectRoles: string[]; //iri
    parentTaxon?: string; //iri
    rank: EntityBones<RankName>;
    serverUpdatedAt: string;
    subjectRoles: string[]; //iri
    updatedBy: string;
};
/* ======================== GROUP =========================================== */
export type Group = {
    displayName: string;
    id: number;
    pluralName: string;
    roots: {
        [key: number]: GroupRoot;
    };
};
export type GroupRoot = {
    id: number;
    name: string;
    subRanks: RankName[];
    taxon: number;
};
/* ========================= RANK =========================================== */
export type Rank = {
    id: number;
    displayName: RankName;
    ordinal: number;
    pluralName: string;
};
export type RankName =
    | "Domain"
    | "Kingdom"
    | "Phylum"
    | "Class"
    | "Order"
    | "Family"
    | "Genus"
    | "Species";

export type OrderedRanks = [
    "Domain",
    "Kingdom",
    "Phylum",
    "Class",
    "Order",
    "Family",
    "Genus",
    "Species"
];
