/**
 * Loads the page the feedback was submitted from. Shows a popup with the submitted
 * feedback data and the admin fields to manage the feedback.
 *
 * TOC
 * 	  SHOW CONTEXT PAGE
 *    POPUP
 *    	SUBMITTED FEEDBACK
 *    		SUBMITTED BY
 *    		TOPIC
 *    		FEEDBACK TEXT
 *      ADMIN RESPONSE-FIELDS
 *      	FEEDBACK ASSIGNED TO
 *      	FEEDBACK STATUS
 *      	ADMIN NOTES
 *      POPUP FOOTER
 *      BUILD AND APPEND POPUP
 *   HANDLE SUBMIT
 *   	TOGGLE SUBMIT
 *   	SUBMIT UPDATE
 *   CLOSE POPUP
 */
import * as el from '@elems';
import { OptionObject } from '@types';
import { sendAjaxQuery, ucfirst } from '@util';

let submitEnabled = false;
let feedbackId: number;

$( '.open-feedback' ).each( ( i, el ) => { el.addEventListener( 'click', showContextPage ); } );
/* ========================= SHOW CONTEXT PAGE ============================== */
/** Shows the page the feedback was submitted from. */
function showContextPage ( ev: Event ): void {
    ev.preventDefault();
    if ( !ev.target ) return;  // Should always be present
    feedbackId = $( ev.target ).data( 'id' );
    sendAjaxQuery( null, `feedback/load/${ feedbackId }`, feedbackEntryReceived );
    clearBodyAndBuildIframe( $( ev.target ).data( 'url' ) );
}
function clearBodyAndBuildIframe ( pageUrl: string ): void {
    const $iframe = $( '<iframe id="feedback-context-frame"></iframe>' );
    $( 'body' ).empty();
    $( 'html' ).css( { 'overflow-y': 'hidden' } );
    $( 'body' ).append( $iframe );
    $( '#feedback-context-frame' ).attr( 'src', pageUrl );
}
type User = {
    email: string;
    id: number;
    name: string;
};
type Feedback = {
    assigned: User | Partial<User>;
    content: string;
    from: User;
    id: string;
    notes: string | null;
    status: number;
    submitted: { date: string, timezone_type: number, timezone: string; };
    topic: string;
    users: User[];
};
function feedbackEntryReceived ( data: { feedback: Feedback; }, _: string, _2: object ): void {
    createFeedbackResponsePopup( data.feedback );
}
/* ============================= POPUP ====================================== */
function createFeedbackResponsePopup ( feedback: Feedback ): void {
    const data = getEditableFeedbackData( feedback );
    const elems = [
        ...getSubmittedFeedbackElems( feedback ),
        document.createElement( 'hr' ),
        ...getAdminResponseFields( feedback, data ),
        getFeedbackResponseFooter()
    ];
    buildAndShowResponsePopup( elems );
}
type EditableFeedback = {
    status: number;
    assignedUser: number;
    notes: string;
};
function getEditableFeedbackData ( feedback: Feedback ): EditableFeedback {
    const userId: number = feedback.assigned.id ?? 0;
    return {
        assignedUser: userId,
        notes: feedback.notes ?? '',
        status: feedback.status,
    };
}
/* ------------------------ FIELD HELPERS ----------------------------------- */
function getMultiFieldRow ( fields: HTMLElement[] ): HTMLElement {
    const row = el.getElem( 'div', { class: 'flex-row' } );
    $( row ).append( fields );
    return row;
}
function getFeedbackFieldConfig ( name: string, label?: string ): el.FieldConfig {
    return {
        flow: 'row',
        label: label ? label : `${ ucfirst( name ) }:`,
        name: name,
    };
}
/* ----------------------- SUBMITTED FEEDBACK ------------------------------- */
function getSubmittedFeedbackElems ( feedback: Feedback ): HTMLElement[] {
    return [
        getDisplayElems( 'fUser', feedback.from.name, 'User:' ),
        getDisplayElems( 'fDate', formatDate( feedback.submitted.date ), 'Date:' ),
        getDisplayElems( 'topic', feedback.topic ),
        getDisplayElems( 'feedback', feedback.content ),
    ];
}
function getDisplayElems ( name: string, dText: string, label?: string ): HTMLElement {
    const config = getFeedbackFieldConfig( name, label );
    const displayText = el.getElem( 'span', { text: dText } );
    return el.getFieldElems( config, displayText );
}
function formatDate ( dateStr: string ): string {
    const date = new Date( dateStr );
    return date.getDateTimeSentence();
}
/* -------------------- ADMIN RESPONSE-FIELDS ------------------------------- */
function getAdminResponseFields ( feedback: Feedback, data: EditableFeedback ): HTMLElement[] {
    return [
        getTopAdminResponseRow( feedback, data ),
        getAdminNotesElem( feedback.notes ?? '' )
    ];
}
function getTopAdminResponseRow ( feedback: Feedback, data: EditableFeedback ): HTMLElement {
    const fields = [
        getAssignedUserField( feedback.users, data.assignedUser ),
        getFeedbackStatusElem( feedback.status )
    ];
    return getMultiFieldRow( fields );
}
/* ____________________ FEEDBACK ASSIGNED TO ________________________________ */
function getAssignedUserField ( users: User[], assignedId: number ): HTMLElement {
    const config = getFeedbackFieldConfig( 'fAssigned', 'Assigned to:' );
    const select = buildUserSelect( users, assignedId );
    return el.getFieldElems( config, select );
}
function buildUserSelect( users: User[], assignedId: number ): HTMLSelectElement {
    const attrs = { id: 'sel-assignedUser', onChange: onDataChange, value: assignedId };
    const select = el.getElem<HTMLSelectElement>( 'select', attrs, getUserOpts( users ) );
    $( select ).data( 'original', assignedId );
    return select;
}
function getUserOpts ( users: User[] ): OptionObject[] {
    const opts = [ { text: '- None - ', value: '0' } ];
    users.forEach( user => opts.push( { text: user.name, value: user.id.toString() } ) );
    return opts;
}
/* ________________________ FEEDBACK STATUS _________________________________ */
function getFeedbackStatusElem ( curStatus: number ): HTMLElement {
    const config = getFeedbackFieldConfig( 'fStatus', 'Status:' );
    const select = getStatusSelect( curStatus );
    return el.getFieldElems( config, select );
}
function getStatusSelect( curStatus: number ):HTMLSelectElement {
    const attrs = { id: 'sel-feedbackStatus', onChange: onDataChange, value: curStatus };
    const select = el.getElem<HTMLSelectElement>( 'select', attrs, getStatusOpts() );
    $( select ).data( 'original', curStatus );
    return select;
}
function getStatusOpts (): OptionObject[] {
    const statuses = [ 'Closed', 'Follow-Up', 'Read', 'Unread' ];
    return statuses.map( ( status, idx ) => { return { text: status, value: idx.toString() }; } );
}
/* _________________________ ADMIN NOTES ____________________________________ */
function getAdminNotesElem ( notes: string ): HTMLElement {
    const config = getFeedbackFieldConfig( 'fNotes', 'Notes:' );
    const input = buildAdminNotesTextarea( notes );
    return el.getFieldElems( config, input );
}
function buildAdminNotesTextarea ( notes: string ): HTMLTextAreaElement {
    const attr = {
        id: 'feedback-notes',
        placeholder: 'Add notes about this feedback here...',
        text: notes
    };
    const input = el.getElem<HTMLTextAreaElement>( 'textarea', attr );
    $( input ).data( 'original', notes ).keyup( onDataChange );
    return input;
}
/* ------------------------ POPUP FOOTER ------------------------------------ */
function getFeedbackResponseFooter (): HTMLElement {
    const config = {
        action: 'edit',
        formName: 'Feedback',
        onSubmit: updateFeedback,
        onCancel: closePopup
    };
    return el.getFormFooter( config );
}
/* -------------------- BUILD AND APPEND POPUP ------------------------------ */
function buildAndShowResponsePopup ( elems: HTMLElement[] ): void {
    const $popup = getFeedbackResponsePopup();
    $( 'body' ).prepend( $popup );
    $popup.append( elems ).fadeIn( "fast", initFeedbackCombos );
}
function getFeedbackResponsePopup (): JQuery<Element> {
    const $popup = $( el.getElem( 'div', { id: 'feedback-popup' } ) );
    $popup.css( {
        display: 'none'
    } );
    return $popup;
}
function initFeedbackCombos() {
    initAssignedToCombo();
    initStatusCombo();
}
function initAssignedToCombo() {
    const config = {
        id: '#sel-assignedUser',
        onChange: onDataChange,
        name: 'FeedbackAssignedTo'
    };
    el.initCombobox( config );
}
function initStatusCombo() {
    const config = {
        id: '#sel-feedbackStatus',
        onChange: onDataChange,
        name: 'FeedbackStatus'
    };
    el.initCombobox( config );
}
/* ========================= HANDLE SUBMIT ================================== */
function onDataChange (): void {
    if ( !submitEnabled && hasChangedData() ) {
        toggleFeedbackSubmitButton();
    } else if ( submitEnabled && !hasChangedData() ) {
        toggleFeedbackSubmitButton( false );
    }
}
function hasChangedData (): string | boolean {
    const fields = [ '#sel-feedbackStatus', '#sel-assignedUser', '#feedback-notes' ];
    return fields.find( fieldHasChanges ) || false;
}
function fieldHasChanges ( field: string ): boolean {
    return $( field ).val() !== $( field ).data( 'original' );
}
/* ----------------------------- TOGGLE SUBMIT ------------------------------ */
function toggleFeedbackSubmitButton ( enable = true ): void {
    const opac = enable ? 1 : .35;
    submitEnabled = enable;
    $( '#Feedback-submit' ).fadeTo( 'fast', opac ).attr( { 'disabled': !enable } );
}
/* ---------------------------- SUBMIT UPDATE ------------------------------- */
function updateFeedback (): void {
    const url = 'feedback/update/' + feedbackId;
    const userId = $( '#sel-assignedUser' ).val();
    const data = {
        assignedUserId: userId === 0 ? null : userId,
        adminNotes: $( '#feedback-notes' ).val(),
        status: $( '#sel-feedbackStatus' ).val()
    };
    sendAjaxQuery( data, url, () => closePopup() );
}
/* =========================== CLOSE POPUP ================================== */
function closePopup (): void {
    $( '#feedback-popup' ).fadeOut( 'slow', () => document.location.reload() );
    //todo: destroy combos
}