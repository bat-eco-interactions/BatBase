/* ========= DEV ======== */
// const CircularDependencyPlugin = require( 'circular-dependency-plugin' );
/* ========= PROD ======= */
const { sentryWebpackPlugin } = require( "@sentry/webpack-plugin" );
/* ======== ALL =========== */
const TSConfigPathsPlugin = require( 'tsconfig-paths-webpack-plugin' );
const Encore = require( '@symfony/webpack-encore' );
// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if ( !Encore.isRuntimeEnvironmentConfigured() ) {
    // eslint-disable-next-line
    Encore.configureRuntimeEnvironment( process.env.NODE_ENV || 'dev' );
}

// if ( !Encore.isProduction() ) {
//     Encore.addPlugin( new CircularDependencyPlugin( {
//             // exclude detection of files based on a RegExp
//             exclude: /a\.js|node_modules/,
//             // add errors to webpack instead of warnings
//             // failOnError: true,
//             // set the current working directory for displaying module paths
//             cwd: process.cwd(),
//         } ), );
// }
/** ======================== DEV SERVER ================================== */
if ( Encore.isDevServer() || Encore.isProduction() ) {
    Encore
        .setPublicPath( '/build' );
    if ( Encore.isDevServer() ) {
        Encore
            .configureDevServerOptions( options => {
                options.liveReload = true; //HMR works automatically with CSS but only works with some JavaScript (like Vue.js).
                options.static = { //required to disable the default reloading of files from the static directory
                    watch: false
                };
                options.watchFiles = { //live reload for your PHP code and templates
                    paths: [ 'src/**/*.php', 'templates/**/*' ],
                };
                // options.host = '0.0.0.0'; //Doesn't work from here. Use cli params
                // options.port = 9999;  //Doesn't work from here. Use cli params
            } )
            // will be applied for `encore dev --watch` and `encore dev-server` commands
            .configureWatchOptions( watchOptions => {
                watchOptions.poll = 250; // check for changes every 250 milliseconds
                // watchOptions.aggregateTimeout = 300;
                watchOptions.ignored = [
                    './var/',
                    './node_modules/',
                    './vendor/',
                    './cache/',
                ];
            } )
    }
} else {
    Encore
        .setPublicPath( '/BatBase/public_html/build' ) //APACHE
        // show OS notifications when builds finish/fail
        .enableBuildNotifications( true, ( options ) => {
            options.alwaysNotify = true;
        } );
}
/** ======================== SERVER CONFIGURATION ================================== */
if ( Encore.isProduction() ) {
    // Encore
    //     /* PUSH TO MASTER FIRST. Sends source maps to Sentry for bug/issue tracking. */
    //     .addPlugin( sentryWebpackPlugin( {
    //         // include: '.', test: [/\.js$/],
    //         release: {
    //             name: '20250103_BB',
    //         },
    //         authToken: 'change-me',
    //         include: '.',
    //         test: [ /\.js$/ ],
    //         project: 'bat-base',
    //         org: 'eco-interactions-bat',
    //         debug: true,
    //         ignore: [ 'web', 'node_modules', 'webpack.config.js',
    //             'vendor', '/assets/js/libs/*', '/assets/libs/*', 'var', 'features' ],
    //     } ) )
}
/** ======================== BASIC CONFIGURATION ================================== */
Encore
    // directory where compiled assets will be stored
    .setOutputPath( 'public_html/build' )
    // only needed for CDN's or sub-directory deploy
    // .setManifestKeyPrefix('build')
    // enable source maps
    .enableSourceMaps( true )
    // empty the outputPath dir before each build
    // .cleanupOutputBeforeBuild()
    //CIRCLE BACK TO FIXING WHY EXCLUDE DOESN'T EXIST IN THE PLUGIN, AND THUS DOES NOTHING, AFTER UPDATING EVERYTHING
    // .cleanupOutputBeforeBuild(['public_html/build/'], (options) => {
    //     options.verbose = true;
    //     // options.root = __dirname;
    //     // options.exclude = ['images', 'files', 'assets'];
    // })
    //{"verbose":false,"cleanOnceBeforeBuildPatterns":["**/*","!manifest.json"],"cleanStaleWebpackAssets":false}
    // .cleanupOutputBeforeBuild()
    // .cleanupOutputBeforeBuild(['build'], (options) => {
    //     // options.verbose = true;
    //     options.exclude = ['images'];
    //   })

    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning( Encore.isProduction() )
    // you can use this method to provide other common global variables,
    // such as '_' for the 'underscore' library
    .autoProvideVariables( {
        L: 'leaflet',
        Sentry: '@sentry/browser',
        $: 'jquery',
        jQuery: 'jquery' } )
    /** ------- Loaders ----------------- */
    .enableStylusLoader()
    .addLoader( {
        test: /\.(pdf)$/,
        use: [ {
            loader: 'file-loader',
            options: {
                name: '[name].[ext]',
                outputPath: './assets/files/'
            }
        } ]
    } )
    // eslint-disable-next-line no-unused-vars
    .enableTypeScriptLoader( function ( tsConfig ) { // https://github.com/TypeStrong/ts-loader/blob/master/README.md#loader-options
        // HANDLED IN TSCONFIG
    } )

    // optionally enable forked type script for faster builds
    // https://www.npmjs.com/package/fork-ts-checker-webpack-plugin
    // requires that you have a tsconfig.json file that is setup correctly.
    //.enableForkedTypeScriptTypesChecking()
    /** ------- Files to process ----------------- */
    .copyFiles( [ {
        from: './assets/images',
        to: 'images/[name].[ext]'
    }, {
        from: './assets/files',
        to: 'files/[name].[ext]'
    } ] )
    /** ------- Site Js/Style Entries ----------------- */
    .addEntry( 'app', './assets/js/main.ts' )
    // .addEntry( 'feedback-create', './assets/js/core/feedback/feedback.ts' )
    .addEntry( 'feedback-manage', './assets/js/core/feedback/feedback-viewer.ts' )
    .addEntry( 'pdfs', './assets/js/page/pdf/manage-pdfs.ts' )
    .addEntry( 'entity', './assets/js/page/show/entity-show-main.ts' )
    .addEntry( 'explore', './assets/js/page/explore/explore-main.js' )
    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()
    // When enabled, Webpack "splits" your files into smaller pieces for greater optimization.
    .splitEntryChunks()

    // uncomment to get integrity="..." attributes on your script & link tags
    // requires WebpackEncoreBundle 1.4 or higher
    .enableIntegrityHashes( Encore.isProduction() );
/** ======================== FINALIZE ======================================= */
const config = Encore.getWebpackConfig();

config.resolve.plugins = [ new TSConfigPathsPlugin( { configFile: 'assets/tsconfig.json' } ) ];

/* Force Webpack to display errors/warnings */
config.stats.errors = true;
config.stats.warnings = true;

module.exports = config;
