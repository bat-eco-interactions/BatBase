<?php

namespace App\Tests\Behat\contexts;

use Behat\Mink\Element\DocumentElement;
use Behat\Mink\Session;
use Behat\Testwork\Tester\Result\TestResult;
use Behat\Testwork\Tester\Result\ExceptionResult;
use Behat\Behat\Context\Context;
use Behat\MinkExtension\Context\RawMinkContext;
use Behat\Behat\Hook\Scope\AfterStepScope as AfterStepScope;
use PHPUnit\Framework\Assert as Assert;

/**
 * Context utility methods.
 *
 * TOC
 *    ALIASES
 *    SELECTORS
 *    UTILITY
 *    UI INTERACTIONS
 *    FORMS
 *        FIELDS
 *    ASSERTS
 *        MODAL
 *    COMBOBOX
 *        STANDARD
 *        DYNAMIC
 *        UTILITY
 *        ASSERTS
 */
class BaseContext extends RawMinkContext implements Context
{
    protected static bool $dbChanges;
    protected static Session $curUser;

    /**
     * @AfterStep
     * @param AfterStepScope $scope
     */
    public function printSmallStacktraceAfterFailure(AfterStepScope $scope): void
    {
        $testResult = $scope->getTestResult();
        if ($testResult->getResultCode() === TestResult::FAILED) {
            if ($testResult instanceof ExceptionResult && $testResult->hasException()) {
                print mb_substr($testResult->getException()->getTraceAsString(), 0, 1250);
            }
        }
    }
    /** ==================== ALIASES ======================================== */
    protected function execute($statement): void
    {
        $this->getUserSession()->executeScript($statement);
    }
    protected function evaluate($statement): mixed
    {
        return $this->getUserSession()->evaluateScript($statement);
    }
    protected function wait($statement, $time = 5000): bool
    {
        return $this->getUserSession()->wait($time, $statement);
    }
    protected function getMinkPage(): DocumentElement
    {
        return $this->getUserSession()->getPage();
    }
    protected function log($msg): void
    {
        fwrite(STDOUT, (string) $msg);
    }
    protected function getUserSession(): Session
    {

        return self::$curUser ?? $this->getSession();
    }
    /** ==================== SELECTORS ====================================== */
    protected function getAlertId($label): string
    {
        return '#' . $this->getFieldName($label) . '_alert';
    }
    protected function getComboId($label): string
    {
        return '#sel-' . $this->getFieldName($label);
    }
    protected function getInputSelector($label, $formLevel = false): string
    {
        $selector = $this->buildInputSelector($label);
        return $formLevel ? "#$formLevel-form $selector" : $selector;
    }
    protected function buildInputSelector($label): string
    {
        $map = [
            'Date' => '#int-date-start'
        ];
        if (array_key_exists($label, $map)) {
            return $map[$label];
        }
        return '#' . $this->getFieldName($label) . '_f .f-input';
    }
    protected function getFieldSelector($label): string
    {
        return $this->getOpenFormId() . ' ' . $this->getInputSelector($label);
    }
    protected function getFieldName($label): string
    {
        return str_replace(' ', '', $label);
    }
    protected function getNameFilter($entity): string
    {
        return 'input[name="name-' . $entity . '"]';
    }
    /** Returns the id for the deepest open form.  */
    protected function getOpenFormId(): string
    {
        $prefix = $this->getOpenFormPrefix();
        return '#' . $prefix . '-form';
    }
    protected function getOpenFormPrefix(): string
    {
        $forms = ['sub2', 'sub', 'top'];
        foreach ($forms as $prefix) {
            $selector = '#' . $prefix . '-form';
            $elem = $this->getMinkPage()->find('css', $selector);
            if ($elem !== null) {
                return $prefix;
            }
        }
        return 'top';
    }
    /** =================== UTILITY ========================================= */
    protected function spin($lambda, $errMsg, $wait = 5): bool | null
    {
        for ($i = 0; $i < $wait; $i++) {
            try {
                if ($lambda()) {
                    return true;
                }
            } catch (\Exception) {
                // do nothing
            }
            sleep(1);
        }
        if ($errMsg) {
            $this->iPutABreakpoint($errMsg);
        }
        return null;
    }
    protected function iPutABreakpoint($msg = null): void
    {
        if ($msg !== null) {
            $this->log("\n" . $msg . "\n");
        }
        $this->log("\033[s    \033[93m[Breakpoint] Press \033[1;93m[RETURN]\033[0;93m to continue...\033[0m");
        while (fgets(STDIN, 1024) == '') {
        }
        $this->log("\033[u");
    }
    /** ================ UI INTERACTIONS ==================================== */
    protected function clickOnPageElement($selector): void
    {
        $this->spin(function () use ($selector) {
            $elem = $this->getMinkPage()->find('css', $selector);
            if (!$elem) {
                return false;
            }
            $elem->click();
            return true;
        }, "Couldn't click on the [$selector] element.");
    }
    protected function pressTheButton($bttnText): void
    {
        $this->spin(function () use ($bttnText) {
            try {
                $this->getMinkPage()->pressButton($bttnText);
            } catch (\Exception) {
                return false;
            } finally {
                return true;
            }
        }, "Couldn't interact with button [$bttnText]");
    }
    protected function pressTheMapButton($selector): void
    {
        $this->wait("$('$selector').length");
        $bttn = $this->getMinkPage()->find('css', $selector);
        if ($bttn) {
            $bttn->click();
        }
    }
    /** ======================== FORMS ====================================== */
    protected function submitTheForm($text = null, $action = 'submit'): void
    {
        $selector = "#{$this->getOpenFormPrefix()}-$action";
        $bttnText = $text ?: $this->evaluate("$('$selector').val();");
        $this->wait("!$('$selector').prop('disabled');");
        $this->execute("$('$selector').trigger('click');");

        if ($bttnText === 'Update') {
            $this->wait('!$(".form-popup").length');
        }
        self::$dbChanges = true;
    }
    /** Check after submitting the Interaction Edit form. */
    protected function ensureThatFormClosed(): void
    {
        $this->spin(function () {
            try {
                $form = $this->getMinkPage()->find('css', '.form-popup');
                if ($form) {
                    $this->submitTheForm('Update');
                }
                return !$form;
            } catch (\Exception) {
                return false;
            } finally {
                return true;
            }
        }, "Form did not submit/close as expected.");
    }
    /* ------------------------------ FIELDS -------------------------------- */
    protected function getFieldTextOrValue($fieldId): string
    {
        $val = $this->getFieldInnerText($fieldId);
        if ($val === null || $val === "") {
            $val = $this->getFieldValue($fieldId);
        }                                                                       //$this->log("\n field [$fieldId] val [$val]\n");
        return $val;
    }
    protected function getFieldInnerText($fieldId): string | null
    {
        return $this->evaluate("$('$fieldId')[0].innerText;");
    }
    protected function getFieldValue($fieldId): mixed
    {
        return  $this->evaluate("$('$fieldId')[0].value;");
    }
    /** ======================= ASSERTS ===================================== */
    /**
     * TODO: Replace with spin and @ifContainsText
     */
    protected function handleContainsAssert($ndl, $hystk, $isIn, $msg): void
    {                                                                           //$this->log('Haystack = '.$hystk.', needle = '.$ndl);
        if ($isIn && !str_contains((string) $hystk, (string) $ndl) || !$isIn && str_contains((string) $hystk, (string) $ndl)) {
            $this->iPutABreakpoint($msg);
        }
    }
    protected function ifContainsText($hystk, $ndl): bool
    {
        return str_contains((string) $hystk, (string) $ndl);
    }
    protected function handleEqualAssert($first, $second, $isEq = true, $msg = null): void
    {
        $msg = $msg ?: "[$first] [$second] should have been " . ($isEq ? 'equal' : 'not equal');

        if ($isEq && $first != $second || !$isEq && $first == $second) {
            $this->iPutABreakpoint($msg);
        }
    }
    protected function handleNullAssert($elem, $isNull, $msg): void
    {
        if ($isNull && $elem != null || !$isNull && $elem == null) {
            $this->iPutABreakpoint($msg);
        }
    }
    /** ---------------------- MODAL ---------------------------------------- */
    /** TODO: CHECKS IF ANY MODAL IS OPEN. USE ANYWHERE MODALS ARE CHECKED */
    protected function ensureModalOpened(): void
    {
        $this->spin(function () {
            try {
                return $this->getMinkPage()->find('css', '.introjs-tooltiptext');
            } catch (\Exception) {
                return false;
            }
        }, "Modal did not open");
    }
    /* ======================= COMBOBOX ===================================== */
    /** ---------------------- STANDARD ------------------------------------- */
    public function removeTextFromCombobox($prop, $text): void
    {
        $selId = $this->getComboId($prop);
        $val = $this->getValueToSelect($selId, $text);
        if (is_array($val)) {
            $this->setMultiComboboxValues($selId, $val, 'remove');
        } else {
            $this->execute("$('$selId')[0].selectize.removeItem('$val');");
        }
    }
    /**
     * Selects a single value from a select combobox. If the combobox can have
     * multiple options selected, the passed value must be an array,
     */
    protected function selectComboValue($selId, $val): void
    {                                                                           //$this->log("\n  selectComboValue [$selId] -> [$val]");
        if (is_array($val)) {
            $this->setMultiComboboxValues($selId, $val);
        } else {
            $this->execute("$('$selId')[0].selectize.addItem('$val');");
        }
    }
    protected function selectTextInCombobox($field, $text): void
    {
        $selId = $this->getComboId($field);
        $this->selectTextInFieldCombobox($selId, $text);
    }
    protected function selectTextInFieldCombobox($selId, $text, $new = false): void
    {
        $val = $new ? 'create' : $this->getValueToSelect($selId, $text);        //$this->log("\n  selectTextInFieldCombobox new?[$new]");

//         if ($text && !$val) {
//             $this->iPutABreakpoint("Could not find value for [$text] in [$selId]");
//         }
                                                                               //$this->log("\n  selectTextInFieldCombobox [$selId] -> [$val]");
        $this->spin(function () use ($selId, $val) {
            $this->selectComboValue($selId, $val);
            return $val === 'create' || $this->isValSelected($selId, $val);
        }, "Could not select [$text] in [$selId]");
    }
    /** ---------------------- DYNAMIC -------------------------------------- */
    protected function blurNextDynamicCombobox($selId, $count): void
    {
        $selector = $selId . ++$count;
        $this->spin(function () use ($selector) {
            $this->execute("$('$selector')[0].selectize.blur();");
            return true;
        }, 'Unable to blur [$selector] combobox ');
    }
    protected function setMultiComboboxValues($selId, $val, $remove = false): void
    {
        $handler = $remove ? 'removeItem' : 'addItem';     //$this->log("\n  setting multiselect [$selId] -> [$val]");
        foreach ($val as $v) {
            $this->execute("$('$selId')[0].selectize.$handler('$v');");
        }
    }
    protected function removeMultiComboboxValues($selId, $val): void
    {                                                                           //$this->log("\n  setting multiselect [$selId] -> [$val]");
        foreach ($val as $v) {
            $this->execute("$('$selId')[0].selectize.addItem('$v');");
        }
    }
    /** ---------------------- UTILITY -------------------------------------- */
    protected function getValueToSelect($selId, $text): mixed
    {
        $opts = $this->getComboboxOptions($selId);
        if (str_contains((string) $text, '[')) {
            return $this->getArrayValues($text, $opts);
        }
        return $this->getValueForText($text, $opts);
    }
    protected function getArrayValues($text, $opts): array
    {                                                                          //$this->log("\ngetArrayValues = [$text]");
        $textAry = explode(', ', str_replace(['[', ']'], '', $text));
        return array_map(fn($t) => $this->getValueForText($t, $opts), $textAry);
    }
    protected function getValueForText($text, $opts): mixed
    {                                                                           //$this->log("\ngetValueForText = [$text]");
        foreach ($opts as $key => $optAry) {                                    //$this->log("\n      option text[".$optAry['text']."] value[".$optAry['value']."]");
            if ($optAry['text'] !== $text) {
                continue;
            }                                                                   //$this->log("\n  value = ".$optAry['value']);
            return $optAry['value'];
        }
        return null;
    }
    protected function getComboboxOptions($selId): array
    {
        $opts = [];
        $this->spin(function () use (&$opts, $selId) {
            $opts = $this->evaluate(
                "$('$selId').length ? $('$selId')[0].selectize.options : false;"
            );
            return !!$opts;
        }, "Could not find options for [$selId]");
        return $opts;
    }
    /** ---------------------- ASSERTS -------------------------------------- */
    protected function isValSelected($selId, $val): bool
    {
        $selected = $this->evaluate("$('$selId').val();");
//        $logVal = print_r($val, true);
//        $logSelected = print_r($selected, true);
//        $this->log("\n     val[$logVal] isSelected?[$logSelected]");

        if (is_array($selected)) { // Multi-select combos
            if (is_array($val)) {
                return count(array_intersect($val, $selected)) == count($val);
            }
            return in_array($val, $selected);
        } else {
            return $selected == $val;
        }
    }
    protected function iSeeOnPage($text): void
    {
        $this->spin(function () use ($text) {
            $actual = $this->getMinkPage()->getText();
            $actual = preg_replace('/\s+/u', ' ', $actual);
            $regex = '/' . preg_quote((string) $text, '/') . '/ui';
            return preg_match($regex, $actual);
        }, "Did not find [$text] anywhere on page.");
    }
}
