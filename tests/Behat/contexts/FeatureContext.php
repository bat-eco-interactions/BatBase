<?php

namespace App\Tests\Behat\contexts;

use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\BeforeScenarioScope as BeforeScenarioScope;

/**
 * Utility feature steps
 *
 * TOC
 *    UI INTERACTIONS
 *    COMBOBOX
 *        STANDARD
 *        DYNAMIC
 *        ASSERTS
 *    ASSERTS
 *        LOCAL STORAGE
 */
class FeatureContext extends BaseContext implements Context
{
    /**
     * Pauses the scenario until the user presses a key. Useful when debugging a scenario.
     *
     * @Then (I )break :msg
     * @Then (I )break
     */
    public function iAddABreakpoint($msg = null): void
    {
        $this->iPutABreakpoint($msg);
    }
    /**
     * @Given I log in as :username
     */
    public function iLogInAs($username): void
    {
        $this->visitPath('/login');
        $this->setLoginEmail($this->getUserEmail($username));
        $this->setPassword();
        $this->pressTheButton('_submit');
        $this->iSeeUsername($username);
    }
    private function setLoginEmail($email): void
    {
        $this->getMinkPage()->fillField('_email', $email);
        $this->spin(
            function () use ($email) {
                $value = $this->evaluate("$('input[name=\"_email\"]')[0].value");
                return $value === $email;
            },
            "Could not set email: [$email]"
        );
    }
    private function setPassword(): void
    {
        $this->getMinkPage()->fillField('_password', 'passwordhere');
        $showPw = $this->getMinkPage()->find('css', '.showHiddenPassword-toggle');
        $showPw->click();
        $this->spin(
            function () {
                $value = $this->evaluate("$('input[name=\"_password\"]')[0].value");
                return $value === 'passwordhere';
            },
            "Could not set password."
        );
    }
    private function getUserEmail($username): string
    {
        $email = $this->buildUserEmail($username);
        return "$email@email.com";
    }
    private function buildUserEmail($username): string
    {
        $words = explode(' ', (string) $username);
        array_shift($words);
        return strtolower(join('.', $words));
    }
    private function iSeeUsername($name): void
    {
        $username = join('', explode(' ', (string) $name));
        $this->iSee($username);
    }
    /* ======================= COMBOBOX ===================================== */
    /** ---------------------- STANDARD ------------------------------------- */
    /**
     * @When I add :text to the :label combobox
     * Note: Selectize create method.
     */
    public function iAddToTheCombobox($text, $label): void
    {
        $selId = $this->getComboId($label);

        $this->spin(function () use ($selId, $text) {
            try {
                $this->execute("$('$selId')[0].selectize.createItem('$text');");
            } catch (\Throwable) {
                $this->iAddABreakpoint('Error with combobox API: $("'.$selId.'")[0].selectize.createItem()');
            }
            return true;
        }, "Couldn't find combobox [$selId]");
    }
    /**
     * @When I select :text from the :prop combobox
     * Note: Selectize select method.
     */
    public function iSelectFromTheCombobox($text, $prop): void
    {
        $this->selectTextInCombobox($prop, $text);
    }
    /**
     * @Then I remove :text from the :prop combobox
     */
    public function iRemoveFromTheCombobox($text, $prop): void
    {
        $this->removeTextFromCombobox($prop, $text);
    }
    // private function textContainedInField($text, $fieldId, $isIn = true)
    // {
    //     // $should_nt = $isIn ? 'Should' : "Shouldn't";
    //     $fieldVal = $this->getFieldTextOrValue($fieldId);
    //     return !$isIn && strpos($fieldVal, $text) === false ||
    //         $isIn && (strpos($fieldVal, $text) != false || $fieldVal == $text); //strpos fails on exact match
    // }
    /** ---------------------- DYNAMIC -------------------------------------- */
    /**
     * @When I clear the :ordinal :prop dynamic combobox
     */
    public function iClearTheDynamicCombobox($ordinal, $prop): void
    {
        $selId = $this->getComboId($prop) . $ordinal[0];
        $this->execute("$('$selId')[0].selectize.clear()");
    }
    /**
     * @When I change :text in the :prop dynamic combobox
     * @When I change :text in the :prop :ord dynamic combobox
     */
    public function iChangeInTheDynamicCombobox($text, $prop, $ord = false): void
    {
        $ordinal = $ord ?: $this->getCurrentFieldCount($prop) - 1;
        $this->iSelectFromTheDynamicCombobox($text, $prop, $ordinal);
        $this->blurNextDynamicCombobox($this->getComboId($prop), $ordinal);
    }
    /**
     * @When I add :text to the :prop dynamic combobox
     */
    public function iAddToTheDynamicCombobox($text, $prop): void
    {
        $newFormLvl = $this->getOpenFormPrefix() === 'sub' ? 'sub2' : 'sub';
        $ord = $this->getCurrentFieldCount($prop);                              //$this->log("--iAddToTheDynamicCombobox text[$text] prop[$prop] ord?[$ord]");
        $this->iSelectFromTheDynamicCombobox($text, $prop, $ord, 'new');
        $this->waitForTheFormToOpen($newFormLvl);
    }
    private function waitForTheFormToOpen($type): void
    {
        $this->spin(function () use ($type) {
            $form = $this->getMinkPage()->find('css', "#$type-form");           //$this->log("\niWaitForTheFormToClose ? ". (!!$form === true ? 'true' : 'false'));
            return !!$form;
        }, "Form [$type] did not open.");
    }
    /**
     * @When I select :text from the :prop dynamic combobox
     * Note: Changes the combobox at the ordinal, or the last filled (!$new).
     */
    public function iSelectFromTheDynamicCombobox($text, $prop, $o = null, $new = null): void
    {
        $ord = $o ?: $this->getCurrentFieldCount($prop);
        $selId = $this->getComboId($prop) . $ord;                               //$this->log("\n Select from the $selId new?[$new]");
        $this->selectTextInFieldCombobox($selId, $text, $new);
    }
    private function getCurrentFieldCount($prop): int|string
    {
        $selCntnrId = '#' . $this->getFieldName($prop) . '_f-cntnr';
        $cnt = $this->evaluate("$('$selCntnrId').data('cnt');");
        return $cnt ?: 1;
    }
    /** ---------------------- ASSERTS -------------------------------------- */
    /**
     * @Then I should see :text in the :label combobox
     */
    public function iShouldSeeInTheCombobox($text, $label): void
    {
        $selId = $this->getComboId($label);
        $val = $this->getValueToSelect($selId, $text);
        $this->spin(fn() => $this->isValSelected($selId, $val), "Did not find [$text => $val] in the [$selId] combobox.");
    }
    /**
     * @Then I should see :text in the :prop dynamic combobox
     */
    public function iShouldSeeInTheDynamicCombobox($text, $prop): void
    {
        $selId = $this->getComboId($prop);
        $count = $this->getCurrentFieldCount($prop);

        $this->spin(function () use ($text, $count, $selId) {
            while ($count > 0) {
                $selector = $selId . $count;
                $selected = $this->evaluate("$('$selector').text();");          //$this->log("\n[$selector] selected = [$selected]");
                if ($text == $selected) {
                    return true;
                }
                --$count;
            }
        }, "Did not find [$text] in [$selId].");
    }
    /**
     * @Then I should not see :text in the :label combobox
     */
    public function iShouldNotSeeInTheCombobox($text, $label): void
    {
        $selId = $this->getComboId($label);
        $val = $this->getValueToSelect($selId, $text);
        $this->spin(fn() => !$this->isValSelected($selId, $val), "Should not find [$text => $val] in the [$selId] combobox.");
    }
    /**
     * @Then the :prop select field should be empty
     */
    public function theSelectFieldShouldBeEmpty($prop): void
    {
        $selId = $this->getComboId($prop);
        $val = $this->evaluate("$('$selId')[0].selectize.getValue();");
        if (is_array($val)) {
            foreach ($val as $value) {
                if ($value) {
                    $val = false;
                }
            }
            if ($val !== false) {
                $val = '';
            }
        }
        $this->handleEqualAssert($val, '', true, "The [$prop] should be empty.");
    }
    /** ================ UI INTERACTIONS ==================================== */
    /**
     * @Given I click on :text link
     */
    public function iClickOnLink($text): void
    {
        $map = ['use the map interface' => '.map-link'];
        $this->clickOnPageElement($map[$text]);
    }
    /**
     * @When I check the Show All Fields box
     */
    public function iCheckTheShowAllFieldsBox(): void
    {
        $this->spin(function () {
            $form = $this->getOpenFormPrefix();
            $selector = '#' . $form . '-all-fields';
            $checkbox = $this->getMinkPage()->find('css', $selector);
            if (!$checkbox) {
                return false;
            }
            $checkbox->check();
            return $checkbox->isSelected();
        }, 'Could not check "Show all fields".');
    }
    /**
     * @When I press :bttnText :count times
     */
    public function iPressTimes($bttnText, $count): void
    {
        for ($i = 0; $i < $count; $i++) {
            $this->getMinkPage()->pressButton($bttnText);
        }
    }
    // /**
    //  * @When /^(?:|I )click (?:on |)(?:|the )"([^"]*)"(?:|.*)$/
    //  * @Then /^(?:|I )click (?:on |)(?:|the )"([^"]*)"(?:|.*)$/
    //  */
    // public function iClickOn($arg1)
    // {
    //     $findName = $this->getSession()->getPage()->find("css", $arg1);
    //     if (!$findName) {
    //         throw new Exception($arg1 . " could not be found");
    //     } else {
    //         $findName->click();
    //     }
    // }
    /**
     * @When I press the :bttnText button
     */
    public function iPressTheButton($bttnText): void
    {
        if ($bttnText === "Update" || $bttnText === "Create") {
            $this->submitTheForm($bttnText);
            return;
        }
        $this->pressTheButton($bttnText);

        if ($bttnText === 'Map View') {
            $this->getMinkPage()->pressButton($bttnText);
        }
    }
    /**
     * @Given I exit the tutorial
     * @Given they exit the tutorial
     */
    public function iExitTheTutorial(): void
    {
        $this->spin(function () {
            $this->execute("$('.introjs-overlay').trigger('click');");
            return $this->evaluate("!$('.introjs-overlay').length");
        }, 'Tutorial not closed.');
    }
    /** ======================= ASSERTS ===================================== */
    /**
     * @Given I see :text
     */
    public function iSee($text): void
    {
        $this->iSeeOnPage($text);
    }

    /** ----------------------- LOCAL STORAGE ------------------------------- */
    /**
     * @Given the fixtures have been reloaded
     */
    public function theFixturesHaveBeenReloaded(): void
    {
        if (!self::$dbChanges) {
            return;
        }
        fwrite(STDOUT, "\n\n\nResetting database\.\n\n");
        exec('php bin/console doctrine:schema:drop --force --env=test');
        exec('php bin/console doctrine:schema:update --force --complete --env=test');
        exec('php bin/console doctrine:fixtures:load -n --env=test --group=test --group=e2e -vvv');
        self::$dbChanges = false;
    }
    /**
     * @Given the database has loaded
     */
    public function theDatabaseHasLoaded(): void
    {
        $this->spin(fn() => $this->evaluate("$('#filter-status').text() === 'No Active Filters.';"), 'The database did not load as expected', 10);
    }
}
