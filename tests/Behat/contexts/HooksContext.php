<?php

namespace App\Tests\Behat\contexts;

use Behat\Behat\Context\Context;

/**
 * Suite and Feature hook methods
 */
class HooksContext extends BaseContext implements Context
{
    static bool $testDatabaseInitialized = false;

    /**
     * @BeforeFeature
     *
     * Creates test database with fixture data.
     */
    public static function initTestDatabase(): void
    {
        if (self::$testDatabaseInitialized) {
            return;
        }
        fwrite(STDOUT, "\n\n\n\n\n\n\nLoading database.\n\n\n\n");
        exec('php bin/console doctrine:database:drop --force --env=test');
        exec('php bin/console doctrine:database:create --env=test');
        exec('php bin/console doctrine:schema:drop --force --env=test');
        exec('php bin/console doctrine:schema:update --force --complete --env=test');
        exec('php bin/console doctrine:fixtures:load -n --env=test --group=test --group=e2e -vvv');
        self::$testDatabaseInitialized = true;
    }

    /**
     * @AfterFeature
     *
     * Resets back to fixture data.
     */
    public static function afterFeature(): void
    {
        fwrite(STDOUT, "\n\n\nResetting database\.\n\n");
        exec('php bin/console doctrine:schema:drop --force --env=test');
        exec('php bin/console doctrine:schema:update --force --complete --env=test');
        exec('php bin/console doctrine:fixtures:load -n --env=test --group=test --group=e2e -vvv');
        self::$dbChanges = false;
    }

    /**
     * @AfterSuite
     *
     * Deletes test database.
     */
    public static function afterSuite(): void
    {
        fwrite(STDOUT, "\n\n\nDeleting database.\n\n\n");
        exec('php bin/console doctrine:database:drop --force --env=test');
    }
}
