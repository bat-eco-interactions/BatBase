<?php

namespace App\Tests\Behat\contexts;

use Behat\Behat\Context\Context;

/**
 * Explore page Data Table: feature and utility methods.
 *
 * TOC
 *    TOP SORTS
 *    TABLE DATA-TREE
 *        GET
 *        TOGGLE
 *        ASSERT
 *    TABLE ROWS
 *        GET
 *        ASSERT
 *    ROW BUTTONS
 *    ASSERT
 *        INTERACTIONS
 */
class DataTableContext extends BaseContext implements Context
{
    /** ================== TOP SORTS ======================================== */
    /**
     * @Given the explore table is focused on :entity
     */
    public function theExploreTableIsFocusedOn($entity): void
    {
        $newElems = [
            'Location' => $this->getComboId('Region Filter'),
            'Source' => $this->getComboId('Publication Type Filter'),
            'Taxon' => $this->getComboId('Species Filter'),
        ];
        $this->changeTableSort($this->getComboId('Focus'), $entity, $newElems[$entity]);
    }
    /**
     * @Given I view interactions by :type
     */
    public function iViewInteractionsBy($type): void
    {
        $selId = $this->getComboId('View');
        $newElems = [
            'Arthropods' => $this->getComboId('Order Filter'),
            'Authors' => $this->getNameFilter('Author'),
            'Bats' => $this->getComboId('Taxon Group Filter'),
            'Worms' => $this->getComboId('Species Filter'),
            'Parasites' => $this->getComboId('Species Filter'),
            'Plants' => $this->getComboId('Species Filter'),
            'Publications' => $this->getComboId('Publication Type Filter'),
            'Publishers' => $this->getNameFilter('Publisher')
        ];
        $this->changeTableSort($selId, $type, $newElems[$type]);
    }
    /**
     * @Then I should see the table displayed in :entity view
     */
    public function iShouldSeeTheTableDisplayedInView($entity): void
    {
        $map = ['Location' => 'locs', 'Taxon' => 'taxa', 'Source' => 'srcs'];
        $focusId = $this->getComboId('Focus');
        $view = $this->evaluate("$('$focusId')[0].selectize.getValue();");
        $this->handleEqualAssert(
            $view,
            $map[$entity],
            true,
            "DB in [$view] view. Expected [$map[$entity]]"
        );
    }
    /**
     * @Given I display locations in :loc View
     */
    public function iDisplayLocationsInView($loc): void
    {
        $newElems = [
            'Map Data' => '#map',
            'Table Data' => '#explore-table'
        ];
        $this->changeTableSort($this->getComboId('View'), $loc, $newElems[$loc]);
    }
    /**
     * @When I select the Location view :view
     */
    public function iSelectTheLocationView($view): void
    {
        $this->changeTableSort($this->getComboId('View'), 'Map Data', '#map');
    }
    /**
     * Updates a select elem and checks the page updated by finding a 'new' elem.
     */
    private function changeTableSort($selId, $val, $newElemSel): void
    {                                                                           //$this->log("\nchangeTableSort. [$selId] new = [$newElemSel]\n");
        $this->spin(function () use ($selId, $val, $newElemSel) {
            $this->wait("$('$selId').length");
            if ($this->ifTableAlreadySorted($selId, $val)) { return true; }
            $this->selectTextInFieldCombobox($selId, $val);
            $this->wait("$('$newElemSel').length");
            return $this->evaluate("$('$newElemSel').length;");
        }, "UI did not update as expected. Did not find [$newElemSel].");
    }

    private function ifTableAlreadySorted($selId, $val): bool
    {
        $expected = $this->getValueToSelect($selId, $val);
        return $this->isValSelected($selId, $expected);
    }
    /** ====================== TABLE DATA-TREE ============================== */
    /* --------------------------- GET -------------------------------------- */
    private function getRowTreeNode($text)
    {
        $row = null;

        $this->spin(function () use ($text, &$row) {
            $rows = $this->getAllTreeNodes();
            foreach ($rows as $r) {
                if (!$this->isTextFoundInRow($r->getText(), $text)) {
                    continue;
                }
                $row = $r;
                return true;
            }
        }, "$text node not found in tree;");

        return $row;                                                            //$this->log("\nrowText = [".$treeNode->getText()."] text = [$text]");
    }
    private function getAllTreeNodes()
    {
        $nodes = $this->getMinkPage()->findAll('css', 'div.ag-row [colid="name"]');
        return count($nodes) ? $nodes : [];
    }
    /* ---------------------- TOGGLE ---------------------------------------- */
    /**
     * @When I expand :text in the data tree
     */
    public function iExpandInTheDataTree($text): void
    {
        $this->spin(function () use ($text) {
            $treeNode = $this->getRowTreeNode($text);
            if (!$treeNode) {
                return false;
            }
            $treeNode->doubleClick();
            if (!$this->isRowExpanded($treeNode)) {
                $treeNode->click();
            } //needed for one weird instance of this step "OGenus Species" expansion
            return $this->isRowExpanded($treeNode);
        }, "Couldn't expand [$text] row.");
    }
    /**
     * @When I expand :txt in level :num of the data tree
     */
    public function iExpandInLevelOfTheDataTree($txt, $num): void
    {
        $row = null;
        $this->spin(function () use (&$row, $txt, $num) {
            $selector = 'div.ag-row-level-' . --$num . ' [colid="name"]';
            $treeNodes = $this->getMinkPage()->findAll('css', $selector);
            if (!$treeNodes) {
                return false;
            }
            for ($i = 0; $i < count($treeNodes); $i++) {
                if ($this->isTextFoundInRow($treeNodes[$i]->getText(), $txt)) {
                    $row = $treeNodes[$i];
                    break;
                }
            }
            return $row;
        }, "Didn't find the [$txt] tree node.");
        $row->doubleClick();
    }
    /**
     * @When I collapse :text in the data tree
     */
    public function iCollapseInTheDataTree($text): void
    {
        $this->spin(function () use ($text) {
            $treeNode = $this->getRowTreeNode($text);
            if (!$treeNode) {
                return false;
            }
            $treeNode->doubleClick();
            return $this->isRowExpanded($treeNode) == false;
        }, "Could not collapse [$text] row.");
    }
    /* ------------------------- ASSERT ------------------------------------- */
    /**
     * @Then I should see :text in the tree
     */
    public function iShouldSeeInTheTree($text): void
    {
        $this->spin(fn() => $this->isInDataTree($text), "[$text] is not displayed in table data-tree.");
    }
    /**
     * @Then I should not see :text in the tree
     */
    public function iShouldNotSeeInTheTree($text): void
    {
        $this->spin(fn() => !$this->isInDataTree($text), "[$text] should not be displayed in table data-tree.");
    }
    /**
     * @Then I should see :text under :parentTxt in the tree
     */
    public function iShouldSeeUnderInTheTree($text, $parentTxt): void
    {
        $this->spin(function () use ($text, $parentTxt) {
            $this->iExpandInTheDataTree($parentTxt);
            $row = $this->getTableRow($text);
            if (!$row) {
                return false;
            }
            $this->iCollapseInTheDataTree($parentTxt);
            return true;
        }, "\nDid not find [$text] under [$parentTxt]");
    }
    /**
     * @Then I should not see :text under :parentNode in the tree
     */
    public function iShouldNotSeeUnderInTheTree($text, $parentNode): void
    {
        $this->iExpandInTheDataTree($parentNode);
        $row = $this->getTableRow($text);
        $this->handleNullAssert($row, true, "Shouldn't have found $text under $parentNode.");
    }
    private function isInDataTree($text)
    {
        $treeNodes = $this->getAllTreeNodes();
        $inTree = false;
        for ($i = 0; $i < count($treeNodes); $i++) {
            if ($this->isTextFoundInRow($treeNodes[$i]->getText(), $text)) {
                $inTree = true;
            }
        }
        return $inTree;
    }
    private function isTextFoundInRow($nodeText, $text)
    {
        return $nodeText === $text || $nodeText === "$text *";
    }
    /** ========================= TABLE ROWS ================================ */
    /* --------------------------- GET -------------------------------------- */
    private function getTableRow($text)
    {
        $row = null;

        $this->spin(function () use ($text, &$row) {
            $rows = $this->getAllTableRows();
            foreach ($rows as $r) {
                $treeNode = $r->find('css', '[colid="name"]');                  //$this->log("\nrowText = [".$treeNode->getText()."] text = [$text]");
                if (!$this->isTextFoundInRow($treeNode->getText(), $text)) {
                    continue;
                }
                $row = $r;
                return true;
            }
        }, null);

        return $row;
    }
    private function getAllTableRows()
    {
        $rows = $this->getMinkPage()->findAll('css', '.ag-body-container .ag-row');
        $this->handleNullAssert($rows, false, 'No nodes found in data tree.');
        return $rows;
    }
    /**
     * Either returns all the interaction rows displayed in the table, or a subset
     * under a specified node in the tree.
     */
    private function getInteractionsRows($specifiedNode = false)
    {
        $intRows = [];
        $startCapture = $specifiedNode === false;
        $rows = $this->getAllTableRows();
        foreach ($rows as $row) {
            $nodeText = $row->find('css', '[colid="name"]')->getText();
            if ($this->isTextFoundInRow($nodeText, $specifiedNode)) {
                $startCapture = true;
                continue;
            }
            if ($startCapture && $this->isInteractionRow($nodeText)) {
                array_push($intRows, $row);
            }
            if ($startCapture && $specifiedNode && !$this->isInteractionRow($nodeText)) {
                break;
            }
        }
        $this->handleNullAssert($intRows, false, 'No interaction rows found.');
        return $intRows;
    }
    private function isInteractionRow($nodeText)
    {
        return $nodeText  === '' || $nodeText  === '*';  // asterik replaces show-icon when data quarantined
    }
    /* ---------------------- ASSERT ---------------------------------------- */
    private function isRowExpanded($row)
    {
        $checkbox = $row->find('css', 'span.ag-group-expanded');
        return $checkbox == null ? false : $checkbox->isVisible();
    }
    /* ====================== ROW BUTTONS =================================== */
    /**
     * @When I click on the map pin for :text
     */
    public function iClickOnTheMapPinFor($text): void
    {
        $this->spin(function () use ($text) {
            $row = $this->getTableRow($text);
            if (!$row) {
                return false;
            }
            $pin = $row->find('css', '[alt="Map Icon"]');
            if (!$pin) {
                return false;
            }
            $pin->click();
            return true;
        }, "Couldn't click [$text] row map pin.");
    }
    /**
     * @Given I click on the edit pencil for the :text row
     */
    public function iClickOnTheEditPencilForTheRow($text): void
    {
        $this->spin(function () use ($text) {
            $row = $this->getTableRow($text);
            if (!$row) {
                return false;
            }
            $this->clickRowEditPencil($row);
            return true;
        }, "Couldn't find [$text] row edit pencil. ");
    }
    /**
     * @Given I click on the edit pencil for the first interaction of :nodeTxt
     */
    public function iClickOnTheEditPencilForTheFirstInteractionOf($nodeTxt): void
    {
        $this->iExpandInTheDataTree($nodeTxt);
        $intRows = $this->getInteractionsRows();
        $this->clickRowEditPencil(reset($intRows));
    }
    private function clickRowEditPencil($row): void
    {
        $this->spin(function () use ($row) {
            $pencil = $row->find('css', '.tbl-edit');
            if (!$pencil) {
                return false;
            }
            $pencil->click();
            return true;
        }, null);
    }
    /** ====================== ASSERT ======================================= */
    /** ------------------- INTERACTIONS ------------------------------------ */
    /**
     * @Then the count column should show :count interactions
     */
    public function theCountColumnShouldShowInteractions($count): void
    {
        $this->spin(function () use ($count) {
            $intCnt = $this->evaluate('$("#tbl-cnt").text()');  // Handles groups without a central root
            $colCnt = $this->evaluate('$("[row=0] [colId=\"intCnt\"]").text()');
            return $this->ifContainsText($colCnt, $count) || $this->ifContainsText($intCnt, $count);
        }, "Count coulmn does not show [$count] interactions.");
    }
    /**
     * @Given I should see :cnt interactions in the table
     */
    public function iShouldSeeInteractionsInTheTable($cnt): void
    {
        $this->spin(
            function () use ($cnt) {
                $intCount = $this->evaluate("$('#tbl-cnt').text();");
                return str_contains(strtolower($intCount), strtolower((string) $cnt));
                // $curCnt = $this->evaluate("$('#tbl-cnt').text().match(/\d+/)[0];");
                // return $curCnt == $cnt;
            },
            "Didn't find [$cnt] interactions in the table."
        );
    }
    /**
     * @Then data in the interaction rows
     * @Then I should see data in the interaction rows
     * Note: Data is checked in the Subject Taxon column only.
     */
    public function dataInTheInteractionRows(): void
    {
        $this->spin(function () {
            $subj = $this->evaluate('$("[colid=\"subject\"] span").text()');
            return $subj;
        }, 'No data found in the interaction rows.');
    }
    /**
     * @Then I (should) see :count row(s) in the table data tree
     */
    public function iShouldSeeRowsInTheTableDataTree($count): void
    {
        $rowCnt = 0;
        $this->spin(function () use ($count, &$rowCnt) {
            $rowCnt = $this->evaluate('$(".ag-body-container>div").length');
            return !$rowCnt ? false : ($count == $rowCnt);
        }, "Didn't find [$count] rows in the table, found [$rowCnt]");
    }
    /**
     * @Then I (should) see :count total rows in the data table
     */
    public function iShouldSeeTotalRowsInTheDataTable($count): void
    {
        $rowCnt = 0;
        $this->spin(function () use ($count, $rowCnt) {
            $this->execute("$('#xpand-all').click()");
            $rowCnt = $this->evaluate('$(".ag-body-container>div").length');
            return !$rowCnt ? false : ($count == $rowCnt);
        }, "Didn't find [$count] rows in the table, found [$rowCnt]");
    }
    /**
     * @Then I should see :count interaction(s) under :nodeTxt
     */
    public function iShouldSeeInteractionsUnder($count, $nodeTxt): void
    {
        $this->iExpandInTheDataTree($nodeTxt);
        $rows = $this->getInteractionsRows($nodeTxt);
        $this->handleEqualAssert(
            count($rows),
            $count,
            true,
            "Found [" . count($rows) . "] rows under $nodeTxt. Expected $count"
        );
    }
    /**
     * @Then I should see :count interactions attributed
     */
    public function iShouldSeeInteractionsAttributed($count): void
    {
        $rows = $this->getInteractionsRows();
        $this->handleEqualAssert(
            count($rows),
            $count,
            true,
            "Found [" . count($rows) . "] interaction rows. Expected [$count]"
        );
    }
    /**
     * @Then the expected data in the interaction row
     * @Then the expected data in the :focus interaction row
     */
    public function theExpectedDataInTheInteractionRow($focus = null): void
    {
        // $cols = [ 'subject', 'object', 'interactionType', 'tags', 'citation',
        //     'habitat', 'location', 'country', 'region', 'note' ];
        // if ($focus) { unset($cols[array_search($focus ,$cols)]); }
        // $this->spin(function() {
        //     $intRows = $this->getInteractionsRows();

        //     foreach ($intRows as $row) {
        //         foreach ($cols as $colId) {
        //             $selector = '[colid="'.$colId.'"]';
        //             $data = $row->find('css', $selector);
        //             return $data->getText();
        //         }
        //     }
        // }, "No data found in at least one interaction column.");
    }
}
