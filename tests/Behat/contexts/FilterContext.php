<?php

namespace App\Tests\Behat\contexts;

use Behat\Behat\Context\Context;

/**
 * Explore-page filters: feature and utility methods.
 *
 * TOC
 *    FILTER PANEL
 *        FILTER SET
 *    DATE FILTER
 *    ASSERT
 *        STATUS MSG
 */
class FilterContext extends BaseContext implements Context
{
    /* ====================== FILTER PANEL ================================== */
    /**
     * @Given I toggle :state the filter panel
     */
    public function iToggleTheFilterPanel($state)
    {
        $isClosed = $this->evaluate("$('#filter-pnl').hasClass('closed');");
        if ($isClosed && $state == 'close' || !$isClosed && $state == 'open') {
            return true;
        }
        $toggleBttn = $this->getMinkPage()->find('css', '#filter');
        if (!$toggleBttn) {
            $this->iPutABreakpoint('"Filters" button not found.');
        }
        $toggleBttn->click();

        $this->spin(function () use ($state, $toggleBttn) {
            sleep(1);
            $closed = $this->evaluate("$('#filter-pnl').hasClass('closed');");
            if ($closed && $state == 'close' || !$closed && $state == 'open') {
                return true;
            }
        }, 'Filter panel not ' . ($state == 'open' ? "expanded" : "collapsed"));

        return true;
    }
    /**
     * @When I type :text in the :type text box and press enter
     */
    public function iTypeInTheTextBoxAndPressEnter($text, $type): void
    {
        $fId = $this->getNameFilter($type);
        $input = $this->getMinkPage()->find('css', $fId);
        $input->setValue($text);
        $input->keypress('13');
    }
    /* ---------------- FILTER SET ------------------------------------------ */
    /**
     * @When I should see :text in the save modal
     */
    public function iShouldSeeInTheSaveModal($text): void
    {
        $this->spin(
            function () use ($text) {
                $modalText = $this->evaluate("$('.introjs-tooltiptext').text();");
                return str_contains($modalText, (string) $text);
            },
            "Did not find [$text] in the modal."
        );
    }
    /* ======================= DATE FILTER ================================== */
    /**
     * @When I uncheck the date-updated filter
     */
    public function iUncheckTheDateUpdatedFilter(): void
    {
        $this->iToggleTheFilterPanel('open');
        $this->toggleTheDateFilter(false);
        $this->iToggleTheFilterPanel('close');
    }
    /**
     * @Given I set the date :type filter to :date
     */
    public function iSetTheDateFilterTo($type, $date): void
    {
        $initialCount = $this->evaluate("$('#tbl-cnt').text().match(/\d+/)[0];");
        $selId = $this->getComboId('Date Filter Type');

        $this->setDateFilterDefault($date);
        $this->selectComboValue($selId, $type);
        $this->handleEqualAssert($this->getFieldValue($selId), $type);
        $this->toggleTheDateFilter(true);

        $this->clickOnPageElement('#filter-col1');

        $this->spin(function () use ($initialCount) {
            $postCount = $this->evaluate("$('#tbl-cnt').text().match(/\d+/)[0];");
            return $postCount !== $initialCount && $postCount > 0;
        }, "Date did not update as expected. type = [$type]. date = [$date]. initial [$initialCount]. (After can't be zero)");

        $this->setDateFilterDefault(false);
    }
    /**
     * There doesn't seem to be a way to set the date on the flatpickr calendar
     * from these tests. Adding a data property to the calendar elem that will be
     * read on calendar load and set that date as the default for the calendar.
     * Doesn't quite test the user's input, but should test the filter functionality.
     */
    public function setDateFilterDefault($defaultDate): void
    {
        // $selId = $this->getComboId('Date Filter Type');
        $this->execute("$('#sel-DateFilterType').data('default', '$defaultDate');");
    }
    public function toggleTheDateFilter($enable): void
    {
        $checkbox = $this->getMinkPage()->find('css', 'input#shw-chngd');
        $this->spin(function () use ($enable, &$checkbox) {
            if ($enable) {
                $checkbox->check();
            } else {
                $checkbox->uncheck();
            }
            return $checkbox->isSelected() == $enable;
        }, 'Date filter not [' . ($enable ? 'en' : 'dis') . 'abled].');
    }
    /**
     * @Given I filter the table to interactions created today
     */
    public function iFilterTheTableToInteractionsCreatedToday(): void
    {
        $this->spin(fn() => $this->iToggleTheFilterPanel('open'), 'Problem opening filter panel');

        $this->iSetTheDateFilterTo('updated', 'today');
        $this->iToggleTheFilterPanel('close');

        $this->spin(function () {
            $checked = $this->evaluate("$('#shw-chngd').prop('checked');");
            return $checked;
        }, 'Date filter checkbox not checked.');
    }
    /* ====================== ASSERT ======================================== */
    /** ------------------- STATUS MSG -------------------------------------- */
    /**
     * @Then I should see :text in the taxon filter status bar
     */
    public function iShouldSeeInTheTaxonFilterStatusBar($text): void
    {
        $this->spin(
            function () use ($text) {
                $filterMsg = $this->evaluate("$('#view-fltr').text();");
                return str_contains(strtolower($filterMsg), strtolower((string) $text));
            },
            "Did not find [$text] in the filter status bar."
        );
    }
    /**
     * @Then I should see :text in the filter status bar
     */
    public function iShouldSeeInTheFilterStatusBar($text): void
    {
        $this->spin(
            function () use ($text) {
                $filterMsg = $this->evaluate("$('#filter-status').text();");
                return str_contains(strtolower($filterMsg), strtolower((string) $text));
            },
            "Did not find [$text] in the filter status bar."
        );
    }
}
