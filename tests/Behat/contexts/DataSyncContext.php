<?php

namespace App\Tests\Behat\contexts;

use Behat\Mink\Driver\Selenium2Driver;
use Behat\Mink\Session;
use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\BeforeScenarioScope as BeforeScenarioScope;


/**
 * Test data syncing data changes between multiple editors
 *
 * TOC
 *    INIT EDITOR SESSION
 *    CREATE INTERACTIONS
 *        CREATE CHAIN
 *        FILL DATA
 *    EDIT DATA
 *        LOCATION
 *        TAXON
 *    SYNC DATA
 *        RELOAD
 *        VALIDATE
 */ class DataSyncContext extends BaseContext implements Context
{
    private $editor1;
    private $editor2;

    private $dataEntry;
    private $dataTable;
    private $feature;
    private $filter;

    /** @BeforeScenario */
    public function gatherContexts(BeforeScenarioScope $scope): void
    {
        $environment = $scope->getEnvironment();

        $this->dataEntry = $environment->getContext(DataEntryContext::class);
        $this->dataTable = $environment->getContext(DataTableContext::class);
        $this->feature = $environment->getContext(FeatureContext::class);
        $this->filter = $environment->getContext(FilterContext::class);
    }

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    // public function __construct()
    // {
    // }
    /** ================== INIT EDITOR SESSION ============================== */
    /**
     * @Given editor :cnt visits the explore page
     */
    public function anEditorVisitsTheExplorePage($cnt): void
    {                                                                           //$this->log("\n---- Editor $cnt logging in.\n");
        $editor = 'editor' . $cnt;
        $this->$editor = $this->createUserSession();
        self::$curUser = $this->$editor;
        $this->userLogIn($this->$editor, 'editor@email.com');
        $this->userVisitsExplorePage($this->$editor);                            //$this->log("\n        Visits Explore page.\n");
        usleep(50000);
    }
    public function createUserSession()
    {
        $opts = [
            'browser' => 'chrome',
            'chrome' => [
                /*'binary' => '/Applications/Chromium.app/Contents/MacOS/Chromium',*/
                'args' => ['--disable-gpu', '--window-size=1220,800']
            ],
            'marionette' => true,
        ];
        $driver = new Selenium2Driver('chrome', $opts);
        $session = new Session($driver);
        $session->start();
        return $session;
    }
    public function userLogIn($userSession, $email): void
    {
        $userSession->visit('http://localhost:9000/test.php/login');
        $userSession->getPage()->fillField('_email', $email);
        $userSession->getPage()->fillField('_password', 'passwordhere');
        $userSession->getPage()->pressButton('_submit');
    }
    public function userVisitsExplorePage($editor): void
    {
        $editor->visit('http://localhost:9000/test.php/explore');
        $this->feature->theDatabaseHasLoaded();
    }
    /** ================== CREATE INTERACTIONS ============================== */
    /**
     * @Given editor :cnt creates two interactions
     */
    public function editorCreatesTwoInteractions($cnt): void
    {
        $map = [[1, 2], [3, 4]];
        $editor = 'editor' . $cnt;
        self::$curUser = $this->$editor;
        $this->userCreatesInteractions(self::$curUser, $map[$cnt - 1]);
    }
    public function userCreatesInteractions($editor, $cntAry = [1, 2]): void
    {
        $this->log("\---- userCreatesInteractions.\n");
        $this->pressTheButton('New');
        foreach ($cntAry as $cnt) {
            $this->log("\n    Creating interaction $cnt\n");
            $this->iSubmitTheNewInteractionFormWithTheFixtureEntities($cnt);
        }
        $this->log("\n    Interactions added. Exiting form\n");
        $this->dataEntry->iExitTheFormWindow();
    }
    /** ------------------ CREATE CHAIN ------------------------------------- */
    /**
     * @Given I create an interaction
     */
    public function iSubmitTheNewInteractionFormWithTheFixtureEntities($count = 1): void
    {
        $this->log("\n        Filling form with fixture data\n");
        $srcLocData = [
            'Publication' => 'Revista de Biologia Tropical',
            'Citation Title' => 'Two cases of bat pollination in Central America',
            'Country-Region' => 'Central America',
            'Location' => 'Panama'
        ];
        $this->dataEntry->fillSrcAndLocFields($srcLocData);
        $taxaData = ['Genus' => 'Artibeus', 'Family' => 'Sphingidae'];
        $this->dataEntry->fillTaxaFields($taxaData);
        $miscData = [
            'Type' => 'Prey',
            'Source' => 'Secondary',
            'Note' => 'Interaction ' . $count,
            'Season' => 'Dry',
            'Date' => 'Select an easy date to remember.',
            'Pages' => '123-345'
        ];
        $this->dataEntry->fillMiscIntFields($miscData);
        self::$curUser->getPage()->pressButton('Create');
        // $this->dataEntry->iPressSubmitInTheConfirmationPopup();
        $this->waitForInteractionFormToReset();
    }
    private function waitForInteractionFormToReset(): void
    {
        $this->spin(fn() => self::$curUser->evaluateScript("$('#success').length > 0"), 'Interaction form did not reset.');
    }
    /** ------------------ FILL DATA ---------------------------------------- */
    /** ================== EDIT DATA ======================================== */
    /**
     * @Given editor :cnt edits some sub-entity data
     * TODO: WHY IS SOURCE DATA NOT BEING EDITED?
     */
    public function editorEditsSomeSubEntityData($cnt): void
    {
        $editor = 'editor' . $cnt;
        self::$curUser = $this->$editor;       // dump(self::$curUser);
        $this->filter->iUncheckTheDateUpdatedFilter();
        if ($cnt == 1) {
            $this->editorChangesLocationData();
        } else {
            $this->editorChangesTaxonData();
        }
    }
    /** ------------------- LOCATION ---------------------------------------- */
    public function editorChangesLocationData(): void
    {
        $this->log("\n---- User changing Location data.\n");
        $this->dataTable->theExploreTableIsFocusedOn('Locations');
        $this->editLocationData();
        $this->moveLocationInteraction();
    }
    private function editLocationData(): void
    {
        $this->log("\n        Editing Location data.\n");
        $this->dataTable->iExpandInTheDataTree('Central America');
        $this->dataTable->iExpandInTheDataTree('Costa Rica');
        $this->dataTable->iClickOnTheEditPencilForTheRow('Santa Ana-Forest');
        $this->dataEntry->iChangeTheFieldTo('Display Name', 'input', 'Santa Ana-Desert');
        $this->selectTextInCombobox('Habitat Type', 'Desert');
        self::$curUser->getPage()->pressButton('Update');
        $this->dataEntry->iWaitForTheFormToClose('top');
    }
    private function moveLocationInteraction(): void
    {
        $this->log("\n        Editing interaction location data.\n");
        $this->dataTable->iExpandInTheDataTree('Central America');
        $this->dataTable->iExpandInTheDataTree('Costa Rica');
        $this->dataTable->iClickOnTheEditPencilForTheFirstInteractionOf('Santa Ana-Desert');
        $this->selectTextInCombobox('Location', 'Costa Rica');
        self::$curUser->getPage()->pressButton('Update');
        $this->dataEntry->iWaitForTheFormToClose('top');
        $this->dataTable->theExploreTableIsFocusedOn('Locations');
        $this->filter->iUncheckTheDateUpdatedFilter();
        $this->dataTable->iExpandInTheDataTree('Central America');
        $this->dataTable->iExpandInTheDataTree('Costa Rica');
        $this->dataTable->iShouldSeeInteractionsUnder('2', 'Unspecified Costa Rica Interactions');
    }
    /** ---------------------- TAXON ---------------------------------------- */
    public function editorChangesTaxonData(): void
    {
        $this->log("\n        Editor changing Taxon data.\n");
        $this->dataTable->theExploreTableIsFocusedOn('Taxa');
        $this->dataTable->iViewInteractionsBy('Arthropods');
        $this->editTaxonData();
        $this->moveTaxonInteraction();
    }
    private function editTaxonData(): void
    {
        $this->dataTable->iExpandInTheDataTree('Order Lepidoptera');
        $this->dataTable->iClickOnTheEditPencilForTheRow('Family Sphingidae');
        $this->dataEntry->iChangeTheFieldTo('Display Name', 'input', 'Sphingidaey');
        self::$curUser->getPage()->pressButton('Update');
        $this->dataEntry->iWaitForTheFormToClose('top');
    }
    private function moveTaxonInteraction(): void
    {
        $this->log("\n--- Editor changing Taxon interaction data.\n");
        $this->dataTable->iExpandInTheDataTree('Order Lepidoptera');
        $this->dataTable->iClickOnTheEditPencilForTheFirstInteractionOf('Unspecified Lepidoptera Interactions');
        $this->dataEntry->iOpenTheTaxonSelectForm('Object');
        $this->selectTextInCombobox('Group', 'Arthropod');
        $this->selectTextInCombobox('Family', 'Sphingidaey');
        $this->pressTheButton('Select Taxon');
        $this->dataEntry->iWaitForTheFormToClose('sub');
        self::$curUser->getPage()->pressButton('Update');
        $this->dataEntry->iWaitForTheFormToClose('top');
        $this->filter->iUncheckTheDateUpdatedFilter();
        $this->dataTable->theExploreTableIsFocusedOn('Taxa');
        $this->dataTable->iViewInteractionsBy('Arthropods');
        $this->dataTable->iExpandInTheDataTree('Order Lepidoptera');
        $this->dataTable->iShouldSeeInteractionsUnder('1', 'Unspecified Lepidoptera Interactions');
        $this->dataTable->iExpandInTheDataTree('Family Sphingidaey');
        $this->dataTable->iShouldSeeInteractionsUnder('6', 'Unspecified Sphingidaey Interactions');
    }
    /** ===================== SYNC DATA ===================================== */
    /** ---------------------- RELOAD --------------------------------------- */
    /**
     * @When each reloads the explore page
     */
    public function eachReloadsTheExplorePage(): void
    {
        self::$curUser = $this->editor1;
        $this->userVisitsExplorePage(self::$curUser);
        self::$curUser = $this->editor2;
        $this->userVisitsExplorePage(self::$curUser);
    }
    /**
     * @Then the new data should sync between the editors
     */
    public function theNewDataShouldSyncBetweenTheEditors(): void
    {
        $this->editorTableLoads($this->editor1);
        $this->editorTableLoads($this->editor2);
    }
    private function editorTableLoads($editor): void
    {
        $this->spin(fn() => $editor->evaluateScript("$('.ag-row').length > 0"), 'Rows not loaded in table.');
    }
    /** ---------------------- VALIDATE ------------------------------------- */
    /**
     * @Then they should see the expected changes in the data table
     */
    public function theyShouldSeeTheExpectedChangesInTheDataTable(): void
    {
        self::$curUser = $this->editor1;
        $this->checkSourceData();
        $this->checkTaxonData();

        self::$curUser = $this->editor2;
        $this->checkSourceData();
        $this->checkLocationData();
    }
    private function checkLocationData(): void
    {
        $this->dataTable->theExploreTableIsFocusedOn('Locations');
        $this->dataTable->iExpandInTheDataTree('Central America');
        $this->dataTable->iExpandInTheDataTree('Costa Rica');
        $this->dataTable->iShouldSeeInteractionsUnder('2', 'Unspecified Costa Rica Interactions');
        $this->dataTable->iShouldSeeInteractionsUnder('1', 'Santa Ana-Desert');
    }
    private function checkSourceData(): void
    {
        $this->dataTable->theExploreTableIsFocusedOn('Sources');
        $this->dataTable->iExpandInTheDataTree('Revista de Biologia Tropical');
        $this->dataTable->iExpandInTheDataTree('Two cases of bat pollination in Central America');
        $this->dataTable->iShouldSeeInteractionsAttributed(6);
    }
    private function checkTaxonData(): void
    {
        $this->dataTable->theExploreTableIsFocusedOn('Taxa');
        $this->dataTable->iViewInteractionsBy('Arthropods');
        $this->dataTable->iExpandInTheDataTree('Order Lepidoptera');
        $this->dataTable->iShouldSeeInteractionsUnder('1', 'Unspecified Lepidoptera Interactions');
        $this->dataTable->iExpandInTheDataTree('Family Sphingidaey');
        $this->dataTable->iShouldSeeInteractionsUnder('6', 'Unspecified Sphingidaey Interactions');
    }
}
