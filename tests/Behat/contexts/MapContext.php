<?php

namespace App\Tests\Behat\contexts;

use Behat\Behat\Context\Context;

/**
 * Leaflet Map: feature and utility methods.
 *
 * TOC
 *    MAP INTERACTIONS
 *    ASSERT
 *        MARKERS
 *        POPUPS
 */
class MapContext extends BaseContext implements Context
{
    /* =================== MAP INTERACTIONS ================================= */
    /**
     * @When I click on a map marker
     */
    public function iClickOnAMapMarker(): void
    {
        $marker = $this->getMinkPage()->find('css', '.leaflet-marker-icon');
        $this->handleNullAssert($marker, false, "Couldn't find marker on the map.");
        try {
            $marker->doubleClick();
        } catch (\Exception) {
            $this->iPutABreakpoint("Couldn't click elem.");
        }
    }
    /**
     * @When I click on the map
     */
    public function iClickOnTheMap(): void
    {
        $this->execute("$('#loc-map').trigger('click');");
    }
    /**
     * @Given I press the :type button in the map
     */
    public function iPressTheButtonInTheMap($type): void
    {
        $selector = [
            'New Location' => '.leaflet-control-create-icon',
            'Click to select position' => '.leaflet-control-click-create-icon'
        ][$type];
        $this->pressTheMapButon($selector);
    }
    /**
     * @When I press :bttnText in the added green pin's popup
     */
    public function iPressInTheAddedGreenPinsPopup($bttnText): void
    {
        $this->clickOnPageElement('.leaflet-marker-icon.new-loc');
        $this->clickOnPageElement("input[value='$bttnText']");
    }
    /**
     * @Then I click on an existing location marker
     */
    public function iClickOnAnExistingLocationMarker(): void
    {
        $markers = $this->getMinkPage()->findAll('css', 'img.leaflet-marker-icon');
        $this->handleNullAssert($markers, false, 'No markers found on map.');
        $markers[0]->click();
    }
    /* ====================== ASSERT ======================================== */
    /**
     * @Then I should see the map loaded
     */
    public function iShouldSeeTheMapLoaded(): void
    {
        $mapTile = $this->getMinkPage()->find('css', '.leaflet-tile-pane');
        $this->handleNullAssert($mapTile, false, 'No map tiles found.');
    }
    /**
     * @Then the map should close
     */
    public function theMapShouldClose(): void
    {
        $map = $this->getMinkPage()->find('css', '#loc-map');
        $this->handleNullAssert($map, true, 'Map should not be displayed, and yet...');
    }
    /** ------------------ MARKERS ------------------------------------------ */
    /**
     * @Then I should see :count interactions shown on the map
     */
    public function iShouldSeeInteractionsShownOnTheMap($count): void
    {
        $this->spin(function () use ($count) {
            $legendTxt = $this->evaluate("$('#int-legend').text()");
            return strpos($legendTxt, $count . ' shown');
        }, "Did not find [$count] in the map interaction count legend.");
    }
    /**
     * @When I see the location's pin on the map
     * @When I see the :type location's pin on the map
     */
    public function iSeeTheLocationsPinOnTheMap($type = null): void
    {
        $class = '.leaflet-marker-icon';
        $msg = 'Marker not found on map';
        if ($type === 'new') {
            $class = $class . '.new-loc';
            $msg = 'New Location marker not found on map';
        }
        $this->spin(function () use ($class, $msg) {
            $marker = $this->evaluate("$('$class').length > 0");
            return $marker !== null;
        }, $msg);
    }
    /**
     * @Then I should see the map with markers
     * @Then I should see markers on the map
     */
    public function iShouldSeeTheMapWithMarkers(): void
    {
        $markers = $this->evaluate("$('.leaflet-marker-icon').length > 0;");
        $this->handleNullAssert($markers, false, "Map did not update as expected. No markers found.");
    }
    /**
     * @Then the marker's popup should have a description of the position
     * Note: The description is derived from the reverse geocode results.
     */
    public function theMarkersPopupShouldHaveADescriptionOfThePosition(): void
    {
        $this->iPutABreakpoint("theMarkersPopupShouldHaveADescriptionOfThePosition");
    }
    /**
     * @Then I should see :mCount location markers
     * @Then I should see :mCount location markers and :cCount location clusters
     * Note: Cluster count returns one extra for some reason I have yet to identify
     */
    public function iShouldSeeLocationMarkers($mCount, $cCount): void
    {
        $markers = $this->getMinkPage()->findAll('css', 'img.leaflet-marker-icon');
        if ($cCount) {
            $clusters = $this->getMinkPage()->findAll(
                'css',
                'div.leaflet-marker-icon'
            );
            $this->handleEqualAssert(count($clusters), $cCount + 1, true, 'Found [' . count($clusters) . "] clusters. Expected [$cCount].");
        }
        $this->handleEqualAssert(count($markers), $mCount, true, 'Found [' . count($markers) . "] markers. Expected [$mCount].");
    }
    /** ------------------ POPUPS ------------------------------------------- */
    /**
     * @Then I should see :text in popup
     */
    public function iShouldSeeInPopup($text): void
    {
        $this->spin(function () use ($text) {
            $elem = $this->getMinkPage()->find('css', '.leaflet-popup-content');
            if (!$elem) {
                return false;
            }
            return str_contains((string) $elem->getHtml(), (string) $text);
        }, "Did not find [$text] in the map popup.");
    }
    /**
     * @Then I should see the map with the location summary popup
     */
    public function iShouldSeeTheMapWithTheLocationSummaryPopup(): void
    {
        $popup = $this->getUserSession()
            ->evaluateScript("$('.leaflet-popup-content-wrapper').length > 0;");
        $this->handleEqualAssert($popup, true, true, "Location summary popup not displayed.");
    }
}
