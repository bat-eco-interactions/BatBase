<?php

namespace App\Tests\Behat\contexts;

use Behat\Behat\Context\Context;

/**
 * MISC MINOR FEATURES
 *
 * TOC
 *    BASE UI
 *    INTERACTION LIST
 */
class MiscContext extends BaseContext implements Context
{
    /** ========================= BASE UI =================================== */
    /**
     * @When the data-statistics load in the page header
     * Note: The totals, once loaded, will not start with a 0
     */
    public function theDataStatisticsLoadInThePageHeader(): void
    {
        $this->spin(function () {
            $hdrTxt = $this->evaluate('$("#headline-stats").text();');
            $pieces = explode('|', $hdrTxt);
            return trim($pieces[0])[0] !== '0';
        }, "Page header statistics did not load as expected.");
    }

    /**
     * @Then I should see :arg1 :arg2 in the page header
     */
    public function iShouldSeeInThePageHeader2($count, $dataType): void
    {
        $hdrTxt = $this->evaluate('$("#headline-stats").text();');
        $dataCounts = explode('|', (string) $hdrTxt);

        foreach ($dataCounts as $section) {
            if (!str_contains($section, (string) $dataType)) {
                continue;
            }
            $this->handleContainsAssert($count, $section, true, "[$count] not found in [$section].");
        }
    }
    /** ====================== INTERACTION LIST ============================= */
    /**
     * @When I select :modType :selType from the list modification panel
     */
    public function iSelectFromTheListModificationPanel($modType, $selType): void
    {
        $radioId = $this->buildRadioId($selType);
        $radio = $this->getMinkPage()->find('css', $radioId);
        $this->handleNullAssert($radio, false, "No [$selType] radio found.");
        $radio->click();
        $this->spin(fn() => $this->evaluate("$('input$radioId:checked').val() == 'on'"), "The [$selType] radio is not checked.");
    }
    private function buildRadioId($selType)
    {
        $vals = ['All Shown' => 'all', 'Select Rows' => 'some'];
        $val = $vals[$selType];
        return "#list-$val-radio";
    }
    /**
     * @Then I should see :count interactions in the list
     */
    public function iShouldSeeInteractionsInTheList($count): void
    {
        $this->spin(function () use ($count) {
            $uiCount = $this->evaluate("$('#int-list-cnt').text();");
            if (!$uiCount) {
                return false;
            }
            return $count === $uiCount;
        }, "Should have seen $count interactions in list.");
    }
    /**
     * @Given I toggle :state the data lists panel
     * Refactor combin with filter panel
     */
    public function iToggleTheDataListsPanel($state): void
    {
        $isClosed = $this->evaluate("$('#list-pnl').hasClass('closed');");
        if ($isClosed && $state == 'close' || !$isClosed && $state == 'open') {
            return;
        }
        $dataListsPanel = $this->getMinkPage()->find('css', '#lists');
        if (!$dataListsPanel) {
            $this->iPutABreakpoint('"Lists" button not found.');
        }
        $dataListsPanel->click();
        /* -- Spin until finished -- */
        $this->spin(function () use ($state) {
            $closed = $this->evaluate("$('#list-pnl').hasClass('closed');");
            if ($closed && $state == 'close' || !$closed && $state == 'open') {
                return true;
            }
        }, 'Data Lists panel not ' . ($state == 'open' ? "expanded" : "collapsed"));
    }
}
