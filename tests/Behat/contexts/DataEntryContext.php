<?php

namespace App\Tests\Behat\contexts;

use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\BeforeScenarioScope as BeforeScenarioScope;

/**
 * Data Entry Forms: feature and utility methods.
 *
 * TOC
 *    ENTITY
 *        LOCATION
 *        TAXON
 *        SOURCE
 *            AUTHOR
 *            CITATION
 *            LOCATION
 *            PUBLICATION
 *            PUBLISHER
 *    FIELDS
 *        SET
 *        ASSERT
 *    ALERTS
 *        ASSERT
 *    SUBMIT
 *    OPEN/CLOSE FORM
 *    ASSERTS
 */
class DataEntryContext extends BaseContext implements Context
{
    private DataTableContext $dataTable;
    private FeatureContext $feature;
    private MapContext $map;

    /** @BeforeScenario */
    public function gatherContexts(BeforeScenarioScope $scope): void
    {
        $environment = $scope->getEnvironment();
        $this->dataTable = $environment->getContext(DataTableContext::class);
        $this->feature = $environment->getContext(FeatureContext::class);
        $this->map = $environment->getContext(MapContext::class);
    }
    /* ========================== ENTITY ==================================== */
    /* --------------------------- INTERACTION ---------------------------------- */
    /**
     * @Given I open the interaction edit-form for :name
     */
    public function iOpenTheInteractionEditFormFor($name): void
    {
        $this->expandToInteractionsFor($name);
        $this->dataTable->iClickOnTheEditPencilForTheFirstInteractionOf($name);
    }
    protected function expandToInteractionsFor($name): void
    {
        $rowsToExpand = $this->getRowsToExpand($name);
        foreach ($rowsToExpand as $rowName) {
            $this->dataTable->iExpandInTheDataTree($rowName);
        }
    }
    protected function getRowsToExpand($name): array
    {
        $rows = [
            'Summit Experimental Gardens' => ['Central America', 'Costa Rica'],
        ];
        return $rows[$name];
    }

    /* ----------------------------- LOCATION ----------------------------------- */
    /* __________________________ CREATE ____________________________________ */
    /**
     * @Given I open the location form with data for :name
     */
    public function iOpenTheLocationFormWithDataFor($name): void
    {
        switch ($name) {
            case 'Test Location With GPS':
                $this->createTestLocationWithGps($name);
                break;
            case 'Test Location Without GPS':
                $this->createTestLocationWithoutGps($name);
        }
    }
    /** Note: tests the data-entry form-map's New Location custom icon. */
    protected function createTestLocationWithGps($name): void
    {
        $this->useMapToOpenCreateForm();
        $this->iTypeInTheField($name, 'DisplayName', 'input');
        $this->enterTestLocationCoords("9.79026", "-83.91546");
        $this->enterTestLocationDetails($name);
        $this->submitTheForm();
        $this->iWaitForTheFormToClose('sub');
    }
    protected function useMapToOpenCreateForm($value = ''): void
    {
        $this->clickOnPageElement('.map-link'); // 'use the map interface'
        $this->spin(function () {
            $this->pressTheMapButton('.leaflet-control-create-icon'); //New Location
            return $this->evaluate('$("#sub-form").length');
        }, 'Map icon click did not open Create Location form');
        $this->iWaitForTheFormToOpen('sub');
    }
    protected function enterTestLocationCoords($lat, $lng): void
    {
        $this->iTypeInTheField($lat, 'Latitude', 'input');
        $this->iTypeInTheField($lng, 'Longitude', 'input');
        $this->map->iSeeTheLocationsPinOnTheMap('new');
    }
    protected function createTestLocationWithoutGps($name): void
    {
        $this->iOpenTheSubEntityForm('Location', 'sub', $name);
        $this->enterTestLocationDetails($name);
        $this->submitTheForm();
        $this->iWaitForTheFormToClose('sub');
    }
    protected function enterTestLocationDetails($name): void
    {
        $this->iSeeInTheField($name, 'DisplayName', 'input');
        $this->selectTextInCombobox('Country', 'Costa Rica');
        $this->selectTextInCombobox('HabitatType', 'Savanna');
        $this->iTypeInTheField('1500', 'Elevation', 'input');
        $this->iTypeInTheField('2500', 'ElevationMax', 'input');
    }
    /* ___________________________ EDIT _____________________________________ */
    /**
     * @Given I open the location edit-form for :name
     */
    public function iOpenTheLocationEditFormFor($name): void
    {
        // $this->feature->iAddToTheCombobox($name, $rank);
        // $this->iWaitForTheFormToOpen('sub2');
        // $this->iSeeInTheField($name, 'DisplayName', 'input', 'sub2');
        // $this->iSubmitTheForm();
        // $this->iWaitForTheFormToClose('sub2');
        // $this->feature->iShouldSeeInTheCombobox($name, $rank);
    }


    /* ----------------------------- TAXON -------------------------------------- */
    /**
     * @Given I create taxon: :rank :name
     * @Given I create taxon: :rank :name :type
     */
    public function iCreateTaxon($rank, $name, $type = 'standard'): void
    {
        $this->handleTaxonCreate($rank, $name);
        $displayName = $type === 'contribution' ? "*$name" : $name;
        $this->feature->iShouldSeeInTheCombobox($displayName, $rank);
    }
    protected function handleTaxonCreate($rank, $name): void
    {
        $this->feature->iAddToTheCombobox($name, $rank);
        $this->iWaitForTheFormToOpen('sub2');
        $this->iSeeInTheField($name, 'DisplayName', 'input', 'sub2');
        $this->iSubmitTheForm();
        $this->iWaitForTheFormToClose('sub2');
    }
    /* ------------------------------ SOURCE ------------------------------------ */
    private function getNextSubFormLevel(): string
    {
        $all = ['top', 'sub', 'sub2'];
        $current = array_search($this->getOpenFormPrefix(), $all);
        return $all[++$current];
    }
    /* __________________________ AUTHOR ____________________________________ */
    /**
     * @Given I create an author: :first :middle :last :sfx
     * @Given I create an author: "([^"]*)" "([^"]*)" "([^"]*)" "([^"]*)"
     */
    public function iCreateAnAuthor($first, $middle, $last, $sfx): void
    {
        $formLevel = $this->getNextSubFormLevel();
        $this->iOpenTheSubEntityForm('Author', $formLevel, 'create', true);
        $this->feature->iCheckTheShowAllFieldsBox();
        $this->enterAuthorName($first, $middle, $last, $sfx);
        $this->addWebsiteAndSubmitTheForm('author', $formLevel);
    }
    /**
     * @Given I create an editor, :first :middle :last :sfx
     */
    public function iCreateAnEditor($first, $middle, $last, $sfx): void
    {
        $formLevel = $this->getNextSubFormLevel();
        $this->iOpenTheSubEntityForm('Editor', $formLevel, 'create', true);
        $this->feature->iCheckTheShowAllFieldsBox();
        $this->enterAuthorName($first, $middle, $last, $sfx);
        $this->addWebsiteAndSubmitTheForm('editor', $formLevel);
    }

    private function enterAuthorName($first, $middle, $last, $sfx): void
    {
        $this->iTypeInTheField($last, 'LastName', 'input');
        $this->iTypeInTheField($first, 'FirstName', 'input');
        $this->iTypeInTheField($middle, 'MiddleName', 'input');
        $this->iTypeInTheField($last, 'LastName', 'input');
        $this->iTypeInTheField($sfx, 'Suffix', 'input');
    }
    private function addWebsiteAndSubmitTheForm($domain, $formLevel): void
    {
        $this->iTypeInTheField("https://www.$domain.com", 'Website', 'input', $formLevel);
        $this->submitTheForm();
        $this->wait('!$(".form-popup").length');
        $this->iPressSubmitInTheConfirmationPopup();
        $this->iWaitForTheFormToClose($formLevel);
    }
    /* __________________________ CITATION __________________________________ */
    /**
     * @Given I create a citation for :title
     */
    public function iCreateACitationFor($title): void
    {
        $this->iWaitForTheFormToOpen('sub');
        $this->enterCitationDetails($title);
        $this->submitTheForm();
        $this->iWaitForTheFormToClose('sub');
    }
    /**
     * @Given I enter citation data for :name
     */
    public function iEnterCitationDataFor($name): void
    {
        $this->iShouldSeeInTheFormHeader('Citation Create');
        $this->enterCitationDetails($name);
    }
    private function enterCitationDetails($title): void
    {
        switch ($title) {
                /* data_create */
            case 'Test Book':
                $this->enterTestBookCitationDetails();
                break;
            case 'Test Chapter Title':
                $this->enterTestCollectionCitationDetails($title);
                break;
            case 'Test Article Title':
                $this->enterTestArticleCitationDetails();
                break;
            case 'Test Dissertation':
                $this->enterTestDissertationCitationDetails();
                break;
            case 'Test Other':
                $this->enterTestOtherCitationDetails();
                /* contribute_create */
                break;
            case 'Quarantined Book':
                $this->enterQuarantinedBookCitationDetails();
                break;
            case 'Test Collection Chapter':
                $this->handleBasicCollectionDetails($title);
        }
    }
    private function enterTestBookCitationDetails(): void
    {
        $this->feature->iShouldSeeInTheCombobox('Book', 'CitationType');
        $this->iTypeInTheField('29', 'Edition', 'input');
        $this->iSeeInTheField('Callaye, B. J. 1990. Test Book. 29. Test Publisher, Nice, France.', 'Description', 'textarea');
    }
    private function enterQuarantinedBookCitationDetails(): void
    {
        $this->feature->iShouldSeeInTheCombobox('Book', 'CitationType');
        $this->iTypeInTheField('29', 'Edition', 'input');
        $this->iSeeInTheField('Ember, S., S. J. Smith & S. Ember. 1990. *Quarantined Book. 29. *Test Publisher, Nice, France.', 'Description', 'textarea');
    }
    private function enterTestCollectionCitationDetails($name): void
    {
        $this->handleBasicCollectionDetails($name);
        $this->feature->iChangeInTheDynamicCombobox('Cockle, Anya', 'Author');
        $this->iSeeInTheField('Cockle, A. 1990. Test Chapter Title. In: Test Collection (B. J. Callaye, ed.). pp. 666-999. Test Publisher, Nice, France.', 'Description', 'textarea');
    }
    private function handleBasicCollectionDetails($name): void
    {
        $this->feature->iShouldSeeInTheCombobox('Chapter', 'CitationType');
        $this->iTypeInTheField($name, 'DisplayName', 'input');
        $this->iTypeInTheField('666-999', 'Pages', 'input');
        $this->iTypeInTheField('Test Abstract Text', 'Abstract', 'textarea');
    }
    private function enterTestArticleCitationDetails(): void
    {
        $this->feature->iShouldSeeInTheCombobox('Article', 'CitationType');
        $this->feature->iChangeInTheDynamicCombobox('Cockle, Anya', 'Author');
        $this->iTypeInTheField('Test Article Title', 'DisplayName', 'input');
        $this->iTypeInTheField('1990', 'Year', 'input');
        $this->iTypeInTheField('666-999', 'Pages', 'input');
        $this->iTypeInTheField('Test Abstract Text', 'Abstract', 'textarea');
        $this->iTypeInTheField('4', 'Volume', 'input');
        $this->iTypeInTheField('1', 'Issue', 'input');
        $this->iSeeInTheField('Cockle, A. 1990. Test Article Title. Test Journal 4 (1): 666-999.', 'Description', 'textarea');
    }
    private function enterTestDissertationCitationDetails(): void
    {
        $this->feature->iShouldSeeInTheCombobox('Ph.D. Dissertation', 'CitationType');
        $this->iSeeInTheField('Callaye, B. J. 1990. Test Dissertation. Ph.D. Dissertation. Test Publisher, Nice, France.', 'Description', 'textarea');
    }
    private function enterTestOtherCitationDetails(): void
    {
        $this->feature->iShouldSeeInTheCombobox('Other', 'CitationType');
        $this->iSeeInTheField('Callaye, B. J. 1990. Test Other. Test Publisher, Nice, France.', 'Description', 'textarea');
    }
    /* __________________________ PUBLICATION _______________________________ */
    /**
     * @Given I open the publication form with data for :name
     */
    public function iOpenThePublicationFormWithDataFor($name): void
    {
        $this->iOpenTheSubEntityForm('Publication', 'sub', $name);
        $this->feature->iCheckTheShowAllFieldsBox();
        $this->iEnterThePublicationDetails($name);
    }
    private function iEnterThePublicationDetails($name): void
    {
        switch ($name) {
            case 'Quarantined Book':
            case 'Test Book':
                $this->enterTestBookPublicationDetails($name);
                break;
            case 'Test Collection':
                $this->enterTestCollectionPublicationDetails($name);
                break;
            case 'Test Journal':
                $this->enterTestJournalPublicationDetails($name);
                break;
            case 'Test Dissertation':
                $this->enterTestDissertationPublicationDetails($name);
                break;
            case 'Test Other':
                $this->enterTestOtherPublicationDetails($name);
        }
    }
    private function handleBasicPublicationDetails($name): void
    {
        $this->iSeeInTheField($name, 'DisplayName', 'input', 'sub');
        $this->iTypeInTheField('1990', 'Year', 'input');
        $this->iTypeInTheField('Test Description', 'Description', 'textarea');
        $this->iTypeInTheField("https://doi.org/10.1037/rmh0000008", 'Doi', 'input');
        $this->iTypeInTheField("https://www.publication.com", 'Website', 'input');
    }
    private function enterTestBookPublicationDetails($name): void
    {
        $this->handleBasicPublicationDetails($name);
        $this->selectTextInCombobox('PublicationType', 'Book');
    }
    private function enterTestCollectionPublicationDetails($name): void
    {
        $this->enterTestBookPublicationDetails($name);
        $this->feature->iChangeInTheDynamicCombobox('Callaye, Bendry J Jr', 'Editor');
        $this->selectTextInCombobox('Publisher', 'Test Publisher');
    }
    private function enterTestJournalPublicationDetails($name): void
    {
        $this->iSeeInTheField($name, 'DisplayName', 'input', 'sub');
        $this->selectTextInCombobox('PublicationType', 'Journal');
    }
    private function enterTestDissertationPublicationDetails($name): void
    {
        $this->handleBasicPublicationDetails($name);
        $this->selectTextInCombobox('PublicationType', 'Thesis/Dissertation');
        $this->feature->iChangeInTheDynamicCombobox('Callaye, Bendry J Jr', 'Author');
        $this->selectTextInCombobox('Publisher', 'Test Publisher');
    }
    private function enterTestOtherPublicationDetails($name): void
    {
        $this->handleBasicPublicationDetails($name);
        $this->selectTextInCombobox('PublicationType', 'Other');
        $this->feature->iChangeInTheDynamicCombobox('Callaye, Bendry J Jr', 'Author');
        $this->selectTextInCombobox('Publisher', 'Test Publisher');
    }
    /* __________________________ PUBLISHER _________________________________ */
    /**
     * @Given I create a publisher, :name
     */
    public function iCreateAPublisher($name): void
    {
        $formLevel = $this->getNextSubFormLevel();
        $this->iOpenTheSubEntityForm('Publisher', $formLevel, $name);
        $this->feature->iCheckTheShowAllFieldsBox();
        $this->iSeeInTheField($name, 'DisplayName', 'input', $formLevel);

        $this->iTypeInTheField('Nice', 'City', 'input');
        $this->iTypeInTheField('France', 'Country', 'input');
        $this->iTypeInTheField('Publisher Description', 'Description', 'textarea');
        $this->addWebsiteAndSubmitTheForm('publisher', $formLevel);
    }
    /* ========================== FIELDS ==================================== */
    /**
     * @Given I open the :field select-form
     */
    public function iOpenTheTaxonSelectForm($field): void
    {
        $selId = $this->getComboId($field);
        $this->wait("$('$selId').length;");
        $this->execute("$('$selId')[0].selectize.focus();");
        $this->iWaitForTheFormToOpen('sub');
    }

    /**
     * @Given I pin the :prop field
     */
    public function iPinTheField($prop): void
    {
        $selector = '#' . $this->getFieldName($prop) . '_pin';
        $this->execute("$('$selector').trigger('click');");
        $checked = $this->evaluate("$('$selector').prop('checked');");
        $this->handleEqualAssert($checked, true, true, "The [$prop] field is not checked.");
    }
    /* ------------------------------ SET ----------------------------------- */
    /**
     * @When I change the :prop field :type to :text
     */
    public function iChangeTheFieldTo($prop, $type, $text, $formLevel = false): void
    {
        $selector = $this->getInputSelector($prop, $formLevel);
        $this->addValueToFormInput($selector, $text);
        $this->assertFieldValueIs($text, $selector);
    }
    /**
     * @When I type :text in the :prop field :type
     * @When I type :text in the :prop field :type :formLevel
     */
    public function iTypeInTheField($text, $prop, $type, $formLevel = false): void
    {
        $selector = $this->getInputSelector($prop, $formLevel);
        $this->addValueToFormInput($selector, $text);
    }

    /**
     * @Given I fill the new interaction form with the test values
     */
    public function iFillTheNewInteractionFormWithTheTestValues(): void
    {
        $srcLocData = [
            'Publication' => "Test Collection",
            'Citation Title' => "Test Chapter Title",
            'Country-Region' => 'Costa Rica',
            'Location' => "Test Location With GPS"
        ];
        $this->fillSrcAndLocFields($srcLocData);
        $taxaData = [
            'Genus' => 'BatB',
            'Species' => "BugD species"
        ];
        $this->fillTaxaFields($taxaData);
        $miscData = [
            'Type' => 'Host (host of)',
            'Source' => 'Secondary',
            'Note' => 'Detailed interaction notes.',
            'Season' => 'Dry',
            'Date' => 'Select an easy date to remember.',
            'Pages' => '123-345'
        ];
        $this->fillMiscIntFields($miscData);
    }
    private function addValueToFormInput($selector, $text): void
    {
        $this->spin(function () use ($selector, $text) {
            $input = $this->getMinkPage()->find('css', $selector);
            if (!$input) {
                return false;
            }
            if ($input->getValue() == $text) {
                return true;
            }
            $input->setValue($text);
            $this->execute("$('$selector').val('$text').change();");
            return $input->getValue() == $text;
        }, "Could not set [$text] in [$selector]");
    }
    public function fillSrcAndLocFields($data): void
    {
        $this->log("\n        Filling Source and Location fields.\n");
        foreach ($data as $field => $value) {
            $this->selectTextInCombobox($field, $value);
        }
    }
    public function fillTaxaFields($data): void
    {
        $this->log("\n        Filling Taxa fields.\n");
        $ranks = array_keys($data);
        $this->iOpenTheTaxonSelectForm('Subject');
        $this->selectTextInCombobox($ranks[0], $data[$ranks[0]]);
        $this->pressTheButton('Select Taxon');
        $this->iWaitForTheFormToClose('sub');
        $this->iOpenTheTaxonSelectForm('Object');
        $this->selectTextInCombobox('Group', 'Arthropod');
        $this->selectTextInCombobox($ranks[1], $data[$ranks[1]]);
        $this->pressTheButton('Select Taxon');
        $this->iWaitForTheFormToClose('sub');
    }
    public function fillMiscIntFields($data): void
    {
        $this->log("\n        Filling Misc fields.\n");
        $this->selectTextInCombobox('Interaction Type', $data['Type']);
        $this->iTypeInTheField($data['Note'], 'Note', 'textarea');
        $this->iTypeInTheField('This is a quote.', 'Quote', 'textarea');
        if (array_key_exists('Pages', $data)) {
            $this->iTypeInTheField($data['Pages'], 'Pages', 'input');
        }
        if (array_key_exists('Source', $data)) {
            $this->selectTextInCombobox('Source', $data['Source']);
        }
        if (array_key_exists('Tag', $data)) {
            $this->selectTextInCombobox('Interaction Tags', $data['Tag']);
        }
//        if (array_key_exists('Date', $data)) {
//            // $this->iPutABreakpoint("MANUAL -> Select [" . $data['Date'] . "]");
//            // $this->iSetTheDateFilterTo('updated', ,$data['Date']);
//        }
    }
    /* ------------------------------ ASSERT -------------------------------- */
    /**
     * @When I see :text in the :prop field :type
     * @Then I should see :text in the :prop field :type
     */
    public function iSeeInTheField($text, $prop, $type, $formLevel = false): void
    {
        $selector = $this->getInputSelector($prop, $formLevel);
        $this->assertFieldValueIs($text, $selector);
    }
    /**
     * @Then the :prop field should be empty
     */
    public function theFieldShouldBeEmpty($prop): void
    {
        $selector = $this->getInputSelector($prop);
        $val = $this->evaluate("$('$selector')[0].innerText");
        $this->handleEqualAssert($val, '', true, "The [$prop] should be empty.");
    }
    private function assertFieldValueIs($text, $fieldId, $isIn = true): void
    {
        $should_nt = $isIn ? 'Should' : "Shouldn't";
        $this->spin(function () use ($text, $fieldId, $isIn) {
            $fieldVal = $this->getFieldTextOrValue($fieldId);
            return $isIn ? $fieldVal == $text : $fieldVal != $text;
        }, "$should_nt  have found [$text] in [$fieldId].");
    }
    /* ========================== ALERTS ==================================== */
    /**
     * @Then I click on the :name alert
     */
    public function iClickOnTheAlert($name): void
    {
        $id = $this->getAlertId($name);
        $this->execute("$('$id').trigger('click')");
    }
    /* ------------------------------ CLEAR --------------------------------- */
    /**
     * @Then I select the existing entity in the duplicate-data alert
     */
    public function selectExistingEntityInAlert(): void
    {
        $formLevel = $this->getOpenFormPrefix();
        $this->wait("$('#" . $formLevel . "_alert').length");
        $this->execute("$('#" . $formLevel . "_alert p').trigger('click');");
        $this->iWaitForTheFormToClose($formLevel);
    }
    /* ------------------------------ ASSERT -------------------------------- */
    /**
     * @Then I should see a form :name alert
     */
    public function iShouldSeeAnAlertForTheField($name): void
    {
        $id = $this->getAlertId($name);

        $this->spin(fn() => $this->evaluate("$('$id')[0].innerHTML !== ''"), "No alert found for the [$name] field.");
    }
    /**
     * @Then the alert for the :name field should clear
     */
    public function theAlertForTheFieldShouldClearItself($name): void
    {
        $id = $this->getAlertId($name);

        $this->spin(fn() => $this->evaluate("$('$id')[0].innerHTML === ''"), "Alert still shown for the [$name] field.", 6);
    }
    /* ========================== SUBMIT ======================================== */
    /**
     * @Given I submit the form
     */
    public function iSubmitTheForm($text = null): void
    {
        $this->submitTheForm($text);
    }
    /**
     * @Given I submit the form from the confirmation popup
     */
    public function iSubmitTheFormFromTheConfirmationPopup($text = null): void
    {
        $formLevel = $this->getOpenFormPrefix();
        $this->submitTheForm($text);
        $this->iPressSubmitInTheConfirmationPopup($formLevel);
    }
    /**
     * @When I wait for form to submit successfully
     */
    public function iWaitForFormToSubmitSuccessfully(): void
    {
        $this->wait("!$('#c-overlay').length");
    }
    /**
     * @When I press submit in the confirmation popup
     * @When I press submit in the :level confirmation popup
     */
    public function iPressSubmitInTheConfirmationPopup($level = false): void
    {
        $formLevel = $level ?: $this->getOpenFormPrefix();
        $this->ensureModalOpened();
        $this->submitTheForm(''); //click submit, but skip form-close check
        $this->submitModalPopup($formLevel);
    }
    private function submitModalPopup($formLevel): void
    {
        $noSubmitOverlay = "!$('.introjs-helperLayer').length";
        $noFormElem = "!$('#$formLevel-form').length;";

        $this->spin(function () use ($noSubmitOverlay, $noFormElem) {
            try {
                $this->execute("$('.introjs-tooltipbuttons input').trigger('click');");
                $this->wait($noSubmitOverlay);
                $this->wait($noFormElem);
                return $this->execute($noSubmitOverlay) && $this->wait($noFormElem);
            } catch (\Exception) {
                return false;
            } finally {
                return true;
            }
        }, "Confirmation popup and form{[$formLevel}] did not close.");
    }
    /* ---------------------------- DELETE ---------------------------------- */
    /**
     * @Given I delete the entity from the confirmation popup
     */
    public function iDeleteTheEntityFromTheConfirmationPopup(): void
    {
        $this->submitTheForm(null, 'delete');
        $this->ensureModalOpened();
        $this->submitModalPopup('top');
    }



    /* ======================= OPEN/CLOSE FORM ============================== */
    /* ------------------------------ OPEN ---------------------------------- */
    /**
     * @Given I open the new Interaction form
     */
    public function iOpenTheNewInteractionForm(): void
    {
        $this->getMinkPage()->pressButton('New');
    }
    /**
     * @When I wait for the :type form to open
     */
    public function iWaitForTheFormToOpen($type): void
    {
        $this->spin(fn() => $this->getMinkPage()->find('css', "#$type-form"), "Form [$type] did not open.");
    }
    /**
     * @Given I open the :field :type form for :value
     */
    public function iOpenTheSubEntityForm($field, $type, $value, $isDynamic = false): void
    {
        if ($isDynamic) {
            $this->feature->iAddToTheDynamicCombobox($value, $field);
        } else {
            $this->feature->iAddToTheCombobox($value, $field);
        }
        $this->iWaitForTheFormToOpen($type);
    }
    /* ------------------------------ CLOSE --------------------------------- */
    /**
     * @When I exit the form window
     */
    public function iExitTheFormWindow(): void
    {
        try {
            $this->execute("$('#exit-form').trigger('click');");
        } catch (\Exception) {
            $this->iPutABreakpoint('Error while exiting form.');
            // print($e);
        }
    }
    /**
     * @When I wait for the :type form to close
     */
    public function iWaitForTheFormToClose($type): true
    {
        $this->spin(function () use ($type) {
            $form = $this->getMinkPage()->find('css', "#$type-form");    //$this->log("\niWaitForTheFormToClose ? ". (!!$form === true ? 'true' : 'false'));
            return !$form;
        }, "Form [$type] did not close.");
        return true;
    }
    /* ========================== ASSERTS =================================== */
    /**
     * @Then I should see :text in the :entity detail panel
     */
    public function iShouldSeeInTheDetailPanel($text, $entity): void
    {
        $elemId = '#' . strtolower(substr((string) $entity, 0, 3)) . '-det';
        $elem = $this->getMinkPage()->find('css', $elemId);
        $this->handleContainsAssert(
            $text,
            $elem->getHtml(),
            true,
            "Should have found [$text] in [$entity] details"
        );
    }
    /**
     * @Then I (should) see :text in the form header
     */
    public function iShouldSeeInTheFormHeader($text): void
    {
        $this->getUserSession()->wait(10000, "$('#success p').length;");
        $formId = $this->getOpenFormId();
        $elem = $this->getMinkPage()->find('css', $formId);
        $html = $elem->getHtml();
        $this->handleNullAssert($html, false, 'Nothing found in header. Expected [' . $text . ']');
        $this->handleContainsAssert(
            $text,
            $html,
            true,
            "Should have found [$text] in the form header."
        );
    }
    /**
     * @Then the coordinate fields should be filled
     */
    public function theCoordinateFieldsShouldBeFilled(): void
    {
        $this->iPutABreakpoint("theCoordinateFieldsShouldBeFilled");
    }
}
