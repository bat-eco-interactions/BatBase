@data-init
Feature: Explore Page Database Initialization
    In order to find data about bat eco-interactions
    As a web visitor
    I need to be able to load the local database

    ### WHAT IS BEING TESTED ###
    # INITIAL DATA LOADS, STORES, AND DISPLAYS AS EXPECTED

    Background:
        Given I am on "/explore"
        And the database has loaded
        Then I exit the tutorial

    ###----------------------- Taxon View -----------------------------------###
    @javascript
    Scenario:  There should be 17 initial bat interactions in the data table.
        Given I view interactions by "Bats"
        And I break "Open console"
        Then I see "Order Chiroptera"
        And the count column should show "17" interactions
        And data in the interaction rows

    @javascript
    Scenario:  There should be 7 initial plant interactions in the data table.
        Given I view interactions by "Plants"
        Then I see "Kingdom Plantae"
        And the count column should show "7" interactions
        And data in the interaction rows

    @javascript
    Scenario:  There should be 3 initial arthropod interactions in the data table.
        Given I view interactions by "Arthropods"
        Then I see "Phylum Arthropoda"
        And the count column should show "3" interactions
        And data in the interaction rows

    @javascript
    Scenario:  There should be 2 initial Parasite interactions in the data table.
        Given I break
        Given I view interactions by "Parasites"
        Then I see "Family Orange"
        And the count column should show "1" interactions
        And data in the interaction rows

    @javascript
    Scenario:  There should be 2 initial Worm interactions in the data table.
        Given I view interactions by "Worms"
        Then I see "Phylum Acanthocephala"
        And the count column should show "4" interactions
        And data in the interaction rows
    ##------------------------- Location View -------------------------------###
    @javascript
    Scenario:  There should be 3 region location in initial the data table.
        Given the explore table is focused on "Location"
        Then I should see "3" rows in the table data tree
        And I see "Central America"
        And data in the interaction rows

    @javascript
    Scenario:  There should be 8 interactions in the initial map view.
        Given the explore table is focused on "Location"
        And I display locations in "Map Data" View
        Then I should see "8" interactions shown on the map
        And I should see markers on the map

    ##------------------------- Source View ---------------------------------###
    @javascript
    Scenario:  There should be 4 publications in the initial data table.
        Given the explore table is focused on "Source"
        Then I should see "4" rows in the table data tree
        And I see "Journal of Mammalogy"
        And data in the interaction rows

    @javascript
    Scenario:  There should be 4 authors in the initial data table.
        Given the explore table is focused on "Source"
        And I view interactions by "Authors"
        Then I should see "4" rows in the table data tree
        And I see "Cockle, Anya"
        And data in the interaction rows

    @javascript
    Scenario:  There should be 3 publishers in the initial data table.
        Given the explore table is focused on "Source"
        And I view interactions by "Publishers"
        Then I should see "3" rows in the table data tree
        And I see "University of Paris VI"
        And data in the interaction rows
