@data-ui
Feature: Explore page features and data table controls
    In order to interact with the database
    As a web visitor
    I should be able to use the various features and controls on the explore page

    ### WHAT IS BEING TESTED ###
    # EXPAND AND COLLAPSE ROWS in data table
    # START TUTORIAL
    # OPEN SEARCH TIPS
    # DISPLAY INTERACTIONS IN TABLE ON THE MAP
    # RETURN THEM TO THE TABLE
    ## TODO
    # CSV downloads
    # run through all slides in tutorial

    Background:
        Given I am on "/explore"
        And the database has loaded
        And I exit the tutorial
        And the explore table is focused on "Taxon"
        And I view interactions by "Bats"
## --------------- EXPAND AND COLLAPSE ROWS IN DATA TABLE ----------- ##
    @javascript
    Scenario:  I should be able to expand the data tree completely
        Given I see "3" rows in the table data tree
        And I break "Open console"
        When I press "Expand All"
        Then I should see "17" interactions in the table
        Then I should see "30" rows in the table data tree

    @javascript
    Scenario:  I should be able to collapse the data tree completely
        Given I press "Expand All"
        And I see "27" rows in the table data tree
        When I press "Collapse All"
        Then I should see "1" rows in the table data tree

    @javascript
    Scenario:  I should be able to expand the data tree by one
        Given I see "3" rows in the table data tree
        When I press "xpand-1"
        And I should see "8" rows in the table data tree

    @javascript
    Scenario:  I should be able to collapse the data tree by one
        Given I see "3" rows in the table data tree
        When I press "collapse-1"
        And I should see "1" rows in the table data tree

    @javascript
    Scenario:  The toggle tree button text should sync with tree state.
        Given I see "3" rows in the table data tree
        When I press "xpand-1" "3" times
        Then I should see "27" rows in the table data tree
        And i see "Collapse All"

    @javascript
    Scenario:  The toggle tree button text should sync with tree state.
        Given I press "xpand-1" "3" times
        And I see "27" rows in the table data tree
        And I see "Collapse All"
        When I press "collapse-1"
        Then i see "Expand All"
## ------------------ SHOW SEARCH TIPS ---------------------------------- ##
    @javascript
    Scenario:  I should be able to show the search tips
        When I press "Tips"
        Then i see "Tips for searching"
# ------------------ START TUTORIAL ------------------------------------- ##
    @javascript
    Scenario:  I should be able to start the tutorial
        When I press "Tutorial"
        Then i see "Table Data"

    @javascript
    Scenario:  I should be able to jump to the map section of the tutorial
        When I press "Tutorial"
        And I break "Click 'Map View' button"
        # And I press the "Map View" button
        Then I see "Map View"
## --------- DISPLAY INTERACTIONS IN TABLE ON MAP ----------------------- ##
    @javascript
    Scenario:  I should be able to show interactions with gps data on the map
        Given the explore table is focused on "Source"
        When I select "Journal" from the "Publication Type Filter" combobox
        And I see "Journal of Mammalogy"
        And I should see "2" rows in the table data tree
        And I press the "Map Interactions" button
        Then I should see "1" interactions shown on the map
        And I click on a map marker
        And I should see "Journal of Mammalogy -" in popup

    @javascript
    Scenario:  I should be able to view interactions on the map
        Given the explore table is focused on "Location"
        When I select the Location view "Map Data"
        Then I should see the map with markers

    @javascript
    Scenario:  I should be able to show interactions in the table from a map marker
        Given the explore table is focused on "Location"
        And I select the Location view "Map Data"
        When I click on a map marker
        And I press the "Show Interactions In Data-Table" button
        Then I should see data in the interaction rows

    @javascript
    Scenario:  I should be able to view a specific location on the map
        Given the explore table is focused on "Location"
        When I expand "Central America" in the data tree
        And I expand "Panama" in the data tree
        And I click on the map pin for "Summit Experimental Gardens"
        Then I should see the map with the location summary popup
        And i see "Habitat: forest"
        And i see "Cited bats: Artibeus lituratus"
