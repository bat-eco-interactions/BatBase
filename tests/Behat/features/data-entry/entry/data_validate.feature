@data-validate
Feature: Custom form-validation alerts editor when there are validation issues.
    In order to enter accurate data into the bat eco-interactions database
    As an editor
    I need to see UI alerts when there are validation issues in the form data.

    ### WHAT IS BEING TESTED ###
    #       VALID INTERACTION TYPES LOAD AUTOMATICALLY
    #       DUPLICATE-ENTITY ALERT AND RESOLVE (AUTHOR/EDITOR, LOCATION, TAXON)
    #       VARIOUS FIELD-SPECIFIC VALIDATION
    #       ENTITY WITH DEPENDENT-DATA CAN'T BE DELETED
    #
    #   TOC
    #       INTERACTION
    #       SOURCE
    #       LOCATION
    #       TAXON

    Background:
        Given the fixtures have been reloaded
        And I log in as "Test Editor"
        And I am on "/explore"
        And the database has loaded
        And I exit the tutorial
    ## ========================== INTERACTION ================================= ##
    @javascript
    Scenario:  I should see an alert if there are no valid interaction types for
        the selected taxon groups

        Given I press the "New" button
        And I break "Open console"
        And I see "Interaction Create"
        ## --- SUBJECT --- ##
        When I open the "Subject" select-form
        And I see "Select Subject Taxon"
        And I select "Reptile" from the "Group" combobox
        And I press the "Select Unspecified" button
        And I wait for the "sub" form to close
        ## --- OBJECT --- ##
        When I open the "Object" select-form
        And I see "Select Object Taxon"
        And I select "Bird" from the "Group" combobox
        And I press the "Select Unspecified" button
        And I wait for the "sub" form to close
        ## --- ALERT --- ##
        Then I should see a form "Interaction Type" alert
        And I open the "Subject" select-form
        And I see "Select Subject Taxon"
        And I select "Bat" from the "Group" combobox
        And I press the "Select Unspecified" button
        And the alert for the "Interaction Type" field should clear
        And I break "Check for unhandled errors"

    ## ========================== SOURCE ====================================== ##

    @javascript
    Scenario:  I should see an alert if I try to create an author|editor with the same
        display name as another author|editor.

        Given I press the "New" button
        And I see "Interaction Create"
        And I add "Test Book" to the "Publication" combobox
        And I see "Publication Create"
        And I select "Book" from the "Publication Type" combobox
        When I add "New Author" to the "Author" dynamic combobox
        ## --- ALERT --- ##
        And I type "Anya" in the "First Name" field "input"
        And I type "Cockle" in the "Last Name" field "input"
        # Note: Validation happens on form-submit
        And I submit the form
        Then I should see a form "sub2" alert
        ## --- RESOLVE --- ##
        And I select the existing entity in the duplicate-data alert
        And I should see "Cockle, Anya" in the "Author" dynamic combobox
        And I break "Check for unhandled errors"

    @javascript
    Scenario:  I should see an alert if there is a blank in the selected author order
        Given I press the "New" button
        And I see "Interaction Create"
        And I add "Test Book" to the "Publication" combobox
        And I see "Publication Create"
        And I select "Book" from the "Publication Type" combobox
        And I select "Bloedel, P" from the "Editor" dynamic combobox
        And I select "Cockle, Anya" from the "Editor" dynamic combobox
        And I select "Baker, Herbert G" from the "Editor" dynamic combobox
        ## --- ALERT --- ##
        When I clear the "2nd" "Editor" dynamic combobox
        Then I should see a form "Editor" alert
        And I break "Check for unhandled errors"

    #Note: Validation happens on form-submit
    @javascript
    Scenario:  I should see an alert if I enter an invalid page-range
        Given I press the "New" button
        And I see "Interaction Create"
        And I select "Journal of Mammalogy" from the "Publication" combobox
        And I add "Test Citation" to the "Citation Title" combobox
        And I select "Baker, Herbert G" from the "Author" dynamic combobox
        And I type "2000" in the "Year" field "input"
        ## --- ALERT --- ##
        When I type "999-666" in the "Pages" field "input" "sub"
        And I submit the form
        Then I should see a form "Pages" alert
        And I type "666-999" in the "Pages" field "input"
        And I type "666-999" in the "Pages" field "input" "sub"
        And the alert for the "Pages" field should clear
        And I break "Check for unhandled errors"

    @javascript
    Scenario:  I should not be able to delete a source with dependent-data Given the explore table is focused on "Source"
        Given the explore table is focused on "Source"
        And I expand "Revista de Biologia Tropical" in the data tree
        And I click on the edit pencil for the "Two cases of bat pollination in Central America" row
        And I see "Citation Edit"
        And I delete the entity from the confirmation popup
        Then I should see a form "top" alert
        And I break "Check for unhandled errors"
    ## ========================== LOCATION ==================================== ##
    @javascript
    Scenario:  I should see an alert if I create an unspecific location that already exists
        Given I press the "New" button
        And I see "Interaction Create"
        And I add "New" to the "Location" combobox
        And I see "Location Create"
        ## --- ALERT --- ##
        When I type "Name" in the "Display Name" field "input"
        And I select "Costa Rica" from the "Country" combobox
        And I select "Savanna" from the "Habitat Type" combobox
        And I submit the form
        Then I should see a form "sub" alert
        And I select the existing entity in the duplicate-data alert
        And I should see "Costa Rica-Savanna" in the "Location" combobox
        And I break "Check for unhandled errors"

    @javascript
    Scenario:  I should see an alert if I enter an invalid elevation-range
        Given I press the "New" button
        And I see "Interaction Create"
        And I add "New" to the "Location" combobox
        And I see "Location Create"
        And I select "Costa Rica" from the "Country" combobox
        ## --- ALERT --- ##
        When I type "600" in the "Elevation" field "input"
        And I type "300" in the "Elevation Max" field "input"
        And I submit the form
        Then I should see a form "Elevation Max" alert
        When I type "900" in the "Elevation Max" field "input"
        And the alert for the "Elevation Max" field should clear
        And I break "Check for unhandled errors"

    # @javascript  //todo
    # Scenario:  I should see an alert if I enter an invalid longitude and latitude
    #     Given I press the "New" button
    #     And I see "Interaction Create"
    #     And I add "New" to the "Location" combobox
    #     And I see "Location Create"
    #     And I select "Costa Rica" from the "Country" combobox
    #     ## --- ALERT --- ##
    #     When I type "600" in the "Elevation" field "input"
    #     And I type "300" in the "Elevation Max" field "input"
    #     And I submit the form
    #     Then I should see a form "Elevation Max" alert
    #     When I type "900" in the "Elevation Max" field "input"
    #     And the alert for the "Elevation Max" field should clear

    @javascript
    Scenario:  I should not be able to delete a location with dependent-data
        Given the explore table is focused on "Location"
        And I expand "Central America" in the data tree
        And I expand "Costa Rica" in the data tree
        And I click on the edit pencil for the "Santa Ana-Forest" row
        And I see "Location Edit"
        And I delete the entity from the confirmation popup
        Then I should see a form "top" alert
        And I break "Check for unhandled errors"
    ## =========================== TAXON ====================================== ##
    @javascript
    Scenario:  I should see an alert if I try to create a genus taxon without a
        Family selected AND if I try to create a species taxon without a
        Genus selected AND if I try to save a species with an incorrect binomial

        Given I press the "New" button
        And I see "Interaction Create"
        And I open the "Subject" select-form
        And I see "Select Subject Taxon"
        ## --- ALERT --- ##
        When I add "test" to the "Genus" combobox
        Then I should see a form "Genus" alert
        And I select "Phyllostomidae" from the "Family" combobox
        And the alert for the "Genus" field should clear
        ## --- ALERT --- ##
        When I add "test" to the "Species" combobox
        Then I should see a form "Species" alert
        And I select "Artibeus" from the "Genus" combobox
        And the alert for the "Species" field should clear
        ## --- ALERT --- ##
        When I add "Test Species" to the "Species" combobox
        And I see "Taxon Create"
        And I should see a form "DisplayName" alert
        And I type "Artibeus Species" in the "Display Name" field "input"
        And I submit the form
        And I wait for the "sub2" form to close
        And I break "Check for unhandled errors"

    @javascript
    Scenario:  I should see an alert if I try to change the level of a genus taxon
        with species children

        Given the explore table is focused on "Taxon"
        And I view interactions by "Bats"
        And I expand "Family Phyllostomidae" in the data tree
        And I click on the edit pencil for the "Genus Artibeus" row
        And I see "Taxon Edit"
        ## --- ALERT --- ##
        When I select "Family" from the "Rank" combobox
        Then I should see a form "Rank" alert
        And I select "Genus" from the "Rank" combobox
        And the alert for the "Rank" field should clear
        And I break "Check for unhandled errors"

    @javascript
    Scenario:  I should see an alert if I try to set a non-genus parent for a species taxon
        Given the explore table is focused on "Taxon"
        And I view interactions by "Bats"
        And I expand "Family Phyllostomidae" in the data tree
        And I expand "Genus Artibeus" in the data tree
        And I click on the edit pencil for the "Artibeus lituratus" row
        And I see "Taxon Edit"
        And I open the "Parent" select-form
        ## --- ALERT --- ##
        And I press the "Select Unspecified" button
        Then I should see a form "Parent" alert
        And I press the "Select Taxon" button
        And I wait for the "sub2" form to close
        And I break "Check for unhandled errors"

    @javascript
    Scenario:  I should see an alert if I try to select a rank that is lower than
        that of the taxon's children AND if I select a taxon-parent that is a
        lower rank than the taxon being edited.

        Given the explore table is focused on "Taxon"
        And I view interactions by "Bats"
        And I click on the edit pencil for the "Family Phyllostomidae" row
        And I see "Taxon Edit"
        ## --- ALERT --- ##
        When I select "Genus" from the "Rank" combobox
        Then I should see a form "Rank" alert
        And I select "Family" from the "Rank" combobox
        And the alert for the "Rank" field should clear
        ## --- ALERT --- ##
        And I open the "Parent" select-form
        And I see "Select Parent Taxon"
        When I select "Rhinophylla" from the "Genus" combobox
        And I press the "Select Taxon" button
        Then I should see a form "Parent" alert
        And I break "Check for unhandled errors"

    @javascript
    Scenario:  I should not be able to delete a taxon with dependent-data
        Given the explore table is focused on "Taxon"
        And I view interactions by "Plants"
        And I expand "Family Lauraceae" in the data tree
        And I click on the edit pencil for the "Genus Beilschmiedia" row
        And I see "Taxon Edit"
        And I delete the entity from the confirmation popup
        Then I should see a form "top" alert
        And I break "Check for unhandled errors"
