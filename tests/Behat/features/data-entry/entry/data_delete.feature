@data-delete
Feature: Delete data from the database
    In order to have accurate data about bat eco-interactions
    As an editor
    I need to be able to remove data from the database

    ### WHAT IS BEING TESTED ###
        # ALL ENTITY TYPES ARE "DELETED" AND HIDDEN FROM THE UI

    Background:
        Given I am on "/login"
        And I log in as "Test Manager"
        And I am on "/explore"
        And the database has loaded
        And I exit the tutorial

    @javascript
    Scenario:  I delete an interaction
        Given the explore table is focused on "Source"
        And I expand "Revista de Biologia Tropical" in the data tree
        And I click on the edit pencil for the first interaction of "Two cases of bat pollination in Central America"
        And I see "Interaction Edit"
        And I delete the entity from the confirmation popup
        And I expand "Revista de Biologia Tropical" in the data tree
        And I click on the edit pencil for the first interaction of "Two cases of bat pollination in Central America"
        And I see "Interaction Edit"
        And I delete the entity from the confirmation popup
        And I expand "Revista de Biologia Tropical" in the data tree
        Then I should see "0" interactions under "Two cases of bat pollination in Central America"

    @javascript
    Scenario:  I delete a location
        Given the explore table is focused on "Location"
        And I expand "Central America" in the data tree
        And I expand "Costa Rica" in the data tree
        And I click on the edit pencil for the "Santa Ana-Forest" row
        And I see "Location Edit"
        And I delete the entity from the confirmation popup
        And I expand "Central America" in the data tree
        And I should not see "Santa Ana-Forest" under "Costa Rica" in the tree
        And I break "Check for errors"

    @javascript
    Scenario:  I delete all source-types (todo: all remaining types)
        Given the explore table is focused on "Source"
        And I expand "Revista de Biologia Tropical" in the data tree
        And I click on the edit pencil for the "Two cases of bat pollination in Central America" row
        And I see "Citation Edit"
        And I delete the entity from the confirmation popup
        And I should not see "Two cases of bat pollination in Central America" under "Revista de Biologia Tropical" in the tree
        And I break "Check for errors"

    @javascript
    Scenario:  I delete a taxon
        Given the explore table is focused on "Taxon"
        And I view interactions by "Plants"
        And I expand "Family Fabaceae" in the data tree
        And I expand "Genus Mucuna" in the data tree
        And I click on the edit pencil for the "Mucuna andreana" row
        And I see "Taxon Edit"
        And I delete the entity from the confirmation popup
        And I expand "Family Fabaceae" in the data tree
        And I break "I should not see [Mucuna andreana] under [Genus Mucuna]"
