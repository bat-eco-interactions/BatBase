@data-create
Feature: Add new data to the database
    In order to have accurate data about bat eco-interactions
    As an editor
    I need to be able to add new data to the database

    ### WHAT IS BEING TESTED ###
        # INTERACTION CREATE FORM FILL, SUBMISSION, AND TABLE RELOADS
        # ALL SUB-ENTITY CREATE FORMS
        # AUTO-GENERATION OF CITATION TEXT
        ## TODO
        # VARIOUS SOURCE TYPE SELECTIONS and their respective UI changes
    ### NOTE
        # SOME STEPS ARE COMBINED TO MAKE TESTING FASTER AFTER CONFIRMING EACH
        # SECTION WORKS AS EXPECTED.

    Background:
        Given I log in as "Test Editor"
        And I am on "/explore"
        And the database has loaded
        And I exit the tutorial

## ======= SINGLE SCENARIO COMBINING THE TESTS COMMENTED BELOW ============== ##
## Creates all sub-entities ##
    @javascript
    Scenario:  I create all possible sub-entities in the interaction form.
        Given I press the "New" button
        And I see "Interaction Create"
        ## Publication Book ##
        And I open the publication form with data for "Test Book"
        And I create a publisher, "Test Publisher"
        And I create an author: "Bendry" "J" "Callaye" "Jr"
        And I submit the form from the confirmation popup
        ## Book Citation ##
        And I create a citation for "Test Book"
        ## Publication Collection ##
        And I open the publication form with data for "Test Collection"
        And I submit the form from the confirmation popup
        ## Chapter Citation ##
        And I create a citation for "Test Chapter Title"
        ## Publication Journal ##
        And I open the publication form with data for "Test Journal"
        And I submit the form
        ## Article Citation ##
        And I create a citation for "Test Article Title"
        ## Publication Thesis/Dissertation ##
        And I open the publication form with data for "Test Dissertation"
        And I submit the form from the confirmation popup
        ## Article Citation ##
        And I create a citation for "Test Dissertation"
        ## Publication Other ##
        And I open the publication form with data for "Test Other"
        And I submit the form from the confirmation popup
        ## Other Citation ##
        And I create a citation for "Test Other"
        ## ---- Location --- ##
        ## With GPS ##
        And I open the location form with data for "Test Location With GPS"
        ## Without GPS ##
        And I open the location form with data for "Test Location Without GPS"
        ## --- Taxon --- ##
        ### Subject ##
        And I open the "Subject" select-form
        ### Subject Family ##
        And I create taxon: "Family" "BatA"
        # ### Subject Genus ##
        And I create taxon: "Genus" "BatB"
        # ### Subject Species ##
        And I create taxon: "Species" "BatB species" ""
        And I press the "Select Taxon" button
        And I wait for the "sub" form to close
        ### Object ##
        And I open the "Object" select-form
        ### Object Group-Root ##
        # And I select "Worm" from the "Group" combobox
        # And I should see "Phylum Acanthocephala" in the "Group-Root" combobox
        # And I select "Phylum Nematoda" from the "Group-Root" combobox
        ### Object Class ##
        And I select "Arthropod" from the "Group" combobox
        And I create taxon: "Class" "BugA"
        ### Object Order ##
        And I create taxon: "Order" "BugB"
        ### Object Family ##
        And I create taxon: "Family" "BugC"
        ### Object Genus ##
        And I create taxon: "Genus" "BugD"
        # ### Subject Species ##
        And I create taxon: "Species" "BugD species" ""
        And I press the "Select Taxon" button
        And I wait for the "sub" form to close

    ## -------------------------- Interaction ----------------------------------##
    @javascript
    Scenario:  I should be able to create a new interaction with all fields filled
        Given I press the "New" button
        And I see "Interaction Create"
        And I fill the new interaction form with the test values
        And I submit the form
        Then I should see "New Interaction successfully created." in the form header

    @javascript
    Scenario:  Pinned field values should remain after interaction form submission (all others should clear)
        Given I press the "New" button
        And I see "Interaction Create"
        And I fill the new interaction form with the test values
        When I pin the "Citation Title" field
        And I pin the "Location" field
        And I pin the "Subject" field
        And I pin the "Interaction Type" field
        And I pin the "Interaction Tags" field
        And I submit the form
        Then I should see "New Interaction successfully created." in the form header
        And I should see "Test Collection" in the "Publication" combobox
        And I should see "Test Chapter Title" in the "Citation Title" combobox
        And I should see "Costa Rica" in the "Country-Region" combobox
        And I should see "Test Location With GPS" in the "Location" combobox
        And I should see "Genus BatB" in the "Subject" combobox
        And I should see "Host (host of)" in the "Interaction Type" combobox
        ##Tests that the field cleared as expected
        And I should see "Primary" in the "Source" combobox
        And the "Object" select field should be empty
        And the "Interaction Tags" select field should be empty
        And the "Note" field should be empty
        And the "Season" field should be empty
        And the "Date" field should be empty
        And the "Quote" field should be empty

## ======= SINGLE SCENARIO COMBINING THE TESTS COMMENTED BELOW ============== ##
    @javascript
    Scenario:  I should see the newly created interactions in the grid #COMBO
        ## --- Source --- ##
        Given the explore table is focused on "Source"
        And I filter the table to interactions created today
        ## Collection ##
        And I expand "Test Collection" in the data tree
        And I should see "2" interactions under "Test Chapter Title"
        And the expected data in the interaction row
        ## Author [Cockle, Anya] ##
        And I view interactions by "Authors"
        And I filter the table to interactions created today
        And I expand "Cockle, Anya" in the data tree
        And I should see "2" interactions under "Test Chapter Title"
        And the expected data in the interaction row
        And I collapse "Cockle, Anya" in the data tree
        ## Author [Smith, George Michael Sr] ##
        And I expand "Callaye, Bendry J Jr" in the data tree
        And I expand "Test Collection" in the data tree
        And I expand "Test Chapter Title" in level "3" of the data tree
        And I should see "2" interactions attributed
        And the expected data in the interaction row
        ## --- Location --- ##
        Given the explore table is focused on "Location"
        And I filter the table to interactions created today
        When I expand "Central America" in the data tree
        And I expand "Costa Rica" in the data tree
        Then I should see "2" interactions under "Test Location With GPS"
        And the expected data in the location interaction row
        ## --- Taxon --- ##
        ## Subject ##
        Given the explore table is focused on "Taxon"
        And I view interactions by "Bats"
        And I filter the table to interactions created today
        When I expand "Family BatA" in the data tree
        And I expand "Genus BatB" in the data tree
        Then I should see "2" interactions under "Unspecified BatB Interactions"
        And the expected data in the interaction row
        ## Object ##
        And I view interactions by "Arthropods"
        And I filter the table to interactions created today
        And I expand "Class BugA" in the data tree
        And I expand "Order BugB" in the data tree
        And I expand "Family BugC" in the data tree
        And I expand "Genus BugD" in the data tree
        And I expand "BugD species" in the data tree
        Then I should see "2" interactions under "BugD species"
        And the expected data in the interaction row

    ## -------------------------- Interaction ----------------------------------##
    @javascript
    Scenario:  I should be able to create a new interaction with all fields filled
        Given I press the "New" button
        And I see "Interaction Create"
        And I fill the new interaction form with the test values
        And I submit the form
        Then I should see "New Interaction successfully created." in the form header

    @javascript
    Scenario:  Pinned field values should remain after interaction form submission (all others should clear)
        Given I press the "New" button
        And I see "Interaction Create"
        And I fill the new interaction form with the test values
        When I pin the "Citation Title" field
        And I pin the "Location" field
        And I pin the "Subject" field
        And I pin the "Interaction Type" field
        And I pin the "Interaction Tags" field
        And I submit the form
        Then I should see "New Interaction successfully created." in the form header
        And I should see "Test Collection" in the "Publication" combobox
        And I should see "Test Chapter Title" in the "Citation Title" combobox
        And I should see "Costa Rica" in the "Country-Region" combobox
        And I should see "Test Location With GPS" in the "Location" combobox
        And I should see "Genus BatB" in the "Subject" combobox
        And I should see "Host (host of)" in the "Interaction Type" combobox
        ##Tests that the field cleared as expected
        And I should see "Primary" in the "Source" combobox
        And the "Object" select field should be empty
        # And the "Interaction Tags" select field should be empty
        And the "Note" field should be empty
        And the "Season" field should be empty
        And the "Date" field should be empty
        And the "Quote" field should be empty

## ======================== CHECK TABLE DATA ================================ ##
    @javascript
    Scenario:  I should see the newly created interactions in the grid
        ## --- Source --- ##
        Given the explore table is focused on "Source"
        And I filter the table to interactions created today
        ## Collection ##
        And I expand "Test Collection" in the data tree
        And I should see "4" interactions under "Test Chapter Title"
        And the expected data in the interaction row
        ## Author [Cockle, Anya] ##
        And I view interactions by "Authors"
        And I filter the table to interactions created today
        And I expand "Cockle, Anya" in the data tree
        And I should see "4" interactions under "Test Chapter Title"
        And the expected data in the interaction row
        And I collapse "Cockle, Anya" in the data tree
        ## Author [Smith, George Michael Sr] ##
        And I expand "Callaye, Bendry J Jr" in the data tree
        And I expand "Test Collection" in the data tree
        And I expand "Test Chapter Title" in level "3" of the data tree
        And I should see "4" interactions attributed
        And the expected data in the interaction row
        ## --- Location --- ##
        Given the explore table is focused on "Location"
        And I filter the table to interactions created today
        When I expand "Central America" in the data tree
        And I expand "Costa Rica" in the data tree
        Then I should see "4" interactions under "Test Location With GPS"
        And the expected data in the location interaction row
        ## --- Taxon --- ##
        ## Subject ##
        Given the explore table is focused on "Taxon"
        And I view interactions by "Bats"
        And I filter the table to interactions created today
        When I expand "Family BatA" in the data tree
        And I expand "Genus BatB" in the data tree
        Then I should see "4" interactions under "Unspecified BatB Interactions"
        And the expected data in the interaction row
        ## Object ##
        And I view interactions by "Arthropods"
        And I filter the table to interactions created today
        And I expand "Class BugA" in the data tree
        And I expand "Order BugB" in the data tree
        And I expand "Family BugC" in the data tree
        And I expand "Genus BugD" in the data tree
        And I expand "BugD species" in the data tree
        Then I should see "4" interactions under "BugD species"
        And the expected data in the interaction row

## ===================== AFTER INTERACTION FORM SUBMIT ====================== ##
    @javascript
    Scenario:  The table should not change views when form closes without submitting
        Given I press the "New" button
        And I see "Interaction Create"
        And the explore table is focused on "Location"
        When I exit the form window
        Then I should see the table displayed in "Location" view
