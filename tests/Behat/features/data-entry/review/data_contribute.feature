@data-contribute
Feature: Users can contribute data to be reviewed, quarantined to themselves until then.
    In order to have accurate data about bat eco-interactions
    As a contributor
    I need to be able to contribute data to be reviewed by a data-manager.

#   DATA-SET
#       CREATE: INTERACTION
#           CREATE: PUBLICATION WITH EDITORS
#               CREATE: EDITOR ONE
#               CREATE: EDITOR TWO
#               (SELECT NEW PUBLISHER)
#           CREATE: CITATION - CHAPTER
#               CREATE: AUTHOR ONE
#               CREATE: EDITOR TWO (TRIGGERS DUPLICATE ENTITY FAIL)
#           CREATE: LOCATION
#               (WITH GPS COORDINATES)
#           CREATE: TAXON SUBJECT
#               (SELECT NEW ARTHROPOD CLASS)
#               CREATE: TAXON ORDER
#               CREATE: TAXON FAMILY (tests that quarantined parent id is handled during server push and local data sync)
#           CREATE: TAXON OBJECT - BAT FAMILY
#
#       CREATE: INTERACTION
#           (SELECT ALL CREATED DATA ABOVE)
#
#       CREATE: INTERACTION
#           CREATE: PUBLICATION - BOOK
#               CREATE: AUTHOR TWO
#               (SELECT EXISTING)
#               CREATE: EDITOR TWO
#               (SELECT NEW PUBLISHER)
#           CREATE: CITATION - BOOK (tests autofilled book citation with quarantined data)
#           (SELECT MISC DATA)
#
#       EDIT: TAXON BAT (CHANGE NAME, RANK, AND PARENT:
#           CREATE ARTHROPOD CLASS) - Duplicated
#
#       EDIT: SOURCE PUBLICATION
#           CREATE: DUPLICATED AUTHOR
#           SELECT QUARANTINED: SOURCE PUBLISHER
#
#   TODO
#       TEST ADDING NOTES AND MESSAGES
#       CONTRIBUTOR LOCAL STORAGE SYNCS WITH PARTIALLY REVIEWED DATA

    Background:
        Given I log in as "Test Contributor"
        And I am on "/explore"
        And the database has loaded
        And I exit the tutorial
## ======================== CREATE ALL ENTITIES ============================= ##
    @javascript
    Scenario:  I create all sub-entities in the interaction form.
        Given I press the "New" button
        And I see "Interaction Create"
        ## Publication Collection ##
        And I open the publication form with data for "Test Collection"
        And I create an editor, "Bendry" "J" "Callaye" "Jr"
        And I create an editor, "Smith" "J" "Johnson" "Jr"
        And I create a publisher, "Test Publisher"
        And I submit the form from the confirmation popup
        ## Chapter Citation ##
        And I enter citation data for "Test Collection Chapter"
        And I create an author: "Bendry" "J" "Callaye" "Jr"
        And I create an author: "Sue" "J" "Smith" "Jr"
        And I submit the form
        And I wait for the "sub" form to close
        ## ---- Location --- ##
        ## With GPS ##
        And I open the location form with data for "Test Location With GPS"
        ## --- Taxon --- ##
        ### Subject ##
        And I open the "Subject" select-form
        ### Subject Family ##
        And I create taxon: "Family" "BatA" "contribution"
        # ### Subject Genus ##
        And I create taxon: "Genus" "BatB" "contribution"
        # ### Subject Species ##
        And I create taxon: "Species" "BatB species" "contribution"
        And I press the "Select Taxon" button
        And I wait for the "sub" form to close
        ### Object ##
        And I open the "Object" select-form
        ### Object Class ##
        And I select "Arthropod" from the "Group" combobox
        And I create taxon: "Class" "BugA" "contribution"
        ### Object Order ##
        And I create taxon: "Order" "BugB" "contribution"
        ### Object Family ##
        And I create taxon: "Family" "BugC" "contribution"
        ### Object Genus ##
        And I create taxon: "Genus" "BugD" "contribution"
        ### Subject Species ##
        And I create taxon: "Species" "BugD species" "contribution"
        And I press the "Select Taxon" button
        And I wait for the "sub" form to close
#        And I break "Check for errors"

    ## -------------------------- Interaction ----------------------------------##
    @javascript
    Scenario:  I should be able to create a new interaction with the quarantined data
        and all pinned field-data should persist through submit
        Given I press the "New" button
        And I see "Interaction Create"
        And I fill the new interaction form with the quarantined values
        When I pin the "Citation Title" field
        And I pin the "Location" field
        And I pin the "Subject" field
        And I pin the "Object" field
        And I pin the "Interaction Type" field
        And I pin the "Interaction Tags" field
        And I submit the form
        Then I should see "New Interaction successfully created." in the form header
        And I should see "*Test Collection" in the "Publication" combobox
        And I should see "*Test Collection Chapter" in the "Citation Title" combobox
        And I should see "Costa Rica" in the "Country-Region" combobox
        And I should see "*Test Location With GPS" in the "Location" combobox
        And I should see "*Genus BatB" in the "Subject" combobox
        And I should see "*BugD species" in the "Object" combobox
        And I should see "Host (host of)" in the "Interaction Type" combobox
#        And I break "Any errors?"

    @javascript
    Scenario:  I should be able to create a second interaction with a book publication
        and citation and the previously created quarantined data
        # TESTS ANOTHER ASPECT OF THE QUARANTINED DATA SUBMIT PROCESS
        Given I press the "New" button
        And I see "Interaction Create"
        And I fill the new interaction form with the quarantined values
        ## Publication Collection ##
        And I open the publication form with data for "Quarantined Book"
        When I create an author: "Sarah" "" "Ember" "Sr"
        And I select "*Smith, Sue J Jr" from the "Author" dynamic combobox
        And I create an author: "Sarah" "" "Ember" "Jr"
        And I select "*Test Publisher" from the "Publisher" combobox
        And I submit the form from the confirmation popup
        ## Chapter Citation ##
        And I enter citation data for "Quarantined Book"
        And I submit the form
        Then I wait for the "sub" form to close
        ## Submit Interaction ##
        And I submit the form
        Then I should see "New Interaction successfully created." in the form header

    ## -------------------------- CHECK DATA ---------------------------------##
    @javascript
    Scenario:  I should see the newly created interactions in the grid
        ## --- Source --- ##
        Given the explore table is focused on "Source"
        And I filter the table to interactions created today
        ## Collection ##
        And I expand "*Test Collection" in the data tree
        And I should see "1" interactions under "*Test Collection Chapter"
        And the expected data in the interaction row
        ## Book ##
        And I expand "*Quarantined Book" in the data tree
        And I should see "1" interactions under "Whole work cited."
        And the expected data in the interaction row
        ## Author [Johnson, Smith J Jr] ##
        And I view interactions by "Authors"
        And I filter the table to interactions created today
        And I expand "*Johnson, Smith J Jr" in the data tree
        And I expand "*Test Collection" in the data tree
        And I should see "1" interactions under "*Test Collection Chapter"
        And the expected data in the interaction row
        And I collapse "*Johnson, Smith J Jr" in the data tree
        ## Author [Smith, Sue J Jr] ##
        And I expand "*Smith, Sue J Jr" in the data tree
        And I should see "1" interactions under "*Test Collection Chapter"
        And I expand "*Quarantined Book" in the data tree
        And I should see "1" interactions under "Whole work cited."
        And the expected data in the interaction row
        And I collapse "*Callaye, Bendry J Jr" in the data tree
        ## Author [Callaye, Bendry J Jr] ##
        And I expand "*Callaye, Bendry J Jr" in the data tree
        And I should see "1" interactions under "*Test Collection Chapter"
        And the expected data in the interaction row
        And I collapse "*Callaye, Bendry J Jr" in the data tree
        ## Author [Ember, Sarah Sr] ##
        And I expand "*Ember, Sarah Sr" in the data tree
        And I expand "*Quarantined Book" in the data tree
        And I should see "1" interactions under "Whole work cited."
        And the expected data in the interaction row
        And I collapse "*Ember, Sarah Sr" in the data tree
        ## Author [Ember, Sarah Jr] ##
        And I expand "*Ember, Sarah Jr" in the data tree
        And I expand "*Quarantined Book" in the data tree
        And I should see "1" interactions under "Whole work cited."
        And the expected data in the interaction row
        And I collapse "*Ember, Sarah Jr" in the data tree
        ## --- Location --- ##
        Given the explore table is focused on "Location"
        And I filter the table to interactions created today
        When I expand "Central America" in the data tree
        And I expand "Costa Rica" in the data tree
        Then I should see "2" interactions under "*Test Location With GPS"
        And the expected data in the location interaction row
        ## --- Taxon --- ##
        ## Subject ##
        Given the explore table is focused on "Taxon"
        And I view interactions by "Bats"
        And I filter the table to interactions created today
        When I expand "*Family BatA" in the data tree
        And I expand "*Genus BatB" in the data tree
        Then I should see "2" interactions under "Unspecified *BatB Interactions"
        And the expected data in the interaction row
        ## Object ##
        And I view interactions by "Arthropods"
        And I filter the table to interactions created today
        And I expand "*Class BugA" in the data tree
        And I expand "*Order BugB" in the data tree
        And I expand "*Family BugC" in the data tree
        And I expand "*Genus BugD" in the data tree
        Then I should see "2" interactions under "*BugD species"
        And the expected data in the interaction row

## ======================== EDIT ENTITY DATA ================================ ##
  ## -------------------------- Taxon ----------------------------------------##
  # #TODO: TEST DUPLICATE ENTITY ERROR HERE
    @javascript
    Scenario:  I should be able to edit taxon data
        Given the explore table is focused on "Taxon"
        And I view interactions by "Worms"
        And I expand "Phylum Nematoda" in the data tree
        And I click on the edit pencil for the "Class Striped" row
        And I see "Taxon Edit"
        # EDIT DATA #
        When I change the "Display Name" field "input" to "Dots"
        And I select "Order" from the "Rank" combobox
        And I open the "Parent" select-form
        And I see "Select Parent Taxon"
        When I select "Arthropod" from the "Group" combobox
        And I create taxon: "Class" "BugA" "contribution"
        And I press the "Select Taxon" button
        And I submit the form
        And I wait for the "top" form to close
        # CHECK DATA #
        Then I expand "Phylum Nematoda" in the data tree
        And I should not see "*Class Striped" in the tree
        And I should not see "*Order Dots" in the tree
        And I view interactions by "Arthropods"
        And I should see "*Class BugA" in the tree
        And I expand "*Class BugA" in the data tree
        And I should see "*Order Dots" in the tree

  # -------------------------- Source ---------------------------------------##
    @javascript
    Scenario:  I should be able to edit the data of an existing publication
        Given the explore table is focused on "Source"
        And I click on the edit pencil for the "Journal of Mammalogy" row
        And I see "Publication Edit"
        # EDIT DATA #
        When I change the "Display Name" field "input" to "Book of Mammalogy"
        And I select "Book" from the "Publication Type" combobox
        And I change the "Year" field "input" to "1993"
        And I select "*Test Publisher" from the "Publisher" combobox
        And I create an author: "Smith" "J" "Johnson" "Jr"
        And I submit the form
        And I wait for the "top" form to close
        # CHECK DATA #
        And I select "Book" from the "Publication Type Filter" combobox
        And I click on the edit pencil for the "*Book of Mammalogy" row
        And I see "Publication Edit"
        Then I should see "Book" in the "Publication Type" combobox
        Then I should see "*Test Publisher" in the "Publisher" combobox
        Then I should see "*Johnson, Smith J Jr" in the "Author" dynamic combobox
        Then I print all Review Entry JSON
        And I break "COPY PENDINGDATA JSON"
