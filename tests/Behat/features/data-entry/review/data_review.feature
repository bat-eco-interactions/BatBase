@data-review
Feature: Data-managers are able to review data entered by contributors.
    In order to have accurate data about bat eco-interactions
    As a data-manager and a data-contributor
    I need to be able to review data entered by Contributors

    # THE DATA-REVIEW PROCESS
    ## UI
    ### SIDE PANEL SECTION LOADS
    #### PARENT SECTIONS CLOSES WHEN SUB-FORM OPENS
    ## ----------------- REVIEWS COMPLEX  ------------------- ##
    @javascript
    Scenario:  Manager reviews data using the data-review wizard.
        # MANAGER: LOADS PAGE AND PARTIALLY REVIEWED DATA SYNC WITHOUT ERROR
        Given I log in as "Test Manager"
        And there are data pending review
        And I am on "/explore"
        And the database has loaded
        And I exit the tutorial
        And I break "Open console"
        And I start the data review process
        # --------- INTERACTION CREATE ------------- #
        # ------ PUBLICATION ------ #
        # MANAGER: REJECT EDITOR
        And I wait for the "sub2" form to open
        When I add review notes to the "LastName" field
        And I add a message for the review
        And I "Reject" the "Editor1" Review Entry
        # MANAGER: APPROVE EDITOR
        Given I wait for the "sub2" form to open
        When I add review notes to the "FirstName" field
        And I add a message for the review
        And I "Approve" the "Editor2" Review Entry
        # MANAGER: APPROVE PUBLISHER
        Given I wait for the "sub2" form to open
        When I add review notes to the "City" field
        And I add a message for the review
        And I "Approve" the "Publisher" Review Entry
        # MANAGER: RETURN PUBLICATION
        Given I wait for the "sub2" form to close
        When I add review notes to the "DisplayName" field
        And I add a message for the review
        And I "Return" the "Publication" Review Entry
        # ------ LOCATION ------ #
        # MANAGER: APPROVE LOCATION
        Given I wait for the "sub" form to open
        When I add review notes to the "Elevation" field
        And I add a message for the review
        And I "Approve" the "Location" Review Entry
        # TODO: TEST WHEN LOCATION IS FLAGGED AS DUPLICATE AND REPLACED
        # ------ SUBJECT ------ #
        # MANAGER: RETURN TAXON
        Given I wait for the "sub2" form to open
        When I add review notes to the "DisplayName" field
        And I add a message for the review
        And I "Return" the "Subject" Review Entry
        # ------ OBJECT ------ #
        # MANAGER: APPROVE TAXON
        Given I see "Select Object Taxon"
        And I wait for the "sub2" form to open
        When I add review notes to the "DisplayName" field
        And I add a message for the review
        And I "Approve" the "Class" Review Entry
        # MANAGER: APPROVE TAXON
        Given I wait for the "sub2" form to open
        When I add review notes to the "DisplayName" field
        And I add a message for the review
        And I "Approve" the "Order" Review Entry
        # MANAGER: APPROVE TAXON
        Given I wait for the "sub2" form to open
        When I add review notes to the "DisplayName" field
        And I add a message for the review
        And I "Approve" the "Family" Review Entry
        # MANAGER: REJECT TAXON
        Given I wait for the "sub2" form to open
        When I add review notes to the "DisplayName" field
        And I add a message for the review
        And I "Reject" the "Object" Review Entry
        # MANAGER: RETURN INTERACTION
        Given I wait for the "sub" form to close
        When I add review notes to the "Season" field
        And I add a message for the review
        And I "Return" the Review Entry
        # -------- INTERACTION CREATE ----------- #
        # ------ PUBLICATION ------ #
        # MANAGER: REJECT AUTHOR
        And I wait for the "sub2" form to open
        When I add review notes to the "LastName" field
        And I add a message for the review
        And I "Approve" the "Author1" Review Entry
        # MANAGER: APPROVE AUTHOR
        Given I wait for the "sub2" form to open
        When I add review notes to the "FirstName" field
        And I add a message for the review
        And I "Approve" the "Author2" Review Entry
        # MANAGER: APPROVE AUTHOR
        Given I wait for the "sub2" form to open
        When I add review notes to the "FirstName" field
        And I add a message for the review
        And I "Approve" the "Author3" Review Entry
        # MANAGER: RETURN PUBLICATION
        Given I wait for the "sub2" form to close
        When I add review notes to the "DisplayName" field
        And I add a message for the review
        And I "Approve" the "Publication" Review Entry
        # MANAGER: APPROVE CITATION
        Given I wait for the "sub" form to open
        When I add review notes to the "Abstract" field
        And I add a message for the review
        And I "Approve" the "Citation" Review Entry
        # MANAGER: PAUSE SECOND INTERACTION
        Given I wait for the "top" form to open
        And I should see "Quarantined Book" in the "Publication" combobox
        And I should see "Quarantined Book" in the "CitationTitle" combobox
        And I should see "Test Location With GPS" in the "Location" combobox
        And I see the "Returned" stage in the "Subject" combo
        And I see the "Rejected" stage in the "Object" combo
        When I "Pause" the Review Entry
        # And I wait for the "top" form to close
        # ------------- TAXON EDIT ------------- #
        # MANAGER: REPLACE DUPLICATE TAXON AND RETURN
        Given I wait for the "sub2" form to open
        When I approve the data, then select existing in the duplicate-data alert
        Then I should see "Class BugA" in the "Parent" combobox
        When I "Return" the Review Entry
        # And I wait for the "top" form to close
        # --------- PUBLICATION EDIT ------------- #
        # MANAGER: REPLACE DUPLICATE AUTHOR AND RETURN
        Given I wait for the "sub" form to open
        When I approve the data, then select existing in the duplicate-data alert
        And I should see "Test Publisher" in the "Publisher" combobox
        And I "Return" the Review Entry
        Then I wait for the "top" form to close
        # -------- INTERACTION CREATE ----------- #
        Given I press the "Start Review" button
        And I wait for the "top" form to open
        And I should see "Quarantined Book" in the "Publication" combobox
        And I should see "Quarantined Book" in the "CitationTitle" combobox
        And I should see "Test Location With GPS" in the "Location" combobox
        And I see the "Returned" stage in the "Subject" combo
        And I see the "Rejected" stage in the "Object" combo
        When I "Return" the Review Entry
        And I wait for the "top" form to close
        And I break "any errors?"

    # ## -------------- DATA PARTIALLY REVIEWED FOR CONTRIBUTOR ------------------- ##
    # COMPLEX LOCAL-STORAGE QUARANTINED-DATA AUTOSYNC ON DATABASE PAGE-LOAD
    #   Citation/Publication: author/editor, publisher
    #   Interaction (pending/returned)
    #       taxon: obj/subj
    #       location: geojson (todo)
    #       source: citation
    #   Taxon: parent
    @javascript
    Scenario: Quarantined-data sync, on page-load, in the Contributor's local-storage,
        with changes made to related quarantined-data that have been rejected or
        replaced, for each ReviewEntry record that is still awaiting final-review.

        Given I log in as "Test Contributor"
        And I am on "/explore"
        And the database has loaded
        And I exit the tutorial
        And I break "Check for JS errors with local-data sync..."

    @javascript
    Scenario: Contributor is able is able to engage in the review process: by
        responding to messages and field notes added by the Data Manager when
        data has been returned for further action, and by viewing and confirming
        data that has been either approved or rejected by the Manager.

        Given I log in as "Test Contributor"
        And I am on "/explore"
        And the database has loaded
        And I exit the tutorial
        And I start the data review process
        # --------- INTERACTION CREATE ------------- #
        # ------ PUBLICATION ------ #
        # CONTRIBUTOR: CONFIRM REJECTED EDITOR
        And I wait for the "sub2" form to open
        And I see review notes in the "LastName" field
        And I see the logs and message added by the data manager
        When I confirm the data-review results
        # CONTRIBUTOR: CONFIRM APPROVED EDITOR
        Given I wait for the "sub2" form to open
        And I see review notes in the "FirstName" field
        And I see the logs and message added by the data manager
        And I confirm the data-review results
        # CONTRIBUTOR: CONFIRM APPROVED PUBLISHER
        Given I wait for the "sub2" form to open
        And I see review notes in the "City" field
        And I see the logs and message added by the data manager
        When I confirm the data-review results
        # CONTRIBUTOR: REPLACE EDITOR AND RESUBMIT PUBLICATION
        And I change "Smith, Sue J Jr" in the "Editor" "1" dynamic combobox
        And I submit the form from the confirmation popup
        # ------ LOCATION ------ #
        # CONTRIBUTOR: CONFIRM APPROVED LOCATION
        Given I wait for the "sub" form to open
        And I see review notes in the "Elevation" field
        And I see the logs and message added by the data manager
        When I confirm the data-review results
        # ------ SUBJECT ------ #
        # CONTRIBUTOR: EDIT AND RESUBMIT TAXON
        Given I wait for the "sub2" form to open
        And I see review notes in the "DisplayName" field
        And I see the logs and message added by the data manager
        When I add a message for the review
        And I submit the form from the confirmation popup
        # ------ OBJECT ------ #
        # CONTRIBUTOR: CONFIRM APPROVED TAXON
        Given I see "Select Object Taxon"
        And I wait for the "sub2" form to open
        And I see review notes in the "DisplayName" field
        And I see the logs and message added by the data manager
        When I confirm the data-review results
        # CONTRIBUTOR: CONFIRM APPROVED TAXON
        Given I wait for the "sub2" form to open
        And I see review notes in the "DisplayName" field
        And I see the logs and message added by the data manager
        When I confirm the data-review results
        # CONTRIBUTOR: CONFIRM APPROVED TAXON
        Given I wait for the "sub2" form to open
        And I see review notes in the "DisplayName" field
        And I see the logs and message added by the data manager
        When I confirm the data-review results
        # CONTRIBUTOR: CONFIRM REJECTED TAXON
        Given I wait for the "sub2" form to open
        And I see review notes in the "DisplayName" field
        And I see the logs and message added by the data manager
        When I confirm the data-review results
        # CONTRIBUTOR: REPLACE TAXON AND RESUBMIT INTERACTION
        Given I wait for the "sub" form to close
        And I should see "*BugD species" in the "Object" combobox
        And I see review notes in the "Season" field
        And I add a message for the review
        And I submit the form from the confirmation popup
        # -------- INTERACTION CREATE ----------- #
        # CONTRIBUTOR: RESUBMIT SECOND INTERACTION
        Given I wait for the "top" form to open
        # ------ PUBLICATION ------ #
        # CONTRIBUTOR: CONFIRM APPROVED AUTHOR
        And I wait for the "sub2" form to open
        And I see review notes in the "LastName" field
        And I see the logs and message added by the data manager
        When I confirm the data-review results
        # CONTRIBUTOR: CONFIRM APPROVED AUTHOR
        Given I wait for the "sub2" form to open
        And I see review notes in the "FirstName" field
        And I see the logs and message added by the data manager
        And I confirm the data-review results
        # CONTRIBUTOR: CONFIRM APPROVED AUTHOR
        Given I wait for the "sub2" form to open
        And I see review notes in the "FirstName" field
        And I see the logs and message added by the data manager
        And I confirm the data-review results
        # CONTRIBUTOR: CONFIRM APPROVED PUBLICATION
        Given I wait for the "sub2" form to close
        And I see review notes in the "DisplayName" field
        And I see the logs and message added by the data manager
        When I confirm the data-review results
        # CONTRIBUTOR: CONFIRM APPROVED CITATION
        And I submit the form from the confirmation popup
        And I should see "Quarantined Book" in the "Publication" combobox
        And I should see "Quarantined Book" in the "CitationTitle" combobox
        And I should see "Test Location With GPS" in the "Location" combobox
        And I should see "*Genus BatB" in the "Subject" combobox
        And I should see "*BugD species" in the "Object" combobox
        And I submit the form from the confirmation popup
        And I wait for the "top" form to close
        # ------------- TAXON EDIT ------------- #
        # CONTRIBUTOR: CONFIRM REJECTED DUPLICATE AND RESUBMIT
        Given I wait for the "sub2" form to open
        When I confirm the rejection of data identified and replaced as duplicate
        Then I should see "Class BugA" in the "Parent" combobox
        And I submit the form from the confirmation popup
        And I wait for the "sub" form to close
        # --------- PUBLISHER EDIT ------------- #
        # MANAGER: REPLACE DUPLICATE AUTHOR AND RETURN
        Given I wait for the "sub" form to open
        And I should see "Test Publisher" in the "Publisher" combobox
        When I confirm the rejection of data identified and replaced as duplicate
        And I submit the form from the confirmation popup
        And I wait for the "top" form to close
        # Data-review ends
        And I see "No Data Available to Review"
