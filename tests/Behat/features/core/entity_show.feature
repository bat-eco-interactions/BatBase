@entity-show
Feature: Entity show pages should load with the entity's data
In order to see all data on a specific entity
As a web visitor
I should be able see the entity's data displayed on the show page.

### WHAT IS BEING TESTED ###
# Interaction show page
# Taxon Show page

    Background:
        Given I am on "/explore"
        And the database has loaded
        And I exit the tutorial

    ## ------------------------ PAGE HEADER SITE STATISTICS  -------------------- ##
    @javascript
    Scenario:  The Interaction page should load with all entity data displayed
        Given I break "Check interaction show page loads as expected"

    @javascript
    Scenario:  The Taxon page should load with all entity data displayed
        Given I break "Check taxon show page loads as expected"
