<?php

namespace App\Tests\ApiResource;

use App\DataFixtures\Factory\AuthorFactory;
use App\DataFixtures\Factory\CitationFactory;
use App\DataFixtures\Factory\CitationTypeFactory;
use App\DataFixtures\Factory\InteractionFactory;
use App\DataFixtures\Factory\InteractionTypeFactory;
use App\DataFixtures\Factory\LocationFactory;
use App\DataFixtures\Factory\LocationTypeFactory;
use App\DataFixtures\Factory\PublicationFactory;
use App\DataFixtures\Factory\PublicationTypeFactory;
use App\DataFixtures\Factory\PublisherFactory;
use App\DataFixtures\Factory\RankFactory;
use App\DataFixtures\Factory\TaxonFactory;
use App\DataFixtures\Factory\UserFactory;
use Zenstruck\Browser\HttpOptions;
use Zenstruck\Foundry\Test\ResetDatabase;

/**
 * Tests the /api/login endpoint:
 * - POST (valid credentials)
 * - POST (invalid credentials)
 * - POST (invalid content-type header)
 */
class ApiResourceSecurityTest extends ApiTestCase
{
    use ResetDatabase;

    /* ========================== LOGIN ===================================== */

    public function testPostToLogin(): void
    {
        $user = UserFactory::createOne();

        $this->browser()
            ->post('/api/login', [
                'json' => [
                    'email' => $user->getEmail(),
                    'password' => '123456'
                ]
            ])
//            ->dump()
            ->assertStatus(204);
    }

    public function testLoginFailsWithInvalidCredentials(): void
    {
        $user = UserFactory::createOne();

        $this->browser()
            ->post('/api/login', [
                'json' => [
                    'email' => $user->getEmail(),
                    'password' => 'password'
                ]
            ])
//            ->dump()
            ->assertStatus(401);
    }

    public function testLoginFailsWithInvalidContentType(): void
    {
        $user = UserFactory::createOne();

        $this->browser()
            ->setDefaultHttpOptions(
                HttpOptions::create()
                    ->withHeader('Content-Type', 'application/xml')
            )
            ->post('/api/login', [
                'json' => [
                    'email' => $user->getEmail(),
                    'password' => '123456'
                ]
            ])
            ->assertJsonMatches('error', 'Invalid login request: check that the Content-Type header is "application.json"')
            ->assertStatus(401);
    }

    /* ======================== AUTHOR ==================================== */

    public function testAuthorWriteEndpointsAreSecure(): void
    {
        $invalidToken = $this->generateContributorToken();
        $validToken = $this->generateEditorToken();
        $author = AuthorFactory::createOne();
        $json = [
            'lastName' => 'Author'
        ];

        $this->browser()
            ->post('/api/author', [
                'json' => $json,
                'headers' => [
                    'Content-Type' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $invalidToken,
                ]
            ])
            ->assertStatus(403)
            ->post('/api/author', [
                'json' => $json,
                'headers' => [
                    'Content-Type' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $validToken,
                ]
            ])
            ->assertStatus(201)
            ->patch('/api/author/' . $author->getId(), [
                'json' => [
                    'lastName' => 'Patched',
                ],
                'headers' => [
                    'Content-Type' => 'application/merge-patch+json',
                    'Authorization' => 'Bearer ' . $invalidToken,
                ]
            ])
            ->assertStatus(403)
            ->patch('/api/author/' . $author->getId(), [
                'json' => [
                    'displayName' => 'Patched',
                ],
                'headers' => [
                    'Content-Type' => 'application/merge-patch+json',
                    'Authorization' => 'Bearer ' . $validToken,
                ]
            ])
            ->assertStatus(200)
            ->delete('/api/author/' . $author->getId(), [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $invalidToken,
                ]
            ])
            ->assertStatus(403)
            ->delete('/api/author/' . $author->getId(), [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $validToken,
                ]
            ])
            ->assertStatus(204);
    }

    /* ======================== CITATION ==================================== */

    public function testCitationWriteEndpointsAreSecure(): void
    {
        $invalidToken = $this->generateContributorToken();
        $validToken = $this->generateEditorToken();
        $citation = CitationFactory::createOne();
        $json = [
            'title' => 'Citation',
            'citationTypeId' => CitationTypeFactory::createOne()->getId(),
            'publicationId' => PublicationFactory::createOne()->getId(),
            'fullText' => 'We choose to go to the moon.'
        ];

        $this->browser()
            ->post('/api/citation', [
                'json' => $json,
                'headers' => [
                    'Content-Type' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $invalidToken,
                ]
            ])
            ->assertStatus(403)
            ->post('/api/citation', [
                'json' => $json,
                'headers' => [
                    'Content-Type' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $validToken,
                ]
            ])
            ->assertStatus(201)
            ->patch('/api/citation/' . $citation->getId(), [
                'json' => [
                    'lastName' => 'Patched',
                ],
                'headers' => [
                    'Content-Type' => 'application/merge-patch+json',
                    'Authorization' => 'Bearer ' . $invalidToken,
                ]
            ])
            ->assertStatus(403)
            ->patch('/api/citation/' . $citation->getId(), [
                'json' => [
                    'displayName' => 'Patched',
                ],
                'headers' => [
                    'Content-Type' => 'application/merge-patch+json',
                    'Authorization' => 'Bearer ' . $validToken,
                ]
            ])
            ->assertStatus(200)
            ->delete('/api/citation/' . $citation->getId(), [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $invalidToken,
                ]
            ])
            ->assertStatus(403)
            ->delete('/api/citation/' . $citation->getId(), [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $validToken,
                ]
            ])
            ->assertStatus(204);
    }

    /* ======================== INTERACTION ==================================== */

    public function testInteractionWriteEndpointsAreSecure(): void
    {
        $invalidToken = $this->generateContributorToken();
        $validToken = $this->generateEditorToken();
        $interaction = InteractionFactory::createOne();
        LocationFactory::createOne([
            'displayName' => 'Unspecified',
        ]);
        $json = [
            'citationId' => CitationFactory::createOne()->getId(),
            'interactionTypeId' => InteractionTypeFactory::createOne()->getId(),
            'objectId' => TaxonFactory::createOne()->getId(),
            'subjectId' => TaxonFactory::createOne()->getId(),
        ];

        $this->browser()
            ->post('/api/interaction', [
                'json' => $json,
                'headers' => [
                    'Content-Type' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $invalidToken,
                ]
            ])
            ->assertStatus(403)
            ->post('/api/interaction', [
                'json' => $json,
                'headers' => [
                    'Content-Type' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $validToken,
                ]
            ])
            ->assertStatus(201)
            ->patch('/api/interaction/' . $interaction->getId(), [
                'json' => [
                    'quote' => 'quoted',
                ],
                'headers' => [
                    'Content-Type' => 'application/merge-patch+json',
                    'Authorization' => 'Bearer ' . $invalidToken,
                ]
            ])
            ->assertStatus(403)
            ->patch('/api/interaction/' . $interaction->getId(), [
                'json' => [
                    'displayName' => 'Patched',
                ],
                'headers' => [
                    'Content-Type' => 'application/merge-patch+json',
                    'Authorization' => 'Bearer ' . $validToken,
                ]
            ])
            ->assertStatus(200)
            ->delete('/api/interaction/' . $interaction->getId(), [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $invalidToken,
                ]
            ])
            ->assertStatus(403)
            ->delete('/api/interaction/' . $interaction->getId(), [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $validToken,
                ]
            ])
            ->assertStatus(204);
    }

    /* ======================== LOCATION ==================================== */

    public function testLocationWriteEndpointsAreSecure(): void
    {
        $invalidToken = $this->generateContributorToken();
        $validToken = $this->generateEditorToken();
        $loc = LocationFactory::createOne();
        $locJson = [
            'displayName' => 'Test Location',
            'locationTypeId' => LocationTypeFactory::createOne()->getId(),
            'parentId' => LocationFactory::createOne()->getId(),
        ];

        $this->browser()
            ->post('/api/location', [
                'json' => $locJson,
                'headers' => [
                    'Content-Type' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $invalidToken,
                ]
            ])
            ->assertStatus(403)
            ->post('/api/location', [
                'json' => $locJson,
                'headers' => [
                    'Content-Type' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $validToken,
                ]
            ])
            ->assertStatus(201)
            ->patch('/api/location/' . $loc->getId(), [
                'json' => [
                    'displayName' => 'Patched',
                ],
                'headers' => [
                    'Content-Type' => 'application/merge-patch+json',
                    'Authorization' => 'Bearer ' . $invalidToken,
                ]
            ])
            ->assertStatus(403)
            ->patch('/api/location/' . $loc->getId(), [
                'json' => [
                    'displayName' => 'Patched',
                ],
                'headers' => [
                    'Content-Type' => 'application/merge-patch+json',
                    'Authorization' => 'Bearer ' . $validToken,
                ]
            ])
            ->assertStatus(200)
            ->delete('/api/location/' . $loc->getId(), [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $invalidToken,
                ]
            ])
            ->assertStatus(403)
            ->delete('/api/location/' . $loc->getId(), [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $validToken,
                ]
            ])
            ->assertStatus(204);
    }

    /* ======================== PUBLICATION ==================================== */

    public function testPublicationWriteEndpointsAreSecure(): void
    {
        $invalidToken = $this->generateContributorToken();
        $validToken = $this->generateEditorToken();
        $publication = PublicationFactory::createOne();
        $json = [
            'displayName' => 'Publication',
            'publicationTypeId' => PublicationTypeFactory::createOne()->getId(),
        ];

        $this->browser()
            ->post('/api/publication', [
                'json' => $json,
                'headers' => [
                    'Content-Type' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $invalidToken,
                ]
            ])
            ->assertStatus(403)
            ->post('/api/publication', [
                'json' => $json,
                'headers' => [
                    'Content-Type' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $validToken,
                ]
            ])
            ->assertStatus(201)
            ->patch('/api/publication/' . $publication->getId(), [
                'json' => [
                    'lastName' => 'Patched',
                ],
                'headers' => [
                    'Content-Type' => 'application/merge-patch+json',
                    'Authorization' => 'Bearer ' . $invalidToken,
                ]
            ])
            ->assertStatus(403)
            ->patch('/api/publication/' . $publication->getId(), [
                'json' => [
                    'displayName' => 'Patched',
                ],
                'headers' => [
                    'Content-Type' => 'application/merge-patch+json',
                    'Authorization' => 'Bearer ' . $validToken,
                ]
            ])
            ->assertStatus(200)
            ->delete('/api/publication/' . $publication->getId(), [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $invalidToken,
                ]
            ])
            ->assertStatus(403)
            ->delete('/api/publication/' . $publication->getId(), [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $validToken,
                ]
            ])
            ->assertStatus(204);
    }

    /* ======================== PUBLISHER ==================================== */

    public function testPublisherWriteEndpointsAreSecure(): void
    {
        $invalidToken = $this->generateContributorToken();
        $validToken = $this->generateEditorToken();
        $publisher = PublisherFactory::createOne();
        $json = [
            'displayName' => 'Publisher'
        ];

        $this->browser()
            ->post('/api/publisher', [
                'json' => $json,
                'headers' => [
                    'Content-Type' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $invalidToken,
                ]
            ])
            ->assertStatus(403)
            ->post('/api/publisher', [
                'json' => $json,
                'headers' => [
                    'Content-Type' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $validToken,
                ]
            ])
            ->assertStatus(201)
            ->patch('/api/publisher/' . $publisher->getId(), [
                'json' => [
                    'displayName' => 'Patched',
                ],
                'headers' => [
                    'Content-Type' => 'application/merge-patch+json',
                    'Authorization' => 'Bearer ' . $invalidToken,
                ]
            ])
            ->assertStatus(403)
            ->patch('/api/publisher/' . $publisher->getId(), [
                'json' => [
                    'displayName' => 'Patched',
                ],
                'headers' => [
                    'Content-Type' => 'application/merge-patch+json',
                    'Authorization' => 'Bearer ' . $validToken,
                ]
            ])
            ->assertStatus(200)
            ->delete('/api/publisher/' . $publisher->getId(), [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $invalidToken,
                ]
            ])
            ->assertStatus(403)
            ->delete('/api/publisher/' . $publisher->getId(), [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $validToken,
                ]
            ])
            ->assertStatus(204);
    }

    /* ======================== TAXON ==================================== */

    public function testTaxonWriteEndpointsAreSecure(): void
    {
        $invalidToken = $this->generateContributorToken();
        $validToken = $this->generateEditorToken();
        $parent = TaxonFactory::createOne();
        $txnJson = [
            'displayName' => 'Test Taxon',
            'name' => 'Taxon',
            'rankId' => RankFactory::createOne()->getId(),
            'parentId' => $parent->getId(),
        ];

        $this->browser()
            ->post('/api/taxon', [
                'json' => $txnJson,
                'headers' => [
                    'Content-Type' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $invalidToken,
                ]
            ])
            ->assertStatus(403)
            ->post('/api/taxon', [
                'json' => $txnJson,
                'headers' => [
                    'Content-Type' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $validToken,
                ]
            ])
            ->assertStatus(201)
            ->patch('/api/taxon/' . $parent->getId(), [
                'json' => [
                    'displayName' => 'Patched',
                ],
                'headers' => [
                    'Content-Type' => 'application/merge-patch+json',
                    'Authorization' => 'Bearer ' . $invalidToken,
                ]
            ])
            ->assertStatus(403)
            ->patch('/api/taxon/' . $parent->getId(), [
                'json' => [
                    'displayName' => 'Patched',
                ],
                'headers' => [
                    'Content-Type' => 'application/merge-patch+json',
                    'Authorization' => 'Bearer ' . $validToken,
                ]
            ])
            ->assertStatus(200)
            ->delete('/api/taxon/' . $parent->getId(), [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $invalidToken,
                ]
            ])
            ->assertStatus(403)
            ->delete('/api/taxon/' . $parent->getId(), [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $validToken,
                ]
            ])
            ->assertStatus(204);
    }
}