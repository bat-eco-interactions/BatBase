<?php

namespace App\Tests\ApiResource;

use App\DataFixtures\Factory\HabitatTypeFactory;
use App\DataFixtures\Factory\LocationFactory;
use App\DataFixtures\Factory\LocationTypeFactory;
use Zenstruck\Browser\Json;
use Zenstruck\Foundry\Test\ResetDatabase;

/**
 * Tests /api/location endpoints
 * @todo
 *  - test all possible validation errors
 */
class LocationApiResourceTest extends ApiTestCase
{
    use ResetDatabase;

    public function testLocationGet(): void
    {
        $location = LocationFactory::createOne();

        $this->browser()
            ->get($this->getEntityIri($location))
//            ->dump()
            ->assertStatus(200);
    }

    public function testLocationGetCollection(): void
    {
        LocationFactory::createMany(2);

        $this->browser()
            ->get('/api/location')
//            ->dump()
            ->assertStatus(200)
            ->assertJsonMatches('"hydra:totalItems"', 2);
    }

    /**
     * Every possible input is sent and all output props should be returned.
     *
     * Side-effects:
     * - GeoJson created.
     *
     * Validation:
     * todo:
     *  - Elevation Max: assert max is greater than elevation
     *  - Location Type: ensure only Point is valid in POST
     */
    public function testLocationPost(): void
    {
        $locJson = [
            'displayName' => 'Test Location',
            'description' => 'Test Description',
            'elevation' => 1000,
            'elevationMax' => 2000,
            'latitude' => 12,
            'longitude' => 67, // Note: by including both lat and lng, a GeoJson entity will be created.
            'locationTypeId' => LocationTypeFactory::createOne()->getId(),
            'parentId' => LocationFactory::createOne()->getId(),
            'habitatTypeId' => HabitatTypeFactory::createOne()->getId(),
        ];

        $this->browser()
            ->post('/api/location', [
                'json' => $locJson,
                'headers' => [
                    'Content-Type' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $this->generateEditorToken(),
                ]
            ])
            ->assertStatus(201)
//            ->dump()
            ->use(function (Json $json) {
                $jsonDecoded = $json->decoded();
                $assertKeys = [
                    '@context', // presence confirms json+ld is returned
                    '@id', // presence confirms json+ld is returned
                    '@type', // presence confirms json+ld is returned
                    'id',
                    'displayName',
                    'description',
                    'elevation',
                    'elevationMax',
                    'elevUnitAbbrv', // presence confirms outputDto was returned
//                    'isoCode', // Only returned for countries, which can not be created with post
                    'latitude',
                    'longitude',
                    'geoJson',  // presence confirms that passing lat & lng resulted in a new GeoJson
                    'habitatType', // presence confirms outputDto was returned
                    'locationType', // presence confirms outputDto was returned
                    'interactions', // presence confirms outputDto was returned
                    'parent', // presence confirms outputDto was returned
                    'children', // presence confirms outputDto was returned
                    'created', // presence confirms timestampable extension is working
                    'createdBy', // presence confirms blameable extension is working
                    'updated', // presence confirms timestampable extension is working
                    'updatedBy' // presence confirms blameable extension is working
                ];

                foreach ($assertKeys as $key) { // All expected keys are present
                    $this->assertArrayHasKey($key, $jsonDecoded);
                }
                foreach ($jsonDecoded as $key => $value) { // No additional keys are present
                    $this->assertTrue(in_array($key, $assertKeys));
                }
            });
    }

    /**
     * Side-effects:
     * - GeoJson modified.
     * - GeoJson removed.
     */
    public function testLocationPatchWithGeoJson(): void
    {
        $location = LocationFactory::new()
            ->withGeoJson('8', '2')
            ->create();

        $this->browser()
            ->patch($this->getEntityIri($location), [
                'json' => [
                    'displayName' => 'Updated Name',
                    'latitude' => 11.1,
                    'longitude' => 99.9,
                ],
                'headers' => [
                    'Content-Type' => 'application/merge-patch+json',
                    'Authorization' => 'Bearer ' . $this->generateEditorToken(),
                ]
            ])
//            ->dump()
            ->assertStatus(200)
            ->assertJsonMatches('displayName', 'Updated Name')
            ->assertJsonMatches('latitude', 11.1)
            ->assertJsonMatches('longitude', 99.9)
            // Asserts GeoJson is updated when lat & lng are updated
            ->assertJsonMatches('geoJson.displayPoint', '[99.9, 11.1]')
            ->assertJsonMatches('geoJson.type', 'Point')
            // Tests that GeoJson is removed when lat & lng are removed
            ->patch($this->getEntityIri($location), [
                'json' => [
                    'latitude' => null,
                    'longitude' => null,
                ],
                'headers' => [
                    'Content-Type' => 'application/merge-patch+json',
                    'Authorization' => 'Bearer ' . $this->generateEditorToken(),
                ]
            ])
            ->assertStatus(200)
//            ->dump()
            ->use(function (Json $json) {
                $jsonDecoded = $json->decoded();
                $keys = [
                    'latitude',
                    'longitude',
                    'geoJson',
                ];

                foreach ($keys as $key) {
                    $this->assertThat(
                        array_key_exists($key, $jsonDecoded),
                        $this->isFalse()
                    );
                }
            });
    }

    public function testLocationDelete(): void
    {
        $location = LocationFactory::createOne();

        $this->browser()
            ->delete($this->getEntityIri($location), [
                'headers' => [
                    'Content-Type' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $this->generateEditorToken(),
                ]
            ])
            ->assertStatus(204);
    }
}