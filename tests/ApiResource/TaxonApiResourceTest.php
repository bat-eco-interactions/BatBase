<?php

namespace App\Tests\ApiResource;

use App\DataFixtures\Factory\RankFactory;
use App\DataFixtures\Factory\TaxonFactory;
use Zenstruck\Browser\Json;
use Zenstruck\Foundry\Test\ResetDatabase;

/**
 * Tests /api/taxon endpoints
 * @todo
 *  - test all possible validation errors
 */
class TaxonApiResourceTest extends ApiTestCase
{
    use ResetDatabase;

    public function testTaxonGet(): void
    {
        $taxon = TaxonFactory::createOne();

        $this->browser()
            ->get($this->getEntityIri($taxon))
//            ->dump()
            ->assertStatus(200);
    }

    public function testTaxonGetCollection(): void
    {
        TaxonFactory::createOne(); // Creates a parentTaxon, GroupRoot, and Group

        $this->browser()
            ->get('/api/taxon')
//            ->dump()
            ->assertStatus(200)
            ->assertJsonMatches('"hydra:totalItems"', 2);
    }

    /**
     * Every possible input is sent and all output props should be returned.
     */
    public function testTaxonPost(): void
    {
        $parentTaxon = TaxonFactory::createOne();
        $rank = RankFactory::createOne();
        $json = [
            'displayName' => 'Family Arcanea',
            'name' => 'Arcanea',
            'parentId' => $parentTaxon->getId(),
            'rankId' => $rank->getId()
        ];

        $this->browser()
            ->post('/api/taxon', [
                'json' => $json,
                'headers' => [
                    'Content-Type' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $this->generateEditorToken(),
                ]
            ])
            ->assertStatus(201)
//            ->dump()
            ->use(function (Json $json) {
                $jsonDecoded = $json->decoded();
                $assertKeys = [
                    '@context', // presence confirms json+ld is returned
                    '@id', // presence confirms json+ld is returned
                    '@type', // presence confirms json+ld is returned
                    'id',
                    'displayName',
                    'name',
                    'isRoot', // presence confirms outputDto was returned
                    'children', // presence confirms outputDto was returned
                    'parent',  // presence confirms outputDto was returned
                    'rank', // presence confirms outputDto was returned
                    'group', // presence confirms outputDto was returned
                    'subjectRoles', // presence confirms outputDto was returned
                    'objectRoles', // presence confirms outputDto was returned
                    'created', // presence confirms timestampable extension is working
                    'createdBy', // presence confirms blameable extension is working
                    'updated', // presence confirms timestampable extension is working
                    'updatedBy' // presence confirms blameable extension is working
                ];

                foreach ($assertKeys as $key) {
                    $this->assertArrayHasKey($key, $jsonDecoded);
                }
            });
    }

    public function testTaxonPatch(): void
    {
        $taxon = TaxonFactory::createOne();
        $newTaxon = TaxonFactory::createOne();
        $rank = RankFactory::createOne();

        $this->browser()
            ->patch($this->getEntityIri($taxon), [
                'json' => [
                    'displayName' => 'Updated Name',
                    'name' => 'Name',
                    'rankId' => $rank->getId(),
                    'parentId' => $newTaxon->getId(),
                ],
                'headers' => [
                    'Content-Type' => 'application/merge-patch+json',
                    'Authorization' => 'Bearer ' . $this->generateEditorToken(),
                ]
            ])
            ->assertStatus(200)
//            ->dump()
            ->assertJsonMatches('displayName', 'Updated Name')
            ->assertJsonMatches('name', 'Name')
            ->assertJsonMatches('parent', $this->getEntityIri($newTaxon))
            ->assertJsonMatches('rank.id', $rank->getId());
    }

    public function testTaxonDelete(): void
    {
        $taxon = TaxonFactory::createOne();

        $this->browser()
            ->delete($this->getEntityIri($taxon), [
                'headers' => [
                    'Content-Type' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $this->generateEditorToken(),
                ]
            ])
            ->assertStatus(204);
    }
}