<?php

namespace App\Tests\ApiResource;

use App\DataFixtures\Factory\CitationFactory;
use App\DataFixtures\Factory\InteractionFactory;
use App\DataFixtures\Factory\InteractionTypeFactory;
use App\DataFixtures\Factory\LocationFactory;
use App\DataFixtures\Factory\TagFactory;
use App\DataFixtures\Factory\TaxonFactory;
use App\Entity\InteractionType;
use Zenstruck\Browser\Json;
use Zenstruck\Foundry\Test\ResetDatabase;

/**
 * Tests /api/interaction endpoints
 * @todo
 *  - test all possible validation errors
 */
class InteractionApiResourceTest extends ApiTestCase
{
    use ResetDatabase;

    public function testInteractionGet(): void
    {
        $interaction = InteractionFactory::createOne();

        $this->browser()
            ->get($this->getEntityIri($interaction))
//            ->dump()
            ->assertStatus(200);
    }

    public function testInteractionGetCollection(): void
    {
        InteractionFactory::createMany(2);

        $this->browser()
            ->get('/api/interaction')
//            ->dump()
            ->assertStatus(200)
            ->assertJsonMatches('"hydra:totalItems"', 2);
    }

    /**
     * Every possible input is sent and all output props should be returned.
     *
     * Side-effects:
     * - Source (author) is created
     * - Display name and full name are auto-generated
     */
    public function testInteractionPost(): void
    {
        $json = [
            'date' => '1990-06-21 to 1993-04-13',
            'note' => 'It is for these reasons that I regard the decision last year to shift our efforts in space from low to high gear as among the most important decisions that will be made during my incumbency in the office of the Presidency.',
            'pages' => '55-57',
            'quote' => 'We choose to go to the moon. We choose to go to the moon in this decade and do the other things, not because they are easy, but because they are hard, because that goal will serve to organize and measure the best of our energies and skills, because that challenge is one that we are willing to accept, one we are unwilling to postpone, and one which we intend to win, and the others, too.',
            'interactionTypeId' => InteractionTypeFactory::createOne()->getId(),
            'citationId' => CitationFactory::createOne()->getId(),
            'locationId' => LocationFactory::createOne()->getId(),
            'subjectId' => TaxonFactory::createOne()->getId(),
            'objectId' => TaxonFactory::createOne()->getId(),
            'tagIds' => [
                TagFactory::createOne()->getId(),
                TagFactory::createOne()->getId()
            ]
        ];

        $this->browser()
            ->post('/api/interaction', [
                'json' => $json,
                'headers' => [
                    'Content-Type' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $this->generateEditorToken(),
                ]
            ])
//            ->dump()
            ->assertStatus(201)
            ->use(function (Json $json) {
                $jsonDecoded = $json->decoded();
                $assertKeys = [
                    '@context', // presence confirms json+ld is returned
                    '@id', // presence confirms json+ld is returned
                    '@type', // presence confirms json+ld is returned
                    'id',
                    'date',
                    'note',
                    'pages',
                    'quote',
                    'interactionType', // presence confirms outputDto was returned
                    'citation', // presence confirms outputDto was returned
                    'location', // presence confirms outputDto was returned
                    'subject', // presence confirms outputDto was returned
                    'subjectGroupId', // presence confirms outputDto was returned
                    'object', // presence confirms outputDto was returned
                    'objectGroupId', // presence confirms outputDto was returned
                    'tags', // presence confirms outputDto was returned
                    'created', // presence confirms timestampable extension is working
                    'createdBy', // presence confirms blameable extension is working
                    'updated', // presence confirms timestampable extension is working
                    'updatedBy' // presence confirms blameable extension is working
                ];

                foreach ($assertKeys as $key) { // All expected keys are present
                    $this->assertArrayHasKey($key, $jsonDecoded);
                }
                foreach ($jsonDecoded as $key => $value) { // No additional keys are present
                    $this->assertTrue(in_array($key, $assertKeys));
                }
            });
    }

    /**
     * Side-effects:
     * - POST without a location will have the Unspecified Location auto-set.
     */
    public function testInteractionPostWithNoLocation()
    {
        $location = LocationFactory::createOne([
            'displayName' => 'Unspecified'
        ]);
        $json = [
            'citationId' => CitationFactory::createOne()->getId(),
            'interactionTypeId' => InteractionTypeFactory::createOne()->getId(),
            'objectId' => TaxonFactory::createOne()->getId(),
            'subjectId' => TaxonFactory::createOne()->getId(),
        ];
        $this->browser()
            ->post('/api/interaction', [
                'json' => $json,
                'headers' => [
                    'Content-Type' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $this->generateEditorToken(),
                ]
            ])
//            ->dump()
            ->assertStatus(201)
            ->assertJsonMatches('location', $this->getEntityIri($location));
    }

    /**
     * Side effects tested:
     * - If Location is removed, the Unspecified Location is added.
     */
    public function testInteractionPatch(): void
    {
        $location = LocationFactory::createOne([
            'displayName' => 'Unspecified'
        ]); // Must be set before the interaction is generated due to some weirdness with how flush is happening
        $interaction = InteractionFactory::createOne();
        $interactionType = InteractionTypeFactory::createOne();

        $this->browser()
            ->patch($this->getEntityIri($interaction), [
                'json' => [
                    'interactionTypeId' => $interactionType->getId(),
                    'locationId' => false,
                ],
                'headers' => [
                    'Content-Type' => 'application/merge-patch+json',
                    'Authorization' => 'Bearer ' . $this->generateEditorToken(),
                ]
            ])
//            ->dump()
            ->assertStatus(200)
            ->assertJsonMatches('location', $this->getEntityIri($location))
            ->assertJsonMatches('interactionType.id', $interactionType->getId());
    }

    /**
     * Side effects:
     * - Each entity related to the interaction has been updated with the interaction.
     * -- Citation->isDirect should be TRUE
     * -- The subject Taxon should have the interaction in their subjRoles
     * -- The object Taxon should have the interaction in their objRoles
     */
    public function testInteractionAddedToRelatedEntities()
    {
        $citation = CitationFactory::createOne();
        $location = LocationFactory::createOne();
        $subjectTaxon = TaxonFactory::createOne();
        $objectTaxon = TaxonFactory::createOne();
        $interaction = InteractionFactory::createOne([
            'location' => $location,
            'subject' => $subjectTaxon,
            'object' => $objectTaxon,
            'source' => $citation->getSource(),
        ]);

        $this->browser()
            // Citation
            ->get($this->getEntityIri($citation))
//            ->dump()
            ->assertStatus(200)
            ->assertJsonMatches('isDirect', true)
            ->use(function (Json $json) use ($interaction) {
                $ints = $json->search("interactions");
                $this->assertTrue(in_array($this->getEntityIri($interaction), $ints));
            })
            // Location
            ->get($this->getEntityIri($location))
//            ->dump()
            ->assertStatus(200)
            ->use(function (Json $json) use ($interaction) {
                $ints = $json->search("interactions");
                $this->assertTrue(in_array($this->getEntityIri($interaction), $ints));
            })
            // Subject Taxon
            ->get($this->getEntityIri($subjectTaxon))
//            ->dump()
            ->assertStatus(200)
            ->use(function (Json $json) use ($interaction) {
                $ints = $json->search("subjectRoles");
                $this->assertTrue(in_array($this->getEntityIri($interaction), $ints));
            })
            // Object Taxon
            ->get($this->getEntityIri($objectTaxon))
//            ->dump()
            ->assertStatus(200)
            ->use(function (Json $json) use ($interaction) {
                $ints = $json->search("objectRoles");
                $this->assertTrue(in_array($this->getEntityIri($interaction), $ints));
            });
    }

    public function testInteractionDelete(): void
    {
        $interaction = InteractionFactory::createOne();

        $this->browser()
            ->delete($this->getEntityIri($interaction), [
                'headers' => [
                    'Content-Type' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $this->generateEditorToken(),
                ]
            ])
            ->assertStatus(204);
    }
}
