<?php

namespace App\Tests\ApiResource;

use ApiPlatform\Api\IriConverterInterface;
use App\DataFixtures\Factory\ApiTokenFactory;
use App\DataFixtures\Factory\UserFactory;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Zenstruck\Browser\HttpOptions;
use Zenstruck\Browser\KernelBrowser;
use Zenstruck\Browser\Test\HasBrowser;

abstract class ApiTestCase extends KernelTestCase
{
    use HasBrowser {
        browser as baseKernelBrowser;
    }

    protected function browser(array $options = [], array $server = []): KernelBrowser
    {
        return $this->baseKernelBrowser($options, $server)
            ->setDefaultHttpOptions(
                HttpOptions::create()
                    ->withHeader('Accept', 'application/ld+json')

            );
    }

    /* =================== ACCESS TOKENS =================================== */
    protected function generateContributorToken(): string
    {
        return ApiTokenFactory::createOne([
            'ownedBy' => UserFactory::createOne(),
            'scopes' => ['ROLE_CONTRIBUTOR']
        ])->getToken();
    }

    protected function generateEditorToken(): string
    {
        return ApiTokenFactory::createOne([
            'ownedBy' => UserFactory::createOne(),
            'scopes' => ['ROLE_EDITOR']
        ])->getToken();
    }

    /* =================== HELPERS ========================================= */
    protected function getEntityIri(object $proxyEntity): string
    {
        $entity = $proxyEntity->_real();
        $classParts = explode('\\', $entity::class);
        $entityName = lcfirst(end($classParts));
        return "/api/$entityName/{$entity->getId()}";
    }
}
