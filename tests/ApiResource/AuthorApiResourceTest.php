<?php

namespace App\Tests\ApiResource;

use App\DataFixtures\Factory\AuthorFactory;
use App\DataFixtures\Factory\SourceTypeFactory;
use Zenstruck\Browser\Json;
use Zenstruck\Foundry\Test\ResetDatabase;

/**
 * Tests /api/author endpoints
 * @todo
 *  - test all possible validation errors
 */
class AuthorApiResourceTest extends ApiTestCase
{
    use ResetDatabase;

    public function testAuthorGet(): void
    {
        $author = AuthorFactory::createOne();

        $this->browser()
            ->get($this->getEntityIri($author))
//            ->dump()
            ->assertStatus(200);
    }

    public function testAuthorGetCollection(): void
    {
        AuthorFactory::createMany(2);

        $this->browser()
            ->get('/api/author')
//            ->dump()
            ->assertStatus(200)
            ->assertJsonMatches('"hydra:totalItems"', 2);
    }

    /**
     * Every possible input is sent and all output props should be returned.
     *
     * Side-effects:
     * - Source (author) is created
     * - Display name and full name are auto-generated
     */
    public function testAuthorPost(): void
    {
        SourceTypeFactory::createOne([
            'displayName' => 'Author'
        ]);
        $json = [
            'lastName' => 'Last',
            'firstName' => 'First',
            'middleName' => 'Middle',
            'suffix' => 'Suffix',
            'description' => 'Description',
            'linkUrl' => 'Link',
        ];

        $this->browser()
            ->post('/api/author', [
                'json' => $json,
                'headers' => [
                    'Content-Type' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $this->generateEditorToken(),
                ]
            ])
//            ->dump()
            ->assertStatus(201)
            ->use(function (Json $json) {
                $jsonDecoded = $json->decoded();
                $assertKeys = [
                    '@context', // presence confirms json+ld is returned
                    '@id', // presence confirms json+ld is returned
                    '@type', // presence confirms json+ld is returned
                    'id',
                    'firstName',
                    'middleName',
                    'lastName',
                    'suffix',
                    'displayName', // presence confirms outputDto was returned
                    'fullName', // presence confirms outputDto was returned
                    'description',
                    'linkUrl',
                    'source', // presence confirms outputDto was returned
                    'created', // presence confirms timestampable extension is working
                    'createdBy', // presence confirms blameable extension is working
                    'updated', // presence confirms timestampable extension is working
                    'updatedBy' // presence confirms blameable extension is working
                ];

                foreach ($assertKeys as $key) { // All expected keys are present
                    $this->assertArrayHasKey($key, $jsonDecoded);
                }
                foreach ($jsonDecoded as $key => $value) { // No additional keys are present
                    $this->assertTrue(in_array($key, $assertKeys));
                }
            });
    }

    /**
     * Side effects:
     * - Display name and full name are auto-generated.
     */
    public function testAuthorPatch(): void
    {
        $author = AuthorFactory::createOne();

        $this->browser()
            ->patch($this->getEntityIri($author), [
                'json' => [
                    'firstName' => 'First',
                    'middleName' => 'Middle',
                    'lastName' => 'Last',
                    'suffix' => 'Suffix',
                ],
                'headers' => [
                    'Content-Type' => 'application/merge-patch+json',
                    'Authorization' => 'Bearer ' . $this->generateEditorToken(),
                ]
            ])
//            ->dump()
            ->assertStatus(200)
            ->assertJsonMatches('displayName', 'Last, First Middle Suffix')
            ->assertJsonMatches('fullName', 'First Middle Last Suffix');
    }

    public function testAuthorDelete(): void
    {
        $author = AuthorFactory::createOne();

        $this->browser()
            ->delete($this->getEntityIri($author), [
                'headers' => [
                    'Content-Type' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $this->generateEditorToken(),
                ]
            ])
            ->assertStatus(204);
    }
}