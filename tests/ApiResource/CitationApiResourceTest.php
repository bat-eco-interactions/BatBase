<?php

namespace App\Tests\ApiResource;

use App\DataFixtures\Factory\AuthorFactory;
use App\DataFixtures\Factory\CitationFactory;
use App\DataFixtures\Factory\CitationTypeFactory;
use App\DataFixtures\Factory\PublicationFactory;
use App\DataFixtures\Factory\SourceTypeFactory;
use Zenstruck\Browser\Json;
use Zenstruck\Foundry\Test\ResetDatabase;

/**
 * Tests /api/citation endpoints
 * @todo
 *  - test all possible validation errors
 */
class CitationApiResourceTest extends ApiTestCase
{
    use ResetDatabase;

    public function testCitationGet(): void
    {
        $citation = CitationFactory::createOne();

        $this->browser()
            ->get($this->getEntityIri($citation))
//            ->dump()
            ->assertStatus(200);
    }

    public function testCitationGetCollection(): void
    {
        CitationFactory::createMany(2);

        $this->browser()
            ->get('/api/citation')
//            ->dump()
            ->assertStatus(200)
            ->assertJsonMatches('"hydra:totalItems"', 2);
    }

    /**
     * Every possible input is sent and all output props should be returned.
     *
     * Side-effects:
     * - Source (citation) is created
     * - Contribution is created
     *
     * Validation:
     * - Year: 4 digits
     * todo:
     *  - Doi: Valid pattern
     *  - LinkUrl: Valid pattern
     *  - publicationPages:
     *   -- Valid pattern = "##-##" || "##"
     *   -- Valid range
     *  - Full text: Valid citation format
     *  - Citation Type: Valid match with publication type
     */
    public function testCitationPost(): void
    {
        SourceTypeFactory::createOne([
            'displayName' => 'Citation'
        ]);
        $author = AuthorFactory::createOne();
        $json = [
            'title' => 'Article Citation',
            'fullText' => 'It is for these reasons that I regard the decision last year to shift our efforts in space from low to high gear as among the most important decisions that will be made during my incumbency in the office of the Presidency.',
            'abstract' => 'We choose to go to the moon. We choose to go to the moon in this decade and do the other things, not because they are easy, but because they are hard, because that goal will serve to organize and measure the best of our energies and skills, because that challenge is one that we are willing to accept, one we are unwilling to postpone, and one which we intend to win, and the others, too.',
            'publicationVolume' => 2,
            'publicationIssue' => 5,
            'publicationPages' => '55-57',
            'year' => 1978,
            'doi' => '10.1093/ajae/aaq063',
            'linkUrl' => 'www.citation.org',
            'citationTypeId' => CitationTypeFactory::createOne([
                'displayName' => 'Article'
            ])->getId(),
            'publicationId' => PublicationFactory::createOne()->getId(),
            'contributors' => [
                $author->getId() => [
                    'authId' => $author->getId(),
                    'ord' => 1
                ]
            ]
        ];

        $this->browser()
            ->post('/api/citation', [
                'json' => $json,
                'headers' => [
                    'Content-Type' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $this->generateEditorToken(),
                ]
            ])
//            ->dump()
            ->assertStatus(201)
            ->use(function (Json $json) {
                $jsonDecoded = $json->decoded();
                $assertKeys = [
                    '@context', // presence confirms json+ld is returned
                    '@id', // presence confirms json+ld is returned
                    '@type', // presence confirms json+ld is returned
                    'id',
                    'isDirect',  // True when related to an interaction
                    'title',
                    'displayName',
                    'fullText',
                    'abstract',
                    'publicationVolume',
                    'publicationIssue',
                    'publicationPages',
                    'year',
                    'doi',
                    'linkUrl',
                    'citationType', // presence confirms outputDto was returned
                    'publication', // presence confirms outputDto was returned
                    'authors', // presence confirms outputDto was returned
                    'contributors',
                    'interactions', // presence confirms outputDto was returned
                    'source', // presence confirms outputDto was returned
                    'created', // presence confirms timestampable extension is working
                    'createdBy', // presence confirms blameable extension is working
                    'updated', // presence confirms timestampable extension is working
                    'updatedBy' // presence confirms blameable extension is working
                ];

                foreach ($assertKeys as $key) { // All expected keys are present
                    $this->assertArrayHasKey($key, $jsonDecoded);
                }
                foreach ($jsonDecoded as $key => $value) { // No additional keys are present
                    $this->assertTrue(in_array($key, $assertKeys));
                }
            });
    }

    /**
     * Side-effects:
     * - Contribution is created
     * - Previous Contribution is removed
     */
    public function testCitationPatch(): void
    {
        $citation = CitationFactory::createOne();
        $author = AuthorFactory::createOne();
        $contributors = [
            $author->getId() => [
                'authId' => $author->getId(),
                'ord' => 1
            ]
        ];

        $this->browser()
            ->patch($this->getEntityIri($citation), [
                'json' => [
                    'title' => 'Patched',
                    'contributors' => $contributors,
                ],
                'headers' => [
                    'Content-Type' => 'application/merge-patch+json',
                    'Authorization' => 'Bearer ' . $this->generateEditorToken(),
                ]
            ])
//            ->dump()
            ->assertStatus(200)
            ->assertJsonMatches('displayName', 'Patched')
            ->use(function (Json $json) use ($author) {
                $authorId = $author->getId();
//                dump($json->search("contributors"));
                $respContribs = $json->search("contributors");
                $this->assertArrayHasKey($authorId, $respContribs);
//                dump($respContribs[$authorId]);
                $this->assertTrue(!$respContribs[$authorId]['isEditor']);
                $this->assertTrue(isset($respContribs[$authorId]['contribId']));
//                $json->assertMatches('keys("contributors")[0]', $author->getId());
            });
    }

    public function testCitationDelete(): void
    {
        $citation = CitationFactory::createOne();

        $this->browser()
            ->delete($this->getEntityIri($citation), [
                'headers' => [
                    'Content-Type' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $this->generateEditorToken(),
                ]
            ])
            ->assertStatus(204);
    }
}