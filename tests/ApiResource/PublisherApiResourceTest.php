<?php

namespace App\Tests\ApiResource;

use App\DataFixtures\Factory\PublisherFactory;
use App\DataFixtures\Factory\SourceTypeFactory;
use Zenstruck\Browser\Json;
use Zenstruck\Foundry\Test\ResetDatabase;

/**
 * Tests /api/publisher endpoints
 * @todo
 *  - test all possible validation errors
 */
class PublisherApiResourceTest extends ApiTestCase
{
    use ResetDatabase;

    public function testPublisherGet(): void
    {
        $publisher = PublisherFactory::createOne();

        $this->browser()
            ->get($this->getEntityIri($publisher))
            ->dump()
            ->assertStatus(200);
    }

    public function testPublisherGetCollection(): void
    {
        PublisherFactory::createMany(2);

        $this->browser()
            ->get('/api/publisher')
            ->dump()
            ->assertStatus(200)
            ->assertJsonMatches('"hydra:totalItems"', 2);
    }

    public function testPublisherPost(): void
    {
        SourceTypeFactory::createOne([
            'displayName' => 'Publisher'
        ]);
        $json = [
            'displayName' => 'Publisher',
            'city' => 'Pueblo',
            'country' => 'Mexico',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod',
            'linkUrl' => 'www.publisher.com',
        ];

        $this->browser()
            ->post('/api/publisher', [
                'json' => $json,
                'headers' => [
                    'Content-Type' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $this->generateEditorToken(),
                ]
            ])
            ->dump()
            ->assertStatus(201)
            ->use(function (Json $json) {
                $jsonDecoded = $json->decoded();
                $assertKeys = [
                    '@context', // presence confirms json+ld is returned
                    '@id', // presence confirms json+ld is returned
                    '@type', // presence confirms json+ld is returned
                    'id',
                    'displayName',
                    'city',
                    'country',
                    'description',
                    'linkUrl',
                    'source', // presence confirms outputDto was returned
                    'created', // presence confirms timestampable extension is working
                    'createdBy', // presence confirms blameable extension is working
                    'updated', // presence confirms timestampable extension is working
                    'updatedBy' // presence confirms blameable extension is working
                ];

                foreach ($assertKeys as $key) { // All expected keys are present
                    $this->assertArrayHasKey($key, $jsonDecoded);
                }
                foreach ($jsonDecoded as $key => $value) { // No additional keys are present
                    $this->assertTrue(in_array($key, $assertKeys));
                }
            });
    }

    public function testPublisherPatch(): void
    {
        $publisher = PublisherFactory::createOne();

        $this->browser()
            ->patch($this->getEntityIri($publisher), [
                'json' => [
                    'country' => 'Brazil'
                ],
                'headers' => [
                    'Content-Type' => 'application/merge-patch+json',
                    'Authorization' => 'Bearer ' . $this->generateEditorToken(),
                ]
            ])
            ->dump()
            ->assertStatus(200)
            ->assertJsonMatches('country', 'Brazil');
    }

    public function testPublisherDelete(): void
    {
        $publisher = PublisherFactory::createOne();

        $this->browser()
            ->delete($this->getEntityIri($publisher), [
                'headers' => [
                    'Content-Type' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $this->generateEditorToken(),
                ]
            ])
            ->assertStatus(204);
    }
}