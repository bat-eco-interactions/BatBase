<?php

namespace App\Tests\ApiResource;

use App\DataFixtures\Factory\AuthorFactory;
use App\DataFixtures\Factory\PublicationFactory;
use App\DataFixtures\Factory\PublicationTypeFactory;
use App\DataFixtures\Factory\PublisherFactory;
use App\DataFixtures\Factory\SourceTypeFactory;
use Zenstruck\Browser\Json;
use Zenstruck\Foundry\Test\ResetDatabase;

/**
 * Tests /api/publication endpoints
 * @todo
 *  - test all possible validation errors
 */
class PublicationApiResourceTest extends ApiTestCase
{
    use ResetDatabase;

    public function testPublicationGet(): void
    {
        $publication = PublicationFactory::createOne();

        $this->browser()
            ->get($this->getEntityIri($publication))
//            ->dump()
            ->assertStatus(200);
    }

    public function testPublicationGetCollection(): void
    {
        PublicationFactory::createMany(2);

        $this->browser()
            ->get('/api/publication')
//            ->dump()
            ->assertStatus(200)
            ->assertJsonMatches('"hydra:totalItems"', 2);
    }

    /**
     * Every possible input is sent and all output props should be returned.
     *
     * Side-effects:
     * - Contributions created
     */
    public function testPublicationPost(): void
    {
        $author = AuthorFactory::createOne();
        $pubType = PublicationTypeFactory::createOne();
        $publisher = PublisherFactory::createOne();
        SourceTypeFactory::createOne([
            'displayName' => 'Publication'
        ]);
        $json = [
            'displayName' => 'Publication',
            'year' => '1978',
            'doi' => '10.1093/ajae/aaq063',
            'linkUrl' => 'www.publication.org',
            'description' => 'Description of publication',
            'publicationTypeId' => $pubType->getId(),
            'publisherId' => $publisher->getId(),
            'contributors' => [
                $author->getId() => [
                    'authId' => $author->getId(),
                    'isEditor' => true,
                    'ord' => 1
                ],
            ]
        ];

        $this->browser()
            ->post('/api/publication', [
                'json' => $json,
                'headers' => [
                    'Content-Type' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $this->generateEditorToken(),
                ]
            ])
            ->dump()
            ->assertStatus(201)
            ->use(function (Json $json) {
                $jsonDecoded = $json->decoded();
                $assertKeys = [
                    '@context', // presence confirms json+ld is returned
                    '@id', // presence confirms json+ld is returned
                    '@type', // presence confirms json+ld is returned
                    'id',
                    'displayName',
                    'year',
                    'doi',
                    'linkUrl',
                    'description',
                    'publicationType', // presence confirms outputDto was returned
                    'publisher', // presence confirms outputDto was returned
//                    'authors', // Only editor or authors can be set at one time (tested in patch)
                    'editors', // presence confirms outputDto was returned
                    'contributors',
                    'source', // presence confirms outputDto was returned
                    'created', // presence confirms timestampable extension is working
                    'createdBy', // presence confirms blameable extension is working
                    'updated', // presence confirms timestampable extension is working
                    'updatedBy' // presence confirms blameable extension is working
                ];

                foreach ($assertKeys as $key) { // All expected keys are present
                    $this->assertArrayHasKey($key, $jsonDecoded);
                }
                foreach ($jsonDecoded as $key => $value) { // No additional keys are present
                    $this->assertTrue(in_array($key, $assertKeys));
                }
            });
    }

    /**
     * Side effects:
     * - Contributions are updated
     */
    public function testPublicationPatch(): void
    {
        $publication = PublicationFactory::createOne();
        $author = AuthorFactory::createOne();
        $contributors = [
            $author->getId() => [
                'authId' => $author->getId(),
                'isEditor' => false,
                'ord' => 1
            ]
        ];

        $this->browser()
            ->patch($this->getEntityIri($publication), [
                'json' => [
                    'displayName' => 'Patched publication',
                    'contributors' => $contributors,
                ],
                'headers' => [
                    'Content-Type' => 'application/merge-patch+json',
                    'Authorization' => 'Bearer ' . $this->generateEditorToken(),
                ]
            ])
//            ->dump()
            ->assertStatus(200)
            ->assertJsonMatches('displayName', 'Patched publication')
            ->use(function (Json $json) use ($author) {
                $authors = $json->search('authors');
                $this->assertTrue(count($authors) === 1);
                $authorId = $author->getId();
//                dump($json->search("contributors"));
                $respContribs = $json->search('contributors');
                $this->assertArrayHasKey($authorId, $respContribs);
//                dump($respContribs[$authorId]);
                $this->assertThat(
                    $respContribs[$authorId]['isEditor'],
                    $this->isFalse()
                );
                $this->assertTrue(isset($respContribs[$authorId]['contribId']));
//                $json->assertMatches('keys("contributors")[0]', $author->getId());
            });
    }

    public function testPublicationDelete(): void
    {
        $publication = PublicationFactory::createOne();

        $this->browser()
            ->delete($this->getEntityIri($publication), [
                'headers' => [
                    'Content-Type' => 'application/ld+json',
                    'Authorization' => 'Bearer ' . $this->generateEditorToken(),
                ]
            ])
            ->assertStatus(204);
    }
}