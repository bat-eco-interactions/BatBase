<?php

declare(strict_types=1);

namespace App\Tests\Service\Aggregator;

use App\DataFixtures\Factory\CitationFactory;
use App\DataFixtures\Factory\GroupRootFactory;
use App\DataFixtures\Factory\InteractionFactory;
use App\DataFixtures\Factory\LocationFactory;
use App\DataFixtures\Factory\TaxonFactory;
use App\DataFixtures\Factory\UserFactory;
use App\Service\Aggregator\ProjectStatSummary;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Zenstruck\Foundry\Test\ResetDatabase;
use Zenstruck\Foundry\Test\Factories;

class ProjectStatSummaryTest extends KernelTestCase
{
    use ResetDatabase;
    use Factories;

    /**
     * Verifies that the service correctly builds the core entity-metrics.
     *
     * @return void
     */
    public function testProjectCoreStatSummary(): void
    {
        $this->initEntityFixturesNeededForCoreMetrics();

        $service = $this->bootKernelAndGetService();
        $summary = $service->getProjectStatSummary('all');
        $expected = [
            'bat' => '5',
            'cite' => '6',
            'int' => '12',
            'loc' => '3',
        ];

        foreach ($expected as $key => $value) {
            $this->assertArrayHasKey($key, $summary);
            $this->assertStringContainsString($value, $summary[$key]);
        }
    }

    /**
     * Verifies that the service correctly builds the "About Database" statistics.
     *
     * @return void
     */
    public function testProjectDatabaseStatSummary(): void
    {
        $this->initEntityFixturesNeededForCoreMetrics();
        $this->initNonBatEntityFixturesNeededForDatabaseMetrics();

        $service = $this->bootKernelAndGetService();
        $summary = $service->getProjectStatSummary('db');
        $expected = [
            'bat' => '5',
            'nonBat' => '3',
            'cite' => '6',
            'int' => '12',
            'loc' => '3',
        ];

        foreach ($expected as $key => $value) {
            $this->assertArrayHasKey($key, $summary);
            $this->assertStringContainsString($value, $summary[$key]);
        }
    }

    /**
     * Verifies that the service correctly builds the "About Project" statistics.
     *
     * @return void
     */
    public function testAboutProjectStatSummary(): void
    {
        $this->initUserFixturesNeededForProjectMetrics();

        $service = $this->bootKernelAndGetService();
        $summary = $service->getProjectStatSummary('project');
        $expected = [
            'editor' => '10',
            'user' => '15',
        ];

        foreach ($expected as $key => $value) {
            $this->assertArrayHasKey($key, $summary);
            $this->assertStringContainsString($value, $summary[$key]);
        }
    }

    /**
     * Verifies that the service correctly builds and combines all metrics.
     *
     * @return void
     */
    public function testProjectCompleteStatSummary(): void
    {
        // Create user fixtures first to prevent auto-generated user in entity fixtures
        $this->initUserFixturesNeededForProjectMetrics();
        $this->initEntityFixturesNeededForCoreMetrics();
        $this->initNonBatEntityFixturesNeededForDatabaseMetrics();

        $service = $this->bootKernelAndGetService();
        $summary = $service->getProjectStatSummary('all');
        $expected = [
            'bat' => '5',
            'nonBat' => '3',
            'cite' => '6',
            'int' => '12',
            'loc' => '3',
            'editor' => '10',
            'user' => '15',
        ];

        foreach ($expected as $key => $value) {
            $this->assertArrayHasKey($key, $summary);
            $this->assertStringContainsString($value, $summary[$key]);
        }
    }

    private function bootKernelAndGetService(): ProjectStatSummary
    {
        self::bootKernel();
        $container = static::getContainer();
        return $container->get(ProjectStatSummary::class);
    }

    private function initEntityFixturesNeededForCoreMetrics(): void
    {
        // Create bat taxa
        $batRoot = GroupRootFactory::new()
            ->withGroupName('Bat')
            ->create();
        $batTaxon = $batRoot->getTaxon();
        $batTaxon->setName('Chiroptera');

        TaxonFactory::new(['parentTaxon' => $batTaxon])
            ->withRank('Species', 100)
            ->many(5)
            ->create();

        // Create citations
        $citations = CitationFactory::createMany(6);

        // Create locations
        $locCnt = 3;
        $locations = LocationFactory::createMany($locCnt);

        // Create interactions
        foreach ($citations as $citation) {
            InteractionFactory::createMany(2, [
                'source' => $citation->getSource(),
                'location' => $locations[--$locCnt],
            ]);
            if ($locCnt === 0) {
                $locCnt = count($locations);
            }
        }
    }

    private function initNonBatEntityFixturesNeededForDatabaseMetrics(): void
    {
        $nonbatRoot = GroupRootFactory::createOne();
        $nonbatTaxon = $nonbatRoot->getTaxon();

        TaxonFactory::new(['parentTaxon' => $nonbatTaxon])
            ->withRank('Species', 100)
            ->many(3)
            ->create();
    }

    private function initUserFixturesNeededForProjectMetrics(): void
    {
        UserFactory::createMany(10);
        UserFactory::createMany(5, ['roles' => ['ROLE_USER']]);
    }
}
