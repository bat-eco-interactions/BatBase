<?php

declare(strict_types=1);

namespace App\Tests\Service\Aggregator;

use App\DataFixtures\Factory\InteractionFactory;
use App\DataFixtures\Factory\LocationFactory;
use App\DataFixtures\Factory\TaxonFactory;
use App\Entity\Taxon;
use App\Service\Aggregator\TaxonBranchInteractionAggregator;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Zenstruck\Foundry\Test\ResetDatabase;
use Zenstruck\Foundry\Test\Factories;

class TaxonBranchInteractionAggregatorTest extends KernelTestCase
{
    use ResetDatabase;
    use Factories;

    /**
     * Verifies that the service correctly Aggregates total counts of interactions
     * by type and geographical location (regions/countries) for a given taxon and
     * all its descendants.
     *
     * Tests:
     * - all interactions for target and descendants are aggregated
     * - the region->sub_region->country hierarchy is correctly built
     *
     * @return void
     */
    public function testAggregate(): void
    {
        self::bootKernel();
        $container = static::getContainer();
        $service = $container->get(TaxonBranchInteractionAggregator::class);

        $expected = [
            'total' => 18,
            'types' => [
                'consumption' => 6,
                'predation' => 6,
                'cohabitation' => 3,
                'prey' => 3,
            ],
            'locations' => [
                'North America' => [
                    'United States' => 6,
                    'Mexico' => 3
                ],
                'Africa' => [
                    'Sub-Saharan Africa' => [
                        'South Africa' => 3,
                        'Unspecified' => 3
                    ],
                    'Unspecified' => 3
                ],
            ]
        ];

        $targetTaxon = $this->initFixtures();

        $results = $service->aggregate($targetTaxon);

        foreach ($expected as $k => $v) {
            $this->assertEquals($v, $results[$k]);
        }
    }


    /**
     * Creates the taxon and location hierarchies, then creates the interactions
     * associated with them.
     *
     * @return Taxon - The aggregation target
     */
    private function initFixtures(): Taxon
    {
        // create taxon hierarchy
        $target = TaxonFactory::createOne();
        $children = TaxonFactory::new(['parentTaxon' => $target])
            ->many(5)
            ->create();
        foreach ($children as $child) {
            TaxonFactory::new(['parentTaxon' => $child])
                ->many(3)
                ->create();
        }

        // create location hierarchy
        // North America: region -> country(2)
        $noram = LocationFactory::new(['displayName' => 'North America'])
            ->withLocationType('Region')
            ->create();
        $us = LocationFactory::new([
            'parentLocation' => $noram,
            'displayName' => 'United States'
        ])->withLocationType('Country')->create();
        $mx = LocationFactory::new([
            'parentLocation' => $noram,
            'displayName' => 'Mexico'
        ])->withLocationType('Country')->create();

        // Africa: region -> sub_region -> country(3)
        $africa = LocationFactory::new(['displayName' => 'Africa'])
            ->withLocationType('Region')
            ->create();
        $ssa = LocationFactory::new([
            'parentLocation' => $africa,
            'displayName' => 'Sub-Saharan Africa'
        ])->withLocationType('Region')->create();
        $southAfrica = LocationFactory::new([
            'parentLocation' => $ssa,
            'displayName' => 'South Africa'
        ])->withLocationType('Country')->create();

        InteractionFactory::new([
            'location' => $mx,
        ])->withInteractionType('prey')->many(3)->create();

        InteractionFactory::new([
            'location' => $us,
        ])->withInteractionType('consumption')->many(6)->create();

        InteractionFactory::new([
            'location' => $southAfrica,
        ])->withInteractionType('cohabitation')->many(3)->create();

        InteractionFactory::new([
            'location' => $ssa,
        ])->withInteractionType('predation')->many(3)->create();

        InteractionFactory::new([
            'location' => $africa,
        ])->withInteractionType('predation')->many(3)->create();

        return $target->_real();
    }
}
