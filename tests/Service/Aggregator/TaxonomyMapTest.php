<?php

declare(strict_types=1);

namespace App\Tests\Service\Aggregator;

use App\DataFixtures\Factory\InteractionFactory;
use App\DataFixtures\Factory\LocationFactory;
use App\DataFixtures\Factory\TaxonFactory;
use App\Entity\Taxon;
use App\Service\Aggregator\TaxonBranchInteractionAggregator;
use App\Service\Aggregator\TaxonomyMap;
use App\Service\Aggregator\TaxonomyNode;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Zenstruck\Foundry\Test\ResetDatabase;
use Zenstruck\Foundry\Test\Factories;

/**
 * Tests for the TaxonomyMap service which generates hierarchical representations
 * of taxonomic relationships.
 */
class TaxonomyMapTest extends KernelTestCase
{
    use ResetDatabase;
    use Factories;

    /**
     * Tests the generation of a taxonomy map when the target is a root taxon.
     *
     * This test verifies that when given a root taxon, the service:
     * 1. Creates a complete hierarchical representation starting from the root
     * 2. Includes all descendant taxa in their correct positions
     * 3. Maintains the expected number of children at each level:
     *    - Level 1: Root node with 6 children (one being a leaf)
     *    - Level 2: Nodes with 3 children each
     *    - Level 3: Nodes with 3 children each
     *    - Level 4: Leaf nodes with no children
     *
     * The test uses a fixture that creates a four-level deep taxonomy
     * and verifies the structure matches the expected hierarchy.
     *
     * @return void
     */
    public function testGenerateMapWithRootTarget(): void
    {
        self::bootKernel();
        $container = static::getContainer();
        $service = $container->get(TaxonomyMap::class);

        $expected = [
            1 => [
                'firstLeaf' => true,
                'children' => 6,
            ],
            2 => [
                'children' => 3
            ],
            3 => [
                'children' => 3
            ],
            4 => [
                'children' => 0
            ]
        ];

        $targetTaxon = $this->initFixtures('root');
        $results = $service->generateMap($targetTaxon);
        $this->assertChildCount($results,1, $expected);
    }

    /**
     * Tests the generation of a taxonomy map when the target is not a root taxon.
     *
     * This test verifies that when given a non-root taxon, the service:
     * 1. Creates a path from the root to the target taxon, including only direct ancestors
     * 2. Includes all descendants of the target taxon
     * 3. Maintains the expected number of children at each level:
     *    - Level 1: Root node with single child (creating path to target)
     *    - Level 2: Single node with one child (continuing path)
     *    - Level 3: Target node with 3 children
     *    - Level 4: Leaf nodes with no children
     *
     * The key difference from the root target test is that this creates a
     * linear path of ancestors above the target, rather than including all
     * siblings and cousins of the target taxon.
     *
     * @return void
     */
    public function testGenerateMapWithNonRootTarget(): void
    {
        self::bootKernel();
        $container = static::getContainer();
        $service = $container->get(TaxonomyMap::class);

        $expected = [
            1 => [
                'children' => 1,
            ],
            2 => [
                'children' => 1
            ],
            3 => [ //target
                'children' => 3
            ],
            4 => [
                'children' => 0
            ]
        ];

        $targetTaxon = $this->initFixtures('nonRoot');
        $results = $service->generateMap($targetTaxon);
        $this->assertChildCount($results,1, $expected);
    }

    /**
     * Helper method to verify the structure of the generated taxonomy map.
     *
     * Recursively traverses the taxonomy tree and verifies that each level
     * has the expected number of child nodes. The expected array defines
     * the requirements for each level:
     * - 'children': The number of child nodes expected at this level
     * - 'firstLeaf': If true, verifies that the first child at this level is a leaf
     *
     * @param TaxonomyNode $node The current node being verified
     * @param int $level The current depth in the tree (1-based)
     * @param array $expected Map of level numbers to expected properties
     * @return void
     */
    private function assertChildCount(TaxonomyNode $node, int $level, array $expected): void
    {
        if (!isset($expected[$level])) {
            return;
        }
        $children = $node->getChildren();

        // check child count === expected count at level
        $this->assertCount($expected[$level]['children'], $children);

        // if the first child is a leaf, it should have no children
        if (isset($expected[$level]['firstLeaf']) && $expected[$level]['firstLeaf'] === true) {
            $child = array_shift($children);
            $this->assertCount(0, $child->getChildren());
        }

        // check each child has the expected count
        foreach ($children as $child) {
            $this->assertChildCount($child, $level + 1, $expected);
        }
    }

    /**
     * Creates a test taxonomy hierarchy with the following structure:
     * - A root node
     * - 5 children under the root
     * - 3 grandchildren under each child (15 total)
     * - 3 great-grandchildren under each grandchild (45 total)
     *
     * The method can return either:
     * - The root node (when target = 'root')
     * - The first grandchild node (when target = 'nonRoot')
     *
     * This provides a consistent test hierarchy that can be used to verify
     * both root and non-root taxonomy map generation.
     *
     * @param string $target Whether to return the 'root' or a 'nonRoot' taxon
     * @return Taxon The target taxon for the test
     */
    private function initFixtures(string $target): Taxon
    {
        // create taxon hierarchy
        $root = TaxonFactory::createOne()->getGroupRoot()->getTaxon();
        $children = TaxonFactory::new(['parentTaxon' => $root])
            ->many(5)
            ->create();

        $grandchildren = [];
        foreach ($children as $child) {
            $level = TaxonFactory::new(['parentTaxon' => $child])
                ->many(3)
                ->create();

            $grandchildren = array_merge($grandchildren, $level);
        }
        foreach ($grandchildren as $child) {
            TaxonFactory::new(['parentTaxon' => $child])
                ->many(3)
                ->create();
        }
        return $target === 'root' ? $root : $grandchildren[0]->_real();
    }
}
