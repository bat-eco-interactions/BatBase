<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use App\Entity\Tag;
use App\Service\DataEntry\DataEntryManager;


/**
 * Adds new season tags
 * Deletes interaction errors
 */
class Version20220222Seasons extends AbstractMigration implements ContainerAwareInterface
{
    private $container;
    private $em;
    private $admin;
    private $dataManager;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function setDataManager(DataEntryManager $manager)
    {
        $this->dataManager = $manager;
    }

    private function getEntity($className, $val, $prop = 'id')
    {
        return $this->em->getRepository('App:'.$className)
            ->findOneBy([$prop => $val]);
    }

    public function getEntities($className)
    {
        return $this->em->getRepository('App:'.$className)->findAll();
    }

    public function persistEntity($entity, $creating = false)
    {
        if ($creating) {
            $entity->setCreatedBy($this->admin);
        }
        $entity->setUpdatedBy($this->admin);
        $this->em->persist($entity);
    }
/* ========================== up ============================================ */

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema):void
    {
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        $this->admin = $this->getEntity('User', 6);

        $this->updateTags($this->getEntities('Tag'));
        $this->createTags();
        $this->deleteInteractionErrors();
        $this->em->flush();
    }

    private function createTags()
    {
        $seasons = ['Dry', 'Wet', 'Spring', 'Summer', 'Fall', 'Winter'];

        foreach ($seasons as $season) {
            $tag = new Tag();
            $tag->setDisplayName($season);
            $tag->setType('season');
            $tag->setEntity('interaction');
            $this->persistEntity($tag);
        }
    }

    private function updateTags($tags)
    {
        foreach ($tags as $tag) {
            if ($tag->getDisplayName() === 'Secondary') {
                $tag->setType('source');
            } else {
                $tag->setType('interaction type');
            }
            $tag->setEntity('interaction');
            $this->persistEntity($tag);
        }
    }
    private function deleteInteractionErrors()
    {
        // $ids = [14915, 14924, 14935, 14940-59, 14961, 14962, 14964, 14965, 14966, 14967, 14968,
        //     14969, 14970, 14971, 14972, 14973, 14974, 14975, 14976, 14977, 14978, 14979];
        $ids = [15461, 15462, 15463, 15464, 15465];
        foreach ($ids as $id) {
            $entity = $this->getEntity('Interaction', $id);
            $this->em->remove($entity);
        }
    }

/* ======================== down ============================================ */
    /**
     * @param Schema $schema
     */
    public function down(Schema $schema):void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
