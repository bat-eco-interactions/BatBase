<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use App\Entity\InteractionType;
use App\Service\DataEntry\DataEntryManager;

/**
 * Updates JSON filter-set details
 */
final class Version20220507FilterSets extends AbstractMigration implements ContainerAwareInterface
{
    private $container;
    private $em;
    private $admin;
    private $dataManager;

    public function getDescription(): string
    {
        return '';
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function setDataManager(DataEntryManager $manager)
    {
        $this->dataManager = $manager;
    }

    private function getEntity($className, $val, $prop = 'id')
    {
        return $this->em->getRepository('App:'.$className)
            ->findOneBy([$prop => $val]);
    }

    public function getEntities($className)
    {
        return $this->em->getRepository('App:'.$className)->findAll();
    }

    public function persistEntity($entity, $creating = false)
    {
        if ($creating) {
            $entity->setCreatedBy($this->admin);
        }
        $entity->setUpdatedBy($this->admin);
        $this->em->persist($entity);
    }
/* ========================== up ============================================ */

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema):void
    {
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        $this->admin = $this->getEntity('User', 6);

        $this->updateFilterSets();
        $this->em->flush();
    }
    private function updateFilterSets()
    {
        $sets = $this->em->getRepository('App:UserNamed')->findBy(['type' => 'filter']);

        foreach ($sets as $set) {
            if (stripos($set->getDisplayName(), '(Exmpl)') === false) { continue; }
            $this->updateFilterDetails($set, $set->getDisplayName());
            $this->persistEntity($set);
        }
    }
    private function updateFilterDetails(&$set, $name)
    {                                                           print("\nUpdating set ".$set->getId());
        if (stripos($name, 'Brazil')) { $this->updateBrazilSet($set); }
        if (stripos($name, 'Journal')) { $this->updateJournalSet($set); }
        if (stripos($name, 'Taxon')) { $this->updateTaxonSet($set); }
    }
    private function updateBrazilSet(&$set)
    {
        $set->setDisplayName('(Exmpl) Brazil - Consumption');
        $set->setDetails($this->getBrazilDetails());
    }

    private function getBrazilDetails()
    {
        return <<<EOD
            {
                "focus":"locs",
                "direct":{
                    "combo":{
                        "TaxonGroup":["2"]}},
                "rebuild":{
                    "combo":{
                        "Country":{
                            "text":"Country","value":190}}},
                "table":{
                    "Interaction Type":{
                        "interactionType":["Consumption"]}},
                "view":{
                    "text":"Table Data","value":null}
            }
        EOD;
    }
    private function updateJournalSet(&$set)
    {
        $set->setDisplayName('(Exmpl) Journals - Seed, consumption, >=1990');
        $set->setDetails($this->getJournalDetails());
    }
    private function getJournalDetails()
    {
        return <<<EOD
            {
                "direct":{
                    "date":{
                        "active":true,
                        "time":"1990-01-01",
                        "type":"cited"},
                    "combo":{"PublicationType":"3"}},
                "focus":"srcs",
                "rebuild":{},
                "table":{
                    "Interaction Type":{
                        "interactionType":["Consumption"]},
                    "Tags":{
                        "tags":["Seed"]}},
                "view":{
                    "text":"Publications","value":"pubs"}
            }
        EOD;
    }
    private function updateTaxonSet(&$set)
    {
        $set->setDisplayName('(Exmpl) Cohabitation - Hipposideridae >= 1988');
        $set->setDetails($this->getTaxonDetails());
    }
    private function getTaxonDetails()
    {
        return <<<EOD
            {
                "direct":{
                    "date":{
                        "active":true,
                        "time":"1988-01-01",
                        "type":"cited"}},
                "focus":"taxa",
                "rebuild":{
                    "combo":{
                        "Family":{
                            "text":"Family Hipposideridae",
                            "value":"1850"}}},
                "table":{
                    "Interaction Type":{
                        "interactionType":["Cohabitation"]}},
                "view":{
                    "text":"Bats","value":1}
            }
        EOD;
    }
/* ======================== down ============================================ */
    /**
     * @param Schema $schema
     */
    public function down(Schema $schema):void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
