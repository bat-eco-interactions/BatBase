<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Adds interaction fields: date, page, and quote.
 */
final class Version20220222InteractionFields extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Adds interaction fields: date, page, and quote; and Tag field: type';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE interaction ADD quote LONGTEXT DEFAULT NULL AFTER object_taxon_id, ADD date VARCHAR(255) DEFAULT NULL AFTER object_taxon_id, ADD pages VARCHAR(255) DEFAULT NULL AFTER object_taxon_id, CHANGE note note LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE tag ADD entity VARCHAR(255) DEFAULT NULL AFTER description, ADD type VARCHAR(255) DEFAULT NULL AFTER description, DROP constrained_to_entity');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE interaction DROP date, DROP page, DROP quote, CHANGE note note VARCHAR(255) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_general_ci`');
        $this->addSql('ALTER TABLE tag ADD constrained_to_entity LONGTEXT CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_general_ci`, DROP entity, DROP type');
    }
}
