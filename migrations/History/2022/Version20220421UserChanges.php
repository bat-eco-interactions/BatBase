<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Changes involved with replacing the FOS user bundle.
 * Renamed Valid Interaction properties.
 */
final class Version202204215UserChanges extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Changes involved with replacing the FOS user bundle. Renamed Valid Interaction properties.';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE user DROP last_login, CHANGE aboutMe about_me LONGTEXT NOT NULL, CHANGE enabled is_verified TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE user MODIFY COLUMN roles LONGTEXT NOT NULL AFTER about_me');
        $this->addSql('CREATE TABLE reset_password_request (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, selector VARCHAR(20) NOT NULL, hashed_token VARCHAR(100) NOT NULL, requested_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', expires_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_7CE748AA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE reset_password_request ADD CONSTRAINT FK_7CE748AA76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE valid_interaction CHANGE object_sub_group_id object_group_root_id INT NOT NULL, CHANGE subject_sub_group_id subject_group_root_id INT NOT NULL');
        $this->addSql('ALTER TABLE valid_interaction RENAME INDEX idx_64a93140a381be79 TO IDX_64A931402C37B2F8');
        $this->addSql('ALTER TABLE valid_interaction RENAME INDEX idx_64a931407860da17 TO IDX_64A9314086E61FC3');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `user` ADD last_login DATETIME DEFAULT NULL, CHANGE about_me aboutMe LONGTEXT CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`');
        $this->addSql('DROP TABLE reset_password_request');
    }
}
