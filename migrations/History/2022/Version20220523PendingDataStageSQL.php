<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Adds PendingDataStage Entity
 * Updates Location description field.
 * Makes PendingData softdeleteable
 */
final class Version20220523PendingDataStageSQL extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Adds PendingDataStage Entity. Updates Location description field. Makes PendingData softdeleteable.';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE location CHANGE description description LONGTEXT DEFAULT NULL');

        $this->addSql('ALTER TABLE pending_data ADD deletedAt DATETIME DEFAULT NULL');

        $this->addSql('CREATE TABLE pending_stage (id INT AUTO_INCREMENT NOT NULL, passive_form VARCHAR(255) NOT NULL, active_form VARCHAR(255) DEFAULT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE pending_data CHANGE stage stage_id INT NOT NULL');
        $this->addSql('ALTER TABLE pending_data ADD CONSTRAINT FK_87955E252298D193 FOREIGN KEY (stage_id) REFERENCES pending_stage (id)');
        $this->addSql('CREATE INDEX IDX_87955E252298D193 ON pending_data (stage_id)');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE pending_data DROP FOREIGN KEY FK_87955E252298D193');
        $this->addSql('DROP TABLE pending_stage');
        $this->addSql('DROP INDEX IDX_87955E252298D193 ON pending_data');
        $this->addSql('ALTER TABLE pending_data CHANGE stage_id stage INT NOT NULL');
        $this->addSql('ALTER TABLE `user` CHANGE roles roles LONGTEXT NOT NULL');
    }

    public function isTransactional(): bool
    {
        return false;
    }
}
