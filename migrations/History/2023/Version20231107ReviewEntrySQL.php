<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231107ReviewEntrySQL extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE review_entry (id INT AUTO_INCREMENT NOT NULL, stage_id INT NOT NULL, entity VARCHAR(255) NOT NULL, entity_id VARCHAR(255) DEFAULT NULL, form LONGTEXT NOT NULL, log LONGTEXT NOT NULL, payload LONGTEXT NOT NULL, managed_by INT DEFAULT NULL, created_by INT NOT NULL, created DATETIME NOT NULL, updated_by INT DEFAULT NULL, updated DATETIME NOT NULL, deletedAt DATETIME DEFAULT NULL, INDEX IDX_38AFB16B2298D193 (stage_id), INDEX IDX_38AFB16B8DBE8932 (managed_by), INDEX IDX_38AFB16BDE12AB56 (created_by), INDEX IDX_38AFB16B16FE72E1 (updated_by), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE review_stage (id INT AUTO_INCREMENT NOT NULL, passive_form VARCHAR(255) NOT NULL, active_form VARCHAR(255) DEFAULT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE review_entry ADD CONSTRAINT FK_38AFB16B2298D193 FOREIGN KEY (stage_id) REFERENCES review_stage (id)');
        $this->addSql('ALTER TABLE review_entry ADD CONSTRAINT FK_38AFB16B8DBE8932 FOREIGN KEY (managed_by) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE review_entry ADD CONSTRAINT FK_38AFB16BDE12AB56 FOREIGN KEY (created_by) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE review_entry ADD CONSTRAINT FK_38AFB16B16FE72E1 FOREIGN KEY (updated_by) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE pending_data DROP FOREIGN KEY FK_87955E25FA2425B9');
        $this->addSql('ALTER TABLE pending_data DROP FOREIGN KEY FK_87955E25DE12AB56');
        $this->addSql('ALTER TABLE pending_data DROP FOREIGN KEY FK_87955E252298D193');
        $this->addSql('ALTER TABLE pending_data DROP FOREIGN KEY FK_87955E2516FE72E1');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE pending_data (id INT AUTO_INCREMENT NOT NULL, stage_id INT NOT NULL, managed_by INT DEFAULT NULL, created_by INT NOT NULL, updated_by INT DEFAULT NULL, entity VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, entity_id VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, data LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, created DATETIME NOT NULL, updated DATETIME NOT NULL, deletedAt DATETIME DEFAULT NULL, INDEX IDX_87955E25DE12AB56 (created_by), INDEX IDX_87955E2516FE72E1 (updated_by), INDEX IDX_87955E258DBE8932 (managed_by), INDEX IDX_87955E252298D193 (stage_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE pending_stage (id INT AUTO_INCREMENT NOT NULL, passive_form VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, active_form VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, description VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE pending_data ADD CONSTRAINT FK_87955E25FA2425B9 FOREIGN KEY (managed_by) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE pending_data ADD CONSTRAINT FK_87955E25DE12AB56 FOREIGN KEY (created_by) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE pending_data ADD CONSTRAINT FK_87955E252298D193 FOREIGN KEY (stage_id) REFERENCES pending_stage (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE pending_data ADD CONSTRAINT FK_87955E2516FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE review_entry DROP FOREIGN KEY FK_38AFB16B2298D193');
        $this->addSql('ALTER TABLE review_entry DROP FOREIGN KEY FK_38AFB16B8DBE8932');
        $this->addSql('ALTER TABLE review_entry DROP FOREIGN KEY FK_38AFB16BDE12AB56');
        $this->addSql('ALTER TABLE review_entry DROP FOREIGN KEY FK_38AFB16B16FE72E1');
        $this->addSql('DROP TABLE review_entry');
        $this->addSql('DROP TABLE review_stage');
    }

    public function isTransactional(): bool
    {
        return false;
    }
}
