<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use App\Entity\InteractionType;
use App\Service\DataEntry\DataEntryManager;


/**
 * Template for doctrine migrations where the entity manager is necessary.
 * Note: The 'created/updatedBy' admin is hardcoded to 6, Sarah.
 */
final class Version20230209User extends AbstractMigration implements ContainerAwareInterface
{
    private $container;
    private $em;
    private $admin;
    private $dataManager;

    public function getDescription(): string
    {
        return 'Changes the user, "Sarah", to a generic "Developer". Updates test users.';
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function setDataManager(DataEntryManager $manager)
    {
        $this->dataManager = $manager;
    }

    private function getEntity($className, $val, $prop = 'id')
    {
        return $this->em->getRepository('App:'.$className)
            ->findOneBy([$prop => $val]);
    }

    public function getEntities($className)
    {
        return $this->em->getRepository('App:'.$className)->findAll();
    }

    public function persistEntity($entity, $creating = false)
    {
        if ($creating) {
            $entity->setCreatedBy($this->admin);
        }
        $entity->setUpdatedBy($this->admin);
        $this->em->persist($entity);
    }
/* ========================== up ============================================ */

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema):void
    {
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        $this->admin = $this->getEntity('User', 6);

        $this->updateDevUser();
        $this->updateOtherUsers();

        $this->em->flush();

    }
    private function updateDevUser()
    {
        $user = $this->getEntity('User', 6);
        $user->setUsername('Dev');
        $user->setEmail('dev@batbase.org');
        $user->setFirstName('Dev');
        $user->setLastName('Developer');
        $user->setAboutMe('Primary developer of batbast.org');
        $user->setCountry('USA');
        $user->setInterest('To develop tools that facilitate research and data accessibility.');

        $this->em->persist($user);
    }

    private function updateOtherUsers()
    {
        $qaContributor = $this->getEntity('User', 530);
        $qaContributor->setRoles(['ROLE_QA_CONTRIBUTOR']);
        $this->em->persist($qaContributor);

        $cullenTest = $this->getEntity('User', 10);
        $this->em->remove($cullenTest);
    }
/* ======================== down ============================================ */
    /**
     * @param Schema $schema
     */
    public function down(Schema $schema):void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
