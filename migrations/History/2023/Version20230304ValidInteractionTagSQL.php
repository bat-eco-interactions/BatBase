<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230304ValidInteractionTagSQL extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE valid_interaction_tag DROP FOREIGN KEY FK_C545DB02BAD26311');
        $this->addSql('ALTER TABLE valid_interaction_tag DROP FOREIGN KEY FK_C545DB027F8C546A');
        $this->addSql('ALTER TABLE valid_interaction_tag ADD id INT AUTO_INCREMENT NOT NULL, ADD required TINYINT(1) DEFAULT 0 NOT NULL, DROP PRIMARY KEY, ADD PRIMARY KEY (id)');$this->addSql('ALTER TABLE contribution MODIFY COLUMN is_editor TINYINT(1) DEFAULT NULL AFTER auth_src_id;');
        $this->addSql('ALTER TABLE valid_interaction_tag MODIFY COLUMN valid_interaction_id INT NOT NULL AFTER required;');
        $this->addSql('ALTER TABLE valid_interaction_tag MODIFY COLUMN tag_id INT NOT NULL AFTER required;');
        $this->addSql('ALTER TABLE valid_interaction_tag ADD CONSTRAINT FK_C545DB02BAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id)');
        $this->addSql('ALTER TABLE valid_interaction_tag ADD CONSTRAINT FK_C545DB027F8C546A FOREIGN KEY (valid_interaction_id) REFERENCES valid_interaction (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE valid_interaction_tag MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE valid_interaction_tag DROP FOREIGN KEY FK_C545DB027F8C546A');
        $this->addSql('ALTER TABLE valid_interaction_tag DROP FOREIGN KEY FK_C545DB02BAD26311');
        $this->addSql('DROP INDEX `PRIMARY` ON valid_interaction_tag');
        $this->addSql('ALTER TABLE valid_interaction_tag DROP id, DROP required');
        $this->addSql('ALTER TABLE valid_interaction_tag ADD CONSTRAINT FK_C545DB027F8C546A FOREIGN KEY (valid_interaction_id) REFERENCES valid_interaction (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE valid_interaction_tag ADD CONSTRAINT FK_C545DB02BAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE valid_interaction_tag ADD PRIMARY KEY (valid_interaction_id, tag_id)');
    }

    public function isTransactional(): bool
    {
        return false;
    }
}
