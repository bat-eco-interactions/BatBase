<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Moves the source-publisher descriptions to the publisher entity.
 */
final class Version20231118Descriptions extends AbstractMigration implements ContainerAwareInterface
{
    private $admin;
    private $container;
    private $em;

    public function getDescription(): string
    {
        return 'Moves the source-publisher descriptions to the publisher entity.';
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    private function getEntity($className, $val, $prop = 'id')
    {
        return $this->em->getRepository('App:'.$className)
            ->findOneBy([$prop => $val]);
    }

    public function getEntities($className)
    {
        return $this->em->getRepository('App:'.$className)->findAll();
    }

    public function persistEntity($entity, $creating = false)
    {
        if ($creating) {
            $entity->setCreatedBy($this->admin);
            $entity->setUpdatedBy($this->admin);
        }
        $this->em->persist($entity);
    }
/* ========================== up ============================================ */
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema):void
    {
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        $this->admin = $this->getEntity('User', 6);

        $this->movePublisherDescriptions();
        $this->em->flush();
    }

    private function movePublisherDescriptions()
    {
        $publishers = $this->getEntity('SourceType', 'publisher', 'slug');

        foreach ($publishers->getSources() as $source) {
            $publisher = $source->getPublisher();
            $desc = $source->getDescription();

            $source->setDescription(null);
            $publisher->setDescription($desc);

            $this->persistEntity($publisher);
            $this->persistEntity($source);
        }
    }

/* ======================== down ============================================ */
    /**
     * @param Schema $schema
     */
    public function down(Schema $schema):void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
