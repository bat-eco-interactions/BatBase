<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use App\Entity\Tag;
use App\Entity\ValidInteractionTag;
use App\Service\DataEntry\DataEntryManager;


/**
 * Template for doctrine migrations where the entity manager is necessary.
 * Note: The 'created/updatedBy' admin is hardcoded to 6, Sarah.
 */
final class Version20230307ValidTags extends AbstractMigration implements ContainerAwareInterface
{
    private $container;
    private $em;
    private $admin;
    private $dataManager;

    public function getDescription(): string
    {
        return 'Sets $isRequired for ValidInteractionTags';
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function setDataManager(DataEntryManager $manager)
    {
        $this->dataManager = $manager;
    }

    private function getEntity($className, $val, $prop = 'id')
    {
        return $this->em->getRepository('App:'.$className)
            ->findOneBy([$prop => $val]);
    }

    public function getEntities($className)
    {
        return $this->em->getRepository('App:'.$className)->findAll();
    }

    public function persistEntity($entity, $creating = false)
    {
        if ($creating) {
            $entity->setCreatedBy($this->admin);
        }
        $entity->setUpdatedBy($this->admin);
        $this->em->persist($entity);
    }
/* ========================== up ============================================ */

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema):void
    {
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        $this->admin = $this->getEntity('User', 6);

        $this->setRequiredTags();
        $this->addCohabitationTags();
        $this->deletePreyValidInteractions();

        $this->em->flush();
    }
/* ======================== Required Tags =================================== */

    private function setRequiredTags()
    {
        $hasRequired = [ 11, 12, 13, 17, 18 ];

        foreach ($hasRequired as $id) {
            $validInt = $this->getEntity('ValidInteraction', $id);
            $this->setTagRequired($validInt);
        }
    }

    private function setTagRequired($validInt)
    {
        foreach ($validInt->getValidInteractionTags() as $Tag) {
            $Tag->setIsRequired(true);
            $this->em->persist($Tag);
        }
    }
/* ======================== Cohab Tags ====================================== */

    private function addCohabitationTags()
    {
        $tags = $this->createCohabTags();
        $this->addToCohabValidInteractions($tags);
    }
    private function createCohabTags()
    {
        $names = [ 'Synchronous', 'Asynchronous' ];
        $tags = [];

        foreach ($names as $name) {
            $entity = new Tag();
            $entity->setDisplayName($name);
            $entity->setEntity('interaction');
            $entity->setType('interaction type');
            $this->em->persist($entity);
            array_push($tags, $entity);
        }

        return $tags;
    }
    private function addToCohabValidInteractions($tags)
    {
        $validInts = $this->getEntities('ValidInteraction');

        foreach ($validInts as $validInt) {
            if ($validInt->getInteractionType()->getSlug() !== 'cohabitation') { continue; }

            foreach ($tags as $tag) {
                $entity = new ValidInteractionTag();
                $entity->setValidInteraction($validInt);
                $entity->setTag($tag);
                $this->em->persist($entity);
                $validInt->addValidInteractionTag($entity);
                $this->em->persist($validInt);
            }
        }
    }
/* ======================== Prey Valid Type ================================= */
    private function deletePreyValidInteractions()
    {
        $validInts = $this->getEntities('ValidInteraction');

        foreach ($validInts as $validInt) {
            if ($validInt->getInteractionType()->getDisplayName() !== 'Prey') { continue; }
            $this->em->remove($validInt);
        }
    }
/* ======================== down ============================================ */
    /**
     * @param Schema $schema
     */
    public function down(Schema $schema):void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
