<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230118ReviewedBy extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Removes unnecessary forensic properites from uneditable entities.
            Adds data-review forensic properties to editabe entities.';
    }

    public function up(Schema $schema): void
    {
        // DELETE THE SOURCE FIELD DELETEDAT WITH THE ZEROD DATETIME

        $this->addSql('ALTER TABLE author ADD reviewed_by INT DEFAULT NULL, ADD reviewed DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE author ADD CONSTRAINT FK_BDAFD8C885D7FB47 FOREIGN KEY (reviewed_by) REFERENCES `user` (id)');
        $this->addSql('CREATE INDEX IDX_BDAFD8C885D7FB47 ON author (reviewed_by)');
        $this->addSql('ALTER TABLE citation ADD reviewed_by INT DEFAULT NULL, ADD reviewed DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE citation ADD CONSTRAINT FK_FABD9C7E85D7FB47 FOREIGN KEY (reviewed_by) REFERENCES `user` (id)');
        $this->addSql('CREATE INDEX IDX_FABD9C7E85D7FB47 ON citation (reviewed_by)');
        $this->addSql('ALTER TABLE citation_type DROP FOREIGN KEY FK_435A43F416FE72E1');
        $this->addSql('ALTER TABLE citation_type DROP FOREIGN KEY FK_435A43F4DE12AB56');
        $this->addSql('DROP INDEX IDX_435A43F4DE12AB56 ON citation_type');
        $this->addSql('DROP INDEX IDX_435A43F416FE72E1 ON citation_type');
        $this->addSql('ALTER TABLE citation_type DROP created_by, DROP updated_by, DROP created, DROP updated');
        $this->addSql('ALTER TABLE habitat_type DROP FOREIGN KEY FK_63B2311016FE72E1');
        $this->addSql('ALTER TABLE habitat_type DROP FOREIGN KEY FK_63B23110DE12AB56');
        $this->addSql('DROP INDEX IDX_63B23110DE12AB56 ON habitat_type');
        $this->addSql('DROP INDEX IDX_63B2311016FE72E1 ON habitat_type');
        $this->addSql('ALTER TABLE habitat_type DROP created_by, DROP updated_by, DROP created, DROP updated');
        $this->addSql('ALTER TABLE interaction ADD reviewed_by INT DEFAULT NULL, ADD reviewed DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE interaction ADD CONSTRAINT FK_378DFDA785D7FB47 FOREIGN KEY (reviewed_by) REFERENCES `user` (id)');
        $this->addSql('CREATE INDEX IDX_378DFDA785D7FB47 ON interaction (reviewed_by)');
        $this->addSql('ALTER TABLE interaction_type DROP FOREIGN KEY FK_1E1A822916FE72E1');
        $this->addSql('ALTER TABLE interaction_type DROP FOREIGN KEY FK_1E1A8229DE12AB56');
        $this->addSql('DROP INDEX IDX_1E1A8229DE12AB56 ON interaction_type');
        $this->addSql('DROP INDEX IDX_1E1A822916FE72E1 ON interaction_type');
        $this->addSql('ALTER TABLE interaction_type DROP created_by, DROP updated_by, DROP created, DROP updated');
        $this->addSql('ALTER TABLE location ADD reviewed_by INT DEFAULT NULL, ADD reviewed DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE location ADD CONSTRAINT FK_5E9E89CB85D7FB47 FOREIGN KEY (reviewed_by) REFERENCES `user` (id)');
        $this->addSql('CREATE INDEX IDX_5E9E89CB85D7FB47 ON location (reviewed_by)');
        $this->addSql('ALTER TABLE location_type DROP FOREIGN KEY FK_CDAE26916FE72E1');
        $this->addSql('ALTER TABLE location_type DROP FOREIGN KEY FK_CDAE269DE12AB56');
        $this->addSql('DROP INDEX IDX_CDAE269DE12AB56 ON location_type');
        $this->addSql('DROP INDEX IDX_CDAE26916FE72E1 ON location_type');
        $this->addSql('ALTER TABLE location_type DROP created_by, DROP updated_by, DROP created, DROP updated');
        $this->addSql('ALTER TABLE publication ADD reviewed_by INT DEFAULT NULL, ADD reviewed DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE publication ADD CONSTRAINT FK_AF3C677985D7FB47 FOREIGN KEY (reviewed_by) REFERENCES `user` (id)');
        $this->addSql('CREATE INDEX IDX_AF3C677985D7FB47 ON publication (reviewed_by)');
        $this->addSql('ALTER TABLE publication_type DROP FOREIGN KEY FK_8726D6E416FE72E1');
        $this->addSql('ALTER TABLE publication_type DROP FOREIGN KEY FK_8726D6E4DE12AB56');
        $this->addSql('DROP INDEX IDX_8726D6E4DE12AB56 ON publication_type');
        $this->addSql('DROP INDEX IDX_8726D6E416FE72E1 ON publication_type');
        $this->addSql('ALTER TABLE publication_type DROP created_by, DROP updated_by, DROP created, DROP updated');
        $this->addSql('ALTER TABLE publisher ADD reviewed_by INT DEFAULT NULL, ADD reviewed DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE publisher ADD CONSTRAINT FK_9CE8D54685D7FB47 FOREIGN KEY (reviewed_by) REFERENCES `user` (id)');
        $this->addSql('CREATE INDEX IDX_9CE8D54685D7FB47 ON publisher (reviewed_by)');
        $this->addSql('ALTER TABLE source ADD reviewed_by INT DEFAULT NULL, ADD reviewed DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE source ADD CONSTRAINT FK_5F8A7F7385D7FB47 FOREIGN KEY (reviewed_by) REFERENCES `user` (id)');
        $this->addSql('CREATE INDEX IDX_5F8A7F7385D7FB47 ON source (reviewed_by)');
        $this->addSql('ALTER TABLE source_type DROP FOREIGN KEY FK_8D54D22A16FE72E1');
        $this->addSql('ALTER TABLE source_type DROP FOREIGN KEY FK_8D54D22ADE12AB56');
        $this->addSql('DROP INDEX IDX_8D54D22ADE12AB56 ON source_type');
        $this->addSql('DROP INDEX IDX_8D54D22A16FE72E1 ON source_type');
        $this->addSql('ALTER TABLE source_type DROP created_by, DROP updated_by, DROP created, DROP updated');
        $this->addSql('ALTER TABLE tag DROP FOREIGN KEY FK_389B78316FE72E1');
        $this->addSql('ALTER TABLE tag DROP FOREIGN KEY FK_389B783DE12AB56');
        $this->addSql('DROP INDEX IDX_389B783DE12AB56 ON tag');
        $this->addSql('DROP INDEX IDX_389B78316FE72E1 ON tag');
        $this->addSql('ALTER TABLE tag DROP created_by, DROP updated_by, DROP deletedAt, DROP created, DROP updated');
        $this->addSql('ALTER TABLE taxon ADD reviewed_by INT DEFAULT NULL, ADD reviewed DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE taxon ADD CONSTRAINT FK_5B6723AB85D7FB47 FOREIGN KEY (reviewed_by) REFERENCES `user` (id)');
        $this->addSql('CREATE INDEX IDX_5B6723AB85D7FB47 ON taxon (reviewed_by)');
        $this->addSql('ALTER TABLE tier DROP FOREIGN KEY FK_9AEACC1316FE72E1');
        $this->addSql('ALTER TABLE tier DROP FOREIGN KEY FK_9AEACC13DE12AB56');
        $this->addSql('DROP INDEX IDX_249E978ADE12AB56 ON tier');
        $this->addSql('DROP INDEX IDX_249E978A16FE72E1 ON tier');
        $this->addSql('ALTER TABLE tier DROP created_by, DROP updated_by, DROP created, DROP updated');
        $this->addSql('ALTER TABLE valid_interaction DROP FOREIGN KEY FK_64A9314016FE72E1');
        $this->addSql('ALTER TABLE valid_interaction DROP FOREIGN KEY FK_64A93140DE12AB56');
        $this->addSql('DROP INDEX IDX_64A93140DE12AB56 ON valid_interaction');
        $this->addSql('DROP INDEX IDX_64A9314016FE72E1 ON valid_interaction');
        $this->addSql('ALTER TABLE valid_interaction DROP created_by, DROP updated_by, DROP created, DROP updated');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE author DROP FOREIGN KEY FK_BDAFD8C885D7FB47');
        $this->addSql('DROP INDEX IDX_BDAFD8C885D7FB47 ON author');
        $this->addSql('ALTER TABLE author DROP reviewed_by, DROP reviewed');
        $this->addSql('ALTER TABLE citation DROP FOREIGN KEY FK_FABD9C7E85D7FB47');
        $this->addSql('DROP INDEX IDX_FABD9C7E85D7FB47 ON citation');
        $this->addSql('ALTER TABLE citation DROP reviewed_by, DROP reviewed');
        $this->addSql('ALTER TABLE citation_type ADD created_by INT DEFAULT NULL, ADD updated_by INT DEFAULT NULL, ADD created DATETIME NOT NULL, ADD updated DATETIME NOT NULL');
        $this->addSql('ALTER TABLE citation_type ADD CONSTRAINT FK_435A43F416FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE citation_type ADD CONSTRAINT FK_435A43F4DE12AB56 FOREIGN KEY (created_by) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_435A43F4DE12AB56 ON citation_type (created_by)');
        $this->addSql('CREATE INDEX IDX_435A43F416FE72E1 ON citation_type (updated_by)');
        $this->addSql('ALTER TABLE habitat_type ADD created_by INT DEFAULT NULL, ADD updated_by INT DEFAULT NULL, ADD created DATETIME NOT NULL, ADD updated DATETIME NOT NULL');
        $this->addSql('ALTER TABLE habitat_type ADD CONSTRAINT FK_63B2311016FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE habitat_type ADD CONSTRAINT FK_63B23110DE12AB56 FOREIGN KEY (created_by) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_63B23110DE12AB56 ON habitat_type (created_by)');
        $this->addSql('CREATE INDEX IDX_63B2311016FE72E1 ON habitat_type (updated_by)');
        $this->addSql('ALTER TABLE interaction DROP FOREIGN KEY FK_378DFDA785D7FB47');
        $this->addSql('DROP INDEX IDX_378DFDA785D7FB47 ON interaction');
        $this->addSql('ALTER TABLE interaction DROP reviewed_by, DROP reviewed');
        $this->addSql('ALTER TABLE interaction_type ADD created_by INT DEFAULT NULL, ADD updated_by INT DEFAULT NULL, ADD created DATETIME NOT NULL, ADD updated DATETIME NOT NULL');
        $this->addSql('ALTER TABLE interaction_type ADD CONSTRAINT FK_1E1A822916FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE interaction_type ADD CONSTRAINT FK_1E1A8229DE12AB56 FOREIGN KEY (created_by) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_1E1A8229DE12AB56 ON interaction_type (created_by)');
        $this->addSql('CREATE INDEX IDX_1E1A822916FE72E1 ON interaction_type (updated_by)');
        $this->addSql('ALTER TABLE location DROP FOREIGN KEY FK_5E9E89CB85D7FB47');
        $this->addSql('DROP INDEX IDX_5E9E89CB85D7FB47 ON location');
        $this->addSql('ALTER TABLE location DROP reviewed_by, DROP reviewed');
        $this->addSql('ALTER TABLE location_type ADD created_by INT DEFAULT NULL, ADD updated_by INT DEFAULT NULL, ADD created DATETIME NOT NULL, ADD updated DATETIME NOT NULL');
        $this->addSql('ALTER TABLE location_type ADD CONSTRAINT FK_CDAE26916FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE location_type ADD CONSTRAINT FK_CDAE269DE12AB56 FOREIGN KEY (created_by) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_CDAE269DE12AB56 ON location_type (created_by)');
        $this->addSql('CREATE INDEX IDX_CDAE26916FE72E1 ON location_type (updated_by)');
        $this->addSql('ALTER TABLE publication DROP FOREIGN KEY FK_AF3C677985D7FB47');
        $this->addSql('DROP INDEX IDX_AF3C677985D7FB47 ON publication');
        $this->addSql('ALTER TABLE publication DROP reviewed_by, DROP reviewed');
        $this->addSql('ALTER TABLE publication_type ADD created_by INT DEFAULT NULL, ADD updated_by INT DEFAULT NULL, ADD created DATETIME NOT NULL, ADD updated DATETIME NOT NULL');
        $this->addSql('ALTER TABLE publication_type ADD CONSTRAINT FK_8726D6E416FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE publication_type ADD CONSTRAINT FK_8726D6E4DE12AB56 FOREIGN KEY (created_by) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_8726D6E4DE12AB56 ON publication_type (created_by)');
        $this->addSql('CREATE INDEX IDX_8726D6E416FE72E1 ON publication_type (updated_by)');
        $this->addSql('ALTER TABLE publisher DROP FOREIGN KEY FK_9CE8D54685D7FB47');
        $this->addSql('DROP INDEX IDX_9CE8D54685D7FB47 ON publisher');
        $this->addSql('ALTER TABLE publisher DROP reviewed_by, DROP reviewed');
        $this->addSql('ALTER TABLE source DROP FOREIGN KEY FK_5F8A7F7385D7FB47');
        $this->addSql('DROP INDEX IDX_5F8A7F7385D7FB47 ON source');
        $this->addSql('ALTER TABLE source DROP reviewed_by, DROP reviewed');
        $this->addSql('ALTER TABLE source_type ADD created_by INT DEFAULT NULL, ADD updated_by INT DEFAULT NULL, ADD created DATETIME NOT NULL, ADD updated DATETIME NOT NULL');
        $this->addSql('ALTER TABLE source_type ADD CONSTRAINT FK_8D54D22A16FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE source_type ADD CONSTRAINT FK_8D54D22ADE12AB56 FOREIGN KEY (created_by) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_8D54D22ADE12AB56 ON source_type (created_by)');
        $this->addSql('CREATE INDEX IDX_8D54D22A16FE72E1 ON source_type (updated_by)');
        $this->addSql('ALTER TABLE tag ADD created_by INT DEFAULT NULL, ADD updated_by INT DEFAULT NULL, ADD deletedAt DATETIME DEFAULT NULL, ADD created DATETIME NOT NULL, ADD updated DATETIME NOT NULL');
        $this->addSql('ALTER TABLE tag ADD CONSTRAINT FK_389B78316FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE tag ADD CONSTRAINT FK_389B783DE12AB56 FOREIGN KEY (created_by) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_389B783DE12AB56 ON tag (created_by)');
        $this->addSql('CREATE INDEX IDX_389B78316FE72E1 ON tag (updated_by)');
        $this->addSql('ALTER TABLE taxon DROP FOREIGN KEY FK_5B6723AB85D7FB47');
        $this->addSql('DROP INDEX IDX_5B6723AB85D7FB47 ON taxon');
        $this->addSql('ALTER TABLE taxon DROP reviewed_by, DROP reviewed');
        $this->addSql('ALTER TABLE tier ADD created_by INT DEFAULT NULL, ADD updated_by INT DEFAULT NULL, ADD created DATETIME NOT NULL, ADD updated DATETIME NOT NULL');
        $this->addSql('ALTER TABLE tier ADD CONSTRAINT FK_9AEACC1316FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE tier ADD CONSTRAINT FK_9AEACC13DE12AB56 FOREIGN KEY (created_by) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_249E978ADE12AB56 ON tier (created_by)');
        $this->addSql('CREATE INDEX IDX_249E978A16FE72E1 ON tier (updated_by)');
        $this->addSql('ALTER TABLE valid_interaction ADD created_by INT DEFAULT NULL, ADD updated_by INT DEFAULT NULL, ADD created DATETIME NOT NULL, ADD updated DATETIME NOT NULL');
        $this->addSql('ALTER TABLE valid_interaction ADD CONSTRAINT FK_64A9314016FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE valid_interaction ADD CONSTRAINT FK_64A93140DE12AB56 FOREIGN KEY (created_by) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_64A93140DE12AB56 ON valid_interaction (created_by)');
        $this->addSql('CREATE INDEX IDX_64A9314016FE72E1 ON valid_interaction (updated_by)');
    }

    public function isTransactional(): bool
    {
        return false;
    }
}
