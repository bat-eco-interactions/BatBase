<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Updates old users with their current permissions.
 */
final class Version20231226Permissions extends AbstractMigration implements ContainerAwareInterface
{
    private $admin;
    private $container;
    private $em;

    public function getDescription(): string
    {
        return "Updates old users with their current permissions.";
    }

    public function setContainer(ContainerInterface|null $container = null)
    {
        $this->container = $container;
    }

    private function getEntity($className, $val, $prop = 'id')
    {
        return $this->em->getRepository('App:'.$className)
            ->findOneBy([$prop => $val]);
    }

    public function getEntities($className)
    {
        return $this->em->getRepository('App:'.$className)->findAll();
    }

    public function persistEntity($entity, $creating = false)
    {
        if ($creating) {
            $entity->setCreatedBy($this->admin);
            $entity->setUpdatedBy($this->admin);
        }
        $this->em->persist($entity);
    }
/* ========================== up ============================================ */
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema):void
    {
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        $this->admin = $this->getEntity('User', 6);

        $this->updateUserRoles();
        $this->em->flush();
    }

    private function updateUserRoles()
    {
        $map = [
            10 => null,
            476 => null,
            530 => null,
            99 => 'ROLE_EDITOR',
            543 => 'ROLE_QA_CONTRIBUTOR'
        ];
        foreach ($map as $id => $newRole) {
            $this->updateUser($id, $newRole);
        }
    }

    private function updateUser($id, $newRole) : void {
        $user = $this->getEntity('User', $id);
        if (!$newRole) {
            $user->setRoleToDefault();
        } else {
            $user->setRoles([$newRole]);
        }
        $this->persistEntity($user);
    }

/* ======================== down ============================================ */
    /**
     * @param Schema $schema
     */
    public function down(Schema $schema):void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
