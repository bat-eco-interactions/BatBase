<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Changes all 'prey' interactions into their subject-focused 'predation' inverse.
 */
final class Version20231219BatsAsPrey extends AbstractMigration implements ContainerAwareInterface
{
    private $admin;
    private $container;
    private $em;

    public function getDescription(): string
    {
        return "Changes all 'prey' interactions into their subject-focused 'predation' inverse";
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    private function getEntity($className, $val, $prop = 'id')
    {
        return $this->em->getRepository('App:'.$className)
            ->findOneBy([$prop => $val]);
    }

    public function getEntities($className)
    {
        return $this->em->getRepository('App:'.$className)->findAll();
    }

    public function persistEntity($entity, $creating = false)
    {
        if ($creating) {
            $entity->setCreatedBy($this->admin);
            $entity->setUpdatedBy($this->admin);
        }
        $this->em->persist($entity);
    }
/* ========================== up ============================================ */
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema):void
    {
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        $this->admin = $this->getEntity('User', 6);

        $this->inversePreyInteractions();
        $this->em->flush();
    }

    private function inversePreyInteractions()
    {
        $predationType = $this->getEntity('InteractionType', 'predation', 'slug');
        $preyType = $this->getEntity('InteractionType', 'prey', 'slug');

        foreach ($preyType->getInteractions() as $int) {
            $this->inverseSubjectAndObject($int);
            $int->setInteractionType($predationType);
            $this->persistEntity($int);
        }
    }

    private function inverseSubjectAndObject(&$int) : void {
        $orgSubject = $int->getSubject();
        $orgObject = $int->getObject();
        $int->setSubject($orgObject);
        $int->setObject($orgSubject);
    }

/* ======================== down ============================================ */
    /**
     * @param Schema $schema
     */
    public function down(Schema $schema):void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
