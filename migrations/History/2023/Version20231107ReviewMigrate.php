<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use App\Entity\ReviewStage;
use App\Entity\SystemDate;
use App\Service\DataEntry\DataEntryManager;

/**
 * Adds all review stages to the new table ReviewStage
 */
final class Version20231107ReviewMigrate extends AbstractMigration implements ContainerAwareInterface
{
    private $container;
    private $em;
    private $admin;
    private $dataManager;

    public function getDescription(): string
    {
        return 'Adds all review stages to the new table ReviewStage';
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function setDataManager(DataEntryManager $manager)
    {
        $this->dataManager = $manager;
    }

    private function getEntity($className, $val, $prop = 'id')
    {
        return $this->em->getRepository('App:'.$className)
            ->findOneBy([$prop => $val]);
    }

    public function getEntities($className)
    {
        return $this->em->getRepository('App:'.$className)->findAll();
    }

    public function persistEntity($entity, $creating = false)
    {
        if ($creating) {
            $entity->setCreatedBy($this->admin);
            $entity->setUpdatedBy($this->admin);
        }
        $this->em->persist($entity);
    }
/* ========================== up ============================================ */
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema):void
    {
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        $this->admin = $this->getEntity('User', 6);

        $this->addReviewEntryToSystemDate();
        $this->removePendingDataFromSystemDate();
        $this->addReviewEntryStages();
        $this->em->flush();
        $this->addSql('DROP TABLE pending_data');
        $this->addSql('DROP TABLE pending_stage');
    }
    private function addReviewEntryToSystemDate()
    {
        $entity = new SystemDate();
        $entity->setEntity('ReviewEntry');
        $entity->setUpdated(new \DateTime('now', new \DateTimeZone('UTC')));
        $this->persistEntity($entity);
    }
    private function removePendingDataFromSystemDate()
    {
        $entity = $this->getEntity('SystemDate', 27);
        $this->em->remove($entity);
    }
    private function addReviewEntryStages()
    {
        $stages = $this->getStageData();

        foreach ($stages as $stage) {
            $entity = new ReviewStage();
            $entity->setActiveForm($stage['active']);
            $entity->setPassiveForm($stage['passive']);
            $entity->setDescription($stage['description']);
            $this->persistEntity($entity);
        }
    }
    private function getStageData()
    {
        return  [
            [
                'active' => 'Quarantine',
                'passive' => 'Pending',
                'description' => 'Unreviewed data from contributor'
            ],[
                'active' => 'Lock',
                'passive' => 'Locked',
                'description' => 'Data in an active review process'
            ],[
                'active' => 'Return',
                'passive' => 'Returned',
                'description' => 'Returned data to the contributor for further action'
            ],[
                'active' => 'Pause',
                'passive' => 'Held',
                'description' => 'Data on hold for a specific data-manager'
            ],[
                'active' => 'Reject',
                'passive' => 'Rejected',
                'description' => 'Data are rejected and will be deleted'
            ],[
                'active' => 'Approve',
                'passive' => 'Approved',
                'description' => 'Data are approved and entered into the database'
            ],[
                'active' => 'Complete',
                'passive' => 'Completed',
                'description' => 'Data are no longer pending review.'
            ]
        ];
    }
/* ======================== down ============================================ */
    /**
     * @param Schema $schema
     */
    public function down(Schema $schema):void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
