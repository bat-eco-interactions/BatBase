<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version202110191Misc extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE pending_data DROP FOREIGN KEY FK_87955E25FA2425B9');
        $this->addSql('DROP INDEX idx_87955e25fa2425b9 ON pending_data');
        $this->addSql('CREATE INDEX IDX_87955E258DBE8932 ON pending_data (managed_by)');
        $this->addSql('ALTER TABLE pending_data ADD CONSTRAINT FK_87955E25FA2425B9 FOREIGN KEY (managed_by) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE taxon DROP FOREIGN KEY FK_5B6723AB7616678F');
        $this->addSql('DROP INDEX idx_5b6723ab7616678f ON taxon');
        $this->addSql('CREATE INDEX IDX_5B6723AB249E978A ON taxon (tier)');
        $this->addSql('ALTER TABLE taxon ADD CONSTRAINT FK_5B6723AB7616678F FOREIGN KEY (tier) REFERENCES tier (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE tier DROP FOREIGN KEY FK_9AEACC1316FE72E1');
        $this->addSql('ALTER TABLE tier DROP FOREIGN KEY FK_9AEACC13DE12AB56');
        $this->addSql('DROP INDEX idx_8879e8e5de12ab56 ON tier');
        $this->addSql('CREATE INDEX IDX_249E978ADE12AB56 ON tier (created_by)');
        $this->addSql('DROP INDEX idx_8879e8e516fe72e1 ON tier');
        $this->addSql('CREATE INDEX IDX_249E978A16FE72E1 ON tier (updated_by)');
        $this->addSql('ALTER TABLE tier ADD CONSTRAINT FK_9AEACC1316FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE tier ADD CONSTRAINT FK_9AEACC13DE12AB56 FOREIGN KEY (created_by) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE pending_data DROP FOREIGN KEY FK_87955E258DBE8932');
        $this->addSql('DROP INDEX idx_87955e258dbe8932 ON pending_data');
        $this->addSql('CREATE INDEX IDX_87955E25FA2425B9 ON pending_data (managed_by)');
        $this->addSql('ALTER TABLE pending_data ADD CONSTRAINT FK_87955E258DBE8932 FOREIGN KEY (managed_by) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE taxon DROP FOREIGN KEY FK_5B6723AB249E978A');
        $this->addSql('DROP INDEX idx_5b6723ab249e978a ON taxon');
        $this->addSql('CREATE INDEX IDX_5B6723AB7616678F ON taxon (tier)');
        $this->addSql('ALTER TABLE taxon ADD CONSTRAINT FK_5B6723AB249E978A FOREIGN KEY (tier) REFERENCES tier (id)');
        $this->addSql('ALTER TABLE tier DROP FOREIGN KEY FK_249E978ADE12AB56');
        $this->addSql('ALTER TABLE tier DROP FOREIGN KEY FK_249E978A16FE72E1');
        $this->addSql('DROP INDEX idx_249e978a16fe72e1 ON tier');
        $this->addSql('CREATE INDEX IDX_8879E8E516FE72E1 ON tier (updated_by)');
        $this->addSql('DROP INDEX idx_249e978ade12ab56 ON tier');
        $this->addSql('CREATE INDEX IDX_8879E8E5DE12AB56 ON tier (created_by)');
        $this->addSql('ALTER TABLE tier ADD CONSTRAINT FK_249E978ADE12AB56 FOREIGN KEY (created_by) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE tier ADD CONSTRAINT FK_249E978A16FE72E1 FOREIGN KEY (updated_by) REFERENCES `user` (id)');
    }
}
