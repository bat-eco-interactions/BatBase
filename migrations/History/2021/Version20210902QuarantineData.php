<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Creates PendingData Entity class.
 */
final class Version20210902QuarantineData extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Creates PendingData Entity class.';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE pending_data (id INT AUTO_INCREMENT NOT NULL, stage INT NOT NULL, entity VARCHAR(255) NOT NULL, entity_id VARCHAR(255) DEFAULT NULL, data LONGTEXT NOT NULL, managed_by INT DEFAULT NULL, created_by INT NOT NULL, created DATETIME NOT NULL, updated_by INT DEFAULT NULL, updated DATETIME NOT NULL, INDEX IDX_87955E25FA2425B9 (managed_by), INDEX IDX_87955E25DE12AB56 (created_by), INDEX IDX_87955E2516FE72E1 (updated_by), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE pending_data ADD CONSTRAINT FK_87955E25FA2425B9 FOREIGN KEY (managed_by) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE pending_data ADD CONSTRAINT FK_87955E25DE12AB56 FOREIGN KEY (created_by) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE pending_data ADD CONSTRAINT FK_87955E2516FE72E1 FOREIGN KEY (updated_by) REFERENCES `user` (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE pending_data');
    }
}
