<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Renames Rank to Tier, due to rank being a newly restricted MYSQL term
 */
final class Version202110190Tier extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Renames Rank to Tier, due to rank being a newly restricted MYSQL term';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `rank` RENAME TO tier');
        $this->addSql('ALTER TABLE group_root CHANGE sub_ranks sub_tiers VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE taxon CHANGE rank_id tier INT NOT NULL');
    }

    public function down(Schema $schema): void
    {
    }
}
