<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240617DataTypes extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE author CHANGE slug slug VARCHAR(255) NOT NULL, CHANGE suffix suffix VARCHAR(20) DEFAULT NULL, CHANGE created created DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', CHANGE deletedAt deleted_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE authority CHANGE created created DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE citation CHANGE created created DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', CHANGE deletedAt deleted_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE citation_type CHANGE slug slug VARCHAR(105) NOT NULL, CHANGE display_name display_name VARCHAR(100) NOT NULL');
        $this->addSql('ALTER TABLE content_block CHANGE slug slug VARCHAR(255) NOT NULL, CHANGE name name VARCHAR(255) NOT NULL, CHANGE page page VARCHAR(100) DEFAULT NULL, CHANGE created created DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', CHANGE deletedAt deleted_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE contribution CHANGE created created DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE feedback CHANGE created created DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', CHANGE deletedAt deleted_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE file_upload CHANGE created created DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', CHANGE filename file_name VARCHAR(255) NOT NULL, CHANGE desctiption description LONGTEXT NOT NULL');
        $this->addSql('ALTER TABLE geo_json CHANGE created created DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE `group` DROP FOREIGN KEY FK_A7A91E0BDE12AB56');
        $this->addSql('ALTER TABLE `group` DROP FOREIGN KEY FK_A7A91E0B16FE72E1');
        $this->addSql('ALTER TABLE `group` CHANGE slug slug VARCHAR(255) NOT NULL, CHANGE created created DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE `group` ADD CONSTRAINT FK_6DC044C5DE12AB56 FOREIGN KEY (created_by) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE `group` ADD CONSTRAINT FK_6DC044C516FE72E1 FOREIGN KEY (updated_by) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE group_root DROP FOREIGN KEY FK_6E0C1D55DE13F470');
        $this->addSql('ALTER TABLE group_root DROP FOREIGN KEY FK_6E0C1D55DE12AB56');
        $this->addSql('ALTER TABLE group_root DROP FOREIGN KEY FK_6E0C1D5516FE72E1');
        $this->addSql('ALTER TABLE group_root CHANGE created created DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE group_root ADD CONSTRAINT FK_3FAEA22BDE13F470 FOREIGN KEY (taxon_id) REFERENCES taxon (id)');
        $this->addSql('ALTER TABLE group_root ADD CONSTRAINT FK_3FAEA22BDE12AB56 FOREIGN KEY (created_by) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE group_root ADD CONSTRAINT FK_3FAEA22B16FE72E1 FOREIGN KEY (updated_by) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE habitat_type CHANGE slug slug VARCHAR(105) NOT NULL, CHANGE display_name display_name VARCHAR(100) NOT NULL');
        $this->addSql('ALTER TABLE image_upload CHANGE created created DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', CHANGE desctiption description LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE interaction CHANGE created created DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', CHANGE deletedAt deleted_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE interaction_type CHANGE slug slug VARCHAR(105) NOT NULL, CHANGE display_name display_name VARCHAR(100) NOT NULL');
        $this->addSql('ALTER TABLE issue_report CHANGE created created DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', CHANGE desctiption description LONGTEXT NOT NULL');
        $this->addSql('ALTER TABLE location DROP FOREIGN KEY FK_5E9E89CB9955B331');
        $this->addSql('DROP INDEX IDX_5E9E89CB9955B331 ON location');
        $this->addSql('ALTER TABLE location DROP gps_data, DROP show_on_map, CHANGE created created DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', CHANGE habitat_type_id habitat_type INT DEFAULT NULL, CHANGE deletedAt deleted_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE location ADD CONSTRAINT FK_5E9E89CB63B23110 FOREIGN KEY (habitat_type) REFERENCES habitat_type (id)');
        $this->addSql('CREATE INDEX IDX_5E9E89CB63B23110 ON location (habitat_type)');
        $this->addSql('ALTER TABLE location_type CHANGE slug slug VARCHAR(105) NOT NULL, CHANGE display_name display_name VARCHAR(100) NOT NULL');
        $this->addSql('ALTER TABLE naming CHANGE taxon_id taxon_id INT NOT NULL, CHANGE taxonym_id taxonym_id INT NOT NULL, CHANGE naming_type_id naming_type_id INT NOT NULL, CHANGE authority_id authority_id INT NOT NULL, CHANGE created created DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', CHANGE deletedAt deleted_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE naming_type CHANGE created created DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE publication CHANGE slug slug VARCHAR(255) NOT NULL, CHANGE created created DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', CHANGE deletedAt deleted_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE publication_type CHANGE slug slug VARCHAR(105) NOT NULL, CHANGE display_name display_name VARCHAR(100) NOT NULL');
        $this->addSql('ALTER TABLE publisher CHANGE slug slug VARCHAR(255) NOT NULL, CHANGE created created DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', CHANGE deletedAt deleted_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE review_entry CHANGE created_by created_by INT DEFAULT NULL, CHANGE created created DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', CHANGE deletedAt deleted_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE review_stage CHANGE active_form active_form VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE source CHANGE created created DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', CHANGE deletedAt deleted_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE source_type CHANGE slug slug VARCHAR(105) NOT NULL, CHANGE display_name display_name VARCHAR(100) NOT NULL, CHANGE ordinal ordinal INT NOT NULL');
        $this->addSql('ALTER TABLE system_date CHANGE entity entity VARCHAR(30) NOT NULL');
        $this->addSql('ALTER TABLE tag CHANGE type type VARCHAR(255) NOT NULL, CHANGE entity entity VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE taxon DROP FOREIGN KEY FK_5B6723AB7616678F');
        $this->addSql('ALTER TABLE taxon CHANGE slug slug VARCHAR(255) NOT NULL, CHANGE created created DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', CHANGE deletedAt deleted_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE taxon ADD CONSTRAINT FK_5B6723AB249E978A FOREIGN KEY (tier) REFERENCES tier (id)');
        $this->addSql('ALTER TABLE taxonym CHANGE created created DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', CHANGE deletedAt deleted_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE tier CHANGE ordinal ordinal INT NOT NULL, CHANGE plural_name plural_name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE user CHANGE created created DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', CHANGE deletedAt deleted_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE user_named ADD last_loaded DATETIME DEFAULT NULL, ADD deleted_at DATETIME DEFAULT NULL, DROP loaded, DROP deletedAt, CHANGE created created DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE valid_interaction DROP FOREIGN KEY FK_64A93140A381BE79');
        $this->addSql('ALTER TABLE valid_interaction DROP FOREIGN KEY FK_64A931407860DA17');
        $this->addSql('ALTER TABLE valid_interaction ADD CONSTRAINT FK_64A931402C37B2F8 FOREIGN KEY (object_group_root_id) REFERENCES group_root (id)');
        $this->addSql('ALTER TABLE valid_interaction ADD CONSTRAINT FK_64A9314086E61FC3 FOREIGN KEY (subject_group_root_id) REFERENCES group_root (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE author CHANGE slug slug VARCHAR(128) DEFAULT NULL, CHANGE suffix suffix VARCHAR(255) DEFAULT NULL, CHANGE created created DATETIME NOT NULL, CHANGE deleted_at deletedAt DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE authority CHANGE created created DATETIME NOT NULL');
        $this->addSql('ALTER TABLE citation CHANGE created created DATETIME NOT NULL, CHANGE deleted_at deletedAt DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE citation_type CHANGE slug slug VARCHAR(128) DEFAULT NULL, CHANGE display_name display_name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE content_block CHANGE slug slug VARCHAR(128) DEFAULT NULL, CHANGE name name VARCHAR(255) DEFAULT NULL, CHANGE page page VARCHAR(255) DEFAULT NULL, CHANGE created created DATETIME NOT NULL, CHANGE deleted_at deletedAt DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE contribution CHANGE created created DATETIME NOT NULL');
        $this->addSql('ALTER TABLE feedback CHANGE created created DATETIME NOT NULL, CHANGE deleted_at deletedAt DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE file_upload CHANGE created created DATETIME NOT NULL, CHANGE file_name filename VARCHAR(255) NOT NULL, CHANGE description desctiption LONGTEXT NOT NULL');
        $this->addSql('ALTER TABLE geo_json CHANGE created created DATETIME NOT NULL');
        $this->addSql('ALTER TABLE `group` DROP FOREIGN KEY FK_6DC044C5DE12AB56');
        $this->addSql('ALTER TABLE `group` DROP FOREIGN KEY FK_6DC044C516FE72E1');
        $this->addSql('ALTER TABLE `group` CHANGE slug slug VARCHAR(128) DEFAULT NULL, CHANGE created created DATETIME NOT NULL');
        $this->addSql('ALTER TABLE `group` ADD CONSTRAINT FK_A7A91E0BDE12AB56 FOREIGN KEY (created_by) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE `group` ADD CONSTRAINT FK_A7A91E0B16FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE group_root DROP FOREIGN KEY FK_3FAEA22BDE13F470');
        $this->addSql('ALTER TABLE group_root DROP FOREIGN KEY FK_3FAEA22BDE12AB56');
        $this->addSql('ALTER TABLE group_root DROP FOREIGN KEY FK_3FAEA22B16FE72E1');
        $this->addSql('ALTER TABLE group_root CHANGE created created DATETIME NOT NULL');
        $this->addSql('ALTER TABLE group_root ADD CONSTRAINT FK_6E0C1D55DE13F470 FOREIGN KEY (taxon_id) REFERENCES taxon (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE group_root ADD CONSTRAINT FK_6E0C1D55DE12AB56 FOREIGN KEY (created_by) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE group_root ADD CONSTRAINT FK_6E0C1D5516FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE habitat_type CHANGE slug slug VARCHAR(128) DEFAULT NULL, CHANGE display_name display_name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE image_upload CHANGE created created DATETIME NOT NULL, CHANGE description desctiption LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE interaction CHANGE created created DATETIME NOT NULL, CHANGE deleted_at deletedAt DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE interaction_type CHANGE slug slug VARCHAR(128) DEFAULT NULL, CHANGE display_name display_name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE issue_report CHANGE created created DATETIME NOT NULL, CHANGE description desctiption LONGTEXT NOT NULL');
        $this->addSql('ALTER TABLE location DROP FOREIGN KEY FK_5E9E89CB63B23110');
        $this->addSql('DROP INDEX IDX_5E9E89CB63B23110 ON location');
        $this->addSql('ALTER TABLE location ADD gps_data VARCHAR(255) DEFAULT NULL, ADD show_on_map TINYINT(1) DEFAULT NULL, CHANGE created created DATETIME NOT NULL, CHANGE habitat_type habitat_type_id INT DEFAULT NULL, CHANGE deleted_at deletedAt DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE location ADD CONSTRAINT FK_5E9E89CB9955B331 FOREIGN KEY (habitat_type_id) REFERENCES habitat_type (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_5E9E89CB9955B331 ON location (habitat_type_id)');
        $this->addSql('ALTER TABLE location_type CHANGE slug slug VARCHAR(128) DEFAULT NULL, CHANGE display_name display_name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE naming CHANGE taxon_id taxon_id INT DEFAULT NULL, CHANGE taxonym_id taxonym_id INT DEFAULT NULL, CHANGE naming_type_id naming_type_id INT DEFAULT NULL, CHANGE authority_id authority_id INT DEFAULT NULL, CHANGE created created DATETIME NOT NULL, CHANGE deleted_at deletedAt DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE naming_type CHANGE created created DATETIME NOT NULL');
        $this->addSql('ALTER TABLE publication CHANGE slug slug VARCHAR(128) DEFAULT NULL, CHANGE created created DATETIME NOT NULL, CHANGE deleted_at deletedAt DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE publication_type CHANGE slug slug VARCHAR(128) DEFAULT NULL, CHANGE display_name display_name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE publisher CHANGE slug slug VARCHAR(128) DEFAULT NULL, CHANGE created created DATETIME NOT NULL, CHANGE deleted_at deletedAt DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE review_entry CHANGE created_by created_by INT NOT NULL, CHANGE created created DATETIME NOT NULL, CHANGE deleted_at deletedAt DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE review_stage CHANGE active_form active_form VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE source CHANGE created created DATETIME NOT NULL, CHANGE deleted_at deletedAt DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE source_type CHANGE slug slug VARCHAR(128) DEFAULT NULL, CHANGE display_name display_name VARCHAR(255) NOT NULL, CHANGE ordinal ordinal INT DEFAULT NULL');
        $this->addSql('ALTER TABLE system_date CHANGE entity entity VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE tag CHANGE entity entity VARCHAR(255) DEFAULT NULL, CHANGE type type VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE taxon DROP FOREIGN KEY FK_5B6723AB249E978A');
        $this->addSql('ALTER TABLE taxon CHANGE slug slug VARCHAR(128) DEFAULT NULL, CHANGE created created DATETIME NOT NULL, CHANGE deleted_at deletedAt DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE taxon ADD CONSTRAINT FK_5B6723AB7616678F FOREIGN KEY (tier) REFERENCES tier (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE taxonym CHANGE created created DATETIME NOT NULL, CHANGE deleted_at deletedAt DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE tier CHANGE ordinal ordinal INT DEFAULT NULL, CHANGE plural_name plural_name VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE `user` CHANGE created created DATETIME NOT NULL, CHANGE deleted_at deletedAt DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE user_named ADD loaded DATETIME DEFAULT NULL, ADD deletedAt DATETIME DEFAULT NULL, DROP last_loaded, DROP deleted_at, CHANGE created created DATETIME NOT NULL');
        $this->addSql('ALTER TABLE valid_interaction DROP FOREIGN KEY FK_64A931402C37B2F8');
        $this->addSql('ALTER TABLE valid_interaction DROP FOREIGN KEY FK_64A9314086E61FC3');
        $this->addSql('ALTER TABLE valid_interaction ADD CONSTRAINT FK_64A93140A381BE79 FOREIGN KEY (object_group_root_id) REFERENCES group_root (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE valid_interaction ADD CONSTRAINT FK_64A931407860DA17 FOREIGN KEY (subject_group_root_id) REFERENCES group_root (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
    }

    public function isTransactional(): bool
    {
        return false;
    }
}
