<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240528NullDefaults extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE author CHANGE slug slug VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE citation_type CHANGE slug slug VARCHAR(105) NOT NULL, CHANGE display_name display_name VARCHAR(100) NOT NULL');
        $this->addSql('ALTER TABLE content_block CHANGE slug slug VARCHAR(255) NOT NULL, CHANGE name name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE `group` CHANGE slug slug VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE habitat_type CHANGE slug slug VARCHAR(105) NOT NULL, CHANGE display_name display_name VARCHAR(100) NOT NULL');
        $this->addSql('ALTER TABLE interaction_type CHANGE slug slug VARCHAR(105) NOT NULL, CHANGE display_name display_name VARCHAR(100) NOT NULL');
        $this->addSql('ALTER TABLE location_type CHANGE slug slug VARCHAR(105) NOT NULL, CHANGE display_name display_name VARCHAR(100) NOT NULL');
        $this->addSql('ALTER TABLE naming CHANGE taxon_id taxon_id INT NOT NULL, CHANGE taxonym_id taxonym_id INT NOT NULL, CHANGE naming_type_id naming_type_id INT NOT NULL, CHANGE authority_id authority_id INT NOT NULL');
        $this->addSql('ALTER TABLE publication CHANGE slug slug VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE publication_type CHANGE slug slug VARCHAR(105) NOT NULL, CHANGE display_name display_name VARCHAR(100) NOT NULL');
        $this->addSql('ALTER TABLE publisher CHANGE slug slug VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE review_entry CHANGE created_by created_by INT DEFAULT NULL');
        $this->addSql('ALTER TABLE review_stage CHANGE active_form active_form VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE source_type CHANGE slug slug VARCHAR(105) NOT NULL, CHANGE display_name display_name VARCHAR(100) NOT NULL, CHANGE ordinal ordinal INT NOT NULL');
        $this->addSql('ALTER TABLE tag CHANGE type type VARCHAR(255) NOT NULL, CHANGE entity entity VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE taxon CHANGE slug slug VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE tier CHANGE ordinal ordinal INT NOT NULL, CHANGE plural_name plural_name VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE author CHANGE slug slug VARCHAR(128) DEFAULT NULL');
        $this->addSql('ALTER TABLE citation_type CHANGE slug slug VARCHAR(128) DEFAULT NULL, CHANGE display_name display_name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE content_block CHANGE slug slug VARCHAR(128) DEFAULT NULL, CHANGE name name VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE `group` CHANGE slug slug VARCHAR(128) DEFAULT NULL');
        $this->addSql('ALTER TABLE habitat_type CHANGE slug slug VARCHAR(128) DEFAULT NULL, CHANGE display_name display_name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE interaction_type CHANGE slug slug VARCHAR(128) DEFAULT NULL, CHANGE display_name display_name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE location_type CHANGE slug slug VARCHAR(128) DEFAULT NULL, CHANGE display_name display_name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE naming CHANGE taxon_id taxon_id INT DEFAULT NULL, CHANGE taxonym_id taxonym_id INT DEFAULT NULL, CHANGE naming_type_id naming_type_id INT DEFAULT NULL, CHANGE authority_id authority_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE publication CHANGE slug slug VARCHAR(128) DEFAULT NULL');
        $this->addSql('ALTER TABLE publication_type CHANGE slug slug VARCHAR(128) DEFAULT NULL, CHANGE display_name display_name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE publisher CHANGE slug slug VARCHAR(128) DEFAULT NULL');
        $this->addSql('ALTER TABLE review_entry CHANGE created_by created_by INT NOT NULL');
        $this->addSql('ALTER TABLE review_stage CHANGE active_form active_form VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE source_type CHANGE slug slug VARCHAR(128) DEFAULT NULL, CHANGE display_name display_name VARCHAR(255) NOT NULL, CHANGE ordinal ordinal INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tag CHANGE entity entity VARCHAR(255) DEFAULT NULL, CHANGE type type VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE taxon CHANGE slug slug VARCHAR(128) DEFAULT NULL');
        $this->addSql('ALTER TABLE tier CHANGE ordinal ordinal INT DEFAULT NULL, CHANGE plural_name plural_name VARCHAR(255) DEFAULT NULL');
    }

    public function isTransactional(): bool
    {
        return false;
    }
}
