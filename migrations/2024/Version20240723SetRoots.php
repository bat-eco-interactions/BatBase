<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class Version20240723SetRoots extends AbstractMigration implements ContainerAwareInterface
{
    private $admin;
    private $container;
    private $em;

    public function getDescription(): string
    {
        return "Adds group_root to each taxon that has a root/group.";
    }

    public function setContainer(ContainerInterface|null $container)
    {
        $this->container = $container;
    }

    private function getEntity($className, $val, $prop = 'id')
    {
        return $this->em->getRepository('\App\Entity\\'.$className)
            ->findOneBy([$prop => $val]);
    }

    public function getEntities($className)
    {
        return $this->em->getRepository('\App\Entity\\'.$className)->findAll();
    }

    public function persistEntity($entity, $creating = false)
    {
        if ($creating) {
            $entity->setCreatedBy($this->admin);
            $entity->setUpdatedBy($this->admin);
        }
        $this->em->persist($entity);
    }
/* ========================== up ============================================ */
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema):void
    {
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        $this->admin = $this->getEntity('User', 6);

        $this->setGroupRoots();

        $this->em->flush();
    }

    private function setGroupRoots()
    {
        $roots = $this->getEntities('GroupRoot');

        foreach ($roots as $root) {
            $taxon = $root->getTaxon();
            $this->setGroupRoot($taxon, $root);
        }
    }

    private function setGroupRoot($taxon, $groupRoot)
    {
        $taxon->setGroupRoot($groupRoot);

        $taxon->getChildTaxa()->map(function($child) use ($groupRoot) {
            $this->setGroupRoot($child, $groupRoot);
        });

        $this->persistEntity($taxon);

    }

/* ======================== down ============================================ */
    /**
     * @param Schema $schema
     */
    public function down(Schema $schema):void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
