<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240722Roots extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add group_root to taxon so each taxon has a direct connection to its root.';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE taxon ADD group_root INT DEFAULT NULL, DROP is_root');
        $this->addSql('ALTER TABLE taxon ADD CONSTRAINT FK_5B6723AB3FAEA22B FOREIGN KEY (group_root) REFERENCES group_root (id)');
        $this->addSql('CREATE INDEX IDX_5B6723AB3FAEA22B ON taxon (group_root)');
        $this->addSql('ALTER TABLE taxon MODIFY COLUMN group_root INT DEFAULT NULL AFTER tier;');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE taxon DROP FOREIGN KEY FK_5B6723AB3FAEA22B');
        $this->addSql('DROP INDEX IDX_5B6723AB3FAEA22B ON taxon');
        $this->addSql('ALTER TABLE taxon ADD is_root TINYINT(1) DEFAULT NULL, DROP group_root');
    }

    public function isTransactional(): bool
    {
        return false;
    }
}
