<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

// use App\Entity\InteractionType;
use App\Service\DataEntry\DataEntryManager;

/**
 * Merges data from one entity into another, then deletes the first.
 *
 * TOC
 *     MERGE AND REMOVE
 *     ENTITY IDS
 */
class MergeEntities extends AbstractMigration implements ContainerAwareInterface
{
    protected $admin;
    protected $container;
    protected $em;
    protected $dataManager;

    public function getDescription() : string
    {
        return 'Merges data from one entity into another, then deletes the first.';
    }
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    public function setDataManager(DataEntryManager $manager)
    {
        $this->dataManager = $manager;
    }

    private function getEntity($className, $val, $prop = 'id')
    {
        return $this->em->getRepository('App:'.$className)->findOneBy([$prop => $val]);
    }

    private function persistEntity($entity, $creating = false)
    {
        if ($creating) { $entity->setCreatedBy($this->admin); }
        $entity->setUpdatedBy($this->admin);
        $this->em->persist($entity);
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema):void
    {
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        $this->admin = $this->em->getRepository('App:User')->findOneBy(['id' => 6]);
        //$ents = $this->getMergeIds();
        // $this->mergeEntities($ents, 'Source', 'Publication');
        $this->em->flush();
    }
    /**
     * Loops through sets of entity IDs. The first entity will be removed, with
     * all it's data added to the second, if provided.
     * @param  [ary] $ents        Array: [[ rmvFrom, addTo ]]
     * @param  [str] $entityClass Entity classname
     * @param  [str] $detail      Classname of a detail entity to remove.
     */
    protected function mergeEntities($ents, $entityClass, $detail = false)
    {
        foreach ($ents as $ids) {
            $this->mergeData($ids[0], $ids[1], $entityClass, $detail);
        }

    }
/** ========================= MERGE AND REMOVE ============================== */
    /**
     * Before removing an entity, all data can be merged/added into an entity.
     * The entity being removed is then persisted and deleted.
     * @param  [str] $rmvId       ID of the entity being removed
     * @param  [str] $addId       ID of the entity being merged into
     * @param  [str] $entityClass Entity classname
     * @param  [str] $detail      Classname of a detail entity to remove.
     */
    protected function mergeData($rmvId, $addId, $entityClass, $detail = false)
    {
        $rmv = $this->getEntity($entityClass, $rmvId);                              //print("\n Remove entity id  = ".$rmvId);

        if ($detail) { $this->removeDetailEntityData($detail, $rmv); }
        if ($addId) {
            $add = $this->getEntity($entityClass, $addId);
            $this->transferData($entityClass, $add, $rmv);
            $this->persistEntity($add, true);
        }
        $this->persistEntity($rmv);
        $this->em->remove($rmv);
    }
    private function removeDetailEntityData($type, $coreEntity)
    {
        $getDetail = 'get'.$type;
        $detailEntity = $coreEntity->$getDetail();
        $this->em->remove($detailEntity);
    }
/* ------------------- MERGE ENTITY DATA ------------------------------------ */
    private function transferData($entityClass, &$add, &$rmv)
    {
        // $this->mergeMiscData($rmv, $add);
        $this->transferChildren($rmv, $add, $entityClass);
        $this->transferInts($rmv, $add, $entityClass);
    }
    private function mergeMiscData(&$rmv, &$add)
    {
        //
    }


    /**
     * Moves the children to the new entity.
     * @param  [obj] $rmv         Entity being removed
     * @param  [obj] $add         Entity data is being merged into
     * @param  [str] $entityClass Entity classname
     */
    private function transferChildren(&$rmv, &$add, $type)
    {
        $map = [
            'Location' => [ 'ChildLocs', 'ParentLocation' ],
            'Source' =>   [ 'ChildSources', 'ParentSource' ],
            'Taxon' =>    [ 'ChildTaxa', 'ParentTaxon' ]
        ];
        $getFunc = 'get'.$map[$type][0];
        $setFunc = 'set'.$map[$type][1];
        $children = $rmv->$getFunc();
        if (!count($children)) { return; }                                      print("\nCHILDREN FOUND = ".count($children));

        foreach ($children as $child) {
            $child->$setFunc($add);
            $this->persistEntity($child);
        }
    }
    /**
     * Sets the new entity into each interaction. The entity-class typically is
     * typically also the property name, except for the Subject and Object taxa.
     * @param  [obj] $rmv         Entity being removed
     * @param  [obj] $add         Entity data is being merged into
     * @param  [str] $entityClass Entity classname
     */
    private function transferInts(&$rmv, &$add, $entityClass)
    {
        $prop = $entityClass;

        foreach ($rmv->getInteractions() as $int) {
            if ($entityClass === 'Taxon') { $prop = $this->getRole($rmv, $add, $int); }
            $setFunc = 'set'.$prop;
            $int->$setFunc($add);
            $this->persistEntity($int);
        }
    }
    private function getRole($rmv, $add, $int)
    {
        return $int->getSubject() === $rmv ? 'Subject' : 'Object';
    }
/* =========================== ENTITY IDS =================================== */
    /**
     * The first entity will be removed, with all it's data added to the second,
     * if provided.
     * @return [ary]        Array: [[ *rmvFrom, addTo ], ...]
     */
    private function getMergeIds()
    {
        return [
            [ ]
        ];
    }


/******************************* DOWN *****************************************/
    /**
     * @param Schema $schema
     */
    public function down(Schema $schema):void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
