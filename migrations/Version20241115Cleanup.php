<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class Version20241115Cleanup extends AbstractMigration implements ContainerAwareInterface
{
    private $admin;
    private $container;
    private $em;

    public function getDescription(): string
    {
        return "Cleans up misc minor issues.";
    }

    public function setContainer(ContainerInterface|null $container)
    {
        $this->container = $container;
    }

    private function getEntity($className, $val, $prop = 'id')
    {
        return $this->em->getRepository('\App\Entity\\' . $className)
            ->findOneBy([$prop => $val]);
    }

    public function getEntities($className)
    {
        return $this->em->getRepository('\App\Entity\\' . $className)->findAll();
    }

    public function persistEntity($entity, $creating = false): void
    {
        if ($creating) {
            $entity->setCreatedBy($this->admin);
            $entity->setUpdatedBy($this->admin);
        }
        $this->em->persist($entity);
    }
    /* ========================== up ============================================ */
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        $this->admin = $this->getEntity('User', 6);

        //before running: temporarily add null to return types for all blameable getters
        $this->deleteRecords();
        $this->fixMissingAuditUsers();


    }

    private function deleteRecords(): void
    {
        $int = $this->getEntity('Interaction', 5158);
        $this->em->remove($int);
        $this->em->flush();
    }

    // For every entity that has an api, ensure created by and updated by are set,
    // if null, set admin
    private function fixMissingAuditUsers(): void
    {
        $entities = ['author', 'citation', 'geoJson', 'interaction', 'location',
            'publication', 'publisher', 'source', 'taxon', 'group', 'groupRoot'];

        foreach ($entities as $entity) {
            $records = $this->getEntities(ucfirst($entity));
            $this->fixEntityAudits($records);
            $this->em->flush();
        }

    }

    private function fixEntityAudits(array $records): void
    {
        foreach ($records as $record) {
            $this->handleRecordAudit($record, 'CreatedBy');
            $this->handleRecordAudit($record, 'UpdatedBy');
        }
    }

    private function handleRecordAudit($record, string $type): void
    {
        $getter = 'get' . $type;
        $setter = 'set' . $type;

        $user = $record->$getter();

        if ($user?->getDeletedAt() !== null) {
            $user = null;
        }

        if (!$user) {
            $record->$setter($this->admin);
            $this->persistEntity($record);
        }

    }
    /* ======================== down ============================================ */
    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
