# Eco-Interactions Framework - BatBase.org
<p align="center">
 <img src="./assets/images/logos/batbase/BatLogo_Horizontal_Color.jpg" alt="Bat Eco-Interactions - BatBase.org Logo" width="400">
</p>

At [BatBase.org](https://www.batbase.org), we provide a unique platform for scientists, researchers, and avid amateurs to investigate the important roles bats play in our environment with the goal of promoting bat research and conservation worldwide. Start exploring at [BatBase.org/explore](https://www.batbase.org/explore), or [create an account](https://www.batbase.org/register) to contribute your research findings to our growing database.

Built on Symfony and JavaScript, this framework enables sophisticated data collection, curation, and analysis of bat ecological interactions, with potential applications beyond bat ecology.

## Project Status

**Current Phase**: Alpha
- Production deployment at BatBase.org with key features
- Active development of features, API, and test suite
- Regular updates based on user feedback

## Key Features

- **Advanced Data Entry**: Custom library managing taxonomic and ecological interaction data, featuring dynamic forms, real-time validation, and review workflows.
- **Quality Assurance**: Multi-stage expert validation process ensuring data integrity while enabling broad user participation from citizen scientists to researchers.
- **Powerful Exploration**: Real-time access to the entire interaction database with advanced filtering and visualization tools.
- **Intuitive and Efficient**: Integrated tips, walkthroughs, and tutorials guide users through the more complex features of the framework.

## Technical Highlights

- **Data Integrity**: Complex data-entry tasks are automated where possible, such as formatting [Chicago Manual of Style](https://www.chicagomanualofstyle.org/tools_citationguide.html) citations and hierarchical validation of taxonomic classifications, reducing the risk of errors and increasing efficiency.
- **Adaptive Data-Entry**: Intelligent forms that adapt to user input, dynamically showing relevant fields, sub-forms, and populating values.
- **Performance**: Client-side caching with IndexedDB for handling large datasets (>100k records)
- **API Access**: RESTful API for database interaction and entity model documentation
- **Flexibility**: Adaptable architecture suitable for various ecological research domains

## Quick Start

You will need:
- MacOS, Linux, or WSL2 (Windows 10/11 Pro)
- Docker Compose and Docker CLI
- You might need to [add your user to a docker group](https://medium.com/devops-technical-notes-and-manuals/how-to-run-docker-commands-without-sudo-28019814198f) or run docker commands with sudo.

Installation:
1. Clone repository
2. From project root: `cd docker && docker-compose up -d`
3. Access at `http://localhost:8000`

Development:
- Changes made in the `src/`, `assets/`, and `templates/` directories will be automatically compiled and reflected in the application.
- To apply changes made to dependencies in composer.json or package.json
`docker compose exec web <composer|npm> update`

## Contributing

We welcome contributions! Fork the repository, create a feature branch, and submit a pull request. Our developers are happy to mentor new contributors.

## Roadmap

- [ ] Bulk CSV import functionality (in progress)
- [ ] Enhanced CI/CD pipeline
- [ ] Simplified framework configuration
- [ ] Interactive demo environment

## Adoption

This framework is available for adaptation to other ecological research projects. Contact us to discuss implementing it for your specific use case.

## License
MIT License - see [LICENSE](LICENSE) file
